/*    */ package Logging;
/*    */ 
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.Calendar;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LoggingManager
/*    */ {
/* 17 */   private static LoggingManager instance = null;
/*    */   
/*    */ 
/*    */   LogFile errorlog;
/*    */   
/*    */   LogFile log;
/*    */   
/*    */ 
/*    */   public static LoggingManager getInstance()
/*    */   {
/* 27 */     if (instance == null) {
/* 28 */       instance = new LoggingManager();
/*    */     }
/* 30 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */   public void init()
/*    */   {
/* 36 */     String time = new SimpleDateFormat("ddMMyyyy HHmmss").format(Calendar.getInstance().getTime());
/* 37 */     this.log = new LogFile("ptl", time + ".txt", LogType.INFO);
/* 38 */     this.errorlog = new LogFile("errorptl", time + "_error_log.txt", LogType.ERROR);
/*    */   }
/*    */   
/*    */   public void log_error(Throwable ex) {
/* 42 */     if (this.errorlog == null) init();
/* 43 */     this.errorlog.write(ex);
/*    */   }
/*    */   
/*    */   public void log(String msg) {
/* 47 */     if (this.log == null) init();
/* 48 */     this.log.write(msg);
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Logging/LoggingManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */