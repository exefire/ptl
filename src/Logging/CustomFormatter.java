/*    */ package Logging;
/*    */ 
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.Date;
/*    */ import java.util.logging.Formatter;
/*    */ import java.util.logging.LogRecord;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CustomFormatter
/*    */   extends Formatter
/*    */ {
/*    */   public String format(LogRecord record)
/*    */   {
/* 22 */     StringBuffer buffer = new StringBuffer();
/* 23 */     String time = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date(record.getMillis()));
/* 24 */     buffer.append(time + "|" + record.getMessage() + "\r\n");
/* 25 */     return buffer.toString();
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Logging/CustomFormatter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */