/*    */ package Logging;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import java.util.logging.FileHandler;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ import java.util.logging.SimpleFormatter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LogFile
/*    */ {
/*    */   public Logger logger;
/*    */   private FileHandler fh;
/*    */   private String filename;
/*    */   private LogType type;
/*    */   
/*    */   public LogFile(String id, String file_name, LogType tipo)
/*    */   {
/* 28 */     this.logger = Logger.getLogger(id);
/* 29 */     this.type = tipo;
/* 30 */     this.filename = file_name;
/*    */     
/* 32 */     initLog();
/*    */   }
/*    */   
/*    */   private void initLog() {
/* 36 */     if (this.fh == null)
/*    */     {
/*    */       try
/*    */       {
/*    */ 
/* 41 */         this.fh = new FileHandler("logs/" + this.filename);
/* 42 */         this.logger.addHandler(this.fh);
/*    */         
/* 44 */         if (this.type.equals(LogType.INFO))
/*    */         {
/* 46 */           CustomFormatter cf = new CustomFormatter();
/* 47 */           this.fh.setFormatter(cf);
/* 48 */           this.logger.info("Creando Log....");
/* 49 */         } else if (this.type.equals(LogType.ERROR)) {
/* 50 */           SimpleFormatter formatter = new SimpleFormatter();
/* 51 */           this.fh.setFormatter(formatter);
/* 52 */           this.logger.setLevel(Level.ALL);
/*    */         }
/*    */       }
/*    */       catch (SecurityException e) {
/* 56 */         e.printStackTrace();
/*    */       } catch (IOException e) {
/* 58 */         e.printStackTrace();
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(String msg)
/*    */   {
/* 66 */     if (this.fh != null) {
/* 67 */       this.logger.info(msg);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void write(Throwable ex)
/*    */   {
/* 75 */     if (this.fh != null) {
/* 76 */       this.logger.log(Level.SEVERE, ex.getMessage(), ex);
/*    */     }
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Logging/LogFile.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */