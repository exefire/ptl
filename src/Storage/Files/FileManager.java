/*    */ package Storage.Files;
/*    */ 
/*    */ import Configuration.ConfigurationManager;
/*    */ import Logging.LoggingManager;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FileManager
/*    */ {
/* 17 */   private HashMap<String, CsvFile> archivos = null;
/* 18 */   private static FileManager instance = null;
/*    */   
/*    */   protected FileManager() {
/* 21 */     this.archivos = new HashMap();
/*    */   }
/*    */   
/*    */   public static FileManager getInstance() {
/* 25 */     if (instance == null) {
/* 26 */       synchronized (FileManager.class) {
/* 27 */         if (instance == null) {
/* 28 */           instance = new FileManager();
/*    */         }
/*    */       }
/*    */     }
/* 32 */     return instance;
/*    */   }
/*    */   
/*    */   public void newCsvFile(String key, String filename)
/*    */   {
/*    */     try
/*    */     {
/* 39 */       String path = ConfigurationManager.getInstance().getProperty("rutaSalida");
/* 40 */       CsvFile file = new CsvFile(path + "/" + filename);
/* 41 */       file.initFile();
/* 42 */       this.archivos.put(key, file);
/*    */     } catch (Exception ex) {
/* 44 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public void newCsvFile(String key, String path, String filename)
/*    */   {
/*    */     try {
/* 51 */       CsvFile file = new CsvFile(path + "/" + filename);
/* 52 */       file.initFile();
/* 53 */       this.archivos.put(key, file);
/*    */     } catch (Exception ex) {
/* 55 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public void putLineOnFile(String key, String line) {
/*    */     try {
/* 61 */       ((CsvFile)this.archivos.get(key)).writeLine(line);
/*    */     }
/*    */     catch (Exception ex) {
/* 64 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public void closeFile(String key) {
/*    */     try {
/* 70 */       CsvFile f = (CsvFile)this.archivos.get(key);
/* 71 */       f.closeFile();
/*    */     } catch (Exception ex) {
/* 73 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public void finalizar() {
/*    */     try {
/* 79 */       for (CsvFile f : this.archivos.values()) {
/* 80 */         f.closeFile();
/*    */       }
/*    */     } catch (Exception ex) {
/* 83 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public String getFileName(String key) {
/*    */     try {
/* 89 */       return ((CsvFile)this.archivos.get(key)).getFilename();
/*    */     } catch (Exception ex) {
/* 91 */       LoggingManager.getInstance().log_error(ex); }
/* 92 */     return "error";
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Storage/Files/FileManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */