/*    */ package Storage.Files;
/*    */ 
/*    */ import Logging.LoggingManager;
/*    */ import java.io.BufferedWriter;
/*    */ import java.io.FileOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.io.OutputStreamWriter;
/*    */ import java.io.Writer;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FileWriter
/*    */ {
/* 22 */   private Writer writer = null;
/* 23 */   private String filename = "tmp";
/* 24 */   private String extension = ".txt";
/*    */   
/*    */   public FileWriter(String fname, String ext) {
/* 27 */     this.filename = fname;
/* 28 */     this.extension = ext;
/*    */   }
/*    */   
/*    */ 
/*    */   public void init()
/*    */   {
/*    */     try
/*    */     {
/* 36 */       this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.filename + this.extension), "utf-8"));
/*    */     } catch (IOException ex) {
/* 38 */       ex = 
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 45 */         ex;LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */     catch (Exception ex)
/*    */     {
/* 40 */       ex = 
/*    */       
/*    */ 
/*    */ 
/*    */ 
/* 45 */         ex;LoggingManager.getInstance().log_error(ex);
/*    */     } finally {}
/*    */   }
/*    */   
/*    */   public void write(String str) {
/* 50 */     try { this.writer.write(str);
/*    */     } catch (IOException ex) {
/* 52 */       Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, ex);
/* 53 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */     catch (Exception ex) {
/* 56 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public void writeLine(String str) {
/*    */     try {
/* 62 */       write(str + "\r\n");
/*    */     } catch (Exception ex) {
/* 64 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */   public void endFileWrite() {
/*    */     try {
/* 70 */       this.writer.close();
/*    */     } catch (Exception ex) {
/* 72 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Storage/Files/FileWriter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */