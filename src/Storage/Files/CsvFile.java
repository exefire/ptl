/*    */ package Storage.Files;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CsvFile
/*    */ {
/* 15 */   private String EXT = ".txt";
/*    */   private FileWriter fw;
/* 17 */   private String filename = "tmp";
/*    */   
/*    */ 
/*    */   protected CsvFile(String filename)
/*    */   {
/* 22 */     this.fw = new FileWriter(filename, this.EXT);
/* 23 */     this.filename = (filename + this.EXT);
/*    */   }
/*    */   
/*    */   public void initFile()
/*    */   {
/* 28 */     this.fw.init();
/*    */   }
/*    */   
/*    */   public void writeLine(String line)
/*    */   {
/* 33 */     this.fw.writeLine(line);
/*    */   }
/*    */   
/*    */   public void closeFile() {
/* 37 */     this.fw.endFileWrite();
/*    */   }
/*    */   
/*    */   public String getFilename() {
/* 41 */     return this.filename;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Storage/Files/CsvFile.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */