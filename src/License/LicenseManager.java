/*    */ package License;
/*    */ 
/*    */ import java.text.ParseException;
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.Date;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LicenseManager
/*    */ {
/* 29 */   private static LicenseManager instance = null;
/*    */   
/*    */   private Date fechaExpiracion;
/*    */   private Date fechaActual;
/*    */   private Date fechaCompilacion;
/*    */   
/*    */   protected LicenseManager()
/*    */   {
/* 37 */     init();
/*    */   }
/*    */   
/*    */   public static LicenseManager getInstance() {
/* 41 */     if (instance == null) {
/* 42 */       instance = new LicenseManager();
/*    */     }
/* 44 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */   private void init()
/*    */   {
/* 50 */     SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
/* 51 */     String dateInString = "31-12-2017";
/*    */     try {
/* 53 */       this.fechaExpiracion = sdf.parse(dateInString);
/*    */     } catch (ParseException ex) {
/* 55 */       Logger.getLogger(LicenseManager.class.getName()).log(Level.SEVERE, null, ex);
/*    */     }
/*    */     
/* 58 */     this.fechaActual = new Date();
/*    */   }
/*    */   
/*    */   public String getFechaExpiracion()
/*    */   {
/* 63 */     SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
/* 64 */     String fecha = sdf.format(this.fechaExpiracion);
/* 65 */     return fecha;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isExpired()
/*    */   {
/* 72 */     if (this.fechaActual.after(this.fechaExpiracion)) {
/* 73 */       return true;
/*    */     }
/* 75 */     return false;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/License/LicenseManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */