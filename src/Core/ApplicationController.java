/*    */ package Core;
/*    */ 
/*    */ import Communication.CommunicationController;
/*    */ import Configuration.ConfigurationManager;
/*    */ import Database.DataSourceController;
/*    */ import ETL.pentaho.pdi.EtlController;
/*    */ import License.LicenseManager;
/*    */ import Logging.LoggingManager;
/*    */ import Workflow.WorkflowManager;
/*    */ import java.util.HashMap;
/*    */ import ui.License.LicensePanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ApplicationController
/*    */ {
/* 29 */   private static ApplicationController instance = null;
/* 30 */   private LicenseManager license = LicenseManager.getInstance();
/* 31 */   private UiManager uiController = UiManager.getInstance();
/* 32 */   private WorkflowManager processController = WorkflowManager.getInstance();
/* 33 */   private LoggingManager logController = LoggingManager.getInstance();
/* 34 */   private ConfigurationManager configController = ConfigurationManager.getInstance();
/* 35 */   private CommunicationController comManager = CommunicationController.getInstance();
/* 36 */   private DataSourceController dataSourceController = DataSourceController.getInstance();
/* 37 */   private EtlController etlController = EtlController.getInstance();
/*    */   
/*    */ 
/* 40 */   private HashMap variablesGlobales = null;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   protected ApplicationController()
/*    */   {
/* 49 */     this.variablesGlobales = new HashMap();
/*    */   }
/*    */   
/*    */   public static ApplicationController getInstance() {
/* 53 */     if (instance == null) {
/* 54 */       instance = new ApplicationController();
/*    */     }
/*    */     
/* 57 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */   public void initialize()
/*    */   {
/* 63 */     this.uiController.init();
/* 64 */     this.uiController.setStatusInfo("Inciando Log del sistema...");
/* 65 */     this.logController.init();
/* 66 */     this.logController.log("Controlador de Log Iniciado");
/*    */     
/* 68 */     this.uiController.setStatusInfo("Cargando Configuración e software....");
/* 69 */     this.configController.init();
/*    */     
/* 71 */     this.uiController.setStatusInfo("Cargando Configuración de base de datos local...");
/* 72 */     this.dataSourceController.init();
/*    */     
/*    */ 
/*    */ 
/* 76 */     this.uiController.setStatusInfo("Chequeando Comunicación con controlador PTL");
/* 77 */     this.comManager.init();
/*    */     
/*    */ 
/* 80 */     this.uiController.setStatusInfo("Chequeando vigencia de Licencia");
/*    */     
/* 82 */     if (!this.license.isExpired()) {
/* 83 */       this.processController.init();
/* 84 */       this.comManager.addEventListener(this.processController);
/*    */     } else {
/* 86 */       this.uiController.loadPanel(new LicensePanel());
/*    */     }
/* 88 */     this.uiController.setStatusInfo("Piloto - Versión de evaluación - ( Expiración: " + this.license.getFechaExpiracion() + " )");
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void putVariable(String variable, Object valor)
/*    */   {
/* 95 */     this.variablesGlobales.put(variable, valor);
/*    */   }
/*    */   
/*    */   public Object getVariable(String key) {
/* 99 */     return this.variablesGlobales.get(key);
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Core/ApplicationController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */