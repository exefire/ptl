package Workflow;

import java.util.EventListener;

public abstract interface JobListener
  extends EventListener
{
  public abstract void onJobFinished(JobEvent paramJobEvent);
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/JobListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */