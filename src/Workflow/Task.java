/*    */ package Workflow;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public abstract class Task
/*    */ {
/*    */   Task prevTask;
/*    */   Task nextTask;
/*    */   TaskStatus currentState;
/* 19 */   private Job parentJob = null;
/* 20 */   public Boolean autoRun = Boolean.valueOf(false);
/*    */   public String id;
/* 22 */   private List<TaskListener> listeners = new ArrayList();
/* 23 */   public TaskStatus status = null;
/*    */   
/*    */   private String incomingData;
/*    */   
/*    */   private Integer incomingDataPosition;
/*    */   
/*    */   public void manageIncominData(String Data, Integer position)
/*    */   {
/* 31 */     this.incomingData = Data;
/* 32 */     this.incomingDataPosition = position;
/* 33 */     dataArrived();
/*    */   }
/*    */   
/*    */   public abstract void dataArrived();
/*    */   
/*    */   public abstract void performAction();
/*    */   
/*    */   public TaskStatus getStatus() {
/* 41 */     return this.status;
/*    */   }
/*    */   
/*    */   public void setStatus(TaskStatus status) {
/* 45 */     this.status = status;
/*    */   }
/*    */   
/*    */   public Job getParentJob() {
/* 49 */     return this.parentJob;
/*    */   }
/*    */   
/*    */   public void setParentJob(Job parentJob) {
/* 53 */     this.parentJob = parentJob;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getId()
/*    */   {
/* 59 */     return this.id;
/*    */   }
/*    */   
/*    */   public void setId(String id) {
/* 63 */     this.id = id;
/*    */   }
/*    */   
/*    */   public void addListener(TaskListener listener)
/*    */   {
/* 68 */     this.listeners.add(listener);
/*    */   }
/*    */   
/*    */   public void removeListener(TaskListener listener) {
/* 72 */     this.listeners.remove(listener);
/*    */   }
/*    */   
/*    */ 
/*    */   public void TareaTerminada(TaskEvent evento)
/*    */   {
/* 78 */     for (TaskListener listener : this.listeners)
/* 79 */       listener.onTaskFinished(evento);
/*    */   }
/*    */   
/*    */   public Task getNextTask() {
/* 83 */     return null;
/*    */   }
/*    */   
/* 86 */   public String getIncomingData() { return this.incomingData; }
/*    */   
/*    */   public void setIncomingData(String incomingData)
/*    */   {
/* 90 */     this.incomingData = incomingData;
/*    */   }
/*    */   
/*    */   public Integer getIncomingDataPosition() {
/* 94 */     return this.incomingDataPosition;
/*    */   }
/*    */   
/*    */   public void setIncomingDataPosition(Integer incomingDataPosition) {
/* 98 */     this.incomingDataPosition = incomingDataPosition;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/Task.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */