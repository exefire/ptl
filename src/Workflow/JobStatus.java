package Workflow;

public enum JobStatus
{
  WAITING_PTL_DATA,  PROCESSING,  WAITING_UI,  FINISHED;
  
  private JobStatus() {}
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/JobStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */