/*    */ package Workflow;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FinishJobTask
/*    */   extends Task
/*    */ {
/* 13 */   private static FinishJobTask instance = null;
/*    */   
/*    */   public static FinishJobTask getInstance() {
/* 16 */     if (instance == null) {
/* 17 */       instance = new FinishJobTask();
/* 18 */       instance.setId("FinishJobTask");
/*    */     }
/*    */     
/* 21 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 31 */     WorkflowManager.getInstance();
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 36 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/FinishJobTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */