/*    */ package Workflow;
/*    */ 
/*    */ import Communication.CommunicationListener;
/*    */ import Communication.IncomingDataEvent;
/*    */ import Workflow.process.Auth.LoginJob;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class WorkflowManager
/*    */   implements JobListener, CommunicationListener
/*    */ {
/* 20 */   private static WorkflowManager instance = null;
/*    */   
/* 22 */   public Job initJob = LoginJob.getInstance();
/* 23 */   private Job currentJob = null;
/*    */   
/*    */   protected WorkflowManager()
/*    */   {
/* 27 */     LoginJob.getInstance().addListener(this);
/*    */   }
/*    */   
/*    */   public static WorkflowManager getInstance() {
/* 31 */     if (instance == null) {
/* 32 */       instance = new WorkflowManager();
/*    */     }
/* 34 */     return instance;
/*    */   }
/*    */   
/*    */   public void init()
/*    */   {
/* 39 */     this.initJob.init();
/* 40 */     this.currentJob = this.initJob;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void actionPerformed() {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onJobFinished(JobEvent event)
/*    */   {
/* 53 */     Job lastJob = this.currentJob;
/*    */     
/* 55 */     Job j = (Job)event.getSource();
/* 56 */     this.currentJob = j.getNextJob();
/*    */     
/* 58 */     this.currentJob.init();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void manageIncomingData(IncomingDataEvent e)
/*    */   {
/* 66 */     this.currentJob.incomingData(e);
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/WorkflowManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */