/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.CreateUsuarioPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowNewUserPanelTask
/*    */   extends Task
/*    */ {
/*    */   CreateUsuarioPanel panel;
/* 23 */   private static ShowNewUserPanelTask instance = null;
/*    */   
/*    */   public static ShowNewUserPanelTask getInstance() {
/* 26 */     if (instance == null) {
/* 27 */       instance = new ShowNewUserPanelTask();
/* 28 */       instance.setId("ShowNewUserPanelTask");
/*    */     }
/*    */     
/* 31 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 41 */     this.panel = new CreateUsuarioPanel();
/*    */     
/* 43 */     UiManager.getInstance().loadPanel(this.panel);
/* 44 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 50 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 55 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowNewUserPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */