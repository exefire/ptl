/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Logging.LoggingManager;
/*    */ import Models.Entities.local.Destino;
/*    */ import Models.JpaControllers.local.DestinoJpaController;
/*    */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DeleteDestinoTask
/*    */   extends Task
/*    */ {
/* 28 */   private static DeleteDestinoTask instance = null;
/* 29 */   private String destino = "";
/*    */   
/*    */ 
/*    */   public static DeleteDestinoTask getInstance()
/*    */   {
/* 34 */     if (instance == null) {
/* 35 */       instance = new DeleteDestinoTask();
/* 36 */       instance.setId("DeleteDestinoTask");
/*    */     }
/*    */     
/* 39 */     return instance;
/*    */   }
/*    */   
/*    */   protected DeleteDestinoTask()
/*    */   {
/* 44 */     this.destino = "";
/*    */     
/* 46 */     setStatus(TaskStatus.Idle);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 53 */     if (getStatus().equals(TaskStatus.Finished)) {
/* 54 */       setStatus(TaskStatus.Idle);
/*    */     }
/*    */     
/*    */ 
/* 58 */     Destino d = DestinoJpaController.getInstance().findDestinoByNombre(this.destino);
/*    */     try
/*    */     {
/* 61 */       if (d != null) {
/* 62 */         DestinoJpaController.getInstance().destroy(d.getId());
/*    */       }
/*    */     }
/*    */     catch (NonexistentEntityException ex) {
/* 66 */       LoggingManager.getInstance().log_error(ex);
/*    */     } catch (Exception ex) {
/* 68 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */     
/*    */ 
/* 72 */     setStatus(TaskStatus.Finished);
/*    */     
/* 74 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 81 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 86 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public String getDestino() {
/* 90 */     return this.destino;
/*    */   }
/*    */   
/*    */   public void setDestino(String destino) {
/* 94 */     this.destino = destino;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/DeleteDestinoTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */