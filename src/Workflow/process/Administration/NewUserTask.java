/*     */ package Workflow.process.Administration;
/*     */ 
/*     */ import Models.Entities.local.Rol;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.UserJpaController;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class NewUserTask
/*     */   extends Task
/*     */ {
/*  22 */   private static NewUserTask instance = null;
/*  23 */   private String username = "";
/*  24 */   private String password = "";
/*  25 */   private Rol rol = Rol.OPERADOR;
/*     */   
/*     */   public static NewUserTask getInstance()
/*     */   {
/*  29 */     if (instance == null) {
/*  30 */       instance = new NewUserTask();
/*  31 */       instance.setId("NewUserTask");
/*     */     }
/*     */     
/*  34 */     return instance;
/*     */   }
/*     */   
/*     */   protected NewUserTask()
/*     */   {
/*  39 */     this.username = "";
/*  40 */     this.password = "";
/*  41 */     this.rol = Rol.OPERADOR;
/*  42 */     setStatus(TaskStatus.Idle);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  49 */     if (getStatus().equals(TaskStatus.Finished)) {
/*  50 */       setStatus(TaskStatus.Idle);
/*     */     }
/*     */     
/*     */ 
/*  54 */     User u = new User();
/*  55 */     u.setRoleId(this.rol);
/*  56 */     u.setUsername(this.username);
/*  57 */     u.setPassword(this.password);
/*     */     
/*     */ 
/*     */ 
/*  61 */     UserJpaController.getInstance().create(u);
/*     */     
/*  63 */     setStatus(TaskStatus.Finished);
/*     */     
/*  65 */     TareaTerminada(new TaskEvent(this, this));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/*  72 */     return null;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/*  77 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public String getUsername() {
/*  81 */     return this.username;
/*     */   }
/*     */   
/*     */   public void setUsername(String username) {
/*  85 */     this.username = username;
/*     */   }
/*     */   
/*     */   public String getPassword() {
/*  89 */     return this.password;
/*     */   }
/*     */   
/*     */   public void setPassword(String password) {
/*  93 */     this.password = password;
/*     */   }
/*     */   
/*     */   public Rol getRol() {
/*  97 */     return this.rol;
/*     */   }
/*     */   
/*     */   public void setRol(Rol rol) {
/* 101 */     this.rol = rol;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/NewUserTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */