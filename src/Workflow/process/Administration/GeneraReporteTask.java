/*     */ package Workflow.process.Administration;
/*     */ 
/*     */ import Logging.LoggingManager;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Storage.Files.FileManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GeneraReporteTask
/*     */   extends Task
/*     */ {
/*  36 */   private static GeneraReporteTask instance = null;
/*  37 */   private String ola = "";
/*  38 */   private String filename = "";
/*  39 */   private String path = "";
/*     */   
/*     */   public static GeneraReporteTask getInstance() {
/*  42 */     if (instance == null) {
/*  43 */       instance = new GeneraReporteTask();
/*  44 */       instance.setId("GeneraReporteTask");
/*     */     }
/*     */     
/*  47 */     return instance;
/*     */   }
/*     */   
/*     */   protected GeneraReporteTask()
/*     */   {
/*  52 */     this.ola = "";
/*  53 */     setStatus(TaskStatus.Idle);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  60 */     if (getStatus().equals(TaskStatus.Finished)) {
/*  61 */       setStatus(TaskStatus.Idle);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  67 */     List<Object> reporte = PickingJpaController.getInstance().getReportData(this.ola);
/*  68 */     Integer i = Integer.valueOf(0);
/*  69 */     if ((reporte != null) && (reporte.size() > 0)) {
/*     */       try
/*     */       {
/*  72 */         FileManager.getInstance().newCsvFile(this.filename, this.path, this.filename);
/*     */         
/*  74 */         String l = "Nota Venta;destino;articulo;total;consolidado;pendiente";
/*  75 */         FileManager.getInstance().putLineOnFile(this.filename, l);
/*  76 */         Integer localInteger1; Integer localInteger2; for (Iterator localIterator = reporte.iterator(); localIterator.hasNext(); 
/*     */             
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  91 */             localInteger2 = i = Integer.valueOf(i.intValue() + 1))
/*     */         {
/*  76 */           Object row = localIterator.next();
/*  77 */           Integer progreso = Integer.valueOf(i.intValue() * 100 / reporte.size());
/*  78 */           HashMap map = new HashMap();
/*  79 */           map.put("progreso", progreso);
/*  80 */           UiManager.getInstance().updateUiData("progress", map);
/*  81 */           String ola = ((Object[])(Object[])row)[0].toString();
/*  82 */           String destino = ((Object[])(Object[])row)[1].toString();
/*  83 */           String articulo = ((Object[])(Object[])row)[2].toString();
/*  84 */           String total = ((Object[])(Object[])row)[3].toString();
/*  85 */           String consolidado = ((Object[])(Object[])row)[4].toString();
/*  86 */           String pendiente = ((Object[])(Object[])row)[5].toString();
/*     */           
/*  88 */           String linea = ola + ";" + destino + ";" + articulo + ";" + total + ";" + consolidado + ";" + pendiente;
/*  89 */           FileManager.getInstance().putLineOnFile(this.filename, linea);
/*     */           
/*  91 */           localInteger1 = i;
/*     */         }
/*     */         
/*  94 */         FileManager.getInstance().closeFile(this.filename);
/*  95 */         String nombre = FileManager.getInstance().getFileName(this.filename);
/*  96 */         HashMap mapfin = new HashMap();
/*  97 */         mapfin.put("progreso", Integer.valueOf(100));
/*  98 */         mapfin.put("estado_text", "Reporte Generado");
/*  99 */         mapfin.put("estado", "ok");
/* 100 */         mapfin.put("ola", this.ola);
/* 101 */         mapfin.put("path", this.path);
/* 102 */         mapfin.put("filename", nombre);
/*     */         
/* 104 */         UiManager.getInstance().updateUiData("fin", mapfin);
/* 105 */         setStatus(TaskStatus.Finished);
/*     */         
/* 107 */         TareaTerminada(new TaskEvent(this, this));
/*     */       } catch (Exception ex) {
/* 109 */         HashMap mapfin = new HashMap();
/* 110 */         mapfin.put("progreso", Integer.valueOf(100));
/* 111 */         mapfin.put("estado_text", "Reporte No generado");
/* 112 */         mapfin.put("estado", "fail");
/* 113 */         mapfin.put("ola", this.ola);
/* 114 */         mapfin.put("path", this.path);
/* 115 */         mapfin.put("filename", this.filename);
/*     */         
/* 117 */         UiManager.getInstance().updateUiData("fin", mapfin);
/* 118 */         UiManager.getInstance().notificate("Ocurrio un error al intentar generar el archivo, contactese con el administrador.");
/* 119 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/*     */     }
/*     */     else {
/* 124 */       UiManager.getInstance().notificate("El reporte que está intentando generar no contiene información");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/* 136 */     return null;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 141 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public String getOla() {
/* 145 */     return this.ola;
/*     */   }
/*     */   
/*     */   public void setOla(String ola) {
/* 149 */     this.ola = ola;
/*     */   }
/*     */   
/*     */   public String getFilename() {
/* 153 */     return this.filename;
/*     */   }
/*     */   
/*     */   public void setFilename(String filename) {
/* 157 */     this.filename = filename;
/*     */   }
/*     */   
/*     */   public String getPath() {
/* 161 */     return this.path;
/*     */   }
/*     */   
/*     */   public void setPath(String path) {
/* 165 */     this.path = path;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/GeneraReporteTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */