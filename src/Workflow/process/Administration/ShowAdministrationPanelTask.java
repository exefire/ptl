/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.AdministrationPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowAdministrationPanelTask
/*    */   extends Task
/*    */ {
/*    */   AdministrationPanel panel;
/* 20 */   private static ShowAdministrationPanelTask instance = null;
/*    */   
/*    */   public static ShowAdministrationPanelTask getInstance() {
/* 23 */     if (instance == null) {
/* 24 */       instance = new ShowAdministrationPanelTask();
/* 25 */       instance.setId("ShowAdministrationPanelTask");
/*    */     }
/*    */     
/* 28 */     return instance;
/*    */   }
/*    */   
/*    */   protected ShowAdministrationPanelTask()
/*    */   {
/* 33 */     this.panel = new AdministrationPanel();
/* 34 */     UiManager.getInstance().loadHeader("admin");
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 41 */     UiManager.getInstance().loadPanel(this.panel);
/* 42 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 48 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 53 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowAdministrationPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */