/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.ConfigAdministrationPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowConfigAdministrationPanelTask
/*    */   extends Task
/*    */ {
/*    */   ConfigAdministrationPanel panel;
/* 21 */   private static ShowConfigAdministrationPanelTask instance = null;
/*    */   
/*    */   public static ShowConfigAdministrationPanelTask getInstance() {
/* 24 */     if (instance == null) {
/* 25 */       instance = new ShowConfigAdministrationPanelTask();
/* 26 */       instance.setId("ShowConfigAdministrationPanelTask");
/*    */     }
/*    */     
/* 29 */     return instance;
/*    */   }
/*    */   
/*    */   protected ShowConfigAdministrationPanelTask()
/*    */   {
/* 34 */     this.panel = new ConfigAdministrationPanel();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 41 */     UiManager.getInstance().loadPanel(this.panel);
/* 42 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 48 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 53 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowConfigAdministrationPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */