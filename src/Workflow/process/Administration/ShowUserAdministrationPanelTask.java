/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.UsersAdministrationPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowUserAdministrationPanelTask
/*    */   extends Task
/*    */ {
/*    */   UsersAdministrationPanel panel;
/* 22 */   private static ShowUserAdministrationPanelTask instance = null;
/*    */   
/*    */   public static ShowUserAdministrationPanelTask getInstance() {
/* 25 */     if (instance == null) {
/* 26 */       instance = new ShowUserAdministrationPanelTask();
/* 27 */       instance.setId("ShowUserAdministrationPanelTask");
/*    */     }
/*    */     
/* 30 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 40 */     this.panel = new UsersAdministrationPanel();
/*    */     
/* 42 */     UiManager.getInstance().loadPanel(this.panel);
/* 43 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 49 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 54 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowUserAdministrationPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */