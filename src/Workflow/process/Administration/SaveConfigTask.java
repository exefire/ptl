/*     */ package Workflow.process.Administration;
/*     */ 
/*     */ import Configuration.ConfigurationManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SaveConfigTask
/*     */   extends Task
/*     */ {
/*  25 */   private static SaveConfigTask instance = null;
/*     */   
/*  27 */   private String rutaSalida = "";
/*  28 */   private String rutaEntrada = "";
/*  29 */   private String idPtl = "";
/*  30 */   private String ip = "";
/*  31 */   private Integer nLuces = Integer.valueOf(0);
/*     */   
/*     */   public static SaveConfigTask getInstance() {
/*  34 */     if (instance == null) {
/*  35 */       instance = new SaveConfigTask();
/*  36 */       instance.setId("SaveConfigTask");
/*     */     }
/*     */     
/*  39 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   protected SaveConfigTask()
/*     */   {
/*  45 */     setStatus(TaskStatus.Idle);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  52 */     if (getStatus().equals(TaskStatus.Finished)) {
/*  53 */       setStatus(TaskStatus.Idle);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  58 */     ConfigurationManager.getInstance().setProperty("rutaSalida", this.rutaSalida);
/*  59 */     ConfigurationManager.getInstance().setProperty("rutaArchivo", this.rutaEntrada);
/*  60 */     ConfigurationManager.getInstance().setProperty("id_ptl", this.idPtl);
/*  61 */     ConfigurationManager.getInstance().setProperty("ip", this.ip);
/*  62 */     ConfigurationManager.getInstance().setProperty("luces", this.nLuces.toString());
/*     */     
/*  64 */     ConfigurationManager.getInstance().saveConfig();
/*     */     
/*     */ 
/*     */ 
/*  68 */     setStatus(TaskStatus.Finished);
/*     */     
/*  70 */     TareaTerminada(new TaskEvent(this, this));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/*  77 */     return null;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/*  82 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public String getRutaEntrada() {
/*  86 */     return this.rutaEntrada;
/*     */   }
/*     */   
/*     */   public void setRutaEntrada(String rutaEntrada) {
/*  90 */     this.rutaEntrada = rutaEntrada;
/*     */   }
/*     */   
/*     */   public String getRutaSalida() {
/*  94 */     return this.rutaSalida;
/*     */   }
/*     */   
/*     */   public void setRutaSalida(String rutaSalida) {
/*  98 */     this.rutaSalida = rutaSalida;
/*     */   }
/*     */   
/*     */   public String getIdPtl() {
/* 102 */     return this.idPtl;
/*     */   }
/*     */   
/*     */   public void setIdPtl(String idPtl) {
/* 106 */     this.idPtl = idPtl;
/*     */   }
/*     */   
/*     */   public String getIp() {
/* 110 */     return this.ip;
/*     */   }
/*     */   
/*     */   public void setIp(String ip) {
/* 114 */     this.ip = ip;
/*     */   }
/*     */   
/*     */   public Integer getnLuces() {
/* 118 */     return this.nLuces;
/*     */   }
/*     */   
/*     */   public void setnLuces(Integer nLuces) {
/* 122 */     this.nLuces = nLuces;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/SaveConfigTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */