/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Logging.LoggingManager;
/*    */ import Models.Entities.local.User;
/*    */ import Models.JpaControllers.local.UserJpaController;
/*    */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DeleteUserTask
/*    */   extends Task
/*    */ {
/* 26 */   private static DeleteUserTask instance = null;
/* 27 */   private String username = "";
/*    */   
/*    */ 
/*    */   public static DeleteUserTask getInstance()
/*    */   {
/* 32 */     if (instance == null) {
/* 33 */       instance = new DeleteUserTask();
/* 34 */       instance.setId("DeleteUserTask");
/*    */     }
/*    */     
/* 37 */     return instance;
/*    */   }
/*    */   
/*    */   protected DeleteUserTask()
/*    */   {
/* 42 */     this.username = "";
/*    */     
/* 44 */     setStatus(TaskStatus.Idle);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 51 */     if (getStatus().equals(TaskStatus.Finished)) {
/* 52 */       setStatus(TaskStatus.Idle);
/*    */     }
/*    */     
/*    */ 
/* 56 */     User u = UserJpaController.getInstance().findUserByUsername(this.username);
/*    */     try {
/* 58 */       if (u != null) {
/* 59 */         UserJpaController.getInstance().destroy(u.getId());
/*    */       }
/*    */     }
/*    */     catch (NonexistentEntityException ex) {
/* 63 */       LoggingManager.getInstance().log_error(ex);
/*    */     } catch (Exception ex) {
/* 65 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */     
/*    */ 
/* 69 */     setStatus(TaskStatus.Finished);
/*    */     
/* 71 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 78 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 83 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public String getUsername() {
/* 87 */     return this.username;
/*    */   }
/*    */   
/*    */   public void setUsername(String username) {
/* 91 */     this.username = username;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/DeleteUserTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */