/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.ReportesAdministrationPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowReportesAdministrationPanelTask
/*    */   extends Task
/*    */ {
/*    */   ReportesAdministrationPanel panel;
/* 22 */   private static ShowReportesAdministrationPanelTask instance = null;
/*    */   
/*    */   public static ShowReportesAdministrationPanelTask getInstance() {
/* 25 */     if (instance == null) {
/* 26 */       instance = new ShowReportesAdministrationPanelTask();
/* 27 */       instance.setId("ShowReportesAdministrationPanelTask");
/*    */     }
/*    */     
/* 30 */     return instance;
/*    */   }
/*    */   
/*    */   protected ShowReportesAdministrationPanelTask()
/*    */   {
/* 35 */     this.panel = new ReportesAdministrationPanel();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 42 */     UiManager.getInstance().loadPanel(this.panel);
/* 43 */     UiManager.getInstance().clearPanel();
/* 44 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 50 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 55 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowReportesAdministrationPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */