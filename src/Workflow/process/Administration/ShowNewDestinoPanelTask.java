/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.CreateDestinoPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowNewDestinoPanelTask
/*    */   extends Task
/*    */ {
/*    */   CreateDestinoPanel panel;
/* 24 */   private static ShowNewDestinoPanelTask instance = null;
/*    */   
/*    */   public static ShowNewDestinoPanelTask getInstance() {
/* 27 */     if (instance == null) {
/* 28 */       instance = new ShowNewDestinoPanelTask();
/* 29 */       instance.setId("ShowNewDestinoPanelTask");
/*    */     }
/*    */     
/* 32 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 42 */     this.panel = new CreateDestinoPanel();
/*    */     
/* 44 */     UiManager.getInstance().loadPanel(this.panel);
/* 45 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 51 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 56 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowNewDestinoPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */