/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Models.Entities.local.Destino;
/*    */ import Models.JpaControllers.local.DestinoJpaController;
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class NewDestinoTask
/*    */   extends Task
/*    */ {
/* 24 */   private static NewDestinoTask instance = null;
/*    */   
/* 26 */   private String destino = "";
/* 27 */   private Integer ranking = Integer.valueOf(99999);
/*    */   
/*    */ 
/*    */   public static NewDestinoTask getInstance()
/*    */   {
/* 32 */     if (instance == null) {
/* 33 */       instance = new NewDestinoTask();
/* 34 */       instance.setId("NewDestinoTask");
/*    */     }
/*    */     
/* 37 */     return instance;
/*    */   }
/*    */   
/*    */   protected NewDestinoTask()
/*    */   {
/* 42 */     this.destino = "Nuevo Destino";
/* 43 */     this.ranking = Integer.valueOf(99999);
/* 44 */     setStatus(TaskStatus.Idle);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 51 */     if (getStatus().equals(TaskStatus.Finished)) {
/* 52 */       setStatus(TaskStatus.Idle);
/*    */     }
/*    */     
/*    */ 
/* 56 */     Destino d = new Destino();
/* 57 */     d.setNombre(this.destino);
/* 58 */     d.setRanking(this.ranking);
/*    */     
/*    */ 
/*    */ 
/* 62 */     DestinoJpaController.getInstance().create(d);
/*    */     
/* 64 */     setStatus(TaskStatus.Finished);
/*    */     
/* 66 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 73 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 78 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public String getDestino() {
/* 82 */     return this.destino;
/*    */   }
/*    */   
/*    */   public void setDestino(String destino) {
/* 86 */     this.destino = destino;
/*    */   }
/*    */   
/*    */   public Integer getRanking() {
/* 90 */     return this.ranking;
/*    */   }
/*    */   
/*    */   public void setRanking(Integer ranking) {
/* 94 */     this.ranking = ranking;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/NewDestinoTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */