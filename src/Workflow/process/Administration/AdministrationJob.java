/*     */ package Workflow.process.Administration;
/*     */ 
/*     */ import Communication.IncomingDataEvent;
/*     */ import Models.Entities.local.Rol;
/*     */ import Workflow.Job;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import Workflow.WorkflowManager;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AdministrationJob
/*     */   extends Job
/*     */ {
/*     */   private Job nextjob;
/*  24 */   private Task initTask = ShowAdministrationPanelTask.getInstance();
/*  25 */   private ShowUserAdministrationPanelTask showUsersTask = ShowUserAdministrationPanelTask.getInstance();
/*  26 */   private ShowConfigAdministrationPanelTask showConfigTask = ShowConfigAdministrationPanelTask.getInstance();
/*  27 */   private ShowOlaAdministrationPanelTask showOlaTask = ShowOlaAdministrationPanelTask.getInstance();
/*  28 */   private ShowDestinosAdministrationPanelTask showDestinosTask = ShowDestinosAdministrationPanelTask.getInstance();
/*  29 */   private ShowNewUserPanelTask newUserPanel = ShowNewUserPanelTask.getInstance();
/*  30 */   private ShowNewDestinoPanelTask newDestinoPanel = ShowNewDestinoPanelTask.getInstance();
/*  31 */   private NewUserTask newUserTask = NewUserTask.getInstance();
/*  32 */   private NewDestinoTask newDestinoTask = NewDestinoTask.getInstance();
/*  33 */   private SaveConfigTask saveConfigTask = SaveConfigTask.getInstance();
/*  34 */   private DeleteUserTask deleteUserTask = DeleteUserTask.getInstance();
/*  35 */   private DeleteDestinoTask deleteDestinoTask = DeleteDestinoTask.getInstance();
/*  36 */   private ResetOlaTask resetOlaTask = ResetOlaTask.getInstance();
/*  37 */   private ShowReportesAdministrationPanelTask ShowReportesPanelTask = ShowReportesAdministrationPanelTask.getInstance();
/*  38 */   private GeneraReporteTask generaReporteTask = GeneraReporteTask.getInstance();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  44 */   private static AdministrationJob instance = null;
/*     */   
/*     */   public static AdministrationJob getInstance() {
/*  47 */     if (instance == null) {
/*  48 */       instance = new AdministrationJob();
/*     */     }
/*     */     
/*  51 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public void init()
/*     */   {
/*  57 */     addListener(WorkflowManager.getInstance());
/*     */     
/*  59 */     this.newUserTask.addListener(this);
/*  60 */     this.newDestinoTask.addListener(this);
/*  61 */     this.saveConfigTask.addListener(this);
/*  62 */     this.deleteUserTask.addListener(this);
/*  63 */     this.deleteDestinoTask.addListener(this);
/*  64 */     this.resetOlaTask.addListener(this);
/*  65 */     this.initTask.addListener(this);
/*  66 */     this.initTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public Job getNextJob()
/*     */   {
/*  72 */     return this.nextjob;
/*     */   }
/*     */   
/*     */   public void onTaskFinished(TaskEvent event)
/*     */   {
/*  77 */     Task t = (Task)event.getSource();
/*     */     
/*  79 */     if (t.getId().equals("NewUserTask")) {
/*  80 */       if (t.getStatus().equals(TaskStatus.Finished)) {
/*  81 */         this.showUsersTask.performAction();
/*     */       }
/*  83 */     } else if (t.getId().equals("NewDestinoTask")) {
/*  84 */       if (t.getStatus().equals(TaskStatus.Finished)) {
/*  85 */         this.showDestinosTask.performAction();
/*     */       }
/*     */     }
/*  88 */     else if (t.getId().equals("SaveConfigTask")) {
/*  89 */       if (t.getStatus().equals(TaskStatus.Finished)) {
/*  90 */         this.initTask.performAction();
/*  91 */         UiManager.getInstance().notificate("La Configuración ha sido guardada");
/*     */       }
/*  93 */     } else if (t.getId().equals("DeleteUserTask")) {
/*  94 */       if (t.getStatus().equals(TaskStatus.Finished)) {
/*  95 */         this.showUsersTask.performAction();
/*     */       }
/*  97 */     } else if (t.getId().equals("DeleteDestinoTask")) {
/*  98 */       if (t.getStatus().equals(TaskStatus.Finished)) {
/*  99 */         this.showDestinosTask.performAction();
/*     */       }
/* 101 */     } else if ((t.getId().equals("ResetOlaTask")) && 
/* 102 */       (t.getStatus().equals(TaskStatus.Finished))) {
/* 103 */       this.initTask.performAction();
/* 104 */       UiManager.getInstance().notificate("La Ola ha sido reiniciada.");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void incomingData(IncomingDataEvent e)
/*     */   {
/* 112 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void usersAdministration() {
/* 116 */     this.showUsersTask.performAction();
/*     */   }
/*     */   
/*     */   public void destinosAdministration() {
/* 120 */     this.showDestinosTask.performAction();
/*     */   }
/*     */   
/*     */   public void olaAdministration() {
/* 124 */     this.showOlaTask.performAction();
/*     */   }
/*     */   
/*     */   public void configAdministration()
/*     */   {
/* 129 */     this.showConfigTask.performAction();
/*     */   }
/*     */   
/*     */   public void newUser()
/*     */   {
/* 134 */     this.newUserPanel.performAction();
/*     */   }
/*     */   
/*     */   public void newDestino() {
/* 138 */     this.newDestinoPanel.performAction();
/*     */   }
/*     */   
/*     */   public void createUser(String userText, String passText, int rol) {
/* 142 */     this.newUserTask.setUsername(userText);
/* 143 */     this.newUserTask.setPassword(passText);
/* 144 */     if (rol == 0) {
/* 145 */       this.newUserTask.setRol(Rol.ADMINISTRADOR);
/* 146 */     } else if (rol == 1) {
/* 147 */       this.newUserTask.setRol(Rol.OPERADOR);
/*     */     }
/* 149 */     this.newUserTask.performAction();
/*     */   }
/*     */   
/*     */   public void createDestino(String destino, Integer rank)
/*     */   {
/* 154 */     this.newDestinoTask.setDestino(destino);
/* 155 */     this.newDestinoTask.setRanking(rank);
/* 156 */     this.newDestinoTask.performAction();
/*     */   }
/*     */   
/*     */   public void saveChangedConfig(String rutaSalida, String rutaEntrada, String idPtl, String ip, String luces)
/*     */   {
/* 161 */     this.saveConfigTask.setRutaSalida(rutaSalida);
/* 162 */     this.saveConfigTask.setRutaEntrada(rutaEntrada);
/* 163 */     this.saveConfigTask.setIdPtl(idPtl);
/* 164 */     this.saveConfigTask.setIp(ip);
/* 165 */     this.saveConfigTask.setnLuces(Integer.valueOf(Integer.parseInt(luces)));
/*     */     
/*     */ 
/* 168 */     this.saveConfigTask.performAction();
/*     */   }
/*     */   
/*     */   public void deleteUser(String username) {
/* 172 */     this.deleteUserTask.setUsername(username);
/* 173 */     this.deleteUserTask.performAction();
/*     */   }
/*     */   
/*     */   public void olaReset(String ola)
/*     */   {
/* 178 */     this.resetOlaTask.setOla(ola);
/* 179 */     this.resetOlaTask.performAction();
/*     */   }
/*     */   
/*     */   public void deleteDestino(String destino) {
/* 183 */     this.deleteDestinoTask.setDestino(destino);
/* 184 */     this.deleteDestinoTask.performAction();
/*     */   }
/*     */   
/*     */   public void destinosUpdated() {
/* 188 */     this.showDestinosTask.performAction();
/*     */   }
/*     */   
/*     */   public void reportesAdministration() {
/* 192 */     this.ShowReportesPanelTask.performAction();
/*     */   }
/*     */   
/*     */   public void generarReporte(String ola, String path, String filename)
/*     */   {
/* 197 */     this.generaReporteTask.setOla(ola);
/* 198 */     this.generaReporteTask.setPath(path);
/* 199 */     this.generaReporteTask.setFilename(filename);
/* 200 */     this.generaReporteTask.performAction();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/AdministrationJob.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */