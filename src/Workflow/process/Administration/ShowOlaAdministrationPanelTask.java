/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.OlaAdministrationPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowOlaAdministrationPanelTask
/*    */   extends Task
/*    */ {
/*    */   OlaAdministrationPanel panel;
/* 21 */   private static ShowOlaAdministrationPanelTask instance = null;
/*    */   
/*    */   public static ShowOlaAdministrationPanelTask getInstance() {
/* 24 */     if (instance == null) {
/* 25 */       instance = new ShowOlaAdministrationPanelTask();
/* 26 */       instance.setId("ShowOlaAdministrationPanelTask");
/*    */     }
/*    */     
/* 29 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 39 */     this.panel = new OlaAdministrationPanel();
/*    */     
/* 41 */     UiManager.getInstance().loadPanel(this.panel);
/* 42 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 48 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 53 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowOlaAdministrationPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */