/*    */ package Workflow.process.Administration;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Administration.DestinosAdministrationPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowDestinosAdministrationPanelTask
/*    */   extends Task
/*    */ {
/*    */   DestinosAdministrationPanel panel;
/* 21 */   private static ShowDestinosAdministrationPanelTask instance = null;
/*    */   
/*    */   public static ShowDestinosAdministrationPanelTask getInstance() {
/* 24 */     if (instance == null) {
/* 25 */       instance = new ShowDestinosAdministrationPanelTask();
/* 26 */       instance.setId("ShowDestinosAdministrationPanelTask");
/*    */     }
/*    */     
/* 29 */     return instance;
/*    */   }
/*    */   
/*    */   protected ShowDestinosAdministrationPanelTask()
/*    */   {
/* 34 */     this.panel = new DestinosAdministrationPanel();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 41 */     UiManager.getInstance().loadPanel(this.panel);
/* 42 */     UiManager.getInstance().clearPanel();
/* 43 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 49 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 54 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ShowDestinosAdministrationPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */