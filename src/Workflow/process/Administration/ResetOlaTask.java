/*     */ package Workflow.process.Administration;
/*     */ 
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.ArticulosBultoJpaController;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.DetallePickingJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Models.JpaControllers.local.exceptions.IllegalOrphanException;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ResetOlaTask
/*     */   extends Task
/*     */ {
/*  34 */   private static ResetOlaTask instance = null;
/*  35 */   private String ola = "";
/*     */   
/*     */   public static ResetOlaTask getInstance()
/*     */   {
/*  39 */     if (instance == null) {
/*  40 */       instance = new ResetOlaTask();
/*  41 */       instance.setId("ResetOlaTask");
/*     */     }
/*     */     
/*  44 */     return instance;
/*     */   }
/*     */   
/*     */   protected ResetOlaTask()
/*     */   {
/*  49 */     this.ola = "";
/*  50 */     setStatus(TaskStatus.Idle);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  57 */     if (getStatus().equals(TaskStatus.Finished)) {
/*  58 */       setStatus(TaskStatus.Idle);
/*     */     }
/*     */     
/*     */ 
/*  62 */     Picking ola = PickingJpaController.getInstance().findByIdReferencia(this.ola);
/*     */     
/*  64 */     if (ola != null)
/*     */     {
/*     */       try {
/*  67 */         DetalleCajaJpaController.getInstance().deleteAllofPicking(ola.getId());
/*     */         
/*  69 */         CajaJpaController.getInstance().deleteAllfrompicking(ola.getId());
/*     */         
/*  71 */         DetallePickingJpaController.getInstance().deleteAllFromPicking(ola.getId());
/*     */         
/*  73 */         ArticulosBultoJpaController.getInstance().deleteAllOfOla(ola.getIdreferencia());
/*     */         
/*  75 */         PickingJpaController.getInstance().destroy(ola.getId());
/*     */       } catch (IllegalOrphanException ex) {
/*  77 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       catch (NonexistentEntityException ex) {
/*  80 */         LoggingManager.getInstance().log_error(ex);
/*     */       } catch (Exception ex) {
/*  82 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/*  85 */       setStatus(TaskStatus.Finished);
/*     */       
/*  87 */       TareaTerminada(new TaskEvent(this, this));
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*  92 */       UiManager.getInstance().notificate("No se encontró el número de NV ingresado");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/* 104 */     return null;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 109 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public String getOla() {
/* 113 */     return this.ola;
/*     */   }
/*     */   
/*     */   public void setOla(String ola) {
/* 117 */     this.ola = ola;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Administration/ResetOlaTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */