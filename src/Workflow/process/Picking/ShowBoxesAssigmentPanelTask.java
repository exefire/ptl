/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Configuration.ConfigurationManager;
/*     */ import Core.ApplicationController;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import ui.Process.Picking.BoxesAssignmentPanel;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ShowBoxesAssigmentPanelTask
/*     */   extends Task
/*     */ {
/*     */   BoxesAssignmentPanel panel;
/*  31 */   private static ShowBoxesAssigmentPanelTask instance = null;
/*     */   private String numeroPicking;
/*     */   
/*     */   protected ShowBoxesAssigmentPanelTask()
/*     */   {
/*  36 */     this.status = TaskStatus.Waiting;
/*  37 */     this.panel = new BoxesAssignmentPanel();
/*     */   }
/*     */   
/*     */   public static ShowBoxesAssigmentPanelTask getInstance() {
/*  41 */     if (instance == null) {
/*  42 */       instance = new ShowBoxesAssigmentPanelTask();
/*  43 */       instance.setId("ShowBoxesAssigmentPanelTask");
/*     */     }
/*     */     
/*  46 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  56 */     this.panel.clearData();
/*  57 */     UiManager.getInstance().loadPanel(this.panel);
/*     */     
/*  59 */     HashMap attr = new HashMap();
/*     */     
/*  61 */     Picking p = PickingJpaController.getInstance().findByIdReferencia((String)ApplicationController.getInstance().getVariable("picking"));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  68 */     ArrayList<Destino> dests = new ArrayList();
/*     */     
/*  70 */     Collection<Destino> dests2 = (Collection)ApplicationController.getInstance().getVariable("destinos");
/*  71 */     Integer luces = Integer.valueOf(Integer.parseInt(ConfigurationManager.getInstance().getProperty("luces")));
/*     */     
/*  73 */     Integer ndestinos = Integer.valueOf(dests2.size());
/*     */     
/*  75 */     Integer ola = (Integer)ApplicationController.getInstance().getVariable("olaactiva");
/*     */     
/*  77 */     Integer maxLuces = luces;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  82 */     if (ndestinos.intValue() >= luces.intValue())
/*     */     {
/*  84 */       Integer maxDest = Integer.valueOf(luces.intValue() * ola.intValue());
/*     */       
/*  86 */       if (maxDest.intValue() > ndestinos.intValue())
/*     */       {
/*  88 */         maxLuces = Integer.valueOf(ndestinos.intValue() - luces.intValue() * (ola.intValue() - 1));
/*     */       }
/*     */       
/*  91 */       for (int i = 0; i < maxLuces.intValue(); i++) {
/*  92 */         dests.add((Destino)dests2.toArray()[(i + luces.intValue() * (ola.intValue() - 1))]);
/*     */       }
/*     */     } else {
/*  95 */       for (int i = 0; i < ndestinos.intValue(); i++) {
/*  96 */         dests.add((Destino)dests2.toArray()[(i + luces.intValue() * (ola.intValue() - 1))]);
/*     */       }
/*     */     }
/*     */     
/* 100 */     attr.put("destinos", dests);
/* 101 */     ApplicationController.getInstance().putVariable("cantidadDestinos", Integer.valueOf(dests.size()));
/* 102 */     UiManager.getInstance().updateUiData("loadDestinos", attr);
/*     */     
/* 104 */     setStatus(TaskStatus.Finished);
/* 105 */     TareaTerminada(new TaskEvent(this, this));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/* 112 */     return null;
/*     */   }
/*     */   
/*     */   public String getNumeroPicking() {
/* 116 */     return this.numeroPicking;
/*     */   }
/*     */   
/*     */   public void setNumeroPicking(String numeroPicking) {
/* 120 */     this.numeroPicking = numeroPicking;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 125 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/ShowBoxesAssigmentPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */