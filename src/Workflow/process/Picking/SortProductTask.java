/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.DetallePicking;
/*     */ import Models.Entities.local.InformacionBulto;
/*     */ import Models.Entities.local.InformacionPicking;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.DetallePickingJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Workflow.Task;
/*     */ import java.math.BigInteger;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Calendar;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ class SortProductTask
/*     */   extends Task
/*     */ {
/*  43 */   private static SortProductTask instance = null;
/*     */   private SortingStatus sortingStatus;
/*     */   private String codigoBarra;
/*     */   private String currentArticulo;
/*     */   private List<Caja> prendidas;
/*     */   
/*     */   protected SortProductTask()
/*     */   {
/*  51 */     setSortingStatus(SortingStatus.INIT);
/*  52 */     this.currentArticulo = null;
/*  53 */     this.prendidas = new ArrayList();
/*     */   }
/*     */   
/*     */   public static SortProductTask getInstance()
/*     */   {
/*  58 */     if (instance == null) {
/*  59 */       instance = new SortProductTask();
/*  60 */       instance.setId("SortProductTask");
/*     */     }
/*     */     
/*  63 */     return instance;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/*     */     try
/*     */     {
/*  70 */       Integer numero = Integer.valueOf(Integer.parseInt(getIncomingData()));
/*  71 */       Integer confirmado = Integer.valueOf(0);
/*  72 */       Integer posicion = getIncomingDataPosition();
/*     */       
/*  74 */       Caja caja = CajaJpaController.getInstance().findCajaAbiertaByPosicion(posicion);
/*  75 */       Picking p = caja.getPickingId();
/*  76 */       Destino d = caja.getDestinoId();
/*     */       
/*  78 */       List<DetallePicking> dplist = DetallePickingJpaController.getInstance().findByPickingAndDestinoAndArticulo(p, d, this.currentArticulo);
/*  79 */       String fecha = "";
/*     */       try {
/*  81 */         DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
/*     */         
/*  83 */         Date today = Calendar.getInstance().getTime();
/*     */         
/*     */ 
/*  86 */         fecha = df.format(today);
/*     */       }
/*     */       catch (Exception ex) {
/*  89 */         LoggingManager.getInstance().log_error(ex);
/*  90 */         fecha = "...";
/*     */       }
/*     */       
/*  93 */       for (DetallePicking daux : dplist)
/*     */       {
/*  95 */         DetalleCaja detCaja = new DetalleCaja();
/*  96 */         detCaja.setArticulo(this.currentArticulo);
/*  97 */         detCaja.setCajaId(caja);
/*  98 */         detCaja.setFecha(new Date().toString());
/*  99 */         detCaja.setCodigoBarra(this.codigoBarra);
/* 100 */         detCaja.setReferencia(daux.getReferencia());
/* 101 */         detCaja.setCreated(new Date());
/*     */         
/*     */ 
/* 104 */         if (numero.intValue() > 0) {
/* 105 */           if (!daux.getConsolidado().equals(daux.getAconsolidar()))
/*     */           {
/*     */ 
/* 108 */             Integer Auxiliar = Integer.valueOf(daux.getAconsolidar().intValue() - daux.getConsolidado().intValue());
/* 109 */             if (Auxiliar.intValue() >= numero.intValue()) {
/* 110 */               daux.setConsolidado(Integer.valueOf(daux.getConsolidado().intValue() + numero.intValue()));
/* 111 */               detCaja.setCantidad(numero);
/* 112 */               detCaja.setCodigo_retail(daux.getCodigo_retail());
/* 113 */               detCaja.setOc(daux.getOc());
/*     */               try
/*     */               {
/* 116 */                 DetallePickingJpaController.getInstance().edit(daux);
/* 117 */                 DetalleCajaJpaController.getInstance().create(detCaja);
/*     */                 
/* 119 */                 ((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).setDistribuidos(Integer.valueOf(((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).getDistribuidos().intValue() + numero.intValue()));
/* 120 */                 ((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).setPendientes(Integer.valueOf(((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).getPendientes().intValue() - numero.intValue()));
/*     */                 
/* 122 */                 ((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).setDistribuido(Integer.valueOf(((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).getDistribuido().intValue() + numero.intValue()));
/* 123 */                 ((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).setPendiente(Integer.valueOf(((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).getPendiente().intValue() - numero.intValue()));
/*     */                 
/* 125 */                 ApplicationController.getInstance().putVariable("currentPendientesArticulo", Integer.valueOf(((Integer)ApplicationController.getInstance().getVariable("currentPendientesArticulo")).intValue() - numero.intValue()));
/* 126 */                 ApplicationController.getInstance().putVariable("currentDistribuidosArticulo", Integer.valueOf(((Integer)ApplicationController.getInstance().getVariable("currentDistribuidosArticulo")).intValue() + numero.intValue()));
/*     */                 
/* 128 */                 HashMap mapa = new HashMap();
/* 129 */                 mapa.put("fechahora", fecha);
/* 130 */                 mapa.put("articulo", this.currentArticulo);
/* 131 */                 mapa.put("destino", d.getNombre());
/* 132 */                 mapa.put("cantidad", numero.toString());
/* 133 */                 mapa.put("caja", caja.getCodigo());
/* 134 */                 mapa.put("docsalida", daux.getReferencia());
/*     */                 
/*     */ 
/* 137 */                 UiManager.getInstance().updateUiData("transaccion", mapa);
/*     */                 
/*     */ 
/* 140 */                 confirmado = numero;
/* 141 */                 numero = Integer.valueOf(0);
/*     */               } catch (Exception ex) {
/* 143 */                 Logger.getLogger(SortProductTask.class.getName()).log(Level.SEVERE, null, ex);
/* 144 */                 LoggingManager.getInstance().log_error(ex);
/*     */               }
/*     */             }
/*     */             else {
/* 148 */               daux.setConsolidado(Integer.valueOf(daux.getConsolidado().intValue() + Auxiliar.intValue()));
/* 149 */               detCaja.setCantidad(Auxiliar);
/* 150 */               detCaja.setCodigo_retail(daux.getCodigo_retail());
/* 151 */               detCaja.setOc(daux.getOc());
/*     */               try
/*     */               {
/* 154 */                 DetallePickingJpaController.getInstance().edit(daux);
/* 155 */                 DetalleCajaJpaController.getInstance().create(detCaja);
/*     */                 
/* 157 */                 ApplicationController.getInstance().getVariable("infoPicking");
/*     */                 
/* 159 */                 ((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).setDistribuidos(Integer.valueOf(((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).getDistribuidos().intValue() + Auxiliar.intValue()));
/* 160 */                 ((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).setPendientes(Integer.valueOf(((InformacionBulto)ApplicationController.getInstance().getVariable("infoBulto")).getPendientes().intValue() - Auxiliar.intValue()));
/*     */                 
/* 162 */                 ((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).setDistribuido(Integer.valueOf(((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).getDistribuido().intValue() + Auxiliar.intValue()));
/* 163 */                 ((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).setPendiente(Integer.valueOf(((InformacionPicking)ApplicationController.getInstance().getVariable("infoPicking")).getPendiente().intValue() - Auxiliar.intValue()));
/*     */                 
/*     */ 
/* 166 */                 ApplicationController.getInstance().putVariable("currentPendientesArticulo", Integer.valueOf(((Integer)ApplicationController.getInstance().getVariable("currentPendientesArticulo")).intValue() - Auxiliar.intValue()));
/* 167 */                 ApplicationController.getInstance().putVariable("currentDistribuidosArticulo", Integer.valueOf(((Integer)ApplicationController.getInstance().getVariable("currentDistribuidosArticulo")).intValue() + Auxiliar.intValue()));
/*     */                 
/* 169 */                 HashMap mapa = new HashMap();
/* 170 */                 mapa.put("fechahora", fecha);
/* 171 */                 mapa.put("articulo", this.currentArticulo);
/* 172 */                 mapa.put("destino", d.getNombre());
/* 173 */                 mapa.put("cantidad", Auxiliar.toString());
/* 174 */                 mapa.put("caja", caja.getCodigo());
/* 175 */                 mapa.put("docsalida", daux.getReferencia());
/*     */                 
/*     */ 
/* 178 */                 UiManager.getInstance().updateUiData("transaccion", mapa);
/*     */                 
/* 180 */                 numero = Integer.valueOf(numero.intValue() - Auxiliar.intValue());
/*     */                 
/*     */ 
/*     */ 
/* 184 */                 confirmado = Auxiliar;
/*     */               }
/*     */               catch (Exception ex)
/*     */               {
/* 188 */                 Logger.getLogger(SortProductTask.class.getName()).log(Level.SEVERE, null, ex);
/* 189 */                 LoggingManager.getInstance().log_error(ex);
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 200 */       LoggingManager.getInstance().log("SE confirmaron  " + confirmado.toString() + " articulos para el destino " + d.getNombre() + ", articulo: " + this.currentArticulo + ", Caja:  " + caja.getCodigo());
/* 201 */       cajaOff(caja);
/* 202 */       if (canSort()) {
/* 203 */         ApplicationController.getInstance().putVariable("CanSort", Boolean.valueOf(true));
/*     */       } else {
/* 205 */         ApplicationController.getInstance().putVariable("CanSort", Boolean.valueOf(false));
/*     */       }
/*     */       
/* 208 */       UiManager.getInstance().actualizaInfo();
/*     */     } catch (Exception ex) {
/* 210 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/* 221 */     if (canSort()) {
/* 222 */       clearPrendidas();
/*     */       
/*     */       try
/*     */       {
/* 226 */         UiManager.getInstance().clearPanel();
/* 227 */         this.currentArticulo = null;
/*     */         
/*     */ 
/* 230 */         LoggingManager.getInstance().log("Leyendo codigo de bara: " + this.codigoBarra);
/*     */         
/* 232 */         ApplicationController.getInstance().putVariable("currentExiste", Boolean.valueOf(false));
/* 233 */         String ola = (String)ApplicationController.getInstance().getVariable("picking");
/*     */         
/* 235 */         List<Caja> listaCajas = CajaJpaController.getInstance().findCajasAbiertasByPicking(PickingJpaController.getInstance().findByIdReferencia(ola).getId());
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 245 */         Integer cantidadBulto = Integer.valueOf(0);
/* 246 */         Integer cantidadAux = Integer.valueOf(0);
/* 247 */         Integer yaConsolidado = Integer.valueOf(0);
/*     */         
/* 249 */         ApplicationController.getInstance().putVariable("currentCodigo", this.codigoBarra);
/* 250 */         ApplicationController.getInstance().putVariable("currentArticulo", this.codigoBarra);
/* 251 */         ApplicationController.getInstance().putVariable("currentCantidadBulto", cantidadBulto);
/*     */         
/*     */ 
/*     */ 
/* 255 */         List<Object> lll = DetallePickingJpaController.getInstance().findArticuloPickingDestino(PickingJpaController.getInstance().findByIdReferencia(ola), this.codigoBarra);
/*     */         
/* 257 */         Integer PendientesBulto = Integer.valueOf(0);
/* 258 */         if (lll != null)
/*     */         {
/*     */ 
/* 261 */           this.currentArticulo = this.codigoBarra;
/*     */           
/* 263 */           for (Object aux : lll)
/*     */           {
/* 265 */             Integer aDistribuir = Integer.valueOf(0);
/*     */             
/* 267 */             aDistribuir = Integer.valueOf(((BigInteger)((Object[])(Object[])aux)[3]).intValue() - ((BigInteger)((Object[])(Object[])aux)[4]).intValue());
/*     */             
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 273 */             cantidadBulto = Integer.valueOf(cantidadBulto.intValue() + ((BigInteger)((Object[])(Object[])aux)[3]).intValue());
/* 274 */             yaConsolidado = Integer.valueOf(yaConsolidado.intValue() + ((BigInteger)((Object[])(Object[])aux)[4]).intValue());
/*     */             
/*     */ 
/* 277 */             Caja c = CajaJpaController.getInstance().findCajaAbiertaByDestino((Integer)((Object[])(Object[])aux)[1]);
/* 278 */             if (c != null)
/*     */             {
/* 280 */               PendientesBulto = Integer.valueOf(PendientesBulto.intValue() + aDistribuir.intValue());
/*     */               
/*     */ 
/*     */ 
/* 284 */               if (aDistribuir.intValue() > 0) {
/* 285 */                 CommunicationController.getInstance().prenderLuzConNumero(c.getPosicionPtl(), aDistribuir);
/* 286 */                 cajaOn(c);
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/*     */         
/*     */ 
/* 293 */         ApplicationController.getInstance().putVariable("currentExiste", Boolean.valueOf(true));
/* 294 */         ApplicationController.getInstance().putVariable("currentPendientesArticulo", PendientesBulto);
/* 295 */         ApplicationController.getInstance().putVariable("currentDistribuidosArticulo", yaConsolidado);
/* 296 */         ApplicationController.getInstance().putVariable("currentCantidadBulto", cantidadBulto);
/*     */         
/* 298 */         if (canSort()) {
/* 299 */           ApplicationController.getInstance().putVariable("CanSort", Boolean.valueOf(true));
/*     */         } else {
/* 301 */           ApplicationController.getInstance().putVariable("CanSort", Boolean.valueOf(false));
/*     */         }
/*     */         
/* 304 */         UiManager.getInstance().actualizaInfo();
/*     */       }
/*     */       catch (Exception ex)
/*     */       {
/* 308 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public SortingStatus getSortingStatus()
/*     */   {
/* 315 */     return this.sortingStatus;
/*     */   }
/*     */   
/*     */   public void setSortingStatus(SortingStatus sortingStatus) {
/* 319 */     this.sortingStatus = sortingStatus;
/*     */   }
/*     */   
/*     */   public String getCodigoBarra() {
/* 323 */     return this.codigoBarra;
/*     */   }
/*     */   
/*     */   public void setCodigoBarra(String codigoBarra) {
/* 327 */     this.codigoBarra = codigoBarra;
/*     */   }
/*     */   
/*     */   private void clearPrendidas()
/*     */   {
/* 332 */     this.prendidas.clear();
/*     */   }
/*     */   
/*     */   private void cajaOn(Caja c) {
/* 336 */     this.prendidas.add(c);
/*     */   }
/*     */   
/*     */   private void cajaOff(Caja c) {
/* 340 */     this.prendidas.remove(c);
/*     */   }
/*     */   
/*     */   private boolean canSort() {
/* 344 */     return this.prendidas.isEmpty();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/SortProductTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */