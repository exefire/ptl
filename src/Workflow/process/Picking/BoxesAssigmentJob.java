/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.IncomingDataEvent;
/*     */ import Workflow.Job;
/*     */ import Workflow.JobEvent;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import Workflow.WorkflowManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BoxesAssigmentJob
/*     */   extends Job
/*     */ {
/*  23 */   private Task initTask = ShowBoxesAssigmentPanelTask.getInstance();
/*  24 */   private EndBoxesAssignmentTask endAssignmentTask = EndBoxesAssignmentTask.getInstance();
/*  25 */   private NewBoxCodeTask newBoxCodeTask = NewBoxCodeTask.getInstance();
/*  26 */   private AssignBoxTask assignBoxTask = AssignBoxTask.getInstance();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  32 */   private static BoxesAssigmentJob instance = null;
/*     */   private Job nextJob;
/*     */   private String numeroPicking;
/*     */   
/*     */   protected BoxesAssigmentJob() {
/*  37 */     addListener(WorkflowManager.getInstance());
/*  38 */     this.initTask.addListener(this);
/*  39 */     this.assignBoxTask.addListener(this);
/*  40 */     this.endAssignmentTask.addListener(this);
/*     */   }
/*     */   
/*     */   public static BoxesAssigmentJob getInstance() {
/*  44 */     if (instance == null) {
/*  45 */       instance = new BoxesAssigmentJob();
/*     */     }
/*     */     
/*  48 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void init()
/*     */   {
/*  57 */     ((ShowBoxesAssigmentPanelTask)this.initTask).setNumeroPicking(this.numeroPicking);
/*     */     
/*  59 */     this.initTask.performAction();
/*     */   }
/*     */   
/*     */   public Job getNextJob()
/*     */   {
/*  64 */     return this.nextJob;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void onTaskFinished(TaskEvent event)
/*     */   {
/*  71 */     Task n = (Task)event.getSource();
/*     */     
/*     */ 
/*  74 */     if ((n.getId() == "EndBoxesAssignmentTask") && 
/*  75 */       (n.getStatus().equals(TaskStatus.Finished)))
/*     */     {
/*  77 */       this.nextJob = SortingJob.getInstance();
/*  78 */       JobTerminado(new JobEvent(this, this));
/*     */     }
/*     */     
/*  81 */     if (n.getId().equals("AssignBoxTask"))
/*     */     {
/*  83 */       if (n.getStatus().equals(TaskStatus.Finished)) {
/*  84 */         this.nextJob = SortingJob.getInstance();
/*  85 */         JobTerminado(new JobEvent(this, this));
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*  90 */     else if ((n.getId().equals("ShowBoxesAssigmentPanelTask")) && 
/*  91 */       (n.getStatus().equals(TaskStatus.Finished)))
/*     */     {
/*     */ 
/*     */ 
/*  95 */       setCurrentTask(this.assignBoxTask);
/*  96 */       ((AssignBoxTask)getCurrentTask()).restart();
/*  97 */       getCurrentTask().performAction();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void endAssignment()
/*     */   {
/* 111 */     this.endAssignmentTask.performAction();
/*     */   }
/*     */   
/*     */   public void newBoxCode(String picking, String destino, String posicion, String codigo)
/*     */   {
/* 116 */     AssignBoxTask.getInstance().setDestino(destino);
/* 117 */     AssignBoxTask.getInstance().setCodigoCaja(codigo);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void incomingData(IncomingDataEvent e)
/*     */   {
/* 125 */     getCurrentTask().manageIncominData(e.getData(), e.getPoscion());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumeroPicking()
/*     */   {
/* 133 */     return this.numeroPicking;
/*     */   }
/*     */   
/*     */   public void setNumeroPicking(String numeroPicking) {
/* 137 */     this.numeroPicking = numeroPicking;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/BoxesAssigmentJob.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */