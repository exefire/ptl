/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Configuration.ConfigurationManager;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DestinoJpaController;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import Storage.Files.FileManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ import ui.Process.Picking.EnterCodeBoxPanel;
/*     */ import ui.Process.Picking.WaitingPtlPanel;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CloseBoxTask
/*     */   extends Task
/*     */ {
/*  41 */   private static CloseBoxTask instance = null;
/*     */   private ClosingBoxStatus closingStatus;
/*  43 */   private String BoxCode = "";
/*     */   private Integer positionToClose;
/*     */   private EnterCodeBoxPanel enterCodeBoxPanel;
/*     */   private WaitingPtlPanel waitingPtlPanel;
/*     */   private Caja currentBox;
/*     */   
/*     */   protected CloseBoxTask()
/*     */   {
/*  51 */     setStatus(TaskStatus.Waiting);
/*  52 */     setClosingStatus(ClosingBoxStatus.INIT);
/*  53 */     this.waitingPtlPanel = new WaitingPtlPanel();
/*  54 */     this.enterCodeBoxPanel = new EnterCodeBoxPanel();
/*     */   }
/*     */   
/*     */   public static CloseBoxTask getInstance() {
/*  58 */     if (instance == null) {
/*  59 */       instance = new CloseBoxTask();
/*  60 */       instance.setId("CloseBoxTask");
/*     */     }
/*     */     
/*  63 */     return instance;
/*     */   }
/*     */   
/*     */   public void performAction()
/*     */   {
/*  68 */     if (this.status.equals(TaskStatus.Finished))
/*     */     {
/*  70 */       setClosingStatus(ClosingBoxStatus.INIT);
/*  71 */       setStatus(TaskStatus.Waiting);
/*  72 */       CommunicationController.getInstance().apagarTodasLasLuces();
/*     */     }
/*     */     
/*     */ 
/*  76 */     if (getClosingStatus().equals(ClosingBoxStatus.INIT))
/*     */     {
/*     */       try
/*     */       {
/*  80 */         setEstadoInicial();
/*     */       }
/*     */       catch (Exception ex) {
/*  83 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/*  86 */     } else if (getClosingStatus().equals(ClosingBoxStatus.WAITING_PTL))
/*     */     {
/*     */       try
/*     */       {
/*  90 */         this.positionToClose = getIncomingDataPosition();
/*  91 */         CommunicationController.getInstance().apagarTodasLasLuces();
/*     */         
/*  93 */         LoggingManager.getInstance().log("Posicion Elegida: " + this.positionToClose.toString());
/*     */         
/*     */ 
/*  96 */         this.currentBox = CajaJpaController.getInstance().findCajaAbiertaByPosicion(getIncomingDataPosition());
/*     */         
/*  98 */         this.currentBox.setEstado("cerrada");
/*  99 */         this.currentBox.setFechaCierre(new Date());
/*     */         try
/*     */         {
/* 102 */           CajaJpaController.getInstance().edit(this.currentBox);
/* 103 */           LoggingManager.getInstance().log("Caja cerrada: " + this.currentBox.getCodigo());
/*     */         } catch (NonexistentEntityException ex) {
/* 105 */           Logger.getLogger(CloseBoxTask.class.getName()).log(Level.SEVERE, null, ex);
/* 106 */           LoggingManager.getInstance().log_error(ex);
/*     */         } catch (Exception ex) {
/* 108 */           Logger.getLogger(CloseBoxTask.class.getName()).log(Level.SEVERE, null, ex);
/* 109 */           LoggingManager.getInstance().log_error(ex);
/*     */         }
/*     */         
/*     */ 
/* 113 */         FileManager.getInstance().newCsvFile(this.currentBox.getCodigo(), this.currentBox.getCodigo());
/* 114 */         LoggingManager.getInstance().log("Creando Archivo de Caja Cerrada");
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 125 */         List<Object> documentos = DetalleCajaJpaController.getInstance().findAllDocumentos(this.currentBox);
/*     */         
/*     */ 
/*     */ 
/* 129 */         if (documentos != null)
/*     */         {
/* 131 */           for (Object o : documentos)
/*     */           {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 138 */             String referencia = (String)((Object[])(Object[])o)[1];
/*     */             
/*     */ 
/* 141 */             List<DetalleCaja> listaDetalle2 = DetalleCajaJpaController.getInstance().findAllByCajaAndReferencia(this.currentBox, referencia);
/*     */             
/*     */ 
/* 144 */             if (listaDetalle2 != null) {
/* 145 */               for (DetalleCaja detCaja : listaDetalle2)
/*     */               {
/*     */ 
/* 148 */                 String ola = (String)ApplicationController.getInstance().getVariable("picking");
/* 149 */                 String code = this.currentBox.getCodigo();
/*     */                 
/*     */ 
/*     */ 
/*     */ 
/* 154 */                 String str_usuario = ((User)ApplicationController.getInstance().getVariable("usuario")).getUsername();
/*     */                 
/* 156 */                 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
/* 157 */                 String fechatiempo = df.format(detCaja.getCreated());
/*     */                 
/*     */ 
/* 160 */                 FileManager.getInstance().putLineOnFile(this.currentBox.getCodigo(), ola + ";" + code + ";" + detCaja.getCodigoBarra() + ";" + detCaja.getCantidad() + ";" + this.currentBox.getDestinoId().getNombre() + ";" + str_usuario + ";" + fechatiempo);
/*     */               }
/*     */               
/*     */ 
/*     */ 
/*     */ 
/* 166 */               FileManager.getInstance().closeFile(this.currentBox.getCodigo());
/* 167 */               LoggingManager.getInstance().log("Cerrando Archivo de cierre de Caja: " + this.currentBox.getCodigo());
/*     */             }
/*     */           }
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 177 */         setClosingStatus(ClosingBoxStatus.WAITING_BOXCODE);
/* 178 */         UiManager.getInstance().loadPanel(this.enterCodeBoxPanel);
/* 179 */         UiManager.getInstance().setFocusOnPanel();
/* 180 */         UiManager.getInstance().actualizaInfo();
/*     */         
/* 182 */         CommunicationController.getInstance().prenderLuz(this.positionToClose, null, "green");
/*     */       }
/*     */       catch (Exception ex) {
/* 185 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/* 188 */     } else if (getClosingStatus().equals(ClosingBoxStatus.WAITING_BOXCODE))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 200 */         Caja c = CajaJpaController.getInstance().findCajaByCode(this.BoxCode);
/*     */         
/* 202 */         if (c != null)
/*     */         {
/*     */ 
/* 205 */           UiManager.getInstance().clearPanel();
/* 206 */           UiManager.getInstance().notificate("Este codigo ya ha sido utilizado");
/* 207 */           LoggingManager.getInstance().log("Código ya ha sido utilizado: " + getBoxCode());
/* 208 */           setBoxCode("");
/*     */         }
/*     */         else {
/* 211 */           c = new Caja();
/* 212 */           c.setEstado("abierta");
/* 213 */           c.setCodigo(this.BoxCode);
/* 214 */           c.setFechaCreacion(new Date());
/* 215 */           c.setPosicionPtl(this.positionToClose);
/* 216 */           c.setDestinoId(this.currentBox.getDestinoId());
/*     */           
/* 218 */           c.setPickingId(this.currentBox.getPickingId());
/*     */           
/*     */ 
/* 221 */           CajaJpaController.getInstance().create(c);
/* 222 */           LoggingManager.getInstance().log("Nueva caja creada con código: " + getBoxCode());
/*     */           
/* 224 */           this.positionToClose = null;
/* 225 */           this.currentBox = null;
/* 226 */           UiManager.getInstance().clearPanel();
/* 227 */           setBoxCode("");
/*     */           
/*     */ 
/* 230 */           setStatus(TaskStatus.Finished);
/* 231 */           CommunicationController.getInstance().apagarTodasLasLuces();
/*     */           
/* 233 */           setEstadoInicial();
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       catch (Exception ex)
/*     */       {
/*     */ 
/* 241 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void terminarClosingBox()
/*     */   {
/* 252 */     this.positionToClose = null;
/* 253 */     this.currentBox = null;
/* 254 */     UiManager.getInstance().clearPanel();
/* 255 */     setBoxCode("");
/*     */     
/*     */ 
/* 258 */     setStatus(TaskStatus.Finished);
/* 259 */     CommunicationController.getInstance().apagarTodasLasLuces();
/* 260 */     TareaTerminada(new TaskEvent(this, this));
/*     */   }
/*     */   
/*     */ 
/*     */   public void dataArrived()
/*     */   {
/* 266 */     if ((getClosingStatus().equals(ClosingBoxStatus.WAITING_PTL)) || (getClosingStatus().equals(ClosingBoxStatus.WAITING_BOXCODE))) {
/* 267 */       performAction();
/*     */     } else {
/* 269 */       CommunicationController.getInstance().prenderLuz(getIncomingDataPosition(), null, "green");
/*     */     }
/*     */   }
/*     */   
/*     */   public ClosingBoxStatus getClosingStatus()
/*     */   {
/* 275 */     return this.closingStatus;
/*     */   }
/*     */   
/*     */   public void setClosingStatus(ClosingBoxStatus closingStatus) {
/* 279 */     this.closingStatus = closingStatus;
/*     */   }
/*     */   
/*     */   public String getBoxCode() {
/* 283 */     return this.BoxCode;
/*     */   }
/*     */   
/*     */   public void setBoxCode(String BoxCode) {
/* 287 */     this.BoxCode = BoxCode;
/*     */   }
/*     */   
/*     */   private void setEstadoInicial()
/*     */   {
/* 292 */     LoggingManager.getInstance().log("Iniciando derrado de Caja, Prendiendo luces ");
/* 293 */     CommunicationController.getInstance().apagarTodasLasLuces();
/* 294 */     UiManager.getInstance().loadPanel(this.waitingPtlPanel);
/*     */     
/* 296 */     String ola = (String)ApplicationController.getInstance().getVariable("picking");
/*     */     
/* 298 */     Integer ndestinos = DestinoJpaController.getInstance().countDestinosPickingByOla(PickingJpaController.getInstance().findByIdReferencia(ola));
/* 299 */     Integer luces = Integer.valueOf(Integer.parseInt(ConfigurationManager.getInstance().getProperty("luces")));
/*     */     
/* 301 */     if (luces.intValue() < ndestinos.intValue()) {
/* 302 */       CommunicationController.getInstance().prenderNluces(luces, "blue");
/*     */     }
/*     */     else {
/* 305 */       CommunicationController.getInstance().prenderNluces(ndestinos, "blue");
/*     */     }
/*     */     
/* 308 */     setStatus(TaskStatus.Waiting);
/* 309 */     setClosingStatus(ClosingBoxStatus.WAITING_PTL);
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/CloseBoxTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */