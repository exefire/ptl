/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Communication.IncomingDataEvent;
/*     */ import Core.ApplicationController;
/*     */ import Workflow.Job;
/*     */ import Workflow.JobEvent;
/*     */ import Workflow.JobStatus;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import Workflow.WorkflowManager;
/*     */ import Workflow.process.GroupSelection.GroupSelectionJob;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SortingJob
/*     */   extends Job
/*     */ {
/*  26 */   private Task initTask = ShowSortingPanelTask.getInstance();
/*     */   
/*  28 */   private CloseBoxTask closeBoxTask = CloseBoxTask.getInstance();
/*  29 */   private SortProductTask sortProductTask = SortProductTask.getInstance();
/*  30 */   private EndSortingTask endSortingTask = EndSortingTask.getInstance();
/*  31 */   private EndWaveTask endOlaTask = EndWaveTask.getInstance();
/*  32 */   private ShowInfoBultoPanelTask infoBultoPanel = ShowInfoBultoPanelTask.getInstance();
/*     */   
/*     */ 
/*  35 */   private static SortingJob instance = null;
/*     */   private Job nextJob;
/*     */   private String barcode;
/*     */   
/*     */   protected SortingJob()
/*     */   {
/*  41 */     addListener(WorkflowManager.getInstance());
/*     */     
/*  43 */     this.initTask.addListener(this);
/*  44 */     this.closeBoxTask.addListener(this);
/*  45 */     this.sortProductTask.addListener(this);
/*  46 */     this.endSortingTask.addListener(this);
/*  47 */     this.endOlaTask.addListener(this);
/*  48 */     this.infoBultoPanel.addListener(this);
/*     */   }
/*     */   
/*     */   public static SortingJob getInstance() {
/*  52 */     if (instance == null) {
/*  53 */       instance = new SortingJob();
/*     */     }
/*     */     
/*  56 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void init()
/*     */   {
/*  65 */     setEstado(JobStatus.PROCESSING);
/*  66 */     setCurrentTask(this.initTask);
/*  67 */     this.initTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Job getNextJob()
/*     */   {
/*  76 */     return this.nextJob;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void onTaskFinished(TaskEvent event)
/*     */   {
/*  83 */     Task n = (Task)event.getSource();
/*     */     
/*     */ 
/*  86 */     if ((n.getId() == "CloseBoxTask") && 
/*  87 */       (n.getStatus().equals(TaskStatus.Finished))) {
/*  88 */       setCurrentTask(this.initTask);
/*  89 */       getCurrentTask().performAction();
/*     */     }
/*     */     
/*     */ 
/*  93 */     if ((n.getId().equals("ShowSortingPanelTask")) && 
/*  94 */       (n.getStatus().equals(TaskStatus.Finished))) {
/*  95 */       setCurrentTask(this.sortProductTask);
/*     */     }
/*  97 */     if ((n.getId().equals("EndSortingTask")) && 
/*  98 */       (n.getStatus().equals(TaskStatus.Finished))) {
/*  99 */       if (((EndSortingTask)n).isFinolas()) {
/* 100 */         this.nextJob = GroupSelectionJob.getInstance();
/* 101 */         setEstado(JobStatus.FINISHED);
/* 102 */         JobTerminado(new JobEvent(this, this));
/*     */       } else {
/* 104 */         this.nextJob = BoxesAssigmentJob.getInstance();
/* 105 */         ((BoxesAssigmentJob)this.nextJob).setNumeroPicking((String)ApplicationController.getInstance().getVariable("ola"));
/*     */         
/*     */ 
/* 108 */         setEstado(JobStatus.FINISHED);
/* 109 */         JobTerminado(new JobEvent(this, this));
/*     */       }
/*     */     }
/* 112 */     if ((n.getId().equals("EndWaveTask")) && 
/* 113 */       (n.getStatus().equals(TaskStatus.Finished))) {
/* 114 */       this.nextJob = GroupSelectionJob.getInstance();
/* 115 */       setEstado(JobStatus.FINISHED);
/* 116 */       JobTerminado(new JobEvent(this, this));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void incomingData(IncomingDataEvent e)
/*     */   {
/* 130 */     getCurrentTask().manageIncominData(e.getData(), e.getPoscion());
/*     */   }
/*     */   
/*     */   public void barcodeEnter(String bcode)
/*     */   {
/* 135 */     this.barcode = bcode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void closeBox()
/*     */   {
/* 143 */     setCurrentTask(this.closeBoxTask);
/*     */     
/*     */ 
/* 146 */     getCurrentTask().performAction();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void endSorting()
/*     */   {
/* 157 */     this.endSortingTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public void nextWave()
/*     */   {
/* 163 */     setCurrentTask(this.endOlaTask);
/* 164 */     this.endOlaTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void sortBarCode(String text)
/*     */   {
/* 171 */     CommunicationController.getInstance().apagarTodasLasLuces();
/* 172 */     ((SortProductTask)getCurrentTask()).setCodigoBarra(text);
/* 173 */     getCurrentTask().performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public void newCodeEntered(String str)
/*     */   {
/* 179 */     ((CloseBoxTask)getCurrentTask()).setBoxCode(str);
/* 180 */     getCurrentTask().performAction();
/*     */   }
/*     */   
/*     */   void getCurrentPicking()
/*     */   {
/* 185 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void comeBackToSorting()
/*     */   {
/* 190 */     setCurrentTask(this.initTask);
/* 191 */     this.initTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public void ShowBultoInfo(String codigo)
/*     */   {
/* 197 */     this.infoBultoPanel.setCodigoBulto(codigo);
/* 198 */     this.infoBultoPanel.performAction();
/*     */   }
/*     */   
/*     */   public void endClosingBox() {
/* 202 */     ((CloseBoxTask)getCurrentTask()).terminarClosingBox();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/SortingJob.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */