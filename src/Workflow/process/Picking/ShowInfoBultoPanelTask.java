/*    */ package Workflow.process.Picking;
/*    */ 
/*    */ import Models.JpaControllers.local.DetallePickingJpaController;
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskStatus;
/*    */ import java.util.HashMap;
/*    */ import java.util.List;
/*    */ import ui.Process.Picking.InfoBultoPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ class ShowInfoBultoPanelTask
/*    */   extends Task
/*    */ {
/*    */   InfoBultoPanel panel;
/* 26 */   private static ShowInfoBultoPanelTask instance = null;
/* 27 */   private String codigoBulto = "";
/*    */   
/*    */   protected ShowInfoBultoPanelTask() {
/* 30 */     setStatus(TaskStatus.Waiting);
/* 31 */     this.panel = new InfoBultoPanel();
/*    */   }
/*    */   
/*    */   public static ShowInfoBultoPanelTask getInstance() {
/* 35 */     if (instance == null) {
/* 36 */       instance = new ShowInfoBultoPanelTask();
/* 37 */       instance.setId("ShowInfoBultoPanelTask");
/*    */     }
/*    */     
/* 40 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 46 */     this.panel.clearData();
/* 47 */     UiManager.getInstance().loadPanel(this.panel);
/*    */     
/*    */ 
/* 50 */     HashMap mapa = new HashMap();
/*    */     
/* 52 */     List<Object> lista = DetallePickingJpaController.getInstance().getResumeListByPicking(this.codigoBulto);
/*    */     
/* 54 */     mapa.put("codigo", this.codigoBulto);
/* 55 */     mapa.put("articulos", lista);
/*    */     
/*    */ 
/* 58 */     UiManager.getInstance().updateUiData("load", mapa);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 68 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 73 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public String getCodigoBulto() {
/* 77 */     return this.codigoBulto;
/*    */   }
/*    */   
/*    */   public void setCodigoBulto(String codigoBulto) {
/* 81 */     this.codigoBulto = codigoBulto;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/ShowInfoBultoPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */