/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import Storage.Files.FileManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import Workflow.process.GroupSelection.PutWaveOnStandByTask;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EndSortingTask
/*     */   extends Task
/*     */ {
/*  38 */   private static EndSortingTask instance = null;
/*  39 */   private boolean finolas = false;
/*     */   
/*     */   public boolean isFinolas() {
/*  42 */     return this.finolas;
/*     */   }
/*     */   
/*     */   public void setFinolas(boolean finolas) {
/*  46 */     this.finolas = finolas;
/*     */   }
/*     */   
/*     */ 
/*     */   protected EndSortingTask()
/*     */   {
/*  52 */     setStatus(TaskStatus.Waiting);
/*  53 */     this.finolas = false;
/*     */   }
/*     */   
/*     */   public static EndSortingTask getInstance() {
/*  57 */     if (instance == null) {
/*  58 */       instance = new EndSortingTask();
/*  59 */       instance.setId("EndSortingTask");
/*     */     }
/*  61 */     return instance;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/*  66 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  73 */     this.finolas = false;
/*     */     
/*     */ 
/*  76 */     String ola = (String)ApplicationController.getInstance().getVariable("picking");
/*  77 */     Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/*     */     
/*  79 */     Integer OlaActiva = (Integer)ApplicationController.getInstance().getVariable("olaactiva");
/*  80 */     Integer nolas = (Integer)ApplicationController.getInstance().getVariable("nolas");
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  86 */       List<Caja> cajas = CajaJpaController.getInstance().findCajasAbiertasByPicking(p.getId());
/*     */       
/*  88 */       for (localIterator1 = cajas.iterator(); localIterator1.hasNext();) { c = (Caja)localIterator1.next();
/*     */         
/*  90 */         c.setEstado("cerrada");
/*  91 */         c.setFechaCierre(new Date());
/*  92 */         CajaJpaController.getInstance().edit(c);
/*     */         
/*  94 */         FileManager.getInstance().newCsvFile(c.getCodigo(), c.getCodigo());
/*  95 */         List<Object> documentos = DetalleCajaJpaController.getInstance().findAllDocumentos(c);
/*  96 */         if (documentos != null)
/*     */         {
/*  98 */           for (Object o : documentos)
/*     */           {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 104 */             String referencia = (String)((Object[])(Object[])o)[1];
/*     */             
/* 106 */             List<DetalleCaja> listaDetalle2 = DetalleCajaJpaController.getInstance().findAllByCajaAndReferencia(c, referencia);
/*     */             
/*     */ 
/* 109 */             if (listaDetalle2 != null) {
/* 110 */               for (DetalleCaja detCaja : listaDetalle2) {
/* 111 */                 String code = c.getCodigo();
/* 112 */                 String str_usuario = ((User)ApplicationController.getInstance().getVariable("usuario")).getUsername();
/*     */                 
/*     */ 
/* 115 */                 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
/* 116 */                 String fechatiempo = df.format(detCaja.getCreated());
/*     */                 
/* 118 */                 FileManager.getInstance().putLineOnFile(c.getCodigo(), ola + ";" + code + ";" + detCaja.getCodigoBarra() + ";" + detCaja.getCantidad() + ";" + c.getDestinoId().getNombre() + ";" + str_usuario + ";" + fechatiempo);
/*     */               }
/*     */               
/*     */ 
/*     */ 
/* 123 */               FileManager.getInstance().closeFile(c.getCodigo());
/* 124 */               LoggingManager.getInstance().log("Cerrando Archivo de cierre de Caja: " + c.getCodigo());
/*     */             }
/*     */           } }
/*     */       }
/*     */     } catch (NonexistentEntityException ex) {
/*     */       Iterator localIterator1;
/*     */       Caja c;
/* 131 */       Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 132 */       LoggingManager.getInstance().log_error(ex);
/*     */     } catch (Exception ex) {
/* 134 */       Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 135 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */     
/*     */ 
/* 139 */     if ((OlaActiva != null) && (nolas != null) && (OlaActiva.intValue() < nolas.intValue()))
/*     */     {
/* 141 */       OlaActiva = Integer.valueOf(OlaActiva.intValue() + 1);
/* 142 */       ApplicationController.getInstance().putVariable("olaactiva", OlaActiva);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 147 */       boolean terminado = PickingJpaController.getInstance().isFinished(p);
/*     */       
/* 149 */       if (terminado) {
/* 150 */         p.setEstado("finalizado");
/*     */       } else {
/* 152 */         p.setEstado("standby");
/*     */       }
/*     */       try
/*     */       {
/* 156 */         PickingJpaController.getInstance().edit(p);
/*     */       } catch (NonexistentEntityException ex) {
/* 158 */         Logger.getLogger(EndSortingTask.class.getName()).log(Level.SEVERE, null, ex);
/* 159 */         LoggingManager.getInstance().log_error(ex);
/*     */       } catch (Exception ex) {
/* 161 */         Logger.getLogger(EndSortingTask.class.getName()).log(Level.SEVERE, null, ex);
/* 162 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/* 165 */       this.finolas = true;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 172 */     CommunicationController.getInstance().apagarTodasLasLuces();
/* 173 */     setStatus(TaskStatus.Finished);
/* 174 */     TareaTerminada(new TaskEvent(this, this));
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/EndSortingTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */