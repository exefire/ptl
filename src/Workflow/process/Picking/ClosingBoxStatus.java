package Workflow.process.Picking;

public enum ClosingBoxStatus
{
  INIT,  WAITING_PTL,  WAITING_BOXCODE;
  
  private ClosingBoxStatus() {}
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/ClosingBoxStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */