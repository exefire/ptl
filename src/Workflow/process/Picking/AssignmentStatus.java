package Workflow.process.Picking;

public enum AssignmentStatus
{
  Init,  WaitingCode,  WaitingPtlConfirmation;
  
  private AssignmentStatus() {}
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/AssignmentStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */