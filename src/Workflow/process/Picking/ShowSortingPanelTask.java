/*    */ package Workflow.process.Picking;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ import ui.Process.Picking.SortingPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ class ShowSortingPanelTask
/*    */   extends Task
/*    */ {
/*    */   SortingPanel panel;
/* 20 */   private static ShowSortingPanelTask instance = null;
/*    */   
/*    */   protected ShowSortingPanelTask() {
/* 23 */     setStatus(TaskStatus.Waiting);
/* 24 */     this.panel = new SortingPanel();
/*    */   }
/*    */   
/*    */   public static ShowSortingPanelTask getInstance() {
/* 28 */     if (instance == null) {
/* 29 */       instance = new ShowSortingPanelTask();
/* 30 */       instance.setId("ShowSortingPanelTask");
/*    */     }
/*    */     
/* 33 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 39 */     this.panel.clearData();
/* 40 */     UiManager.getInstance().loadPanel(this.panel);
/* 41 */     UiManager.getInstance().setFocusOnPanel();
/* 42 */     setStatus(TaskStatus.Finished);
/* 43 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 49 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 54 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/ShowSortingPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */