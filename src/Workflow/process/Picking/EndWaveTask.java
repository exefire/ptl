/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import Storage.Files.FileManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import Workflow.process.GroupSelection.PutWaveOnStandByTask;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EndWaveTask
/*     */   extends Task
/*     */ {
/*  40 */   private static EndWaveTask instance = null;
/*     */   
/*     */   protected EndWaveTask() {
/*  43 */     setStatus(TaskStatus.Waiting);
/*     */   }
/*     */   
/*     */   public static EndWaveTask getInstance() {
/*  47 */     if (instance == null) {
/*  48 */       instance = new EndWaveTask();
/*  49 */       instance.setId("EndWaveTask");
/*     */     }
/*  51 */     return instance;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/*  56 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*     */     try
/*     */     {
/*  65 */       String ola = (String)ApplicationController.getInstance().getVariable("picking");
/*     */       
/*  67 */       Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/*  68 */       p.setEstado("standby");
/*     */       try {
/*  70 */         PickingJpaController.getInstance().edit(p);
/*     */         
/*  72 */         List<Caja> cajas = CajaJpaController.getInstance().findCajasAbiertasByPicking(p.getId());
/*     */         
/*  74 */         for (localIterator1 = cajas.iterator(); localIterator1.hasNext();) { c = (Caja)localIterator1.next();
/*     */           
/*  76 */           c.setEstado("cerrada");
/*  77 */           c.setFechaCierre(new Date());
/*  78 */           CajaJpaController.getInstance().edit(c);
/*     */           
/*  80 */           FileManager.getInstance().newCsvFile(c.getCodigo(), c.getCodigo());
/*  81 */           List<Object> documentos = DetalleCajaJpaController.getInstance().findAllDocumentos(c);
/*  82 */           if (documentos != null)
/*     */           {
/*  84 */             for (Object o : documentos)
/*     */             {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  90 */               String referencia = (String)((Object[])(Object[])o)[1];
/*     */               
/*  92 */               List<DetalleCaja> listaDetalle2 = DetalleCajaJpaController.getInstance().findAllByCajaAndReferencia(c, referencia);
/*     */               
/*     */ 
/*  95 */               if (listaDetalle2 != null) {
/*  96 */                 for (DetalleCaja detCaja : listaDetalle2) {
/*  97 */                   String code = c.getCodigo();
/*  98 */                   String str_usuario = ((User)ApplicationController.getInstance().getVariable("usuario")).getUsername();
/*  99 */                   DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
/* 100 */                   String fechatiempo = df.format(detCaja.getCreated());
/*     */                   
/*     */ 
/* 103 */                   FileManager.getInstance().putLineOnFile(c.getCodigo(), ola + ";" + code + ";" + detCaja.getCodigoBarra() + ";" + detCaja.getCantidad() + ";" + c.getDestinoId().getNombre() + ";" + str_usuario + ";" + fechatiempo);
/*     */                 }
/*     */                 
/*     */ 
/* 107 */                 FileManager.getInstance().closeFile(c.getCodigo());
/* 108 */                 LoggingManager.getInstance().log("Cerrando Archivo de cierre de Caja: " + c.getCodigo());
/*     */               }
/*     */             } }
/*     */         }
/*     */       } catch (NonexistentEntityException ex) {
/*     */         Iterator localIterator1;
/*     */         Caja c;
/* 115 */         Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 116 */         LoggingManager.getInstance().log_error(ex);
/*     */       } catch (Exception ex) {
/* 118 */         Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 119 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 128 */       CommunicationController.getInstance().apagarTodasLasLuces();
/* 129 */       setStatus(TaskStatus.Finished);
/* 130 */       TareaTerminada(new TaskEvent(this, this));
/*     */ 
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/*     */ 
/* 136 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/EndWaveTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */