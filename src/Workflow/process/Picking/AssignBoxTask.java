/*     */ package Workflow.process.Picking;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Configuration.ConfigurationManager;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DestinoJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Workflow.Task;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AssignBoxTask
/*     */   extends Task
/*     */ {
/*  31 */   private static AssignBoxTask instance = null;
/*     */   private AssignmentStatus estadoAsignacion;
/*     */   private Integer currentPosition;
/*  34 */   private String codigoCaja = "";
/*  35 */   private String destino = "";
/*     */   
/*     */   protected AssignBoxTask()
/*     */   {
/*  39 */     setEstadoAsignacion(AssignmentStatus.Init);
/*  40 */     this.currentPosition = Integer.valueOf(0);
/*  41 */     this.codigoCaja = "";
/*     */   }
/*     */   
/*     */   public static AssignBoxTask getInstance() {
/*  45 */     if (instance == null) {
/*  46 */       instance = new AssignBoxTask();
/*  47 */       instance.setId("AssignBoxTask");
/*     */     }
/*  49 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*     */     try
/*     */     {
/*  59 */       if (this.estadoAsignacion.equals(AssignmentStatus.Init))
/*     */       {
/*     */ 
/*  62 */         this.currentPosition = Integer.valueOf(1);
/*  63 */         HashMap map = new HashMap();
/*     */         
/*     */ 
/*  66 */         Integer maxid = CajaJpaController.getInstance().getMaxId();
/*  67 */         String codigo = formatCode(ConfigurationManager.getInstance().getProperty("id_ptl"), Integer.valueOf(maxid.intValue() + 1));
/*  68 */         this.codigoCaja = codigo;
/*  69 */         map.put("position", this.currentPosition);
/*  70 */         map.put("finalizado", Boolean.valueOf(false));
/*  71 */         map.put("codigo", codigo);
/*  72 */         map.put("lastcodigo", "");
/*  73 */         UiManager.getInstance().updateUiData("nextBox", map);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  78 */         CommunicationController.getInstance().prenderLuz(this.currentPosition, null, "green");
/*  79 */         setEstadoAsignacion(AssignmentStatus.WaitingPtlConfirmation);
/*     */       }
/*  81 */       else if (this.estadoAsignacion.equals(AssignmentStatus.WaitingCode))
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*  86 */         if (!this.codigoCaja.matches("\\bB[0-9]{10}\\b")) {
/*  87 */           UiManager.getInstance().notificate("Este codigo no tiene el formato correcto");
/*  88 */           this.codigoCaja = "";
/*  89 */           UiManager.getInstance().setFocusOnPanel();
/*     */         }
/*     */         else {
/*  92 */           Caja c = CajaJpaController.getInstance().findCajaByCode(this.codigoCaja);
/*     */           
/*  94 */           if (c != null) {
/*  95 */             UiManager.getInstance().notificate("Este codigo ya ha sido utilizado previamente");
/*  96 */             this.codigoCaja = "";
/*  97 */             UiManager.getInstance().setFocusOnPanel();
/*     */           }
/*     */           else {
/* 100 */             CommunicationController.getInstance().prenderLuz(this.currentPosition, null, "green");
/* 101 */             setEstadoAsignacion(AssignmentStatus.WaitingPtlConfirmation);
/*     */ 
/*     */           }
/*     */           
/*     */         }
/*     */         
/*     */ 
/*     */       }
/* 109 */       else if (this.estadoAsignacion.equals(AssignmentStatus.WaitingPtlConfirmation))
/*     */       {
/*     */ 
/* 112 */         Caja c = new Caja();
/* 113 */         c.setCodigo(this.codigoCaja);
/* 114 */         c.setEstado("abierta");
/* 115 */         c.setFechaCreacion(new Date());
/* 116 */         c.setPosicionPtl(this.currentPosition);
/* 117 */         String ola = (String)ApplicationController.getInstance().getVariable("picking");
/* 118 */         Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/*     */         
/* 120 */         c.setPickingId(p);
/*     */         
/* 122 */         Destino d = DestinoJpaController.getInstance().findDEstinoByName(this.destino);
/*     */         
/* 124 */         c.setDestinoId(d);
/* 125 */         CajaJpaController.getInstance().create(c);
/*     */         
/* 127 */         HashMap map = new HashMap();
/* 128 */         if ((Integer)ApplicationController.getInstance().getVariable("cantidadDestinos") == this.currentPosition) {
/* 129 */           map.put("finalizado", Boolean.valueOf(true));
/*     */         } else {
/* 131 */           map.put("finalizado", Boolean.valueOf(false));
/*     */           
/* 133 */           CommunicationController.getInstance().prenderLuz(Integer.valueOf(this.currentPosition.intValue() + 1), null, "green");
/*     */         }
/*     */         
/*     */ 
/* 137 */         AssignBoxTask localAssignBoxTask = this;Integer localInteger1 = localAssignBoxTask.currentPosition;Integer localInteger2 = localAssignBoxTask.currentPosition = Integer.valueOf(localAssignBoxTask.currentPosition.intValue() + 1);
/*     */         
/* 139 */         map.put("position", this.currentPosition);
/* 140 */         map.put("lastcodigo", this.codigoCaja);
/*     */         
/* 142 */         Integer maxid = CajaJpaController.getInstance().getMaxId();
/* 143 */         String codigo = formatCode(ConfigurationManager.getInstance().getProperty("id_ptl"), Integer.valueOf(maxid.intValue() + 1));
/* 144 */         this.codigoCaja = codigo;
/* 145 */         map.put("codigo", this.codigoCaja);
/* 146 */         this.codigoCaja = "";
/* 147 */         UiManager.getInstance().updateUiData("nextBox", map);
/*     */         
/* 149 */         setEstadoAsignacion(AssignmentStatus.WaitingPtlConfirmation);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 156 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */   
/*     */   public Task getNextTask()
/*     */   {
/* 162 */     return null;
/*     */   }
/*     */   
/*     */   public AssignmentStatus getEstadoAsignacion() {
/* 166 */     return this.estadoAsignacion;
/*     */   }
/*     */   
/*     */   public void setEstadoAsignacion(AssignmentStatus estadoAsignacion) {
/* 170 */     this.estadoAsignacion = estadoAsignacion;
/*     */   }
/*     */   
/*     */   public String getCodigoCaja() {
/* 174 */     return this.codigoCaja;
/*     */   }
/*     */   
/*     */   public void setCodigoCaja(String codigoCaja) {
/* 178 */     this.codigoCaja = codigoCaja;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 183 */     if (this.estadoAsignacion.equals(AssignmentStatus.WaitingPtlConfirmation)) {
/* 184 */       performAction();
/*     */     } else {
/* 186 */       CommunicationController.getInstance().prenderLuz(this.currentPosition, null, "blue");
/*     */     }
/*     */   }
/*     */   
/*     */   public String getDestino() {
/* 191 */     return this.destino;
/*     */   }
/*     */   
/*     */   public void setDestino(String destino) {
/* 195 */     this.destino = destino;
/*     */   }
/*     */   
/*     */   public void restart()
/*     */   {
/* 200 */     setEstadoAsignacion(AssignmentStatus.Init);
/* 201 */     this.currentPosition = Integer.valueOf(0);
/* 202 */     this.codigoCaja = "";
/*     */   }
/*     */   
/*     */   private String formatCode(String prefijo, Integer numero)
/*     */   {
/* 207 */     String codigo = prefijo;
/*     */     
/* 209 */     String aux = numero.toString();
/* 210 */     if (aux.length() < 10) {
/* 211 */       for (int i = 0; i < 10 - numero.toString().length(); i++) {
/* 212 */         aux = "0" + aux;
/*     */       }
/*     */     }
/* 215 */     codigo = prefijo + aux;
/*     */     
/* 217 */     return codigo;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Picking/AssignBoxTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */