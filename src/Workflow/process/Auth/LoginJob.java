/*    */ package Workflow.process.Auth;
/*    */ 
/*    */ import Communication.IncomingDataEvent;
/*    */ import Workflow.Job;
/*    */ import Workflow.JobEvent;
/*    */ import Workflow.JobStatus;
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ import Workflow.process.Administration.AdministrationJob;
/*    */ import Workflow.process.GroupSelection.GroupSelectionJob;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LoginJob
/*    */   extends Job
/*    */ {
/* 26 */   private Task initTask = ShowLoginPanelTask.getInstance();
/* 27 */   private AuthenticateTask authTask = AuthenticateTask.getInstance();
/* 28 */   private Task next = null;
/* 29 */   private Task endTask = null;
/* 30 */   private Job nextJob = null;
/*    */   
/*    */   protected LoginJob() {
/* 33 */     setEstado(JobStatus.PROCESSING);
/* 34 */     this.authTask.addListener(this);
/*    */   }
/*    */   
/*    */ 
/* 38 */   private static LoginJob instance = null;
/*    */   
/*    */   public static LoginJob getInstance() {
/* 41 */     if (instance == null) {
/* 42 */       instance = new LoginJob();
/*    */     }
/*    */     
/* 45 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void init()
/*    */   {
/* 52 */     this.initTask.performAction();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onTaskFinished(TaskEvent event)
/*    */   {
/* 60 */     Task n = (Task)event.getSource();
/*    */     
/*    */ 
/* 63 */     if ((n.getId() == "AuthenticateTask") && 
/* 64 */       (n.getStatus().equals(TaskStatus.Finished)))
/*    */     {
/* 66 */       if (((AuthenticateTask)n).isAdmin()) {
/* 67 */         this.nextJob = AdministrationJob.getInstance();
/* 68 */         JobTerminado(new JobEvent(this, this));
/* 69 */       } else if (((AuthenticateTask)n).isUser()) {
/* 70 */         this.nextJob = GroupSelectionJob.getInstance();
/* 71 */         JobTerminado(new JobEvent(this, this));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void autenticar(String user, String password)
/*    */   {
/* 83 */     this.authTask.setUser(user);
/* 84 */     this.authTask.setPassword(password);
/* 85 */     this.authTask.performAction();
/*    */   }
/*    */   
/*    */   public Job getNextJob()
/*    */   {
/* 90 */     return this.nextJob;
/*    */   }
/*    */   
/*    */   public void incomingData(IncomingDataEvent e)
/*    */   {
/* 95 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Auth/LoginJob.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */