/*     */ package Workflow.process.Auth;
/*     */ 
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Rol;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.UserJpaController;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AuthenticateTask
/*     */   extends Task
/*     */ {
/*  25 */   private static AuthenticateTask instance = null;
/*     */   
/*  27 */   private String user = "";
/*  28 */   private String password = "";
/*  29 */   private boolean isuser = false;
/*  30 */   private boolean isadmin = false;
/*     */   
/*     */   public static AuthenticateTask getInstance() {
/*  33 */     if (instance == null) {
/*  34 */       instance = new AuthenticateTask();
/*  35 */       instance.setId("AuthenticateTask");
/*     */     }
/*     */     
/*  38 */     return instance;
/*     */   }
/*     */   
/*     */   protected AuthenticateTask() {
/*  42 */     this.autoRun = Boolean.valueOf(false);
/*  43 */     setStatus(TaskStatus.Waiting);
/*     */   }
/*     */   
/*     */   public String getUser() {
/*  47 */     return this.user;
/*     */   }
/*     */   
/*     */   public String getPassword() {
/*  51 */     return this.password;
/*     */   }
/*     */   
/*     */   public void setUser(String user) {
/*  55 */     this.user = user;
/*     */   }
/*     */   
/*     */   public void setPassword(String password) {
/*  59 */     this.password = password;
/*     */   }
/*     */   
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  65 */     this.isuser = false;
/*  66 */     this.isadmin = false;
/*  67 */     LoggingManager.getInstance().log("Intentando Ingresar");
/*  68 */     User usuario = UserJpaController.getInstance().login(this.user, this.password);
/*     */     
/*  70 */     if ((usuario != null) && (usuario.getRol().equals(Rol.OPERADOR))) {
/*  71 */       this.isuser = true;
/*  72 */       UiManager.getInstance().clearPanel();
/*  73 */       setStatus(TaskStatus.Finished);
/*     */       
/*     */ 
/*  76 */       ApplicationController.getInstance().putVariable("usuario", usuario);
/*     */       
/*     */ 
/*  79 */       LoggingManager.getInstance().log("Ingresando como operador: " + usuario.getUsername());
/*     */       
/*  81 */       TareaTerminada(new TaskEvent(this, this));
/*     */ 
/*     */ 
/*     */     }
/*  85 */     else if ((usuario != null) && (usuario.getRol().equals(Rol.ADMINISTRADOR))) {
/*  86 */       this.isadmin = true;
/*  87 */       UiManager.getInstance().clearPanel();
/*  88 */       setStatus(TaskStatus.Finished);
/*     */       
/*  90 */       ApplicationController.getInstance().putVariable("usuario", usuario);
/*  91 */       LoggingManager.getInstance().log("Ingresando como Administrador: " + usuario.getUsername());
/*  92 */       TareaTerminada(new TaskEvent(this, this));
/*     */     }
/*     */     else
/*     */     {
/*  96 */       this.isuser = false;
/*  97 */       this.isadmin = false;
/*  98 */       LoggingManager.getInstance().log("Intento de ingreso fallido: Usuario y/o Password Incorrecto");
/*  99 */       UiManager.getInstance().clearPanel();
/* 100 */       UiManager.getInstance().notificate("Usuario y/o Password Incorrecto");
/*     */     }
/*     */   }
/*     */   
/*     */   public Task getNextTask()
/*     */   {
/* 106 */     return null;
/*     */   }
/*     */   
/*     */   boolean isUser() {
/* 110 */     return this.isuser;
/*     */   }
/*     */   
/*     */   boolean isAdmin() {
/* 114 */     return this.isadmin;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 119 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Auth/AuthenticateTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */