/*    */ package Workflow.process.Auth;
/*    */ 
/*    */ import Logging.LoggingManager;
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import ui.Process.Auth.LoginPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowLoginPanelTask
/*    */   extends Task
/*    */ {
/*    */   LoginPanel loginform;
/* 22 */   private static ShowLoginPanelTask instance = null;
/*    */   
/*    */   public static ShowLoginPanelTask getInstance() {
/* 25 */     if (instance == null) {
/* 26 */       instance = new ShowLoginPanelTask();
/* 27 */       instance.setId("ShowLoginPanel");
/*    */     }
/*    */     
/* 30 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void performAction()
/*    */   {
/* 41 */     LoggingManager.getInstance().log("Iniciando Pantalla de Login");
/* 42 */     this.loginform = new LoginPanel();
/* 43 */     UiManager.getInstance().loadPanel(this.loginform);
/* 44 */     UiManager.getInstance().setFocusOnPanel();
/* 45 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public Task getNextTask()
/*    */   {
/* 52 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 57 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/Auth/ShowLoginPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */