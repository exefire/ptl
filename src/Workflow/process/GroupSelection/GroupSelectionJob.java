/*     */ package Workflow.process.GroupSelection;
/*     */ 
/*     */ import Communication.IncomingDataEvent;
/*     */ import Workflow.Job;
/*     */ import Workflow.JobEvent;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import Workflow.WorkflowManager;
/*     */ import Workflow.process.Picking.BoxesAssigmentJob;
/*     */ import Workflow.process.Picking.SortingJob;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GroupSelectionJob
/*     */   extends Job
/*     */ {
/*  23 */   private Task initTask = CheckStateTask.getInstance();
/*  24 */   private ShowSelectionPanelTask showSelectionPanel = ShowSelectionPanelTask.getInstance();
/*  25 */   private ShowInitMessagePanelTask showInitMessagePanel = ShowInitMessagePanelTask.getInstance();
/*  26 */   private ShowCloseWarningPanelTask showCloseWarningPanel = ShowCloseWarningPanelTask.getInstance();
/*  27 */   private PutWaveOnStandByTask putWaveOnStandByTask = PutWaveOnStandByTask.getInstance();
/*  28 */   private EndWaveTask endWaveTask = EndWaveTask.getInstance();
/*     */   
/*  30 */   private SelectGroupTask selectGroupTask = SelectGroupTask.getInstance();
/*  31 */   private LoadGroupTask loadGroupTask = LoadGroupTask.getInstance();
/*     */   
/*  33 */   private Task next = null;
/*  34 */   private Task endTask = null;
/*  35 */   private Job nextJob = null;
/*     */   
/*     */   private String currentGroup;
/*  38 */   private static GroupSelectionJob instance = null;
/*     */   
/*     */ 
/*     */   protected GroupSelectionJob()
/*     */   {
/*  43 */     addListener(WorkflowManager.getInstance());
/*  44 */     this.initTask.addListener(this);
/*  45 */     this.selectGroupTask.addListener(this);
/*  46 */     this.showSelectionPanel.addListener(this);
/*  47 */     this.showInitMessagePanel.addListener(this);
/*  48 */     this.showCloseWarningPanel.addListener(this);
/*  49 */     this.putWaveOnStandByTask.addListener(this);
/*  50 */     this.endWaveTask.addListener(this);
/*     */   }
/*     */   
/*     */   public static GroupSelectionJob getInstance() {
/*  54 */     if (instance == null) {
/*  55 */       instance = new GroupSelectionJob();
/*     */     }
/*     */     
/*  58 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public void init()
/*     */   {
/*  64 */     this.initTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public Job getNextJob()
/*     */   {
/*  70 */     return this.nextJob;
/*     */   }
/*     */   
/*     */ 
/*     */   public void onTaskFinished(TaskEvent event)
/*     */   {
/*  76 */     Task n = (Task)event.getSource();
/*     */     
/*  78 */     if (n.getId() == "SelectGroupTask") {
/*  79 */       if (n.getStatus().equals(TaskStatus.Finished)) {
/*  80 */         if (((SelectGroupTask)n).isHasOpenBoxes()) {
/*  81 */           this.nextJob = SortingJob.getInstance();
/*     */         }
/*     */         else {
/*  84 */           this.nextJob = BoxesAssigmentJob.getInstance();
/*  85 */           ((BoxesAssigmentJob)this.nextJob).setNumeroPicking(((SelectGroupTask)n).getNumeroGrupo());
/*     */         }
/*     */         
/*  88 */         JobTerminado(new JobEvent(this, this));
/*     */       }
/*     */       
/*     */     }
/*  92 */     else if (!n.getId().equals("PutWaveOnStandByTask"))
/*     */     {
/*     */ 
/*     */ 
/*  96 */       if (n.getId().equals("EndWaveTask"))
/*     */       {
/*  98 */         this.showCloseWarningPanel.performAction();
/*     */       }
/* 100 */       else if (n.getId() == "CheckStateTask") {
/* 101 */         if (((CheckStateTask)n).isPickingProcesando())
/*     */         {
/*     */ 
/*     */ 
/* 105 */           this.showInitMessagePanel.performAction();
/*     */         }
/* 107 */         else if (((CheckStateTask)n).isPickingIncompleto())
/*     */         {
/* 109 */           this.showSelectionPanel.performAction();
/*     */         }
/*     */         else
/*     */         {
/* 113 */           this.showSelectionPanel.performAction();
/*     */         }
/*     */       }
/* 116 */       else if (!n.getId().equals("LoadGroupTask")) {}
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void groupSelected(String id)
/*     */   {
/* 127 */     this.currentGroup = id;
/* 128 */     this.selectGroupTask.setNumeroGrupo(id);
/* 129 */     this.selectGroupTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public void loadGroup(String id)
/*     */   {
/* 135 */     this.loadGroupTask.setNumeroGrupo(id);
/* 136 */     this.loadGroupTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void incomingData(IncomingDataEvent e) {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void continueLastPicking()
/*     */   {
/* 147 */     this.nextJob = SortingJob.getInstance();
/* 148 */     JobTerminado(new JobEvent(this, this));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void endLastPicking()
/*     */   {
/* 156 */     this.endWaveTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void ContinuarDistribucion()
/*     */   {
/* 163 */     this.initTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void FinalizaOlaPorCambioDeBulto()
/*     */   {
/* 170 */     this.putWaveOnStandByTask.performAction();
/*     */   }
/*     */   
/*     */ 
/*     */   public void restartSelection()
/*     */   {
/* 176 */     this.initTask.performAction();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/GroupSelectionJob.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */