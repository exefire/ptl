/*     */ package Workflow.process.GroupSelection;
/*     */ 
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.InformacionBulto;
/*     */ import Models.Entities.local.InformacionPicking;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CheckStateTask
/*     */   extends Task
/*     */ {
/*  27 */   private static CheckStateTask instance = null;
/*     */   private boolean pickingIncompleto;
/*     */   private boolean pickingProcesando;
/*     */   
/*     */   protected CheckStateTask() {
/*  32 */     setStatus(TaskStatus.Waiting);
/*  33 */     this.pickingIncompleto = false;
/*  34 */     this.pickingProcesando = false;
/*     */   }
/*     */   
/*     */   public static CheckStateTask getInstance()
/*     */   {
/*  39 */     if (instance == null) {
/*  40 */       instance = new CheckStateTask();
/*  41 */       instance.setId("CheckStateTask");
/*     */     }
/*     */     
/*  44 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public void dataArrived()
/*     */   {
/*  50 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void performAction()
/*     */   {
/*  55 */     setStatus(TaskStatus.Waiting);
/*  56 */     this.pickingIncompleto = false;
/*  57 */     this.pickingProcesando = false;
/*     */     
/*     */     try
/*     */     {
/*  61 */       LoggingManager.getInstance().log("Iniciando Chequeo de integridad de datos");
/*  62 */       Picking p = PickingJpaController.getInstance().findByEstadoProcesando();
/*     */       
/*  64 */       if (p != null) {
/*  65 */         LoggingManager.getInstance().log("Existe OLA procesando: " + p.getIdreferencia());
/*  66 */         ApplicationController.getInstance().putVariable("picking", p.getIdreferencia());
/*  67 */         LoggingManager.getInstance().log("Bulto en proceso: " + p.getBultoEnProceso());
/*  68 */         ApplicationController.getInstance().putVariable("codigoBulto", p.getBultoEnProceso());
/*     */         
/*     */ 
/*  71 */         LoggingManager.getInstance().log("Recuperando Información de Bulto en proceso");
/*     */         
/*     */ 
/*  74 */         InformacionBulto infoB = new InformacionBulto();
/*  75 */         infoB.setCodigo("codigo bult");
/*  76 */         infoB.setDistribuidos(Integer.valueOf(0));
/*  77 */         infoB.setTotal(Integer.valueOf(0));
/*  78 */         infoB.setPendientes(Integer.valueOf(0));
/*     */         
/*     */ 
/*     */ 
/*  82 */         infoB.setPicking(p.getId());
/*     */         
/*  84 */         LoggingManager.getInstance().log("Recuperando Información de Ola en proceso");
/*     */         
/*     */ 
/*     */ 
/*  88 */         InformacionPicking infoP = new InformacionPicking();
/*     */         
/*  90 */         infoP.setPendiente(Integer.valueOf(0));
/*  91 */         infoP.setTotal(Integer.valueOf(0));
/*  92 */         infoP.setDistribuido(Integer.valueOf(0));
/*     */         
/*     */ 
/*  95 */         ApplicationController.getInstance().putVariable("nbultos", Integer.valueOf(-1));
/*     */         
/*  97 */         ApplicationController.getInstance().putVariable("infoBulto", infoB);
/*  98 */         ApplicationController.getInstance().putVariable("infoPicking", infoP);
/*     */         
/*     */ 
/* 101 */         setPickingIncompleto(false);
/* 102 */         setPickingProcesando(true);
/*     */       } else {
/* 104 */         LoggingManager.getInstance().log("Buscando OLA incompleta");
/* 105 */         p = PickingJpaController.getInstance().findByEstadoIncompleto();
/*     */         
/* 107 */         if (p != null) {
/* 108 */           LoggingManager.getInstance().log("Ola incompleta encontrada: " + p.getIdreferencia());
/* 109 */           setPickingIncompleto(true);
/* 110 */           setPickingProcesando(false);
/* 111 */           ApplicationController.getInstance().putVariable("picking", p.getIdreferencia());
/* 112 */           ApplicationController.getInstance().putVariable("codigoBulto", p.getBultoEnProceso());
/* 113 */           LoggingManager.getInstance().log("ultimo Bulto en proceso: " + p.getBultoEnProceso());
/*     */         }
/*     */         else {
/* 116 */           setPickingIncompleto(false);
/* 117 */           setPickingProcesando(false);
/* 118 */           ApplicationController.getInstance().putVariable("picking", null);
/* 119 */           LoggingManager.getInstance().log("Información de cierre no encontrada, Realizando un inicio Limpio");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 124 */       setStatus(TaskStatus.Finished);
/* 125 */       TareaTerminada(new TaskEvent(this, this));
/*     */     } catch (Exception ex) {
/* 127 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean isPickingProcesando()
/*     */   {
/* 133 */     return this.pickingProcesando;
/*     */   }
/*     */   
/*     */   public void setPickingProcesando(boolean pickingProcesando) {
/* 137 */     this.pickingProcesando = pickingProcesando;
/*     */   }
/*     */   
/*     */   public boolean isPickingIncompleto() {
/* 141 */     return this.pickingIncompleto;
/*     */   }
/*     */   
/*     */   public void setPickingIncompleto(boolean pickingIncompleto) {
/* 145 */     this.pickingIncompleto = pickingIncompleto;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/CheckStateTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */