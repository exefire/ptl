/*    */ package Workflow.process.GroupSelection;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ import ui.Process.GroupSelection.InitMessagePanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowInitMessagePanelTask
/*    */   extends Task
/*    */ {
/*    */   InitMessagePanel panel;
/* 21 */   private static ShowInitMessagePanelTask instance = null;
/*    */   
/*    */   protected ShowInitMessagePanelTask() {
/* 24 */     setStatus(TaskStatus.Waiting);
/* 25 */     this.panel = new InitMessagePanel();
/*    */   }
/*    */   
/*    */   public static ShowInitMessagePanelTask getInstance() {
/* 29 */     if (instance == null) {
/* 30 */       instance = new ShowInitMessagePanelTask();
/* 31 */       instance.setId("ShowInitMessagePanelTask");
/*    */     }
/*    */     
/* 34 */     return instance;
/*    */   }
/*    */   
/*    */   public void performAction()
/*    */   {
/* 39 */     this.panel.clearData();
/* 40 */     UiManager.getInstance().loadPanel(this.panel);
/* 41 */     UiManager.getInstance().notificate("Hay proceso que está pendiente ¿Que desea hacer?");
/* 42 */     setStatus(TaskStatus.Finished);
/* 43 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */   public Task getNextTask()
/*    */   {
/* 48 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 53 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/ShowInitMessagePanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */