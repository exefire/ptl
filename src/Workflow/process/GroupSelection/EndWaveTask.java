/*     */ package Workflow.process.GroupSelection;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.CajaJpaController;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import Storage.Files.FileManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EndWaveTask
/*     */   extends Task
/*     */ {
/*  41 */   private static EndWaveTask instance = null;
/*     */   
/*     */   protected EndWaveTask() {
/*  44 */     setStatus(TaskStatus.Waiting);
/*     */   }
/*     */   
/*     */   public static EndWaveTask getInstance() {
/*  48 */     if (instance == null) {
/*  49 */       instance = new EndWaveTask();
/*  50 */       instance.setId("EndWaveTask");
/*     */     }
/*  52 */     return instance;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/*  57 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*     */     try
/*     */     {
/*  66 */       String ola = (String)ApplicationController.getInstance().getVariable("picking");
/*     */       
/*  68 */       Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/*  69 */       p.setEstado("standby");
/*     */       try {
/*  71 */         PickingJpaController.getInstance().edit(p);
/*     */         
/*  73 */         List<Caja> cajas = CajaJpaController.getInstance().findCajasAbiertasByPicking(p.getId());
/*     */         
/*  75 */         for (localIterator1 = cajas.iterator(); localIterator1.hasNext();) { c = (Caja)localIterator1.next();
/*     */           
/*  77 */           c.setEstado("cerrada");
/*  78 */           c.setFechaCierre(new Date());
/*  79 */           CajaJpaController.getInstance().edit(c);
/*     */           
/*  81 */           FileManager.getInstance().newCsvFile(c.getCodigo(), c.getCodigo());
/*  82 */           List<Object> documentos = DetalleCajaJpaController.getInstance().findAllDocumentos(c);
/*  83 */           if (documentos != null)
/*     */           {
/*  85 */             for (Object o : documentos)
/*     */             {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  91 */               String referencia = (String)((Object[])(Object[])o)[1];
/*     */               
/*  93 */               List<DetalleCaja> listaDetalle2 = DetalleCajaJpaController.getInstance().findAllByCajaAndReferencia(c, referencia);
/*     */               
/*     */ 
/*  96 */               if (listaDetalle2 != null) {
/*  97 */                 for (DetalleCaja detCaja : listaDetalle2) {
/*  98 */                   String code = c.getCodigo();
/*  99 */                   String str_usuario = ((User)ApplicationController.getInstance().getVariable("usuario")).getUsername();
/*     */                   
/* 101 */                   DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
/* 102 */                   String fechatiempo = df.format(detCaja.getCreated());
/*     */                   
/*     */ 
/* 105 */                   FileManager.getInstance().putLineOnFile(c.getCodigo(), ola + ";" + code + ";" + detCaja.getCodigoBarra() + ";" + detCaja.getCantidad() + ";" + c.getDestinoId().getNombre() + ";" + str_usuario + ";" + fechatiempo);
/*     */                 }
/*     */                 
/* 108 */                 FileManager.getInstance().closeFile(c.getCodigo());
/* 109 */                 LoggingManager.getInstance().log("Cerrando Archivo de cierre de Caja: " + c.getCodigo());
/*     */               }
/*     */             } }
/*     */         }
/*     */       } catch (NonexistentEntityException ex) {
/*     */         Iterator localIterator1;
/*     */         Caja c;
/* 116 */         Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 117 */         LoggingManager.getInstance().log_error(ex);
/*     */       } catch (Exception ex) {
/* 119 */         Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 120 */         LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 129 */       CommunicationController.getInstance().apagarTodasLasLuces();
/* 130 */       setStatus(TaskStatus.Finished);
/* 131 */       TareaTerminada(new TaskEvent(this, this));
/*     */ 
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/*     */ 
/* 137 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/EndWaveTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */