/*     */ package Workflow.process.GroupSelection;
/*     */ 
/*     */ import Configuration.ConfigurationManager;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.InformacionBulto;
/*     */ import Models.Entities.local.InformacionPicking;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.DestinoJpaController;
/*     */ import Models.JpaControllers.local.DetallePickingJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.math.BigDecimal;
/*     */ import java.math.BigInteger;
/*     */ import java.util.Collection;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SelectGroupTask
/*     */   extends Task
/*     */ {
/*  38 */   private static SelectGroupTask instance = null;
/*     */   
/*     */   private String numeroGrupo;
/*     */   private boolean hasOpenBoxes;
/*     */   
/*     */   protected SelectGroupTask()
/*     */   {
/*  45 */     this.hasOpenBoxes = false;
/*     */   }
/*     */   
/*     */   public static SelectGroupTask getInstance() {
/*  49 */     if (instance == null) {
/*  50 */       instance = new SelectGroupTask();
/*  51 */       instance.setId("SelectGroupTask");
/*     */     }
/*     */     
/*  54 */     return instance;
/*     */   }
/*     */   
/*     */   public void performAction()
/*     */   {
/*     */     try
/*     */     {
/*  61 */       String ola = this.numeroGrupo;
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  70 */       Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/*     */       InformacionBulto infoB;
/*  72 */       InformacionPicking infoP; if (p == null)
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  94 */         InformacionBulto infoB = new InformacionBulto();
/*  95 */         infoB.setCodigo("codigo bulto");
/*  96 */         infoB.setDistribuidos(Integer.valueOf(0));
/*  97 */         infoB.setTotal(Integer.valueOf(0));
/*  98 */         infoB.setPendientes(Integer.valueOf(0));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 105 */         List<Object> det = DetallePickingJpaController.getInstance().getTotalesByPicking(ola);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 111 */         Object[] f = (Object[])det.get(0);
/* 112 */         Integer cantidad = Integer.valueOf(((BigInteger)((Object[])f)[1]).intValue());
/* 113 */         Integer consolidado = Integer.valueOf(((BigInteger)((Object[])f)[2]).intValue());
/*     */         
/* 115 */         InformacionPicking infoP = new InformacionPicking();
/*     */         
/* 117 */         infoP.setPendiente(Integer.valueOf(cantidad.intValue() - consolidado.intValue()));
/* 118 */         infoP.setTotal(cantidad);
/* 119 */         infoP.setDistribuido(consolidado);
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/* 129 */         if (p.getEstado().equals("incompleto")) {
/* 130 */           this.hasOpenBoxes = true;
/* 131 */         } else if (p.getEstado().equals("standby")) {
/* 132 */           this.hasOpenBoxes = false;
/*     */         }
/*     */         
/* 135 */         p.setEstado("procesando");
/* 136 */         p.setBultoEnProceso(this.numeroGrupo);
/*     */         try {
/* 138 */           PickingJpaController.getInstance().edit(p);
/*     */         } catch (NonexistentEntityException ex) {
/* 140 */           Logger.getLogger(SelectGroupTask.class.getName()).log(Level.SEVERE, null, ex);
/*     */         } catch (Exception ex) {
/* 142 */           Logger.getLogger(SelectGroupTask.class.getName()).log(Level.SEVERE, null, ex);
/*     */         }
/*     */         
/*     */ 
/* 146 */         Integer ndestinos = DestinoJpaController.getInstance().countDestinosPickingByOla(p);
/* 147 */         Integer luces = Integer.valueOf(Integer.parseInt(ConfigurationManager.getInstance().getProperty("luces")));
/* 148 */         Integer nolas = Integer.valueOf(1);
/* 149 */         if (ndestinos.intValue() <= luces.intValue()) {
/* 150 */           nolas = Integer.valueOf(1);
/*     */         }
/* 152 */         else if (ndestinos.intValue() / luces.intValue() > Math.floor(ndestinos.intValue() / luces.intValue())) {
/* 153 */           nolas = Integer.valueOf(ndestinos.intValue() / luces.intValue() + 1);
/*     */         } else {
/* 155 */           nolas = Integer.valueOf(ndestinos.intValue() / luces.intValue());
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/* 161 */         ApplicationController.getInstance().putVariable("nolas", nolas);
/* 162 */         ApplicationController.getInstance().putVariable("olaactiva", Integer.valueOf(1));
/*     */         
/* 164 */         Collection<Destino> destinos = DestinoJpaController.getInstance().getDestinosPickingByOla(p);
/*     */         
/* 166 */         ApplicationController.getInstance().putVariable("destinos", destinos);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 185 */         infoB = new InformacionBulto();
/* 186 */         infoB.setCodigo("cod bulto");
/* 187 */         infoB.setDistribuidos(Integer.valueOf(0));
/* 188 */         infoB.setTotal(Integer.valueOf(0));
/* 189 */         infoB.setPendientes(Integer.valueOf(0));
/*     */         
/*     */ 
/*     */ 
/* 193 */         infoB.setPicking(p.getId());
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 199 */         List<Object> det = DetallePickingJpaController.getInstance().getTotalesByPicking(ola);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 205 */         Object[] f = (Object[])det.get(0);
/* 206 */         Integer cantidad = Integer.valueOf(((BigDecimal)((Object[])f)[1]).intValue());
/* 207 */         Integer consolidado = Integer.valueOf(((BigDecimal)((Object[])f)[2]).intValue());
/*     */         
/* 209 */         infoP = new InformacionPicking();
/*     */         
/* 211 */         infoP.setPendiente(Integer.valueOf(cantidad.intValue() - consolidado.intValue()));
/* 212 */         infoP.setTotal(cantidad);
/* 213 */         infoP.setDistribuido(consolidado);
/*     */         
/*     */ 
/* 216 */         ApplicationController.getInstance().putVariable("nbultos", Integer.valueOf(-1));
/* 217 */         ApplicationController.getInstance().putVariable("codigoBulto", p.getBultoEnProceso());
/*     */       }
/*     */       
/*     */ 
/* 221 */       ApplicationController.getInstance().putVariable("picking", ola);
/* 222 */       ApplicationController.getInstance().putVariable("infoBulto", infoB);
/* 223 */       ApplicationController.getInstance().putVariable("infoPicking", infoP);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 230 */       ApplicationController.getInstance().putVariable("ola", ola);
/*     */       
/* 232 */       UiManager.getInstance().clearPanel();
/* 233 */       setStatus(TaskStatus.Finished);
/* 234 */       TareaTerminada(new TaskEvent(this, this));
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 238 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/* 246 */     return null;
/*     */   }
/*     */   
/*     */   public String getNumeroGrupo() {
/* 250 */     return this.numeroGrupo;
/*     */   }
/*     */   
/*     */   public void setNumeroGrupo(String numeroGrupo) {
/* 254 */     this.numeroGrupo = numeroGrupo;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 259 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public boolean isHasOpenBoxes() {
/* 263 */     return this.hasOpenBoxes;
/*     */   }
/*     */   
/*     */   public void setHasOpenBoxes(boolean hasOpenBoxes) {
/* 267 */     this.hasOpenBoxes = hasOpenBoxes;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/SelectGroupTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */