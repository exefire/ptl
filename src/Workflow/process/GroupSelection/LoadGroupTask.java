/*     */ package Workflow.process.GroupSelection;
/*     */ 
/*     */ import Configuration.ConfigurationManager;
/*     */ import Core.ApplicationController;
/*     */ import ETL.pentaho.pdi.EtlController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.DetallePicking;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.DestinoJpaController;
/*     */ import Models.JpaControllers.local.DetallePickingJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.io.File;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import ui.Process.GroupSelection.NotTheSameOlaPanel;
/*     */ import ui.UiManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LoadGroupTask
/*     */   extends Task
/*     */ {
/*  34 */   private static LoadGroupTask instance = null;
/*     */   private String numeroGrupo;
/*     */   private NotTheSameOlaPanel notTheSamePanel;
/*     */   
/*     */   protected LoadGroupTask() {
/*  39 */     this.notTheSamePanel = new NotTheSameOlaPanel();
/*  40 */     setStatus(TaskStatus.Waiting);
/*     */   }
/*     */   
/*     */   public static LoadGroupTask getInstance() {
/*  44 */     if (instance == null) {
/*  45 */       instance = new LoadGroupTask();
/*  46 */       instance.setId("LoadGroupTask");
/*     */     }
/*     */     
/*  49 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*     */     try
/*     */     {
/*  61 */       LoggingManager.getInstance().log("Se busca picking: " + this.numeroGrupo);
/*  62 */       String ola = this.numeroGrupo;
/*     */       
/*  64 */       if (ola == null) {
/*  65 */         LoggingManager.getInstance().log("No se encontró el picking seleccionado");
/*  66 */         UiManager.getInstance().notificate("No se encontró el bupicking seleccionado");
/*  67 */         return;
/*     */       }
/*     */       
/*  70 */       Picking picking = PickingJpaController.getInstance().findByIdReferencia(ola);
/*     */       
/*     */ 
/*     */ 
/*  74 */       String ola_proceso = (String)ApplicationController.getInstance().getVariable("picking");
/*     */       
/*     */ 
/*     */ 
/*  78 */       String path_archivo = ConfigurationManager.getInstance().getProperty("rutaArchivo") + "/" + ola + ".xlsx";
/*     */       
/*  80 */       File f = new File(path_archivo);
/*  81 */       if ((!f.exists()) || (f.isDirectory()))
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*  86 */         if (picking == null)
/*     */         {
/*  88 */           LoggingManager.getInstance().log("No se encontró el Arvhivo " + path_archivo);
/*  89 */           UiManager.getInstance().notificate("No se encontró el archivo de la nota: " + ola + " (" + path_archivo + ")");
/*     */           
/*  91 */           return;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  96 */       if (picking == null)
/*     */       {
/*  98 */         picking = new Picking();
/*  99 */         picking.setBultoEnProceso(ola);
/* 100 */         picking.setIdreferencia(ola);
/* 101 */         picking.setEstado("standby");
/*     */         
/* 103 */         PickingJpaController.getInstance().create(picking);
/*     */         
/*     */ 
/* 106 */         EtlController.getInstance().cargaDestinosNuevos(ola);
/* 107 */         EtlController.getInstance().cargaOla(ola);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 112 */       ApplicationController.getInstance().putVariable("picking", ola);
/* 113 */       ola_proceso = ola;
/* 114 */       Integer ndestinos = DestinoJpaController.getInstance().countDestinosPickingByOla(picking);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 127 */       if ((ola_proceso != null) && (!ola_proceso.equals(ola)))
/*     */       {
/* 129 */         LoggingManager.getInstance().log("El bulto seleccionado pertenece a otra ola: " + ola);
/*     */         
/* 131 */         UiManager.getInstance().loadPanel(this.notTheSamePanel);
/* 132 */         UiManager.getInstance().notificate("Este Bulto Pertenece a la OLA: " + ola);
/* 133 */         setStatus(TaskStatus.Finished);
/* 134 */         TareaTerminada(new TaskEvent(this, this));
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/* 142 */       else if (ola != null)
/*     */       {
/*     */ 
/* 145 */         LoggingManager.getInstance().log("El bulto seleccionado esta asociado a la ola: " + ola);
/*     */         
/* 147 */         Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/* 148 */         Collection<DetallePicking> lista = DetallePickingJpaController.getInstance().getDetallePickingCollection(p);
/*     */         
/*     */ 
/*     */ 
/* 152 */         HashMap atts = new HashMap();
/*     */         
/* 154 */         ApplicationController.getInstance().putVariable("picking", ola);
/*     */         
/*     */ 
/*     */ 
/* 158 */         LoggingManager.getInstance().log("Desplegando información de bulto en pantalla");
/* 159 */         Integer luces = Integer.valueOf(Integer.parseInt(ConfigurationManager.getInstance().getProperty("luces")));
/*     */         
/* 161 */         Integer nolas = Integer.valueOf(ndestinos.intValue() / luces.intValue());
/*     */         
/*     */         Integer localInteger1;
/* 164 */         if (ndestinos.intValue() % luces.intValue() > 0) { localInteger1 = nolas;Integer localInteger2 = nolas = Integer.valueOf(nolas.intValue() + 1);
/*     */         }
/* 166 */         atts.put("DetallePicking", lista);
/* 167 */         atts.put("ndestinos", ndestinos);
/* 168 */         atts.put("ola", nolas);
/* 169 */         atts.put("picking", this.numeroGrupo);
/* 170 */         UiManager.getInstance().updateUiData("loadPicking", atts);
/*     */       } else {
/* 172 */         LoggingManager.getInstance().log("No se encontró el bulto seleccionado");
/* 173 */         UiManager.getInstance().notificate("No se encontró el bulto seleccionado");
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/*     */ 
/*     */ 
/* 184 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public Task getNextTask()
/*     */   {
/* 191 */     return null;
/*     */   }
/*     */   
/*     */   public String getNumeroGrupo() {
/* 195 */     return this.numeroGrupo;
/*     */   }
/*     */   
/*     */   public void setNumeroGrupo(String numeroGrupo) {
/* 199 */     this.numeroGrupo = numeroGrupo;
/*     */   }
/*     */   
/*     */   public void dataArrived()
/*     */   {
/* 204 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/LoadGroupTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */