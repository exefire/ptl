/*    */ package Workflow.process.GroupSelection;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ import ui.Process.GroupSelection.CloseAllBoxesWarningPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ class ShowCloseWarningPanelTask
/*    */   extends Task
/*    */ {
/*    */   CloseAllBoxesWarningPanel panel;
/* 21 */   private static ShowCloseWarningPanelTask instance = null;
/*    */   
/*    */   protected ShowCloseWarningPanelTask() {
/* 24 */     setStatus(TaskStatus.Waiting);
/* 25 */     this.panel = new CloseAllBoxesWarningPanel();
/*    */   }
/*    */   
/*    */   public static ShowCloseWarningPanelTask getInstance() {
/* 29 */     if (instance == null) {
/* 30 */       instance = new ShowCloseWarningPanelTask();
/* 31 */       instance.setId("ShowCloseWarningPanelTask");
/*    */     }
/*    */     
/* 34 */     return instance;
/*    */   }
/*    */   
/*    */   public void performAction()
/*    */   {
/* 39 */     this.panel.clearData();
/* 40 */     this.panel.notificate("Debe cerrar todas las Cajas para Continuar");
/* 41 */     UiManager.getInstance().loadPanel(this.panel);
/* 42 */     setStatus(TaskStatus.Finished);
/* 43 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */   public Task getNextTask()
/*    */   {
/* 48 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 53 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/ShowCloseWarningPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */