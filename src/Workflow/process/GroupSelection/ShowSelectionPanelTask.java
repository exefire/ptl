/*    */ package Workflow.process.GroupSelection;
/*    */ 
/*    */ import Workflow.Task;
/*    */ import Workflow.TaskEvent;
/*    */ import Workflow.TaskStatus;
/*    */ import ui.Process.GroupSelection.SelectionPanel;
/*    */ import ui.UiManager;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ class ShowSelectionPanelTask
/*    */   extends Task
/*    */ {
/*    */   SelectionPanel panel;
/* 20 */   private static ShowSelectionPanelTask instance = null;
/*    */   
/*    */   protected ShowSelectionPanelTask() {
/* 23 */     setStatus(TaskStatus.Waiting);
/* 24 */     this.panel = new SelectionPanel();
/*    */   }
/*    */   
/*    */   public static ShowSelectionPanelTask getInstance() {
/* 28 */     if (instance == null) {
/* 29 */       instance = new ShowSelectionPanelTask();
/* 30 */       instance.setId("ShowSelectionPanelTask");
/*    */     }
/*    */     
/* 33 */     return instance;
/*    */   }
/*    */   
/*    */   public void performAction()
/*    */   {
/* 38 */     this.panel.clearData();
/* 39 */     UiManager.getInstance().loadPanel(this.panel);
/* 40 */     UiManager.getInstance().setFocusOnPanel();
/* 41 */     setStatus(TaskStatus.Finished);
/* 42 */     TareaTerminada(new TaskEvent(this, this));
/*    */   }
/*    */   
/*    */   public Task getNextTask()
/*    */   {
/* 47 */     return null;
/*    */   }
/*    */   
/*    */   public void dataArrived()
/*    */   {
/* 52 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/ShowSelectionPanelTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */