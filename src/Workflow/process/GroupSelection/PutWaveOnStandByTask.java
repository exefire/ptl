/*     */ package Workflow.process.GroupSelection;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import Core.ApplicationController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.DetalleCajaJpaController;
/*     */ import Models.JpaControllers.local.PickingJpaController;
/*     */ import Storage.Files.FileManager;
/*     */ import Webservices.WebServiceManager;
/*     */ import Workflow.Task;
/*     */ import Workflow.TaskEvent;
/*     */ import Workflow.TaskStatus;
/*     */ import java.math.BigDecimal;
/*     */ import java.math.BigInteger;
/*     */ import java.util.List;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PutWaveOnStandByTask
/*     */   extends Task
/*     */ {
/*  37 */   private static PutWaveOnStandByTask instance = null;
/*     */   
/*     */   protected PutWaveOnStandByTask() {
/*  40 */     setStatus(TaskStatus.Waiting);
/*     */   }
/*     */   
/*     */ 
/*     */   public static PutWaveOnStandByTask getInstance()
/*     */   {
/*  46 */     if (instance == null) {
/*  47 */       instance = new PutWaveOnStandByTask();
/*  48 */       instance.setId("PutWaveOnStandByTask");
/*     */     }
/*     */     
/*  51 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public void dataArrived()
/*     */   {
/*  57 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void performAction()
/*     */   {
/*  64 */     String ola = (String)ApplicationController.getInstance().getVariable("picking");
/*     */     
/*  66 */     Picking p = PickingJpaController.getInstance().findByIdReferencia(ola);
/*     */     
/*  68 */     WebServiceManager.getInstance().prepareSend();
/*     */     
/*     */     try
/*     */     {
/*  72 */       List<Object> listaDetalle2 = DetalleCajaJpaController.getInstance().findDetallePickingFinal(p.getIdreferencia());
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*  77 */       FileManager.getInstance().newCsvFile(p.getOc(), p.getOc());
/*     */       
/*  79 */       if (listaDetalle2 != null)
/*     */       {
/*  81 */         FileManager.getInstance().putLineOnFile(p.getOc(), "Nota de Venta;Orden de Compra;Bulto;Destino;articulo;Codigo Barra;Cantidad");
/*     */         
/*  83 */         for (Object fila : listaDetalle2)
/*     */         {
/*     */ 
/*  86 */           String str1 = (String)((Object[])(Object[])fila)[0];
/*  87 */           String str2 = (String)((Object[])(Object[])fila)[1];
/*  88 */           String dest = (String)((Object[])(Object[])fila)[2];
/*  89 */           String str3 = (String)((Object[])(Object[])fila)[3];
/*  90 */           BigDecimal str4 = (BigDecimal)((Object[])(Object[])fila)[4];
/*  91 */           String codbarra = (String)((Object[])(Object[])fila)[5];
/*  92 */           String notaventa = (String)((Object[])(Object[])fila)[6];
/*     */           
/*  94 */           if (str1 == null) {
/*  95 */             str1 = "NN";
/*     */           }
/*  97 */           if (dest == null) {
/*  98 */             dest = "NN";
/*     */           }
/* 100 */           if (str2 == null) {
/* 101 */             str2 = "NN";
/*     */           }
/* 103 */           if (str3 == null) {
/* 104 */             str3 = "NN";
/*     */           }
/* 106 */           if (codbarra == null) {
/* 107 */             codbarra = "NN";
/*     */           }
/* 109 */           if (notaventa == null) {
/* 110 */             notaventa = "NN";
/*     */           }
/*     */           
/*     */ 
/* 114 */           FileManager.getInstance().putLineOnFile(p.getOc(), notaventa.trim() + ";" + str1.trim() + ";" + str2.trim() + ";" + dest.trim() + ";" + str3.trim() + ";" + codbarra + ";" + str4.toBigInteger().toString());
/*     */           
/* 116 */           WebServiceManager.getInstance().addDetail(str2, codbarra, Double.valueOf(str4.doubleValue()), "");
/*     */         }
/*     */         
/*     */ 
/* 120 */         FileManager.getInstance().closeFile(p.getOc());
/* 121 */         LoggingManager.getInstance().log("Cerrando Archivo de picking de Nota de Venta: " + p.getIdreferencia());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 129 */       CommunicationController.getInstance().apagarTodasLasLuces();
/*     */     }
/*     */     catch (Exception ex) {
/* 132 */       Logger.getLogger(PutWaveOnStandByTask.class.getName()).log(Level.SEVERE, null, ex);
/* 133 */       LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */     
/* 136 */     WebServiceManager.getInstance().send(ola);
/*     */     
/* 138 */     setStatus(TaskStatus.Finished);
/* 139 */     TareaTerminada(new TaskEvent(this, this));
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/process/GroupSelection/PutWaveOnStandByTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */