/*    */ package Workflow;
/*    */ 
/*    */ import Communication.IncomingDataEvent;
/*    */ import java.util.ArrayList;
/*    */ import java.util.LinkedHashMap;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public abstract class Job
/*    */   implements TaskListener
/*    */ {
/*    */   LinkedHashMap<String, Task> tareas;
/* 21 */   private List<JobListener> listeners = new ArrayList();
/*    */   private JobStatus estado;
/*    */   private Task currentTask;
/*    */   
/*    */   public JobStatus getEstado() {
/* 26 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(JobStatus estado) {
/* 30 */     this.estado = estado;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void executeTask(String id)
/*    */   {
/* 40 */     Task t = (Task)this.tareas.get(id);
/* 41 */     if (t != null) {
/* 42 */       t.performAction();
/*    */     }
/*    */   }
/*    */   
/*    */   public void addTask(Task t) {
/* 47 */     if (t != null) {
/* 48 */       this.tareas.put(t.getId(), t);
/*    */     }
/*    */   }
/*    */   
/*    */   public abstract void init();
/*    */   
/*    */   public void addListener(JobListener listener)
/*    */   {
/* 56 */     this.listeners.add(listener);
/*    */   }
/*    */   
/*    */   public void removeListener(JobListener listener) {
/* 60 */     this.listeners.remove(listener);
/*    */   }
/*    */   
/*    */ 
/*    */   public void JobTerminado(JobEvent evento)
/*    */   {
/* 66 */     for (JobListener listener : this.listeners) {
/* 67 */       listener.onJobFinished(evento);
/*    */     }
/*    */   }
/*    */   
/*    */   public Task getCurrentTask() {
/* 72 */     return this.currentTask;
/*    */   }
/*    */   
/*    */   public void setCurrentTask(Task currentTask) {
/* 76 */     this.currentTask = currentTask;
/*    */   }
/*    */   
/*    */   public abstract Job getNextJob();
/*    */   
/*    */   public abstract void incomingData(IncomingDataEvent paramIncomingDataEvent);
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/Job.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */