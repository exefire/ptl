package Workflow;

public enum TaskStatus
{
  Idle,  Waiting,  Finished;
  
  private TaskStatus() {}
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/TaskStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */