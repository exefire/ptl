package Workflow;

import java.util.EventListener;

public abstract interface TaskListener
  extends EventListener
{
  public abstract void onTaskFinished(TaskEvent paramTaskEvent);
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/TaskListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */