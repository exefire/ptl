/*    */ package Workflow;
/*    */ 
/*    */ import java.util.EventObject;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class JobEvent
/*    */   extends EventObject
/*    */ {
/*    */   protected Job job;
/*    */   
/*    */   public JobEvent(Object source, Job j)
/*    */   {
/* 18 */     super(source);
/* 19 */     this.job = j;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Workflow/JobEvent.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */