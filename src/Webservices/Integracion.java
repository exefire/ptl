/*    */ package Webservices;
/*    */ 
/*    */ import java.net.MalformedURLException;
/*    */ import java.net.URL;
/*    */ import javax.xml.namespace.QName;
/*    */ import javax.xml.ws.Service;
/*    */ import javax.xml.ws.WebEndpoint;
/*    */ import javax.xml.ws.WebServiceClient;
/*    */ import javax.xml.ws.WebServiceException;
/*    */ import javax.xml.ws.WebServiceFeature;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @WebServiceClient(name="Integracion", targetNamespace="http://tempuri.org/", //wsdlLocation="http://justime.sportex.cl/WsPTL/Integracion.svc?singleWsdl")
/*    */ public class Integracion
/*    */   extends Service
/*    */ {
/*    */   private static final URL INTEGRACION_WSDL_LOCATION;
/*    */   private static final WebServiceException INTEGRACION_EXCEPTION;
/* 27 */   private static final QName INTEGRACION_QNAME = new QName("http://tempuri.org/", "Integracion");
/*    */   
/*    */   static {
/* 30 */     URL url = null;
/* 31 */     WebServiceException e = null;
/*    */     try {
/* 33 */      // url = new URL("http://justime.sportex.cl/WsPTL/Integracion.svc?singleWsdl");
/*    */     } catch (MalformedURLException ex) {
/* 35 */       e = new WebServiceException(ex);
/*    */     }
/* 37 */     INTEGRACION_WSDL_LOCATION = url;
/* 38 */     INTEGRACION_EXCEPTION = e;
/*    */   }
/*    */   
/*    */   public Integracion() {
/* 42 */     super(__getWsdlLocation(), INTEGRACION_QNAME);
/*    */   }
/*    */   
/*    */   public Integracion(WebServiceFeature... features) {
/* 46 */     super(__getWsdlLocation(), INTEGRACION_QNAME, features);
/*    */   }
/*    */   
/*    */   public Integracion(URL wsdlLocation) {
/* 50 */     super(wsdlLocation, INTEGRACION_QNAME);
/*    */   }
/*    */   
/*    */   public Integracion(URL wsdlLocation, WebServiceFeature... features) {
/* 54 */     super(wsdlLocation, INTEGRACION_QNAME, features);
/*    */   }
/*    */   
/*    */   public Integracion(URL wsdlLocation, QName serviceName) {
/* 58 */     super(wsdlLocation, serviceName);
/*    */   }
/*    */   
/*    */   public Integracion(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
/* 62 */     super(wsdlLocation, serviceName, features);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @WebEndpoint(name="BasicHttpBinding_IIntegracion")
/*    */   public IIntegracion getBasicHttpBindingIIntegracion()
/*    */   {
/* 72 */     return (IIntegracion)super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IIntegracion"), IIntegracion.class);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @WebEndpoint(name="BasicHttpBinding_IIntegracion")
/*    */   public IIntegracion getBasicHttpBindingIIntegracion(WebServiceFeature... features)
/*    */   {
/* 84 */     return (IIntegracion)super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IIntegracion"), IIntegracion.class, features);
/*    */   }
/*    */   
/*    */   private static URL __getWsdlLocation() {
/* 88 */     if (INTEGRACION_EXCEPTION != null) {
/* 89 */       throw INTEGRACION_EXCEPTION;
/*    */     }
/* 91 */     return INTEGRACION_WSDL_LOCATION;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/Integracion.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */