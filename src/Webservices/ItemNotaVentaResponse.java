/*    */ package Webservices;
/*    */ 
/*    */ import javax.xml.bind.JAXBElement;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElementRef;
/*    */ import javax.xml.bind.annotation.XmlRootElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="", propOrder={"itemNotaVentaResult"})
/*    */ @XmlRootElement(name="Item_NotaVentaResponse")
/*    */ public class ItemNotaVentaResponse
/*    */ {
/*    */   @XmlElementRef(name="Item_NotaVentaResult", namespace="http://tempuri.org/", type=JAXBElement.class, required=false)
/*    */   protected JAXBElement<ArrayOfLinea> itemNotaVentaResult;
/*    */   
/*    */   public JAXBElement<ArrayOfLinea> getItemNotaVentaResult()
/*    */   {
/* 50 */     return this.itemNotaVentaResult;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setItemNotaVentaResult(JAXBElement<ArrayOfLinea> value)
/*    */   {
/* 62 */     this.itemNotaVentaResult = value;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/ItemNotaVentaResponse.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */