/*    */ package Webservices;
/*    */ 
/*    */ import Logging.LoggingManager;
/*    */ import java.io.PrintStream;
/*    */ import java.util.List;
/*    */ import javax.xml.namespace.QName;
/*    */ import javax.xml.transform.Source;
/*    */ import javax.xml.ws.Dispatch;
/*    */ import javax.xml.ws.Service.Mode;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class WebServiceManager
/*    */ {
/* 30 */   private static WebServiceManager instance = null;
/*    */   private ObjectFactory fact;
/*    */   private ArrayOfLinea lineas;
/* 33 */   private Integer idlinea = Integer.valueOf(0);
/*    */   
/*    */   protected WebServiceManager() {
/* 36 */    // this.fact = new ObjectFactory();
/*    */   }
/*    */   
/*    */   public static WebServiceManager getInstance() {
/* 40 */     if (instance == null) {
/* 41 */       instance = new WebServiceManager();
/*    */     }
/* 43 */     return instance;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void init() {}
/*    */   
/*    */ 
/*    */ 
/*    */   public void prepareSend()
/*    */   {
/* 54 */     this.lineas = null;
/* 55 */     this.lineas = this.fact.createArrayOfLinea();
/* 56 */     this.idlinea = Integer.valueOf(1);
/*    */   }
/*    */   
/*    */   public void addDetail(String codigomadre, String codigotecnico, Double cantidad, String observacion)
/*    */   {
/* 61 */     Linea l = this.fact.createLinea();
/* 62 */     l.setCantidad(cantidad);
/* 63 */     l.setIdLinea(this.idlinea);
/* 64 */     l.setObservacion(this.fact.createLineaObservacion(observacion));
/* 65 */     l.setCodigoMadre(this.fact.createLineaCodigoMadre(""));
/* 66 */     l.setCodigoTecnico(this.fact.createLineaCodigoTecnico(codigotecnico.trim()));
/* 67 */     this.lineas.getLinea().add(l);
/* 68 */     WebServiceManager localWebServiceManager = this;Integer localInteger1 = localWebServiceManager.idlinea;Integer localInteger2 = localWebServiceManager.idlinea = Integer.valueOf(localWebServiceManager.idlinea.intValue() + 1);
/*    */   }
/*    */   
/*       public void send(String notaventa)
/*      {
/* 73      Integracion service = new Integracion();
/* 74      QName portQName = new QName("http://tempuri.org/", "BasicHttpBinding_IIntegracion");
/*         
/*         try
/*         {
/* 78        Dispatch<Source> sourceDispatch = null;
/* 79        sourceDispatch = service.createDispatch(portQName, Source.class, Service.Mode.PAYLOAD);
/*         
/*     
/* 82        boolean resultado = service.getBasicHttpBindingIIntegracion().addBulto(new Long(notaventa), new Long(notaventa), this.lineas).booleanValue();
/*          
/*    
/* 85      LoggingManager.getInstance().log("Resultado envío: " + resultado);
/*        }
/*         catch (Exception ex)
/*        {
/* 89       System.out.println(ex.getMessage());
/* 90       LoggingManager.getInstance().log_error(ex);
/*        }
/*       }
    


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/WebServiceManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */