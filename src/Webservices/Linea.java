/*     */ package Webservices;
/*     */ 
/*     */ import javax.xml.bind.JAXBElement;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlElementRef;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="Linea", namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", propOrder={"cantidad", "codigoMadre", "codigoTecnico", "idLinea", "observacion"})
/*     */ public class Linea
/*     */ {
/*     */   @XmlElement(name="Cantidad")
/*     */   protected Double cantidad;
/*     */   @XmlElementRef(name="Codigo_Madre", namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", type=JAXBElement.class, required=false)
/*     */   protected JAXBElement<String> codigoMadre;
/*     */   @XmlElementRef(name="Codigo_Tecnico", namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", type=JAXBElement.class, required=false)
/*     */   protected JAXBElement<String> codigoTecnico;
/*     */   @XmlElement(name="IdLinea")
/*     */   protected Integer idLinea;
/*     */   @XmlElementRef(name="Observacion", namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", type=JAXBElement.class, required=false)
/*     */   protected JAXBElement<String> observacion;
/*     */   
/*     */   public Double getCantidad()
/*     */   {
/*  65 */     return this.cantidad;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCantidad(Double value)
/*     */   {
/*  77 */     this.cantidad = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JAXBElement<String> getCodigoMadre()
/*     */   {
/*  89 */     return this.codigoMadre;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodigoMadre(JAXBElement<String> value)
/*     */   {
/* 101 */     this.codigoMadre = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JAXBElement<String> getCodigoTecnico()
/*     */   {
/* 113 */     return this.codigoTecnico;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodigoTecnico(JAXBElement<String> value)
/*     */   {
/* 125 */     this.codigoTecnico = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getIdLinea()
/*     */   {
/* 137 */     return this.idLinea;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIdLinea(Integer value)
/*     */   {
/* 149 */     this.idLinea = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JAXBElement<String> getObservacion()
/*     */   {
/* 161 */     return this.observacion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setObservacion(JAXBElement<String> value)
/*     */   {
/* 173 */     this.observacion = value;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/Linea.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */