/*     */ package Webservices;
/*     */ 
/*     */ import javax.xml.bind.JAXBElement;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlElementRef;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="", propOrder={"pedidoOrigen", "bultoDestino", "lineas"})
/*     */ @XmlRootElement(name="Add_Bulto")
/*     */ public class AddBulto
/*     */ {
/*     */   @XmlElement(name="PedidoOrigen")
/*     */   protected Long pedidoOrigen;
/*     */   @XmlElement(name="BultoDestino")
/*     */   protected Long bultoDestino;
/*     */   @XmlElementRef(name="Lineas", namespace="http://tempuri.org/", type=JAXBElement.class, required=false)
/*     */   protected JAXBElement<ArrayOfLinea> lineas;
/*     */   
/*     */   public Long getPedidoOrigen()
/*     */   {
/*  59 */     return this.pedidoOrigen;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPedidoOrigen(Long value)
/*     */   {
/*  71 */     this.pedidoOrigen = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Long getBultoDestino()
/*     */   {
/*  83 */     return this.bultoDestino;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBultoDestino(Long value)
/*     */   {
/*  95 */     this.bultoDestino = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JAXBElement<ArrayOfLinea> getLineas()
/*     */   {
/* 107 */     return this.lineas;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLineas(JAXBElement<ArrayOfLinea> value)
/*     */   {
/* 119 */     this.lineas = value;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/AddBulto.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */