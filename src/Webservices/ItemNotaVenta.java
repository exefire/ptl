/*    */ package Webservices;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlRootElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="", propOrder={"numero"})
/*    */ @XmlRootElement(name="Item_NotaVenta")
/*    */ public class ItemNotaVenta
/*    */ {
/*    */   @XmlElement(name="Numero")
/*    */   protected Long numero;
/*    */   
/*    */   public Long getNumero()
/*    */   {
/* 49 */     return this.numero;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNumero(Long value)
/*    */   {
/* 61 */     this.numero = value;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/ItemNotaVenta.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */