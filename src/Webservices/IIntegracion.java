package Webservices;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

//@WebService(name="IIntegracion", targetNamespace="http://tempuri.org/")
//@XmlSeeAlso({ObjectFactory.class})
public abstract interface IIntegracion
{
  @WebMethod(operationName="Add_Bulto", action="http://tempuri.org/IIntegracion/Add_Bulto")
  @WebResult(name="Add_BultoResult", targetNamespace="http://tempuri.org/")
  @RequestWrapper(localName="Add_Bulto", targetNamespace="http://tempuri.org/", className="Webservices.AddBulto")
  @ResponseWrapper(localName="Add_BultoResponse", targetNamespace="http://tempuri.org/", className="Webservices.AddBultoResponse")
  public abstract Boolean addBulto(@WebParam(name="PedidoOrigen", targetNamespace="http://tempuri.org/") Long paramLong1, @WebParam(name="BultoDestino", targetNamespace="http://tempuri.org/") Long paramLong2, @WebParam(name="Lineas", targetNamespace="http://tempuri.org/") ArrayOfLinea paramArrayOfLinea);
  
  @WebMethod(operationName="Item_NotaVenta", action="http://tempuri.org/IIntegracion/Item_NotaVenta")
  @WebResult(name="Item_NotaVentaResult", targetNamespace="http://tempuri.org/")
  @RequestWrapper(localName="Item_NotaVenta", targetNamespace="http://tempuri.org/", className="Webservices.ItemNotaVenta")
  @ResponseWrapper(localName="Item_NotaVentaResponse", targetNamespace="http://tempuri.org/", className="Webservices.ItemNotaVentaResponse")
  public abstract ArrayOfLinea itemNotaVenta(@WebParam(name="Numero", targetNamespace="http://tempuri.org/") Long paramLong);
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/IIntegracion.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */