/*    */ package Webservices;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="ArrayOfLinea", namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", propOrder={"linea"})
/*    */ public class ArrayOfLinea
/*    */ {
/*    */   @XmlElement(name="Linea", nillable=true)
/*    */   protected List<Linea> linea;
/*    */   
/*    */   public List<Linea> getLinea()
/*    */   {
/* 63 */     if (this.linea == null) {
/* 64 */       this.linea = new ArrayList();
/*    */     }
/* 66 */     return this.linea;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/ArrayOfLinea.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */