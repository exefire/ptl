/*     */ package Webservices;
/*     */ 
/*     */ import java.math.BigDecimal;
/*     */ import java.math.BigInteger;
/*     */ import javax.xml.bind.JAXBElement;
/*     */ import javax.xml.bind.annotation.XmlElementDecl;
/*     */ import javax.xml.bind.annotation.XmlRegistry;
/*     */ import javax.xml.datatype.Duration;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ import javax.xml.namespace.QName;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlRegistry
/*     */ public class ObjectFactory
/*     */ {
/*  31 */   private static final QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
/*  32 */   private static final QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
/*  33 */   private static final QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
/*  34 */   private static final QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
/*  35 */   private static final QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
/*  36 */   private static final QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
/*  37 */   private static final QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
/*  38 */   private static final QName _ArrayOfLinea_QNAME = new QName("http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", "ArrayOfLinea");
/*  39 */   private static final QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
/*  40 */   private static final QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
/*  41 */   private static final QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
/*  42 */   private static final QName _Linea_QNAME = new QName("http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", "Linea");
/*  43 */   private static final QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
/*  44 */   private static final QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
/*  45 */   private static final QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
/*  46 */   private static final QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
/*  47 */   private static final QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
/*  48 */   private static final QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
/*  49 */   private static final QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
/*  50 */   private static final QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
/*  51 */   private static final QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
/*  52 */   private static final QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
/*  53 */   private static final QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
/*  54 */   private static final QName _AddBultoLineas_QNAME = new QName("http://tempuri.org/", "Lineas");
/*  55 */   private static final QName _ItemNotaVentaResponseItemNotaVentaResult_QNAME = new QName("http://tempuri.org/", "Item_NotaVentaResult");
/*  56 */   private static final QName _LineaCodigoTecnico_QNAME = new QName("http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", "Codigo_Tecnico");
/*  57 */   private static final QName _LineaObservacion_QNAME = new QName("http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", "Observacion");
/*  58 */   private static final QName _LineaCodigoMadre_QNAME = new QName("http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", "Codigo_Madre");
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ArrayOfLinea createArrayOfLinea()
/*     */   {
/*  72 */     return new ArrayOfLinea();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Linea createLinea()
/*     */   {
/*  80 */     return new Linea();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public AddBultoResponse createAddBultoResponse()
/*     */   {
/*  88 */     return new AddBultoResponse();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public ItemNotaVentaResponse createItemNotaVentaResponse()
/*     */   {
/*  96 */     return new ItemNotaVentaResponse();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public ItemNotaVenta createItemNotaVenta()
/*     */   {
/* 104 */     return new ItemNotaVenta();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public AddBulto createAddBulto()
/*     */   {
/* 112 */     return new AddBulto();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="unsignedLong")
/*     */   public JAXBElement<BigInteger> createUnsignedLong(BigInteger value)
/*     */   {
/* 121 */     return new JAXBElement(_UnsignedLong_QNAME, BigInteger.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="unsignedByte")
/*     */   public JAXBElement<Short> createUnsignedByte(Short value)
/*     */   {
/* 130 */     return new JAXBElement(_UnsignedByte_QNAME, Short.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="unsignedInt")
/*     */   public JAXBElement<Long> createUnsignedInt(Long value)
/*     */   {
/* 139 */     return new JAXBElement(_UnsignedInt_QNAME, Long.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="char")
/*     */   public JAXBElement<Integer> createChar(Integer value)
/*     */   {
/* 148 */     return new JAXBElement(_Char_QNAME, Integer.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="short")
/*     */   public JAXBElement<Short> createShort(Short value)
/*     */   {
/* 157 */     return new JAXBElement(_Short_QNAME, Short.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="guid")
/*     */   public JAXBElement<String> createGuid(String value)
/*     */   {
/* 166 */     return new JAXBElement(_Guid_QNAME, String.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="unsignedShort")
/*     */   public JAXBElement<Integer> createUnsignedShort(Integer value)
/*     */   {
/* 175 */     return new JAXBElement(_UnsignedShort_QNAME, Integer.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", name="ArrayOfLinea")
/*     */   public JAXBElement<ArrayOfLinea> createArrayOfLinea(ArrayOfLinea value)
/*     */   {
/* 184 */     return new JAXBElement(_ArrayOfLinea_QNAME, ArrayOfLinea.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="decimal")
/*     */   public JAXBElement<BigDecimal> createDecimal(BigDecimal value)
/*     */   {
/* 193 */     return new JAXBElement(_Decimal_QNAME, BigDecimal.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="boolean")
/*     */   public JAXBElement<Boolean> createBoolean(Boolean value)
/*     */   {
/* 202 */     return new JAXBElement(_Boolean_QNAME, Boolean.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="duration")
/*     */   public JAXBElement<Duration> createDuration(Duration value)
/*     */   {
/* 211 */     return new JAXBElement(_Duration_QNAME, Duration.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", name="Linea")
/*     */   public JAXBElement<Linea> createLinea(Linea value)
/*     */   {
/* 220 */     return new JAXBElement(_Linea_QNAME, Linea.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="base64Binary")
/*     */   public JAXBElement<byte[]> createBase64Binary(byte[] value)
/*     */   {
/* 229 */     return new JAXBElement(_Base64Binary_QNAME, byte[].class, null, (byte[])value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="int")
/*     */   public JAXBElement<Integer> createInt(Integer value)
/*     */   {
/* 238 */     return new JAXBElement(_Int_QNAME, Integer.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="long")
/*     */   public JAXBElement<Long> createLong(Long value)
/*     */   {
/* 247 */     return new JAXBElement(_Long_QNAME, Long.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="anyURI")
/*     */   public JAXBElement<String> createAnyURI(String value)
/*     */   {
/* 256 */     return new JAXBElement(_AnyURI_QNAME, String.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="float")
/*     */   public JAXBElement<Float> createFloat(Float value)
/*     */   {
/* 265 */     return new JAXBElement(_Float_QNAME, Float.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="dateTime")
/*     */   public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value)
/*     */   {
/* 274 */     return new JAXBElement(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="byte")
/*     */   public JAXBElement<Byte> createByte(Byte value)
/*     */   {
/* 283 */     return new JAXBElement(_Byte_QNAME, Byte.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="double")
/*     */   public JAXBElement<Double> createDouble(Double value)
/*     */   {
/* 292 */     return new JAXBElement(_Double_QNAME, Double.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="QName")
/*     */   public JAXBElement<QName> createQName(QName value)
/*     */   {
/* 301 */     return new JAXBElement(_QName_QNAME, QName.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="anyType")
/*     */   public JAXBElement<Object> createAnyType(Object value)
/*     */   {
/* 310 */     return new JAXBElement(_AnyType_QNAME, Object.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.microsoft.com/2003/10/Serialization/", name="string")
/*     */   public JAXBElement<String> createString(String value)
/*     */   {
/* 319 */     return new JAXBElement(_String_QNAME, String.class, null, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://tempuri.org/", name="Lineas", scope=AddBulto.class)
/*     */   public JAXBElement<ArrayOfLinea> createAddBultoLineas(ArrayOfLinea value)
/*     */   {
/* 328 */     return new JAXBElement(_AddBultoLineas_QNAME, ArrayOfLinea.class, AddBulto.class, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://tempuri.org/", name="Item_NotaVentaResult", scope=ItemNotaVentaResponse.class)
/*     */   public JAXBElement<ArrayOfLinea> createItemNotaVentaResponseItemNotaVentaResult(ArrayOfLinea value)
/*     */   {
/* 337 */     return new JAXBElement(_ItemNotaVentaResponseItemNotaVentaResult_QNAME, ArrayOfLinea.class, ItemNotaVentaResponse.class, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", name="Codigo_Tecnico", scope=Linea.class)
/*     */   public JAXBElement<String> createLineaCodigoTecnico(String value)
/*     */   {
/* 346 */     return new JAXBElement(_LineaCodigoTecnico_QNAME, String.class, Linea.class, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", name="Observacion", scope=Linea.class)
/*     */   public JAXBElement<String> createLineaObservacion(String value)
/*     */   {
/* 355 */     return new JAXBElement(_LineaObservacion_QNAME, String.class, Linea.class, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @XmlElementDecl(namespace="http://schemas.datacontract.org/2004/07/justtime.wcf.ptl", name="Codigo_Madre", scope=Linea.class)
/*     */   public JAXBElement<String> createLineaCodigoMadre(String value)
/*     */   {
/* 364 */     return new JAXBElement(_LineaCodigoMadre_QNAME, String.class, Linea.class, value);
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/ObjectFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */