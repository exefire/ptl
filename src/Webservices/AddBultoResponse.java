/*    */ package Webservices;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlRootElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="", propOrder={"addBultoResult"})
/*    */ @XmlRootElement(name="Add_BultoResponse")
/*    */ public class AddBultoResponse
/*    */ {
/*    */   @XmlElement(name="Add_BultoResult")
/*    */   protected Boolean addBultoResult;
/*    */   
/*    */   public Boolean isAddBultoResult()
/*    */   {
/* 49 */     return this.addBultoResult;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setAddBultoResult(Boolean value)
/*    */   {
/* 61 */     this.addBultoResult = value;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Webservices/AddBultoResponse.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */