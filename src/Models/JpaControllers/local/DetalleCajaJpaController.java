/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.util.Collection;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DetalleCajaJpaController
/*     */   implements Serializable
/*     */ {
/*  31 */   private EntityManagerFactory emf = null;
/*     */   private static DetalleCajaJpaController instance;
/*     */   
/*     */   protected DetalleCajaJpaController()
/*     */   {
/*  36 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static DetalleCajaJpaController getInstance() {
/*  40 */     if (instance == null) {
/*  41 */       synchronized (DetalleCajaJpaController.class) {
/*  42 */         if (instance == null) {
/*  43 */           instance = new DetalleCajaJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  47 */     return instance;
/*     */   }
/*     */   
/*     */   public EntityManager getEntityManager() {
/*  51 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   public void create(DetalleCaja detalleCaja) {
/*  55 */     EntityManager em = null;
/*     */     try {
/*  57 */       em = getEntityManager();
/*  58 */       em.getTransaction().begin();
/*  59 */       User userId = detalleCaja.getUserId();
/*  60 */       if (userId != null) {
/*  61 */         userId = (User)em.getReference(userId.getClass(), userId.getId());
/*  62 */         detalleCaja.setUserId(userId);
/*     */       }
/*  64 */       Caja cajaId = detalleCaja.getCajaId();
/*  65 */       if (cajaId != null) {
/*  66 */         cajaId = (Caja)em.getReference(cajaId.getClass(), cajaId.getId());
/*  67 */         detalleCaja.setCajaId(cajaId);
/*     */       }
/*  69 */       em.persist(detalleCaja);
/*  70 */       if (userId != null) {
/*  71 */         userId.getDetalleCajaCollection().add(detalleCaja);
/*  72 */         userId = (User)em.merge(userId);
/*     */       }
/*  74 */       if (cajaId != null) {
/*  75 */         cajaId.getDetalleCajaCollection().add(detalleCaja);
/*  76 */         cajaId = (Caja)em.merge(cajaId);
/*     */       }
/*  78 */       em.getTransaction().commit();
/*     */     } finally {
/*  80 */       if (em != null) {
/*  81 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void edit(DetalleCaja detalleCaja) throws NonexistentEntityException, Exception {
/*  87 */     EntityManager em = null;
/*     */     try {
/*  89 */       em = getEntityManager();
/*  90 */       em.getTransaction().begin();
/*  91 */       DetalleCaja persistentDetalleCaja = (DetalleCaja)em.find(DetalleCaja.class, detalleCaja.getId());
/*  92 */       User userIdOld = persistentDetalleCaja.getUserId();
/*  93 */       User userIdNew = detalleCaja.getUserId();
/*  94 */       Caja cajaIdOld = persistentDetalleCaja.getCajaId();
/*  95 */       Caja cajaIdNew = detalleCaja.getCajaId();
/*  96 */       if (userIdNew != null) {
/*  97 */         userIdNew = (User)em.getReference(userIdNew.getClass(), userIdNew.getId());
/*  98 */         detalleCaja.setUserId(userIdNew);
/*     */       }
/* 100 */       if (cajaIdNew != null) {
/* 101 */         cajaIdNew = (Caja)em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
/* 102 */         detalleCaja.setCajaId(cajaIdNew);
/*     */       }
/* 104 */       detalleCaja = (DetalleCaja)em.merge(detalleCaja);
/* 105 */       if ((userIdOld != null) && (!userIdOld.equals(userIdNew))) {
/* 106 */         userIdOld.getDetalleCajaCollection().remove(detalleCaja);
/* 107 */         userIdOld = (User)em.merge(userIdOld);
/*     */       }
/* 109 */       if ((userIdNew != null) && (!userIdNew.equals(userIdOld))) {
/* 110 */         userIdNew.getDetalleCajaCollection().add(detalleCaja);
/* 111 */         userIdNew = (User)em.merge(userIdNew);
/*     */       }
/* 113 */       if ((cajaIdOld != null) && (!cajaIdOld.equals(cajaIdNew))) {
/* 114 */         cajaIdOld.getDetalleCajaCollection().remove(detalleCaja);
/* 115 */         cajaIdOld = (Caja)em.merge(cajaIdOld);
/*     */       }
/* 117 */       if ((cajaIdNew != null) && (!cajaIdNew.equals(cajaIdOld))) {
/* 118 */         cajaIdNew.getDetalleCajaCollection().add(detalleCaja);
/* 119 */         cajaIdNew = (Caja)em.merge(cajaIdNew);
/*     */       }
/* 121 */       em.getTransaction().commit();
/*     */     } catch (Exception ex) {
/* 123 */       String msg = ex.getLocalizedMessage();
/* 124 */       if ((msg == null) || (msg.length() == 0)) {
/* 125 */         Integer id = detalleCaja.getId();
/* 126 */         if (findDetalleCaja(id) == null) {
/* 127 */           throw new NonexistentEntityException("The detalleCaja with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/* 130 */       throw ex;
/*     */     } finally {
/* 132 */       if (em != null) {
/* 133 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws NonexistentEntityException {
/* 139 */     EntityManager em = null;
/*     */     try {
/* 141 */       em = getEntityManager();
/* 142 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/* 145 */         DetalleCaja detalleCaja = (DetalleCaja)em.getReference(DetalleCaja.class, id);
/* 146 */         detalleCaja.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 148 */         throw new NonexistentEntityException("The detalleCaja with id " + id + " no longer exists.", enfe); }
/*     */       DetalleCaja detalleCaja;
/* 150 */       User userId = detalleCaja.getUserId();
/* 151 */       if (userId != null) {
/* 152 */         userId.getDetalleCajaCollection().remove(detalleCaja);
/* 153 */         userId = (User)em.merge(userId);
/*     */       }
/* 155 */       Caja cajaId = detalleCaja.getCajaId();
/* 156 */       if (cajaId != null) {
/* 157 */         cajaId.getDetalleCajaCollection().remove(detalleCaja);
/* 158 */         cajaId = (Caja)em.merge(cajaId);
/*     */       }
/* 160 */       em.remove(detalleCaja);
/* 161 */       em.getTransaction().commit();
/*     */     } finally {
/* 163 */       if (em != null) {
/* 164 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<DetalleCaja> findDetalleCajaEntities() {
/* 170 */     return findDetalleCajaEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<DetalleCaja> findDetalleCajaEntities(int maxResults, int firstResult) {
/* 174 */     return findDetalleCajaEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<DetalleCaja> findDetalleCajaEntities(boolean all, int maxResults, int firstResult) {
/* 178 */     EntityManager em = getEntityManager();
/*     */     try {
/* 180 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 181 */       cq.select(cq.from(DetalleCaja.class));
/* 182 */       Query q = em.createQuery(cq);
/* 183 */       if (!all) {
/* 184 */         q.setMaxResults(maxResults);
/* 185 */         q.setFirstResult(firstResult);
/*     */       }
/* 187 */       return q.getResultList();
/*     */     } finally {
/* 189 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public DetalleCaja findDetalleCaja(Integer id) {
/* 194 */     EntityManager em = getEntityManager();
/*     */     try {
/* 196 */       return (DetalleCaja)em.find(DetalleCaja.class, id);
/*     */     } finally {
/* 198 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getDetalleCajaCount() {
/* 203 */     EntityManager em = getEntityManager();
/*     */     try {
/* 205 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 206 */       Root<DetalleCaja> rt = cq.from(DetalleCaja.class);
/* 207 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 208 */       Query q = em.createQuery(cq);
/* 209 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 211 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public List<DetalleCaja> findAllByCaja(Caja caja)
/*     */   {
/* 217 */     EntityManager em = getEntityManager();
/* 218 */     List<DetalleCaja> lista = null;
/*     */     try
/*     */     {
/* 221 */       Query q = em.createNamedQuery("DetalleCaja.findAllByCaja", DetalleCaja.class);
/* 222 */       q.setParameter("caja", caja);
/*     */       
/*     */ 
/* 225 */       lista = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 227 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 234 */         ex;lista = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 229 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 234 */         ex;lista = null;LoggingManager.getInstance().log_error(ex);
/*     */     } finally {}
/* 236 */     return lista;
/*     */   }
/*     */   
/*     */ 
/*     */   public List<DetalleCaja> findAllByCajaAndReferencia(Caja caja, String referencia)
/*     */   {
/* 242 */     EntityManager em = getEntityManager();
/* 243 */     List<DetalleCaja> lista = null;
/*     */     try
/*     */     {
/* 246 */       Query q = em.createNamedQuery("DetalleCaja.findAllByCajaAndReferencia", DetalleCaja.class);
/* 247 */       q.setParameter("caja", caja);
/* 248 */       q.setParameter("referencia", referencia);
/*     */       
/* 250 */       lista = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 252 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 259 */         ex;lista = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 254 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 259 */         ex;lista = null;LoggingManager.getInstance().log_error(ex);
/*     */     } finally {}
/* 261 */     return lista;
/*     */   }
/*     */   
/*     */   public List<Object> findDetallePickingFinal(String numero)
/*     */   {
/* 266 */     EntityManager em = getEntityManager();
/* 267 */     List<Object> lista = null;
/*     */     try
/*     */     {
/* 270 */       Query q = em.createNativeQuery("select dt.oc as 'oc',c.codigo as 'bulto',d.nombre as 'destino', dt.codigo_retail, sum(dt.cantidad) as cantidad, dt.codigo_barra, nv.idreferencia from picking nv inner join caja c on nv.id = c.picking_id inner join destino d on c.destino_id = d.id  inner join detalle_caja dt on dt.caja_id = c.id where nv.idreferencia = '" + numero + "' group by 1,2,3,4");
/*     */       
/* 272 */       lista = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 274 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 281 */         ex;lista = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 276 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 281 */         ex;lista = null;LoggingManager.getInstance().log_error(ex);
/*     */     } finally {}
/* 283 */     return lista;
/*     */   }
/*     */   
/*     */   public List<Object> findAllDocumentos(Caja currentBox)
/*     */   {
/* 288 */     EntityManager em = getEntityManager();
/* 289 */     List<Object> lista = null;
/*     */     try
/*     */     {
/* 292 */       Query q = em.createNativeQuery("select picking_id,referencia, (sum(aconsolidar) - sum(consolidado )) as diferencia from detalle_picking where referencia in (SELECT distinct referencia from detalle_caja where caja_id  = " + currentBox.getId().toString() + ") and picking_id = " + currentBox.getPickingId().getId().toString() + " group by picking_id, referencia");
/*     */       
/*     */ 
/* 295 */       lista = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 297 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 304 */         ex;lista = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 299 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 304 */         ex;lista = null;LoggingManager.getInstance().log_error(ex);
/*     */     } finally {}
/* 306 */     return lista;
/*     */   }
/*     */   
/*     */   public void deleteAllofPicking(Integer id)
/*     */   {
/* 311 */     EntityManager em = getEntityManager();
/* 312 */     em.getTransaction().begin();
/*     */     
/*     */     try
/*     */     {
/* 316 */       Query q = em.createNativeQuery("delete FROM detalle_caja where caja_id in ( select id from caja where picking_id = " + id + " ) and id > 0");
/*     */       
/* 318 */       q.executeUpdate();
/* 319 */       em.getTransaction().commit();
/*     */     }
/*     */     catch (NoResultException localNoResultException) {
/* 322 */       localNoResultException = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 329 */         localNoResultException;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 324 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 329 */         ex;LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */     finally {}
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/DetalleCajaJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */