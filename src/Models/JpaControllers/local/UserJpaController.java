/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.User;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UserJpaController
/*     */   implements Serializable
/*     */ {
/*  32 */   private EntityManagerFactory emf = null;
/*     */   private static UserJpaController instance;
/*     */   
/*     */   protected UserJpaController()
/*     */   {
/*  37 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static UserJpaController getInstance() {
/*  41 */     if (instance == null) {
/*  42 */       synchronized (UserJpaController.class) {
/*  43 */         if (instance == null) {
/*  44 */           instance = new UserJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  48 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public EntityManager getEntityManager()
/*     */   {
/*  55 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   public void create(User user) {
/*  59 */     if (user.getDetalleCajaCollection() == null) {
/*  60 */       user.setDetalleCajaCollection(new ArrayList());
/*     */     }
/*  62 */     EntityManager em = null;
/*     */     try {
/*  64 */       em = getEntityManager();
/*  65 */       em.getTransaction().begin();
/*  66 */       Collection<DetalleCaja> attachedDetalleCajaCollection = new ArrayList();
/*  67 */       for (DetalleCaja detalleCajaCollectionDetalleCajaToAttach : user.getDetalleCajaCollection()) {
/*  68 */         detalleCajaCollectionDetalleCajaToAttach = (DetalleCaja)em.getReference(detalleCajaCollectionDetalleCajaToAttach.getClass(), detalleCajaCollectionDetalleCajaToAttach.getId());
/*  69 */         attachedDetalleCajaCollection.add(detalleCajaCollectionDetalleCajaToAttach);
/*     */       }
/*  71 */       user.setDetalleCajaCollection(attachedDetalleCajaCollection);
/*  72 */       em.persist(user);
/*  73 */       for (DetalleCaja detalleCajaCollectionDetalleCaja : user.getDetalleCajaCollection()) {
/*  74 */         User oldUserIdOfDetalleCajaCollectionDetalleCaja = detalleCajaCollectionDetalleCaja.getUserId();
/*  75 */         detalleCajaCollectionDetalleCaja.setUserId(user);
/*  76 */         detalleCajaCollectionDetalleCaja = (DetalleCaja)em.merge(detalleCajaCollectionDetalleCaja);
/*  77 */         if (oldUserIdOfDetalleCajaCollectionDetalleCaja != null) {
/*  78 */           oldUserIdOfDetalleCajaCollectionDetalleCaja.getDetalleCajaCollection().remove(detalleCajaCollectionDetalleCaja);
/*  79 */           oldUserIdOfDetalleCajaCollectionDetalleCaja = (User)em.merge(oldUserIdOfDetalleCajaCollectionDetalleCaja);
/*     */         }
/*     */       }
/*  82 */       em.getTransaction().commit();
/*     */     } finally {
/*  84 */       if (em != null) {
/*  85 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void edit(User user) throws NonexistentEntityException, Exception {
/*  91 */     EntityManager em = null;
/*     */     try {
/*  93 */       em = getEntityManager();
/*  94 */       em.getTransaction().begin();
/*  95 */       User persistentUser = (User)em.find(User.class, user.getId());
/*  96 */       Collection<DetalleCaja> detalleCajaCollectionOld = persistentUser.getDetalleCajaCollection();
/*  97 */       Collection<DetalleCaja> detalleCajaCollectionNew = user.getDetalleCajaCollection();
/*  98 */       Collection<DetalleCaja> attachedDetalleCajaCollectionNew = new ArrayList();
/*  99 */       for (DetalleCaja detalleCajaCollectionNewDetalleCajaToAttach : detalleCajaCollectionNew) {
/* 100 */         detalleCajaCollectionNewDetalleCajaToAttach = (DetalleCaja)em.getReference(detalleCajaCollectionNewDetalleCajaToAttach.getClass(), detalleCajaCollectionNewDetalleCajaToAttach.getId());
/* 101 */         attachedDetalleCajaCollectionNew.add(detalleCajaCollectionNewDetalleCajaToAttach);
/*     */       }
/* 103 */       detalleCajaCollectionNew = attachedDetalleCajaCollectionNew;
/* 104 */       user.setDetalleCajaCollection(detalleCajaCollectionNew);
/* 105 */       user = (User)em.merge(user);
/* 106 */       for (DetalleCaja detalleCajaCollectionOldDetalleCaja : detalleCajaCollectionOld) {
/* 107 */         if (!detalleCajaCollectionNew.contains(detalleCajaCollectionOldDetalleCaja)) {
/* 108 */           detalleCajaCollectionOldDetalleCaja.setUserId(null);
/* 109 */           detalleCajaCollectionOldDetalleCaja = (DetalleCaja)em.merge(detalleCajaCollectionOldDetalleCaja);
/*     */         }
/*     */       }
/* 112 */       for (DetalleCaja detalleCajaCollectionNewDetalleCaja : detalleCajaCollectionNew) {
/* 113 */         if (!detalleCajaCollectionOld.contains(detalleCajaCollectionNewDetalleCaja)) {
/* 114 */           User oldUserIdOfDetalleCajaCollectionNewDetalleCaja = detalleCajaCollectionNewDetalleCaja.getUserId();
/* 115 */           detalleCajaCollectionNewDetalleCaja.setUserId(user);
/* 116 */           detalleCajaCollectionNewDetalleCaja = (DetalleCaja)em.merge(detalleCajaCollectionNewDetalleCaja);
/* 117 */           if ((oldUserIdOfDetalleCajaCollectionNewDetalleCaja != null) && (!oldUserIdOfDetalleCajaCollectionNewDetalleCaja.equals(user))) {
/* 118 */             oldUserIdOfDetalleCajaCollectionNewDetalleCaja.getDetalleCajaCollection().remove(detalleCajaCollectionNewDetalleCaja);
/* 119 */             oldUserIdOfDetalleCajaCollectionNewDetalleCaja = (User)em.merge(oldUserIdOfDetalleCajaCollectionNewDetalleCaja);
/*     */           }
/*     */         }
/*     */       }
/* 123 */       em.getTransaction().commit();
/*     */     } catch (Exception ex) {
/* 125 */       String msg = ex.getLocalizedMessage();
/* 126 */       if ((msg == null) || (msg.length() == 0)) {
/* 127 */         Integer id = user.getId();
/* 128 */         if (findUser(id) == null) {
/* 129 */           throw new NonexistentEntityException("The user with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/* 132 */       throw ex;
/*     */     } finally {
/* 134 */       if (em != null) {
/* 135 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws NonexistentEntityException {
/* 141 */     EntityManager em = null;
/*     */     try {
/* 143 */       em = getEntityManager();
/* 144 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/* 147 */         User user = (User)em.getReference(User.class, id);
/* 148 */         user.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 150 */         throw new NonexistentEntityException("The user with id " + id + " no longer exists.", enfe); }
/*     */       User user;
/* 152 */       Collection<DetalleCaja> detalleCajaCollection = user.getDetalleCajaCollection();
/* 153 */       for (DetalleCaja detalleCajaCollectionDetalleCaja : detalleCajaCollection) {
/* 154 */         detalleCajaCollectionDetalleCaja.setUserId(null);
/* 155 */         detalleCajaCollectionDetalleCaja = (DetalleCaja)em.merge(detalleCajaCollectionDetalleCaja);
/*     */       }
/* 157 */       em.remove(user);
/* 158 */       em.getTransaction().commit();
/*     */     } finally {
/* 160 */       if (em != null) {
/* 161 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<User> findUserEntities() {
/* 167 */     return findUserEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<User> findUserEntities(int maxResults, int firstResult) {
/* 171 */     return findUserEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<User> findUserEntities(boolean all, int maxResults, int firstResult) {
/* 175 */     EntityManager em = getEntityManager();
/*     */     try {
/* 177 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 178 */       cq.select(cq.from(User.class));
/* 179 */       Query q = em.createQuery(cq);
/* 180 */       if (!all) {
/* 181 */         q.setMaxResults(maxResults);
/* 182 */         q.setFirstResult(firstResult);
/*     */       }
/* 184 */       return q.getResultList();
/*     */     } finally {
/* 186 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public User findUser(Integer id) {
/* 191 */     EntityManager em = getEntityManager();
/*     */     try {
/* 193 */       return (User)em.find(User.class, id);
/*     */     } finally {
/* 195 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getUserCount() {
/* 200 */     EntityManager em = getEntityManager();
/*     */     try {
/* 202 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 203 */       Root<User> rt = cq.from(User.class);
/* 204 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 205 */       Query q = em.createQuery(cq);
/* 206 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 208 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public User login(String user, String password) {
/* 213 */     User u = null;
/* 214 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 217 */       Query query = em.createQuery("select o from User o where o.username = :username and o.password = :password");
/* 218 */       query.setParameter("username", user);
/* 219 */       query.setParameter("password", password);
/*     */       
/* 221 */       List<User> users = query.getResultList();
/* 222 */       if (users.size() == 1) {
/* 223 */         u = (User)users.get(0);
/*     */       }
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 228 */       u = null;
/*     */     }
/*     */     catch (Exception e) {
/* 231 */       e.printStackTrace();
/*     */     } finally {
/* 233 */       em.close();
/*     */     }
/* 235 */     return u;
/*     */   }
/*     */   
/*     */   public User findUserByUsername(String username)
/*     */   {
/* 240 */     User u = null;
/* 241 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 244 */       Query query = em.createNamedQuery("User.findByUsername");
/* 245 */       query.setParameter("username", username);
/*     */       
/* 247 */       u = (User)query.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 251 */       u = null;
/*     */     }
/*     */     catch (Exception e) {
/* 254 */       LoggingManager.getInstance().log_error(e);
/*     */     } finally {
/* 256 */       em.close();
/*     */     }
/* 258 */     return u;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/UserJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */