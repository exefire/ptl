/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetallePicking;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.util.Collection;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DetallePickingJpaController
/*     */   implements Serializable
/*     */ {
/*  32 */   private EntityManagerFactory emf = null;
/*     */   private static DetallePickingJpaController instance;
/*     */   
/*     */   protected DetallePickingJpaController()
/*     */   {
/*  37 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static DetallePickingJpaController getInstance() {
/*  41 */     if (instance == null) {
/*  42 */       synchronized (DetallePickingJpaController.class) {
/*  43 */         if (instance == null) {
/*  44 */           instance = new DetallePickingJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  48 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public EntityManager getEntityManager()
/*     */   {
/*  55 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   public void create(DetallePicking detallePicking) {
/*  59 */     EntityManager em = null;
/*     */     try {
/*  61 */       em = getEntityManager();
/*  62 */       em.getTransaction().begin();
/*  63 */       Picking pickingId = detallePicking.getPickingId();
/*  64 */       if (pickingId != null) {
/*  65 */         pickingId = (Picking)em.getReference(pickingId.getClass(), pickingId.getId());
/*  66 */         detallePicking.setPickingId(pickingId);
/*     */       }
/*  68 */       Destino destinoId = detallePicking.getDestinoId();
/*  69 */       if (destinoId != null) {
/*  70 */         destinoId = (Destino)em.getReference(destinoId.getClass(), destinoId.getId());
/*  71 */         detallePicking.setDestinoId(destinoId);
/*     */       }
/*  73 */       em.persist(detallePicking);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*  78 */       if (destinoId != null) {
/*  79 */         destinoId.getDetallePickingCollection().add(detallePicking);
/*  80 */         destinoId = (Destino)em.merge(destinoId);
/*     */       }
/*  82 */       em.getTransaction().commit();
/*     */     } finally {
/*  84 */       if (em != null) {
/*  85 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void edit(DetallePicking detallePicking) throws NonexistentEntityException, Exception {
/*  91 */     EntityManager em = null;
/*     */     try {
/*  93 */       em = getEntityManager();
/*  94 */       em.getTransaction().begin();
/*  95 */       DetallePicking persistentDetallePicking = (DetallePicking)em.find(DetallePicking.class, detallePicking.getId());
/*  96 */       Picking pickingIdOld = persistentDetallePicking.getPickingId();
/*  97 */       Picking pickingIdNew = detallePicking.getPickingId();
/*  98 */       Destino destinoIdOld = persistentDetallePicking.getDestinoId();
/*  99 */       Destino destinoIdNew = detallePicking.getDestinoId();
/* 100 */       if (pickingIdNew != null) {
/* 101 */         pickingIdNew = (Picking)em.getReference(pickingIdNew.getClass(), pickingIdNew.getId());
/* 102 */         detallePicking.setPickingId(pickingIdNew);
/*     */       }
/* 104 */       if (destinoIdNew != null) {
/* 105 */         destinoIdNew = (Destino)em.getReference(destinoIdNew.getClass(), destinoIdNew.getId());
/* 106 */         detallePicking.setDestinoId(destinoIdNew);
/*     */       }
/* 108 */       detallePicking = (DetallePicking)em.merge(detallePicking);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 117 */       if ((destinoIdOld != null) && (!destinoIdOld.equals(destinoIdNew))) {
/* 118 */         destinoIdOld.getDetallePickingCollection().remove(detallePicking);
/* 119 */         destinoIdOld = (Destino)em.merge(destinoIdOld);
/*     */       }
/* 121 */       if ((destinoIdNew != null) && (!destinoIdNew.equals(destinoIdOld))) {
/* 122 */         destinoIdNew.getDetallePickingCollection().add(detallePicking);
/* 123 */         destinoIdNew = (Destino)em.merge(destinoIdNew);
/*     */       }
/* 125 */       em.getTransaction().commit();
/*     */     } catch (Exception ex) {
/* 127 */       String msg = ex.getLocalizedMessage();
/* 128 */       if ((msg == null) || (msg.length() == 0)) {
/* 129 */         Integer id = detallePicking.getId();
/* 130 */         if (findDetallePicking(id) == null) {
/* 131 */           throw new NonexistentEntityException("The detallePicking with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/* 134 */       throw ex;
/*     */     } finally {
/* 136 */       if (em != null) {
/* 137 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws NonexistentEntityException {
/* 143 */     EntityManager em = null;
/*     */     try {
/* 145 */       em = getEntityManager();
/* 146 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/* 149 */         DetallePicking detallePicking = (DetallePicking)em.getReference(DetallePicking.class, id);
/* 150 */         detallePicking.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 152 */         throw new NonexistentEntityException("The detallePicking with id " + id + " no longer exists.", enfe);
/*     */       }
/*     */       
/*     */ 
/*     */       DetallePicking detallePicking;
/*     */       
/*     */ 
/* 159 */       Destino destinoId = detallePicking.getDestinoId();
/* 160 */       if (destinoId != null) {
/* 161 */         destinoId.getDetallePickingCollection().remove(detallePicking);
/* 162 */         destinoId = (Destino)em.merge(destinoId);
/*     */       }
/* 164 */       em.remove(detallePicking);
/* 165 */       em.getTransaction().commit();
/*     */     } finally {
/* 167 */       if (em != null) {
/* 168 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<DetallePicking> findDetallePickingEntities() {
/* 174 */     return findDetallePickingEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<DetallePicking> findDetallePickingEntities(int maxResults, int firstResult) {
/* 178 */     return findDetallePickingEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<DetallePicking> findDetallePickingEntities(boolean all, int maxResults, int firstResult) {
/* 182 */     EntityManager em = getEntityManager();
/*     */     try {
/* 184 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 185 */       cq.select(cq.from(DetallePicking.class));
/* 186 */       Query q = em.createQuery(cq);
/* 187 */       if (!all) {
/* 188 */         q.setMaxResults(maxResults);
/* 189 */         q.setFirstResult(firstResult);
/*     */       }
/* 191 */       return q.getResultList();
/*     */     } finally {
/* 193 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public DetallePicking findDetallePicking(Integer id) {
/* 198 */     EntityManager em = getEntityManager();
/*     */     try {
/* 200 */       return (DetallePicking)em.find(DetallePicking.class, id);
/*     */     } finally {
/* 202 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getDetallePickingCount() {
/* 207 */     EntityManager em = getEntityManager();
/*     */     try {
/* 209 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 210 */       Root<DetallePicking> rt = cq.from(DetallePicking.class);
/* 211 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 212 */       Query q = em.createQuery(cq);
/* 213 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 215 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public List<DetallePicking> findByPickingAndArticulo(Picking p, String art)
/*     */   {
/* 221 */     List<DetallePicking> lista = null;
/*     */     
/* 223 */     EntityManager em = getEntityManager();
/*     */     
/*     */     try
/*     */     {
/* 227 */       Query q = em.createNamedQuery("DetallePicking.findByPickingAndArticulo", DetallePicking.class);
/* 228 */       q.setParameter("picking", p.getId());
/* 229 */       q.setParameter("articulo", art);
/*     */       
/* 231 */       lista = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 235 */       lista = null;
/*     */     } catch (Exception e) {
/* 237 */       LoggingManager.getInstance().log_error(e);
/* 238 */       lista = null;
/*     */     }
/*     */     finally {
/* 241 */       em.close();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 248 */     return lista;
/*     */   }
/*     */   
/*     */   public List<DetallePicking> findByPickingAndDestinoAndArticulo(Picking p, Destino d, String art)
/*     */   {
/* 253 */     List<DetallePicking> lista = null;
/*     */     
/* 255 */     EntityManager em = getEntityManager();
/*     */     
/*     */     try
/*     */     {
/* 259 */       Query q = em.createNamedQuery("DetallePicking.findByPickingAndDestinoAndArticulo", DetallePicking.class);
/* 260 */       q.setParameter("picking", p.getId());
/* 261 */       q.setParameter("destino", d.getId());
/* 262 */       q.setParameter("articulo", art);
/*     */       
/* 264 */       lista = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 268 */       lista = null;
/*     */     } catch (Exception e) {
/* 270 */       LoggingManager.getInstance().log_error(e);
/* 271 */       lista = null;
/*     */     }
/*     */     finally {
/* 274 */       em.close();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 281 */     return lista;
/*     */   }
/*     */   
/*     */   public List<Object> findArticuloPickingDestino(Picking p, String articulo)
/*     */   {
/* 286 */     List<Object> l = null;
/*     */     
/* 288 */     EntityManager em = getEntityManager();
/*     */     
/* 290 */     String query = "SELECT picking_id,destino_id,articulo, cast(sum(aconsolidar) as unsigned) as aconsolidar , cast(sum(consolidado) as unsigned) as consolidado FROM detalle_picking where picking_id= " + p.getId() + " and articulo = '" + articulo + "' group by picking_id,destino_id,articulo  having (sum(aconsolidar) - sum(consolidado)) > 0";
/*     */     
/*     */     try
/*     */     {
/* 294 */       Query q = em.createNativeQuery(query);
/*     */       
/*     */ 
/* 297 */       l = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 301 */       l = null;
/*     */     } catch (Exception e) {
/* 303 */       LoggingManager.getInstance().log_error(e);
/* 304 */       l = null;
/*     */     }
/*     */     finally {
/* 307 */       em.close();
/*     */     }
/*     */     
/* 310 */     return l;
/*     */   }
/*     */   
/*     */   public Collection<DetallePicking> getDetallePickingCollection(Picking p)
/*     */   {
/* 315 */     Collection<DetallePicking> lista = null;
/*     */     
/* 317 */     EntityManager em = getEntityManager();
/*     */     
/*     */     try
/*     */     {
/* 321 */       Query q = em.createNativeQuery("select * from detalle_picking where picking_id = " + p.getId(), DetallePicking.class);
/*     */       
/*     */ 
/* 324 */       lista = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 328 */       lista = null;
/*     */     } catch (Exception e) {
/* 330 */       LoggingManager.getInstance().log_error(e);
/* 331 */       lista = null;
/*     */     }
/*     */     finally {
/* 334 */       em.close();
/*     */     }
/*     */     
/* 337 */     return lista;
/*     */   }
/*     */   
/*     */ 
/*     */   public void deleteAllFromPicking(Integer id)
/*     */   {
/* 343 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 346 */       em.getTransaction().begin();
/* 347 */       Query q = em.createNativeQuery("delete FROM detalle_picking where picking_id = " + id + " and id > 0");
/*     */       
/* 349 */       q.executeUpdate();
/* 350 */       em.getTransaction().commit();
/*     */     } catch (NoResultException localNoResultException) {
/* 352 */       localNoResultException = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 359 */         localNoResultException;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 354 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 359 */         ex;LoggingManager.getInstance().log_error(ex);
/*     */     } finally {}
/*     */   }
/*     */   
/*     */   public List<Object> getTotalesByPicking(String picking) {
/* 364 */     EntityManager em = getEntityManager();
/*     */     
/* 366 */     List<Object> resultado = null;
/*     */     try
/*     */     {
/* 369 */       String query = "select referencia,sum(cantidad) as cantidad ,sum(consolidado) as consolidado from detalle_picking where referencia = '" + picking + "' group by 1";
/*     */       
/* 371 */       Query q = em.createNativeQuery(query);
/*     */       
/* 373 */       resultado = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 375 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 382 */         ex;resultado = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 377 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 382 */         ex;resultado = null;LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */     finally {}
/*     */     
/* 386 */     return resultado;
/*     */   }
/*     */   
/*     */   public List<Object> getResumeListByPicking(String codigoBulto)
/*     */   {
/* 391 */     EntityManager em = getEntityManager();
/*     */     
/* 393 */     List<Object> resultado = null;
/*     */     try
/*     */     {
/* 396 */       String query = "select referencia,articulo,sum(cantidad) as cantidad ,sum(consolidado) as consolidado from detalle_picking where referencia = '" + codigoBulto + "' group by 1,2";
/*     */       
/* 398 */       Query q = em.createNativeQuery(query);
/*     */       
/* 400 */       resultado = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 402 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 409 */         ex;resultado = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 404 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 409 */         ex;resultado = null;LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */     finally {}
/*     */     
/* 413 */     return resultado;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/DetallePickingJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */