/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetallePicking;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.exceptions.IllegalOrphanException;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DestinoJpaController
/*     */   implements Serializable
/*     */ {
/*  35 */   private EntityManagerFactory emf = null;
/*     */   private static DestinoJpaController instance;
/*     */   
/*     */   protected DestinoJpaController()
/*     */   {
/*  40 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static DestinoJpaController getInstance() {
/*  44 */     if (instance == null) {
/*  45 */       synchronized (DestinoJpaController.class) {
/*  46 */         if (instance == null) {
/*  47 */           instance = new DestinoJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  51 */     return instance;
/*     */   }
/*     */   
/*     */   public EntityManager getEntityManager() {
/*  55 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   public void create(Destino destino) {
/*  59 */     if (destino.getCajaCollection() == null) {
/*  60 */       destino.setCajaCollection(new ArrayList());
/*     */     }
/*  62 */     if (destino.getDetallePickingCollection() == null) {
/*  63 */       destino.setDetallePickingCollection(new ArrayList());
/*     */     }
/*  65 */     EntityManager em = null;
/*     */     try {
/*  67 */       em = getEntityManager();
/*  68 */       em.getTransaction().begin();
/*  69 */       Collection<Caja> attachedCajaCollection = new ArrayList();
/*  70 */       for (Iterator localIterator = destino.getCajaCollection().iterator(); localIterator.hasNext();) { cajaCollectionCajaToAttach = (Caja)localIterator.next();
/*  71 */         cajaCollectionCajaToAttach = (Caja)em.getReference(cajaCollectionCajaToAttach.getClass(), cajaCollectionCajaToAttach.getId());
/*  72 */         attachedCajaCollection.add(cajaCollectionCajaToAttach); }
/*     */       Caja cajaCollectionCajaToAttach;
/*  74 */       destino.setCajaCollection(attachedCajaCollection);
/*  75 */       Object attachedDetallePickingCollection = new ArrayList();
/*  76 */       for (DetallePicking detallePickingCollectionDetallePickingToAttach : destino.getDetallePickingCollection()) {
/*  77 */         detallePickingCollectionDetallePickingToAttach = (DetallePicking)em.getReference(detallePickingCollectionDetallePickingToAttach.getClass(), detallePickingCollectionDetallePickingToAttach.getId());
/*  78 */         ((Collection)attachedDetallePickingCollection).add(detallePickingCollectionDetallePickingToAttach);
/*     */       }
/*  80 */       destino.setDetallePickingCollection((Collection)attachedDetallePickingCollection);
/*  81 */       em.persist(destino);
/*  82 */       for (Caja cajaCollectionCaja : destino.getCajaCollection()) {
/*  83 */         Destino oldDestinoIdOfCajaCollectionCaja = cajaCollectionCaja.getDestinoId();
/*  84 */         cajaCollectionCaja.setDestinoId(destino);
/*  85 */         cajaCollectionCaja = (Caja)em.merge(cajaCollectionCaja);
/*  86 */         if (oldDestinoIdOfCajaCollectionCaja != null) {
/*  87 */           oldDestinoIdOfCajaCollectionCaja.getCajaCollection().remove(cajaCollectionCaja);
/*  88 */           oldDestinoIdOfCajaCollectionCaja = (Destino)em.merge(oldDestinoIdOfCajaCollectionCaja);
/*     */         }
/*     */       }
/*  91 */       for (DetallePicking detallePickingCollectionDetallePicking : destino.getDetallePickingCollection()) {
/*  92 */         Destino oldDestinoIdOfDetallePickingCollectionDetallePicking = detallePickingCollectionDetallePicking.getDestinoId();
/*  93 */         detallePickingCollectionDetallePicking.setDestinoId(destino);
/*  94 */         detallePickingCollectionDetallePicking = (DetallePicking)em.merge(detallePickingCollectionDetallePicking);
/*  95 */         if (oldDestinoIdOfDetallePickingCollectionDetallePicking != null) {
/*  96 */           oldDestinoIdOfDetallePickingCollectionDetallePicking.getDetallePickingCollection().remove(detallePickingCollectionDetallePicking);
/*  97 */           oldDestinoIdOfDetallePickingCollectionDetallePicking = (Destino)em.merge(oldDestinoIdOfDetallePickingCollectionDetallePicking);
/*     */         }
/*     */       }
/* 100 */       em.getTransaction().commit();
/*     */     } finally {
/* 102 */       if (em != null) {
/* 103 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void edit(Destino destino) throws IllegalOrphanException, NonexistentEntityException, Exception {
/* 109 */     EntityManager em = null;
/*     */     try {
/* 111 */       em = getEntityManager();
/* 112 */       em.getTransaction().begin();
/* 113 */       Destino persistentDestino = (Destino)em.find(Destino.class, destino.getId());
/* 114 */       Collection<Caja> cajaCollectionOld = persistentDestino.getCajaCollection();
/* 115 */       Collection<Caja> cajaCollectionNew = destino.getCajaCollection();
/* 116 */       Collection<DetallePicking> detallePickingCollectionOld = persistentDestino.getDetallePickingCollection();
/* 117 */       Collection<DetallePicking> detallePickingCollectionNew = destino.getDetallePickingCollection();
/* 118 */       List<String> illegalOrphanMessages = null;
/* 119 */       for (Iterator localIterator = cajaCollectionOld.iterator(); localIterator.hasNext();) { cajaCollectionOldCaja = (Caja)localIterator.next();
/* 120 */         if (!cajaCollectionNew.contains(cajaCollectionOldCaja)) {
/* 121 */           if (illegalOrphanMessages == null) {
/* 122 */             illegalOrphanMessages = new ArrayList();
/*     */           }
/* 124 */           illegalOrphanMessages.add("You must retain Caja " + cajaCollectionOldCaja + " since its destinoId field is not nullable.");
/*     */         }
/*     */       }
/* 127 */       if (illegalOrphanMessages != null) {
/* 128 */         throw new IllegalOrphanException(illegalOrphanMessages);
/*     */       }
/* 130 */       Object attachedCajaCollectionNew = new ArrayList();
/* 131 */       for (Caja cajaCollectionOldCaja = cajaCollectionNew.iterator(); cajaCollectionOldCaja.hasNext();) { cajaCollectionNewCajaToAttach = (Caja)cajaCollectionOldCaja.next();
/* 132 */         cajaCollectionNewCajaToAttach = (Caja)em.getReference(cajaCollectionNewCajaToAttach.getClass(), cajaCollectionNewCajaToAttach.getId());
/* 133 */         ((Collection)attachedCajaCollectionNew).add(cajaCollectionNewCajaToAttach); }
/*     */       Caja cajaCollectionNewCajaToAttach;
/* 135 */       cajaCollectionNew = (Collection<Caja>)attachedCajaCollectionNew;
/* 136 */       destino.setCajaCollection(cajaCollectionNew);
/* 137 */       Collection<DetallePicking> attachedDetallePickingCollectionNew = new ArrayList();
/* 138 */       for (DetallePicking detallePickingCollectionNewDetallePickingToAttach : detallePickingCollectionNew) {
/* 139 */         detallePickingCollectionNewDetallePickingToAttach = (DetallePicking)em.getReference(detallePickingCollectionNewDetallePickingToAttach.getClass(), detallePickingCollectionNewDetallePickingToAttach.getId());
/* 140 */         attachedDetallePickingCollectionNew.add(detallePickingCollectionNewDetallePickingToAttach);
/*     */       }
/* 142 */       detallePickingCollectionNew = attachedDetallePickingCollectionNew;
/* 143 */       destino.setDetallePickingCollection(detallePickingCollectionNew);
/* 144 */       destino = (Destino)em.merge(destino);
/* 145 */       for (Caja cajaCollectionNewCaja : cajaCollectionNew) {
/* 146 */         if (!cajaCollectionOld.contains(cajaCollectionNewCaja)) {
/* 147 */           Destino oldDestinoIdOfCajaCollectionNewCaja = cajaCollectionNewCaja.getDestinoId();
/* 148 */           cajaCollectionNewCaja.setDestinoId(destino);
/* 149 */           cajaCollectionNewCaja = (Caja)em.merge(cajaCollectionNewCaja);
/* 150 */           if ((oldDestinoIdOfCajaCollectionNewCaja != null) && (!oldDestinoIdOfCajaCollectionNewCaja.equals(destino))) {
/* 151 */             oldDestinoIdOfCajaCollectionNewCaja.getCajaCollection().remove(cajaCollectionNewCaja);
/* 152 */             oldDestinoIdOfCajaCollectionNewCaja = (Destino)em.merge(oldDestinoIdOfCajaCollectionNewCaja);
/*     */           }
/*     */         }
/*     */       }
/* 156 */       for (DetallePicking detallePickingCollectionOldDetallePicking : detallePickingCollectionOld) {
/* 157 */         if (!detallePickingCollectionNew.contains(detallePickingCollectionOldDetallePicking)) {
/* 158 */           detallePickingCollectionOldDetallePicking.setDestinoId(null);
/* 159 */           detallePickingCollectionOldDetallePicking = (DetallePicking)em.merge(detallePickingCollectionOldDetallePicking);
/*     */         }
/*     */       }
/* 162 */       for (DetallePicking detallePickingCollectionNewDetallePicking : detallePickingCollectionNew) {
/* 163 */         if (!detallePickingCollectionOld.contains(detallePickingCollectionNewDetallePicking)) {
/* 164 */           Destino oldDestinoIdOfDetallePickingCollectionNewDetallePicking = detallePickingCollectionNewDetallePicking.getDestinoId();
/* 165 */           detallePickingCollectionNewDetallePicking.setDestinoId(destino);
/* 166 */           detallePickingCollectionNewDetallePicking = (DetallePicking)em.merge(detallePickingCollectionNewDetallePicking);
/* 167 */           if ((oldDestinoIdOfDetallePickingCollectionNewDetallePicking != null) && (!oldDestinoIdOfDetallePickingCollectionNewDetallePicking.equals(destino))) {
/* 168 */             oldDestinoIdOfDetallePickingCollectionNewDetallePicking.getDetallePickingCollection().remove(detallePickingCollectionNewDetallePicking);
/* 169 */             oldDestinoIdOfDetallePickingCollectionNewDetallePicking = (Destino)em.merge(oldDestinoIdOfDetallePickingCollectionNewDetallePicking);
/*     */           }
/*     */         }
/*     */       }
/* 173 */       em.getTransaction().commit();
/*     */     } catch (Exception ex) {
/* 175 */       String msg = ex.getLocalizedMessage();
/* 176 */       if ((msg == null) || (msg.length() == 0)) {
/* 177 */         Integer id = destino.getId();
/* 178 */         if (findDestino(id) == null) {
/* 179 */           throw new NonexistentEntityException("The destino with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/* 182 */       throw ex;
/*     */     } finally {
/* 184 */       if (em != null) {
/* 185 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
/* 191 */     EntityManager em = null;
/*     */     try {
/* 193 */       em = getEntityManager();
/* 194 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/* 197 */         Destino destino = (Destino)em.getReference(Destino.class, id);
/* 198 */         destino.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 200 */         throw new NonexistentEntityException("The destino with id " + id + " no longer exists.", enfe); }
/*     */       Destino destino;
/* 202 */       List<String> illegalOrphanMessages = null;
/* 203 */       Collection<Caja> cajaCollectionOrphanCheck = destino.getCajaCollection();
/* 204 */       for (Iterator localIterator = cajaCollectionOrphanCheck.iterator(); localIterator.hasNext();) { cajaCollectionOrphanCheckCaja = (Caja)localIterator.next();
/* 205 */         if (illegalOrphanMessages == null) {
/* 206 */           illegalOrphanMessages = new ArrayList();
/*     */         }
/* 208 */         illegalOrphanMessages.add("This Destino (" + destino + ") cannot be destroyed since the Caja " + cajaCollectionOrphanCheckCaja + " in its cajaCollection field has a non-nullable destinoId field."); }
/*     */       Caja cajaCollectionOrphanCheckCaja;
/* 210 */       if (illegalOrphanMessages != null) {
/* 211 */         throw new IllegalOrphanException(illegalOrphanMessages);
/*     */       }
/* 213 */       Object detallePickingCollection = destino.getDetallePickingCollection();
/* 214 */       for (DetallePicking detallePickingCollectionDetallePicking : (Collection)detallePickingCollection) {
/* 215 */         detallePickingCollectionDetallePicking.setDestinoId(null);
/* 216 */         detallePickingCollectionDetallePicking = (DetallePicking)em.merge(detallePickingCollectionDetallePicking);
/*     */       }
/* 218 */       em.remove(destino);
/* 219 */       em.getTransaction().commit();
/*     */     } finally {
/* 221 */       if (em != null) {
/* 222 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<Destino> findDestinoEntities() {
/* 228 */     return findDestinoEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<Destino> findDestinoEntities(int maxResults, int firstResult) {
/* 232 */     return findDestinoEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<Destino> findDestinoEntities(boolean all, int maxResults, int firstResult) {
/* 236 */     EntityManager em = getEntityManager();
/*     */     try {
/* 238 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 239 */       cq.select(cq.from(Destino.class));
/* 240 */       Query q = em.createQuery(cq);
/* 241 */       if (!all) {
/* 242 */         q.setMaxResults(maxResults);
/* 243 */         q.setFirstResult(firstResult);
/*     */       }
/* 245 */       return q.getResultList();
/*     */     } finally {
/* 247 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Destino findDestino(Integer id) {
/* 252 */     EntityManager em = getEntityManager();
/*     */     try {
/* 254 */       return (Destino)em.find(Destino.class, id);
/*     */     } finally {
/* 256 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getDestinoCount() {
/* 261 */     EntityManager em = getEntityManager();
/*     */     try {
/* 263 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 264 */       Root<Destino> rt = cq.from(Destino.class);
/* 265 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 266 */       Query q = em.createQuery(cq);
/* 267 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 269 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public List<Destino> getDestinosPickingByOla(Picking p)
/*     */   {
/* 277 */     List<Destino> lista = null;
/* 278 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 281 */       Query q = em.createNativeQuery("select distinct d.* from destino d inner join detalle_picking p on (d.id = p.destino_id) where p.picking_id = " + p.getId().toString() + " order by d.ranking asc", Destino.class);
/* 282 */       lista = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex) {
/* 285 */       lista = null;
/*     */     } catch (Exception e) {
/* 287 */       LoggingManager.getInstance().log_error(e);
/*     */     } finally {
/* 289 */       em.close();
/*     */     }
/*     */     
/* 292 */     return lista;
/*     */   }
/*     */   
/*     */ 
/*     */   public List<Destino> getDestinosPickingByOlaAndNluces(Picking p, Integer nluces)
/*     */   {
/* 298 */     List<Destino> lista = null;
/* 299 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 302 */       Query q = em.createNativeQuery("select distinct d.* from destino d inner join detalle_picking p on (d.id = p.destino_id) where p.picking_id = " + p.getId().toString() + " group by d.nombre order by d.ranking asc, sum(p.aconsolidar - p.consolidado) desc limit " + nluces.toString(), Destino.class);
/* 303 */       lista = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex) {
/* 306 */       lista = null;
/*     */     } catch (Exception e) {
/* 308 */       LoggingManager.getInstance().log_error(e);
/*     */     } finally {
/* 310 */       em.close();
/*     */     }
/*     */     
/* 313 */     return lista;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer countDestinosPickingByOla(Picking p)
/*     */   {
/* 320 */     Integer in = Integer.valueOf(0);
/* 321 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 324 */       Query q = em.createNativeQuery("select count(distinct d.id) from destino d inner join detalle_picking p on (d.id = p.destino_id) where p.picking_id = " + p.getId().toString() + " order by d.ranking asc");
/* 325 */       in = Integer.valueOf(((Long)q.getSingleResult()).intValue());
/*     */     }
/*     */     catch (NoResultException ex) {
/* 328 */       in = null;
/*     */     } catch (Exception e) {
/* 330 */       LoggingManager.getInstance().log_error(e);
/*     */     } finally {
/* 332 */       em.close();
/*     */     }
/*     */     
/* 335 */     return in;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Destino findDEstinoByName(String n)
/*     */   {
/* 342 */     Destino d = null;
/* 343 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 346 */       Query q = em.createNamedQuery("Destino.findByNombre", Destino.class);
/* 347 */       q.setParameter("nombre", n);
/*     */       
/*     */ 
/* 350 */       d = (Destino)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 354 */       d = null;
/*     */     } catch (Exception e) {
/* 356 */       LoggingManager.getInstance().log_error(e);
/* 357 */       d = null;
/*     */     } finally {
/* 359 */       em.close();
/*     */     }
/*     */     
/* 362 */     return d;
/*     */   }
/*     */   
/*     */ 
/*     */   public Destino findDestinoByNombre(String name)
/*     */   {
/* 368 */     Destino d = null;
/* 369 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 372 */       Query q = em.createNamedQuery("Destino.findByNombre", Destino.class);
/* 373 */       q.setParameter("nombre", name);
/*     */       
/* 375 */       d = (Destino)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 379 */       d = null;
/*     */     } catch (Exception e) {
/* 381 */       LoggingManager.getInstance().log_error(e);
/* 382 */       d = null;
/*     */     } finally {
/* 384 */       em.close();
/*     */     }
/*     */     
/* 387 */     return d;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public List<Destino> findDestinoEntitiesOrderByRank()
/*     */   {
/* 394 */     List<Destino> d = null;
/*     */     
/* 396 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 399 */       Query q = em.createNamedQuery("Destino.findAll", Destino.class);
/*     */       
/*     */ 
/* 402 */       d = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 406 */       d = null;
/*     */     } catch (Exception e) {
/* 408 */       LoggingManager.getInstance().log_error(e);
/* 409 */       d = null;
/*     */     } finally {
/* 411 */       em.close();
/*     */     }
/*     */     
/* 414 */     return d;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/DestinoJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */