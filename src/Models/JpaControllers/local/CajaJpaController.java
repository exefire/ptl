/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.Entities.local.DetalleCaja;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.exceptions.IllegalOrphanException;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CajaJpaController
/*     */   implements Serializable
/*     */ {
/*  35 */   private EntityManagerFactory emf = null;
/*     */   private static CajaJpaController instance;
/*     */   
/*     */   protected CajaJpaController()
/*     */   {
/*  40 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static CajaJpaController getInstance() {
/*  44 */     if (instance == null) {
/*  45 */       synchronized (CajaJpaController.class) {
/*  46 */         if (instance == null) {
/*  47 */           instance = new CajaJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  51 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public EntityManager getEntityManager()
/*     */   {
/*  58 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   public void create(Caja caja) {
/*  62 */     if (caja.getDetalleCajaCollection() == null) {
/*  63 */       caja.setDetalleCajaCollection(new ArrayList());
/*     */     }
/*  65 */     EntityManager em = null;
/*     */     try {
/*  67 */       em = getEntityManager();
/*  68 */       em.getTransaction().begin();
/*  69 */       Picking pickingId = caja.getPickingId();
/*  70 */       if (pickingId != null) {
/*  71 */         pickingId = (Picking)em.getReference(pickingId.getClass(), pickingId.getId());
/*  72 */         caja.setPickingId(pickingId);
/*     */       }
/*  74 */       Destino destinoId = caja.getDestinoId();
/*  75 */       if (destinoId != null) {
/*  76 */         destinoId = (Destino)em.getReference(destinoId.getClass(), destinoId.getId());
/*  77 */         caja.setDestinoId(destinoId);
/*     */       }
/*  79 */       Collection<DetalleCaja> attachedDetalleCajaCollection = new ArrayList();
/*  80 */       for (DetalleCaja detalleCajaCollectionDetalleCajaToAttach : caja.getDetalleCajaCollection()) {
/*  81 */         detalleCajaCollectionDetalleCajaToAttach = (DetalleCaja)em.getReference(detalleCajaCollectionDetalleCajaToAttach.getClass(), detalleCajaCollectionDetalleCajaToAttach.getId());
/*  82 */         attachedDetalleCajaCollection.add(detalleCajaCollectionDetalleCajaToAttach);
/*     */       }
/*  84 */       caja.setDetalleCajaCollection(attachedDetalleCajaCollection);
/*  85 */       em.persist(caja);
/*  86 */       if (pickingId != null) {
/*  87 */         pickingId.getCajaCollection().add(caja);
/*  88 */         pickingId = (Picking)em.merge(pickingId);
/*     */       }
/*  90 */       if (destinoId != null) {
/*  91 */         destinoId.getCajaCollection().add(caja);
/*  92 */         destinoId = (Destino)em.merge(destinoId);
/*     */       }
/*  94 */       for (DetalleCaja detalleCajaCollectionDetalleCaja : caja.getDetalleCajaCollection()) {
/*  95 */         Caja oldCajaIdOfDetalleCajaCollectionDetalleCaja = detalleCajaCollectionDetalleCaja.getCajaId();
/*  96 */         detalleCajaCollectionDetalleCaja.setCajaId(caja);
/*  97 */         detalleCajaCollectionDetalleCaja = (DetalleCaja)em.merge(detalleCajaCollectionDetalleCaja);
/*  98 */         if (oldCajaIdOfDetalleCajaCollectionDetalleCaja != null) {
/*  99 */           oldCajaIdOfDetalleCajaCollectionDetalleCaja.getDetalleCajaCollection().remove(detalleCajaCollectionDetalleCaja);
/* 100 */           oldCajaIdOfDetalleCajaCollectionDetalleCaja = (Caja)em.merge(oldCajaIdOfDetalleCajaCollectionDetalleCaja);
/*     */         }
/*     */       }
/* 103 */       em.getTransaction().commit();
/*     */     } finally {
/* 105 */       if (em != null) {
/* 106 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void edit(Caja caja) throws IllegalOrphanException, NonexistentEntityException, Exception {
/* 112 */     EntityManager em = null;
/*     */     try {
/* 114 */       em = getEntityManager();
/* 115 */       em.getTransaction().begin();
/* 116 */       Caja persistentCaja = (Caja)em.find(Caja.class, caja.getId());
/* 117 */       Picking pickingIdOld = persistentCaja.getPickingId();
/* 118 */       Picking pickingIdNew = caja.getPickingId();
/* 119 */       Destino destinoIdOld = persistentCaja.getDestinoId();
/* 120 */       Destino destinoIdNew = caja.getDestinoId();
/* 121 */       Collection<DetalleCaja> detalleCajaCollectionOld = persistentCaja.getDetalleCajaCollection();
/* 122 */       Collection<DetalleCaja> detalleCajaCollectionNew = caja.getDetalleCajaCollection();
/* 123 */       List<String> illegalOrphanMessages = null;
/* 124 */       for (Iterator localIterator = detalleCajaCollectionOld.iterator(); localIterator.hasNext();) { detalleCajaCollectionOldDetalleCaja = (DetalleCaja)localIterator.next();
/* 125 */         if (!detalleCajaCollectionNew.contains(detalleCajaCollectionOldDetalleCaja)) {
/* 126 */           if (illegalOrphanMessages == null) {
/* 127 */             illegalOrphanMessages = new ArrayList();
/*     */           }
/* 129 */           illegalOrphanMessages.add("You must retain DetalleCaja " + detalleCajaCollectionOldDetalleCaja + " since its cajaId field is not nullable.");
/*     */         } }
/*     */       DetalleCaja detalleCajaCollectionOldDetalleCaja;
/* 132 */       if (illegalOrphanMessages != null) {
/* 133 */         throw new IllegalOrphanException(illegalOrphanMessages);
/*     */       }
/* 135 */       if (pickingIdNew != null) {
/* 136 */         pickingIdNew = (Picking)em.getReference(pickingIdNew.getClass(), pickingIdNew.getId());
/* 137 */         caja.setPickingId(pickingIdNew);
/*     */       }
/* 139 */       if (destinoIdNew != null) {
/* 140 */         destinoIdNew = (Destino)em.getReference(destinoIdNew.getClass(), destinoIdNew.getId());
/* 141 */         caja.setDestinoId(destinoIdNew);
/*     */       }
/* 143 */       Object attachedDetalleCajaCollectionNew = new ArrayList();
/* 144 */       for (DetalleCaja detalleCajaCollectionNewDetalleCajaToAttach : detalleCajaCollectionNew) {
/* 145 */         detalleCajaCollectionNewDetalleCajaToAttach = (DetalleCaja)em.getReference(detalleCajaCollectionNewDetalleCajaToAttach.getClass(), detalleCajaCollectionNewDetalleCajaToAttach.getId());
/* 146 */         ((Collection)attachedDetalleCajaCollectionNew).add(detalleCajaCollectionNewDetalleCajaToAttach);
/*     */       }
/* 148 */       detalleCajaCollectionNew = (Collection<DetalleCaja>)attachedDetalleCajaCollectionNew;
/* 149 */       caja.setDetalleCajaCollection(detalleCajaCollectionNew);
/* 150 */       caja = (Caja)em.merge(caja);
/* 151 */       if ((pickingIdOld != null) && (!pickingIdOld.equals(pickingIdNew))) {
/* 152 */         pickingIdOld.getCajaCollection().remove(caja);
/* 153 */         pickingIdOld = (Picking)em.merge(pickingIdOld);
/*     */       }
/* 155 */       if ((pickingIdNew != null) && (!pickingIdNew.equals(pickingIdOld))) {
/* 156 */         pickingIdNew.getCajaCollection().add(caja);
/* 157 */         pickingIdNew = (Picking)em.merge(pickingIdNew);
/*     */       }
/* 159 */       if ((destinoIdOld != null) && (!destinoIdOld.equals(destinoIdNew))) {
/* 160 */         destinoIdOld.getCajaCollection().remove(caja);
/* 161 */         destinoIdOld = (Destino)em.merge(destinoIdOld);
/*     */       }
/* 163 */       if ((destinoIdNew != null) && (!destinoIdNew.equals(destinoIdOld))) {
/* 164 */         destinoIdNew.getCajaCollection().add(caja);
/* 165 */         destinoIdNew = (Destino)em.merge(destinoIdNew);
/*     */       }
/* 167 */       for (DetalleCaja detalleCajaCollectionNewDetalleCaja : detalleCajaCollectionNew) {
/* 168 */         if (!detalleCajaCollectionOld.contains(detalleCajaCollectionNewDetalleCaja)) {
/* 169 */           Caja oldCajaIdOfDetalleCajaCollectionNewDetalleCaja = detalleCajaCollectionNewDetalleCaja.getCajaId();
/* 170 */           detalleCajaCollectionNewDetalleCaja.setCajaId(caja);
/* 171 */           detalleCajaCollectionNewDetalleCaja = (DetalleCaja)em.merge(detalleCajaCollectionNewDetalleCaja);
/* 172 */           if ((oldCajaIdOfDetalleCajaCollectionNewDetalleCaja != null) && (!oldCajaIdOfDetalleCajaCollectionNewDetalleCaja.equals(caja))) {
/* 173 */             oldCajaIdOfDetalleCajaCollectionNewDetalleCaja.getDetalleCajaCollection().remove(detalleCajaCollectionNewDetalleCaja);
/* 174 */             oldCajaIdOfDetalleCajaCollectionNewDetalleCaja = (Caja)em.merge(oldCajaIdOfDetalleCajaCollectionNewDetalleCaja);
/*     */           }
/*     */         }
/*     */       }
/* 178 */       em.getTransaction().commit();
/*     */     } catch (Exception ex) {
/* 180 */       String msg = ex.getLocalizedMessage();
/* 181 */       if ((msg == null) || (msg.length() == 0)) {
/* 182 */         Integer id = caja.getId();
/* 183 */         if (findCaja(id) == null) {
/* 184 */           throw new NonexistentEntityException("The caja with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/* 187 */       throw ex;
/*     */     } finally {
/* 189 */       if (em != null) {
/* 190 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
/* 196 */     EntityManager em = null;
/*     */     try {
/* 198 */       em = getEntityManager();
/* 199 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/* 202 */         Caja caja = (Caja)em.getReference(Caja.class, id);
/* 203 */         caja.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 205 */         throw new NonexistentEntityException("The caja with id " + id + " no longer exists.", enfe); }
/*     */       Caja caja;
/* 207 */       List<String> illegalOrphanMessages = null;
/* 208 */       Collection<DetalleCaja> detalleCajaCollectionOrphanCheck = caja.getDetalleCajaCollection();
/* 209 */       for (DetalleCaja detalleCajaCollectionOrphanCheckDetalleCaja : detalleCajaCollectionOrphanCheck) {
/* 210 */         if (illegalOrphanMessages == null) {
/* 211 */           illegalOrphanMessages = new ArrayList();
/*     */         }
/* 213 */         illegalOrphanMessages.add("This Caja (" + caja + ") cannot be destroyed since the DetalleCaja " + detalleCajaCollectionOrphanCheckDetalleCaja + " in its detalleCajaCollection field has a non-nullable cajaId field.");
/*     */       }
/* 215 */       if (illegalOrphanMessages != null) {
/* 216 */         throw new IllegalOrphanException(illegalOrphanMessages);
/*     */       }
/* 218 */       Picking pickingId = caja.getPickingId();
/* 219 */       if (pickingId != null) {
/* 220 */         pickingId.getCajaCollection().remove(caja);
/* 221 */         pickingId = (Picking)em.merge(pickingId);
/*     */       }
/* 223 */       Destino destinoId = caja.getDestinoId();
/* 224 */       if (destinoId != null) {
/* 225 */         destinoId.getCajaCollection().remove(caja);
/* 226 */         destinoId = (Destino)em.merge(destinoId);
/*     */       }
/* 228 */       em.remove(caja);
/* 229 */       em.getTransaction().commit();
/*     */     } finally {
/* 231 */       if (em != null) {
/* 232 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<Caja> findCajaEntities() {
/* 238 */     return findCajaEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<Caja> findCajaEntities(int maxResults, int firstResult) {
/* 242 */     return findCajaEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<Caja> findCajaEntities(boolean all, int maxResults, int firstResult) {
/* 246 */     EntityManager em = getEntityManager();
/*     */     try {
/* 248 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 249 */       cq.select(cq.from(Caja.class));
/* 250 */       Query q = em.createQuery(cq);
/* 251 */       if (!all) {
/* 252 */         q.setMaxResults(maxResults);
/* 253 */         q.setFirstResult(firstResult);
/*     */       }
/* 255 */       return q.getResultList();
/*     */     } finally {
/* 257 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Caja findCaja(Integer id) {
/* 262 */     EntityManager em = getEntityManager();
/*     */     try {
/* 264 */       return (Caja)em.find(Caja.class, id);
/*     */     } finally {
/* 266 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getCajaCount() {
/* 271 */     EntityManager em = getEntityManager();
/*     */     try {
/* 273 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 274 */       Root<Caja> rt = cq.from(Caja.class);
/* 275 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 276 */       Query q = em.createQuery(cq);
/* 277 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 279 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Caja findCajaByCode(String BoxCode)
/*     */   {
/* 287 */     EntityManager em = getEntityManager();
/* 288 */     Caja c = null;
/*     */     try {
/* 290 */       Query q = em.createNamedQuery("Caja.findByCodigo", Caja.class);
/* 291 */       q.setParameter("codigo", BoxCode);
/*     */       
/* 293 */       c = (Caja)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 297 */       c = null;
/*     */     } catch (Exception e) {
/* 299 */       LoggingManager.getInstance().log_error(e);
/* 300 */       c = null;
/*     */     }
/*     */     finally {
/* 303 */       em.close();
/*     */     }
/*     */     
/* 306 */     return c;
/*     */   }
/*     */   
/*     */ 
/*     */   public Caja findCajaAbiertaByPosicion(Integer pos)
/*     */   {
/* 312 */     EntityManager em = getEntityManager();
/* 313 */     Caja c = null;
/*     */     try {
/* 315 */       Query q = em.createNamedQuery("Caja.findByPosicionPtl", Caja.class);
/* 316 */       q.setParameter("posicionPtl", pos);
/*     */       
/* 318 */       c = (Caja)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 322 */       c = null;
/*     */     } catch (Exception e) {
/* 324 */       LoggingManager.getInstance().log_error(e);
/* 325 */       c = null;
/*     */     }
/*     */     finally {
/* 328 */       em.close();
/*     */     }
/*     */     
/* 331 */     return c;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Caja findCajaAbiertaByDestino(Integer destinoId)
/*     */   {
/* 338 */     EntityManager em = getEntityManager();
/* 339 */     Caja c = null;
/*     */     try {
/* 341 */       Query q = em.createNamedQuery("Caja.findAbiertaByDestino", Caja.class);
/* 342 */       q.setParameter("destino", destinoId);
/*     */       
/* 344 */       c = (Caja)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 348 */       c = null;
/*     */     } catch (Exception e) {
/* 350 */       LoggingManager.getInstance().log_error(e);
/* 351 */       c = null;
/*     */     }
/*     */     finally {
/* 354 */       em.close();
/*     */     }
/*     */     
/* 357 */     return c;
/*     */   }
/*     */   
/*     */ 
/*     */   public List<Caja> findCajasAbiertasByPicking(Integer pickingid)
/*     */   {
/* 363 */     EntityManager em = getEntityManager();
/* 364 */     List<Caja> c = null;
/*     */     try {
/* 366 */       Query q = em.createNamedQuery("Caja.findCajasAbiertasFromPicking", Caja.class);
/* 367 */       q.setParameter("picking", pickingid);
/*     */       
/* 369 */       c = q.getResultList();
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 373 */       c = null;
/*     */     } catch (Exception e) {
/* 375 */       LoggingManager.getInstance().log_error(e);
/* 376 */       c = null;
/*     */     }
/*     */     finally {
/* 379 */       em.close();
/*     */     }
/*     */     
/* 382 */     return c;
/*     */   }
/*     */   
/*     */   public void deleteAllfrompicking(Integer id)
/*     */   {
/* 387 */     EntityManager em = getEntityManager();
/*     */     try
/*     */     {
/* 390 */       em.getTransaction().begin();
/* 391 */       Query q = em.createNativeQuery("delete FROM caja where picking_id = " + id + " and id > 0");
/*     */       
/* 393 */       q.executeUpdate();
/* 394 */       em.getTransaction().commit();
/*     */     } catch (NoResultException localNoResultException) {
/* 396 */       localNoResultException = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 403 */         localNoResultException;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 398 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 403 */         ex;LoggingManager.getInstance().log_error(ex);
/*     */     } finally {}
/*     */   }
/*     */   
/* 407 */   public Integer getMaxId() { EntityManager em = getEntityManager();
/* 408 */     Integer i = null;
/*     */     try
/*     */     {
/* 411 */       Query q = em.createNativeQuery("select max(id) from caja");
/*     */       
/* 413 */       i = (Integer)q.getSingleResult();
/* 414 */       if (i == null) i = Integer.valueOf(0);
/*     */     }
/*     */     catch (NoResultException ex)
/*     */     {
/* 418 */       i = Integer.valueOf(0);
/*     */     } catch (Exception e) {
/* 420 */       LoggingManager.getInstance().log_error(e);
/* 421 */       i = null;
/*     */     }
/*     */     finally {
/* 424 */       em.close();
/*     */     }
/* 426 */     return i;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/CajaJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */