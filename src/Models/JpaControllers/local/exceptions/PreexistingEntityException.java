/*   */ package Models.JpaControllers.local.exceptions;
/*   */ 
/*   */ public class PreexistingEntityException extends Exception {
/*   */   public PreexistingEntityException(String message, Throwable cause) {
/* 5 */     super(message, cause);
/*   */   }
/*   */   
/* 8 */   public PreexistingEntityException(String message) { super(message); }
/*   */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/exceptions/PreexistingEntityException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */