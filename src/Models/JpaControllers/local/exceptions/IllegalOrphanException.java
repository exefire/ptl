/*    */ package Models.JpaControllers.local.exceptions;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ public class IllegalOrphanException extends Exception {
/*    */   private List<String> messages;
/*    */   
/*    */   public IllegalOrphanException(List<String> messages) {
/*  9 */     super((messages != null) && (messages.size() > 0) ? (String)messages.get(0) : null);
/* 10 */     if (messages == null) {
/* 11 */       this.messages = new java.util.ArrayList();
/*    */     }
/*    */     else
/* 14 */       this.messages = messages;
/*    */   }
/*    */   
/*    */   public List<String> getMessages() {
/* 18 */     return this.messages;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/exceptions/IllegalOrphanException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */