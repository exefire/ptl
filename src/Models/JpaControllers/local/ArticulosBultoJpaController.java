/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.ArticulosBulto;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ArticulosBultoJpaController
/*     */   implements Serializable
/*     */ {
/*  28 */   private EntityManagerFactory emf = null;
/*     */   private static ArticulosBultoJpaController instance;
/*     */   
/*     */   protected ArticulosBultoJpaController()
/*     */   {
/*  33 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static ArticulosBultoJpaController getInstance() {
/*  37 */     if (instance == null) {
/*  38 */       synchronized (ArticulosBultoJpaController.class) {
/*  39 */         if (instance == null) {
/*  40 */           instance = new ArticulosBultoJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  44 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public EntityManager getEntityManager()
/*     */   {
/*  50 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public void create(ArticulosBulto articulosBulto)
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aconst_null
/*     */     //   1: astore_2
/*     */     //   2: aload_0
/*     */     //   3: invokevirtual 9	Models/JpaControllers/local/ArticulosBultoJpaController:getEntityManager	()Ljavax/persistence/EntityManager;
/*     */     //   6: astore_2
/*     */     //   7: aload_2
/*     */     //   8: invokeinterface 10 1 0
/*     */     //   13: invokeinterface 11 1 0
/*     */     //   18: aload_2
/*     */     //   19: aload_1
/*     */     //   20: invokeinterface 12 2 0
/*     */     //   25: aload_2
/*     */     //   26: invokeinterface 10 1 0
/*     */     //   31: invokeinterface 13 1 0
/*     */     //   36: aload_2
/*     */     //   37: ifnull +25 -> 62
/*     */     //   40: aload_2
/*     */     //   41: invokeinterface 14 1 0
/*     */     //   46: goto +16 -> 62
/*     */     //   49: astore_3
/*     */     //   50: aload_2
/*     */     //   51: ifnull +9 -> 60
/*     */     //   54: aload_2
/*     */     //   55: invokeinterface 14 1 0
/*     */     //   60: aload_3
/*     */     //   61: athrow
/*     */     //   62: return
/*     */     // Line number table:
/*     */     //   Java source line #54	-> byte code offset #0
/*     */     //   Java source line #56	-> byte code offset #2
/*     */     //   Java source line #57	-> byte code offset #7
/*     */     //   Java source line #58	-> byte code offset #18
/*     */     //   Java source line #59	-> byte code offset #25
/*     */     //   Java source line #61	-> byte code offset #36
/*     */     //   Java source line #62	-> byte code offset #40
/*     */     //   Java source line #61	-> byte code offset #49
/*     */     //   Java source line #62	-> byte code offset #54
/*     */     //   Java source line #65	-> byte code offset #62
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	63	0	this	ArticulosBultoJpaController
/*     */     //   0	63	1	articulosBulto	ArticulosBulto
/*     */     //   1	54	2	em	EntityManager
/*     */     //   49	12	3	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   2	36	49	finally
/*     */   }
/*     */   
/*     */   public void edit(ArticulosBulto articulosBulto)
/*     */     throws NonexistentEntityException, Exception
/*     */   {
/*  68 */     EntityManager em = null;
/*     */     try {
/*  70 */       em = getEntityManager();
/*  71 */       em.getTransaction().begin();
/*  72 */       articulosBulto = (ArticulosBulto)em.merge(articulosBulto);
/*  73 */       em.getTransaction().commit();
/*     */     } catch (Exception ex) {
/*  75 */       String msg = ex.getLocalizedMessage();
/*  76 */       if ((msg == null) || (msg.length() == 0)) {
/*  77 */         Integer id = articulosBulto.getId();
/*  78 */         if (findArticulosBulto(id) == null) {
/*  79 */           throw new NonexistentEntityException("The articulosBulto with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/*  82 */       throw ex;
/*     */     } finally {
/*  84 */       if (em != null) {
/*  85 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws NonexistentEntityException {
/*  91 */     EntityManager em = null;
/*     */     try {
/*  93 */       em = getEntityManager();
/*  94 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/*  97 */         ArticulosBulto articulosBulto = (ArticulosBulto)em.getReference(ArticulosBulto.class, id);
/*  98 */         articulosBulto.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 100 */         throw new NonexistentEntityException("The articulosBulto with id " + id + " no longer exists.", enfe); }
/*     */       ArticulosBulto articulosBulto;
/* 102 */       em.remove(articulosBulto);
/* 103 */       em.getTransaction().commit();
/*     */     } finally {
/* 105 */       if (em != null) {
/* 106 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<ArticulosBulto> findArticulosBultoEntities() {
/* 112 */     return findArticulosBultoEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<ArticulosBulto> findArticulosBultoEntities(int maxResults, int firstResult) {
/* 116 */     return findArticulosBultoEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<ArticulosBulto> findArticulosBultoEntities(boolean all, int maxResults, int firstResult) {
/* 120 */     EntityManager em = getEntityManager();
/*     */     try {
/* 122 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 123 */       cq.select(cq.from(ArticulosBulto.class));
/* 124 */       Query q = em.createQuery(cq);
/* 125 */       if (!all) {
/* 126 */         q.setMaxResults(maxResults);
/* 127 */         q.setFirstResult(firstResult);
/*     */       }
/* 129 */       return q.getResultList();
/*     */     } finally {
/* 131 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public ArticulosBulto findArticulosBulto(Integer id) {
/* 136 */     EntityManager em = getEntityManager();
/*     */     try {
/* 138 */       return (ArticulosBulto)em.find(ArticulosBulto.class, id);
/*     */     } finally {
/* 140 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getArticulosBultoCount() {
/* 145 */     EntityManager em = getEntityManager();
/*     */     try {
/* 147 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 148 */       Root<ArticulosBulto> rt = cq.from(ArticulosBulto.class);
/* 149 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 150 */       Query q = em.createQuery(cq);
/* 151 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 153 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public List<ArticulosBulto> getArticulosBultoByCodigoBulto(String codigo) {
/* 158 */     EntityManager em = getEntityManager();
/* 159 */     List<ArticulosBulto> articulos = null;
/*     */     try {
/* 161 */       Query q = em.createNamedQuery("ArticulosBulto.findByCodigoBulto");
/* 162 */       q.setParameter("codigoBulto", codigo);
/*     */       
/*     */ 
/* 165 */       articulos = q.getResultList();
/*     */     } catch (NoResultException ex) {
/* 167 */       articulos = null;
/*     */     }
/*     */     catch (Exception ex) {
/* 170 */       LoggingManager.getInstance().log_error(ex);
/* 171 */       articulos = null;
/*     */     } finally {
/* 173 */       em.close();
/*     */     }
/*     */     
/* 176 */     return articulos;
/*     */   }
/*     */   
/*     */   public ArticulosBulto findByCodigoAndArticulo(String articulo, String codigo) {
/* 180 */     EntityManager em = getEntityManager();
/* 181 */     ArticulosBulto articuloBulto = null;
/*     */     try {
/* 183 */       Query q = em.createNamedQuery("ArticulosBulto.findByArticuloAndCodigo");
/* 184 */       q.setParameter("articulo", articulo);
/* 185 */       q.setParameter("codigoBulto", codigo);
/*     */       
/*     */ 
/* 188 */       articuloBulto = (ArticulosBulto)q.getSingleResult();
/*     */     } catch (NoResultException ex) {
/* 190 */       articuloBulto = null;
/*     */     }
/*     */     catch (Exception ex) {
/* 193 */       LoggingManager.getInstance().log_error(ex);
/* 194 */       articuloBulto = null;
/*     */     } finally {
/* 196 */       em.close();
/*     */     }
/*     */     
/* 199 */     return articuloBulto;
/*     */   }
/*     */   
/* 202 */   public boolean existeArticuloBulto(String bulto) { EntityManager em = getEntityManager();
/*     */     
/* 204 */     boolean res = false;
/*     */     
/*     */     try
/*     */     {
/* 208 */       em.getTransaction().begin();
/* 209 */       String sql = "SELECT count(*) FROM articulos_bulto where codigo_bulto = '" + bulto + "'";
/* 210 */       Query q = em.createNativeQuery(sql);
/*     */       
/*     */ 
/* 213 */       long result = ((Long)q.getSingleResult()).longValue();
/* 214 */       if (result > 0L) {
/* 215 */         res = true;
/* 216 */       } else if (result <= 0L) {
/* 217 */         res = false;
/*     */       }
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 222 */       LoggingManager.getInstance().log_error(ex);
/* 223 */       res = false;
/*     */     }
/*     */     
/* 226 */     return res;
/*     */   }
/*     */   
/*     */ 
/*     */   public void deleteAllOfOla(String ola)
/*     */   {
/* 232 */     EntityManager em = getEntityManager();
/* 233 */     em.getTransaction().begin();
/*     */     
/*     */     try
/*     */     {
/* 237 */       Query q = em.createNativeQuery("delete FROM articulos_bulto where ola = '" + ola + "' and id > 0");
/*     */       
/* 239 */       q.executeUpdate();
/* 240 */       em.getTransaction().commit();
/*     */     }
/*     */     catch (NoResultException localNoResultException) {
/* 243 */       localNoResultException = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 250 */         localNoResultException;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 245 */       ex = 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 250 */         ex;LoggingManager.getInstance().log_error(ex);
/*     */     }
/*     */     finally {}
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/ArticulosBultoJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */