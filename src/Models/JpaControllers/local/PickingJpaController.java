/*     */ package Models.JpaControllers.local;
/*     */ 
/*     */ import Database.DataSourceController;
/*     */ import Logging.LoggingManager;
/*     */ import Models.Entities.local.Caja;
/*     */ import Models.Entities.local.Picking;
/*     */ import Models.JpaControllers.local.exceptions.IllegalOrphanException;
/*     */ import Models.JpaControllers.local.exceptions.NonexistentEntityException;
/*     */ import java.io.Serializable;
/*     */ import java.math.BigDecimal;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.List;
/*     */ import javax.persistence.EntityManager;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.EntityNotFoundException;
/*     */ import javax.persistence.EntityTransaction;
/*     */ import javax.persistence.NoResultException;
/*     */ import javax.persistence.Query;
/*     */ import javax.persistence.criteria.CriteriaBuilder;
/*     */ import javax.persistence.criteria.CriteriaQuery;
/*     */ import javax.persistence.criteria.Root;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PickingJpaController
/*     */   implements Serializable
/*     */ {
/*  35 */   private EntityManagerFactory emf = null;
/*     */   private static PickingJpaController instance;
/*     */   
/*     */   protected PickingJpaController()
/*     */   {
/*  40 */     this.emf = DataSourceController.getInstance().getEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public static PickingJpaController getInstance() {
/*  44 */     if (instance == null) {
/*  45 */       synchronized (PickingJpaController.class) {
/*  46 */         if (instance == null) {
/*  47 */           instance = new PickingJpaController();
/*     */         }
/*     */       }
/*     */     }
/*  51 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public EntityManager getEntityManager()
/*     */   {
/*  60 */     return this.emf.createEntityManager();
/*     */   }
/*     */   
/*     */   public void create(Picking picking) {
/*  64 */     if (picking.getCajaCollection() == null) {
/*  65 */       picking.setCajaCollection(new ArrayList());
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  70 */     EntityManager em = null;
/*     */     try {
/*  72 */       em = getEntityManager();
/*  73 */       em.getTransaction().begin();
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  87 */       em.persist(picking);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 108 */       em.getTransaction().commit();
/*     */     }
/*     */     catch (Exception ex) {
/* 111 */       LoggingManager.getInstance().log_error(ex);
/*     */     } finally {
/* 113 */       if (em != null) {
/* 114 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void edit(Picking picking) throws IllegalOrphanException, NonexistentEntityException, Exception {
/* 120 */     EntityManager em = null;
/*     */     try {
/* 122 */       em = getEntityManager();
/* 123 */       em.getTransaction().begin();
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 156 */       picking = (Picking)em.merge(picking);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 185 */       em.getTransaction().commit();
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 189 */       LoggingManager.getInstance().log_error(ex);
/* 190 */       String msg = ex.getLocalizedMessage();
/* 191 */       if ((msg == null) || (msg.length() == 0)) {
/* 192 */         Integer id = picking.getId();
/* 193 */         if (findPicking(id) == null) {
/* 194 */           throw new NonexistentEntityException("The picking with id " + id + " no longer exists.");
/*     */         }
/*     */       }
/* 197 */       throw ex;
/*     */     } finally {
/* 199 */       if (em != null) {
/* 200 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
/* 206 */     EntityManager em = null;
/*     */     try {
/* 208 */       em = getEntityManager();
/* 209 */       em.getTransaction().begin();
/*     */       try
/*     */       {
/* 212 */         Picking picking = (Picking)em.getReference(Picking.class, id);
/* 213 */         picking.getId();
/*     */       } catch (EntityNotFoundException enfe) {
/* 215 */         throw new NonexistentEntityException("The picking with id " + id + " no longer exists.", enfe); }
/*     */       Picking picking;
/* 217 */       List<String> illegalOrphanMessages = null;
/* 218 */       Collection<Caja> cajaCollectionOrphanCheck = picking.getCajaCollection();
/* 219 */       for (Caja cajaCollectionOrphanCheckCaja : cajaCollectionOrphanCheck) {
/* 220 */         if (illegalOrphanMessages == null) {
/* 221 */           illegalOrphanMessages = new ArrayList();
/*     */         }
/* 223 */         illegalOrphanMessages.add("This Picking (" + picking + ") cannot be destroyed since the Caja " + cajaCollectionOrphanCheckCaja + " in its cajaCollection field has a non-nullable pickingId field.");
/*     */       }
/* 225 */       if (illegalOrphanMessages != null) {
/* 226 */         throw new IllegalOrphanException(illegalOrphanMessages);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 233 */       em.remove(picking);
/* 234 */       em.getTransaction().commit();
/*     */     }
/*     */     finally {
/* 237 */       if (em != null) {
/* 238 */         em.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public List<Picking> findPickingEntities() {
/* 244 */     return findPickingEntities(true, -1, -1);
/*     */   }
/*     */   
/*     */   public List<Picking> findPickingEntities(int maxResults, int firstResult) {
/* 248 */     return findPickingEntities(false, maxResults, firstResult);
/*     */   }
/*     */   
/*     */   private List<Picking> findPickingEntities(boolean all, int maxResults, int firstResult) {
/* 252 */     EntityManager em = getEntityManager();
/*     */     try {
/* 254 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 255 */       cq.select(cq.from(Picking.class));
/* 256 */       Query q = em.createQuery(cq);
/* 257 */       if (!all) {
/* 258 */         q.setMaxResults(maxResults);
/* 259 */         q.setFirstResult(firstResult);
/*     */       }
/* 261 */       return q.getResultList();
/*     */     } finally {
/* 263 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Picking findPicking(Integer id) {
/* 268 */     EntityManager em = getEntityManager();
/*     */     try {
/* 270 */       return (Picking)em.find(Picking.class, id);
/*     */     } finally {
/* 272 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int getPickingCount() {
/* 277 */     EntityManager em = getEntityManager();
/*     */     try {
/* 279 */       CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
/* 280 */       Root<Picking> rt = cq.from(Picking.class);
/* 281 */       cq.select(em.getCriteriaBuilder().count(rt));
/* 282 */       Query q = em.createQuery(cq);
/* 283 */       return ((Long)q.getSingleResult()).intValue();
/*     */     } finally {
/* 285 */       em.close();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Picking findByIdReferencia(String id)
/*     */   {
/* 293 */     EntityManager em = getEntityManager();
/* 294 */     Picking p = null;
/*     */     try {
/* 296 */       Query q = em.createNamedQuery("Picking.findByIdreferencia");
/* 297 */       q.setParameter("idreferencia", id);
/*     */       
/* 299 */       p = (Picking)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex) {
/* 302 */       p = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 306 */       LoggingManager.getInstance().log_error(ex);
/*     */     } finally {
/* 308 */       em.close();
/*     */     }
/* 310 */     return p;
/*     */   }
/*     */   
/*     */   public Picking findByEstadoProcesando()
/*     */   {
/* 315 */     EntityManager em = getEntityManager();
/* 316 */     Picking p = null;
/*     */     try {
/* 318 */       Query q = em.createNamedQuery("Picking.findByEstado");
/* 319 */       q.setParameter("estado", "procesando");
/*     */       
/* 321 */       p = (Picking)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex) {
/* 324 */       p = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 328 */       LoggingManager.getInstance().log_error(ex);
/*     */     } finally {
/* 330 */       em.close();
/*     */     }
/* 332 */     return p;
/*     */   }
/*     */   
/*     */   public Picking findByEstadoIncompleto() {
/* 336 */     EntityManager em = getEntityManager();
/* 337 */     Picking p = null;
/*     */     try {
/* 339 */       Query q = em.createNamedQuery("Picking.findByEstado");
/* 340 */       q.setParameter("estado", "incompleto");
/*     */       
/* 342 */       p = (Picking)q.getSingleResult();
/*     */     }
/*     */     catch (NoResultException ex) {
/* 345 */       p = null;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 349 */       LoggingManager.getInstance().log_error(ex);
/*     */     } finally {
/* 351 */       em.close();
/*     */     }
/* 353 */     return p;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean isFinished(Picking p)
/*     */   {
/* 359 */     EntityManager em = getEntityManager();
/* 360 */     Integer result = null;
/* 361 */     boolean r = false;
/*     */     try {
/* 363 */       Query q = em.createNativeQuery("SELECT (sum(aconsolidar)-sum(consolidado) ) as faltantes FROM detalle_picking where picking_id = " + p.getId() + " group by picking_id;");
/* 364 */       result = Integer.valueOf(((BigDecimal)q.getSingleResult()).intValueExact());
/* 365 */       if (result.intValue() > 0) {
/* 366 */         r = false;
/* 367 */       } else if (result.intValue() <= 0) {
/* 368 */         r = true;
/*     */       }
/*     */     }
/*     */     catch (NoResultException ex) {
/* 372 */       r = false;
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 376 */       LoggingManager.getInstance().log_error(ex);
/* 377 */       r = false;
/*     */     } finally {
/* 379 */       em.close();
/*     */     }
/*     */     
/* 382 */     return r;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Object> getReportData(String ola)
/*     */   {
/* 390 */     EntityManager em = getEntityManager();
/*     */     
/*     */     try
/*     */     {
/* 394 */       Query q = em.createNativeQuery("select picking.idreferencia as ola, destino.nombre as destino, detalle_picking.articulo, sum(detalle_picking.cantidad) as pedido, sum(detalle_picking.consolidado) as distribuido, (sum(detalle_picking.cantidad) - sum(detalle_picking.consolidado)) as pendiente  from picking left join detalle_picking on (picking.id = detalle_picking.picking_id ) left join destino on (detalle_picking.destino_id = destino.id) where picking.idreferencia = '" + ola + "' group by 1,2,3");
/* 395 */       rep = q.getResultList();
/*     */     } catch (NoResultException ex) {
/*     */       List<Object> rep;
/* 398 */       rep = null;
/*     */     }
/*     */     catch (Exception ex) {
/*     */       List<Object> rep;
/* 402 */       LoggingManager.getInstance().log_error(ex);
/* 403 */       rep = null;
/*     */     } finally { List<Object> rep;
/* 405 */       em.close();
/*     */     }
/*     */     List<Object> rep;
/* 408 */     return rep;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/JpaControllers/local/PickingJpaController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */