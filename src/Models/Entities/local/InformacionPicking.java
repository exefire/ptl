/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="informacion_picking")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="InformacionPicking.findAll", query="SELECT i FROM InformacionPicking i"), @javax.persistence.NamedQuery(name="InformacionPicking.findById", query="SELECT i FROM InformacionPicking i WHERE i.id = :id"), @javax.persistence.NamedQuery(name="InformacionPicking.findByTotal", query="SELECT i FROM InformacionPicking i WHERE i.total = :total"), @javax.persistence.NamedQuery(name="InformacionPicking.findByDistribuido", query="SELECT i FROM InformacionPicking i WHERE i.distribuido = :distribuido"), @javax.persistence.NamedQuery(name="InformacionPicking.findByPendiente", query="SELECT i FROM InformacionPicking i WHERE i.pendiente = :pendiente")})
/*     */ public class InformacionPicking
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="total")
/*     */   private Integer total;
/*     */   @Column(name="distribuido")
/*     */   private Integer distribuido;
/*     */   @Column(name="pendiente")
/*     */   private Integer pendiente;
/*     */   @Column(name="picking")
/*     */   private Integer picking;
/*     */   
/*     */   public InformacionPicking() {}
/*     */   
/*     */   public InformacionPicking(Integer id)
/*     */   {
/*  51 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  55 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  59 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getTotal() {
/*  63 */     return this.total;
/*     */   }
/*     */   
/*     */   public void setTotal(Integer total) {
/*  67 */     this.total = total;
/*     */   }
/*     */   
/*     */   public Integer getDistribuido() {
/*  71 */     return this.distribuido;
/*     */   }
/*     */   
/*     */   public void setDistribuido(Integer distribuido) {
/*  75 */     this.distribuido = distribuido;
/*     */   }
/*     */   
/*     */   public Integer getPendiente() {
/*  79 */     return this.pendiente;
/*     */   }
/*     */   
/*     */   public void setPendiente(Integer pendiente) {
/*  83 */     this.pendiente = pendiente;
/*     */   }
/*     */   
/*     */   public Integer getPicking() {
/*  87 */     return this.picking;
/*     */   }
/*     */   
/*     */   public void setPicking(Integer picking) {
/*  91 */     this.picking = picking;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/*  99 */     int hash = 0;
/* 100 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 101 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 107 */     if (!(object instanceof InformacionPicking)) {
/* 108 */       return false;
/*     */     }
/* 110 */     InformacionPicking other = (InformacionPicking)object;
/* 111 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 112 */       return false;
/*     */     }
/* 114 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 119 */     return "Models.Entities.local.InformacionPicking[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/InformacionPicking.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */