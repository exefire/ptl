/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Collection;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.OneToMany;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlTransient;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="destino")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="Destino.findAll", query="SELECT d FROM Destino d order by d.ranking asc"), @javax.persistence.NamedQuery(name="Destino.findById", query="SELECT d FROM Destino d WHERE d.id = :id"), @javax.persistence.NamedQuery(name="Destino.findByNombre", query="SELECT d FROM Destino d WHERE d.nombre = :nombre"), @javax.persistence.NamedQuery(name="Destino.findByNombre", query="SELECT d FROM Destino d WHERE d.nombre = :nombre"), @javax.persistence.NamedQuery(name="Destino.findByRanking", query="SELECT d FROM Destino d WHERE d.ranking = :ranking")})
/*     */ public class Destino
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="nombre")
/*     */   private String nombre;
/*     */   @Column(name="ranking")
/*     */   private Integer ranking;
/*     */   @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="destinoId")
/*     */   private Collection<Caja> cajaCollection;
/*     */   @OneToMany(mappedBy="destinoId")
/*     */   private Collection<DetallePicking> detallePickingCollection;
/*     */   
/*     */   public Destino() {}
/*     */   
/*     */   public Destino(Integer id)
/*     */   {
/*  59 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  63 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  67 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getNombre()
/*     */   {
/*  72 */     return this.nombre;
/*     */   }
/*     */   
/*     */   public void setNombre(String nombre) {
/*  76 */     this.nombre = nombre;
/*     */   }
/*     */   
/*     */   public Integer getRanking() {
/*  80 */     return this.ranking;
/*     */   }
/*     */   
/*     */   public void setRanking(Integer ranking) {
/*  84 */     this.ranking = ranking;
/*     */   }
/*     */   
/*     */   @XmlTransient
/*     */   public Collection<Caja> getCajaCollection() {
/*  89 */     return this.cajaCollection;
/*     */   }
/*     */   
/*     */   public void setCajaCollection(Collection<Caja> cajaCollection) {
/*  93 */     this.cajaCollection = cajaCollection;
/*     */   }
/*     */   
/*     */   @XmlTransient
/*     */   public Collection<DetallePicking> getDetallePickingCollection() {
/*  98 */     return this.detallePickingCollection;
/*     */   }
/*     */   
/*     */   public void setDetallePickingCollection(Collection<DetallePicking> detallePickingCollection) {
/* 102 */     this.detallePickingCollection = detallePickingCollection;
/*     */   }
/*     */   
/*     */   public int hashCode()
/*     */   {
/* 107 */     int hash = 0;
/* 108 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 109 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 115 */     if (!(object instanceof Destino)) {
/* 116 */       return false;
/*     */     }
/* 118 */     Destino other = (Destino)object;
/* 119 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 120 */       return false;
/*     */     }
/* 122 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 127 */     return "Models.Entities.local.Destino[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/Destino.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */