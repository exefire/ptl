/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="informacion_bulto")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="InformacionBulto.findAll", query="SELECT i FROM InformacionBulto i"), @javax.persistence.NamedQuery(name="InformacionBulto.findById", query="SELECT i FROM InformacionBulto i WHERE i.id = :id"), @javax.persistence.NamedQuery(name="InformacionBulto.findByCodigo", query="SELECT i FROM InformacionBulto i WHERE i.codigo = :codigo"), @javax.persistence.NamedQuery(name="InformacionBulto.findByTotal", query="SELECT i FROM InformacionBulto i WHERE i.total = :total"), @javax.persistence.NamedQuery(name="InformacionBulto.findByDistribuidos", query="SELECT i FROM InformacionBulto i WHERE i.distribuidos = :distribuidos"), @javax.persistence.NamedQuery(name="InformacionBulto.findByPendientes", query="SELECT i FROM InformacionBulto i WHERE i.pendientes = :pendientes"), @javax.persistence.NamedQuery(name="InformacionBulto.findByPicking", query="SELECT i FROM InformacionBulto i WHERE i.picking = :picking")})
/*     */ public class InformacionBulto
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="codigo")
/*     */   private String codigo;
/*     */   @Column(name="total")
/*     */   private Integer total;
/*     */   @Column(name="distribuidos")
/*     */   private Integer distribuidos;
/*     */   @Column(name="pendientes")
/*     */   private Integer pendientes;
/*     */   @Column(name="picking")
/*     */   private Integer picking;
/*     */   
/*     */   public InformacionBulto() {}
/*     */   
/*     */   public InformacionBulto(Integer id)
/*     */   {
/*  55 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  59 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  63 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getCodigo() {
/*  67 */     return this.codigo;
/*     */   }
/*     */   
/*     */   public void setCodigo(String codigo) {
/*  71 */     this.codigo = codigo;
/*     */   }
/*     */   
/*     */   public Integer getTotal() {
/*  75 */     return this.total;
/*     */   }
/*     */   
/*     */   public void setTotal(Integer total) {
/*  79 */     this.total = total;
/*     */   }
/*     */   
/*     */   public Integer getDistribuidos() {
/*  83 */     return this.distribuidos;
/*     */   }
/*     */   
/*     */   public void setDistribuidos(Integer distribuidos) {
/*  87 */     this.distribuidos = distribuidos;
/*     */   }
/*     */   
/*     */   public Integer getPendientes() {
/*  91 */     return this.pendientes;
/*     */   }
/*     */   
/*     */   public void setPendientes(Integer pendientes) {
/*  95 */     this.pendientes = pendientes;
/*     */   }
/*     */   
/*     */   public Integer getPicking() {
/*  99 */     return this.picking;
/*     */   }
/*     */   
/*     */   public void setPicking(Integer picking) {
/* 103 */     this.picking = picking;
/*     */   }
/*     */   
/*     */   public int hashCode()
/*     */   {
/* 108 */     int hash = 0;
/* 109 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 110 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 116 */     if (!(object instanceof InformacionBulto)) {
/* 117 */       return false;
/*     */     }
/* 119 */     InformacionBulto other = (InformacionBulto)object;
/* 120 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 121 */       return false;
/*     */     }
/* 123 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 128 */     return "Models.Entities.local.InformacionBulto[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/InformacionBulto.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */