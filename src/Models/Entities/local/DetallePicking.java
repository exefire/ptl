/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.JoinColumn;
/*     */ import javax.persistence.ManyToOne;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="detalle_picking")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="DetallePicking.findAll", query="SELECT d FROM DetallePicking d"), @javax.persistence.NamedQuery(name="DetallePicking.findById", query="SELECT d FROM DetallePicking d WHERE d.id = :id"), @javax.persistence.NamedQuery(name="DetallePicking.findByReferencia", query="SELECT d FROM DetallePicking d WHERE d.referencia = :referencia"), @javax.persistence.NamedQuery(name="DetallePicking.findByArticulo", query="SELECT d FROM DetallePicking d WHERE d.articulo = :articulo"), @javax.persistence.NamedQuery(name="DetallePicking.findByCantidad", query="SELECT d FROM DetallePicking d WHERE d.cantidad = :cantidad"), @javax.persistence.NamedQuery(name="DetallePicking.findByConsolidado", query="SELECT d FROM DetallePicking d WHERE d.consolidado = :consolidado"), @javax.persistence.NamedQuery(name="DetallePicking.findByAconsolidar", query="SELECT d FROM DetallePicking d WHERE d.aconsolidar = :aconsolidar"), @javax.persistence.NamedQuery(name="DetallePicking.findByPickingAndArticulo", query="SELECT d FROM DetallePicking d WHERE d.pickingId.id = :picking and d.articulo = :articulo"), @javax.persistence.NamedQuery(name="DetallePicking.findByPickingAndDestinoAndArticulo", query="SELECT d FROM DetallePicking d WHERE d.pickingId.id = :picking and d.destinoId.id = :destino and d.articulo = :articulo ")})
/*     */ public class DetallePicking
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="referencia")
/*     */   private String referencia;
/*     */   @Column(name="articulo")
/*     */   private String articulo;
/*     */   @Column(name="cantidad")
/*     */   private Integer cantidad;
/*     */   @Column(name="consolidado")
/*     */   private Integer consolidado;
/*     */   @Column(name="codigo_retail")
/*     */   private String codigo_retail;
/*     */   @Column(name="oc")
/*     */   private String oc;
/*     */   @Column(name="aconsolidar")
/*     */   private Integer aconsolidar;
/*     */   @JoinColumn(name="picking_id", referencedColumnName="id")
/*     */   @ManyToOne
/*     */   private Picking pickingId;
/*     */   @JoinColumn(name="destino_id", referencedColumnName="id")
/*     */   @ManyToOne
/*     */   private Destino destinoId;
/*     */   
/*     */   public DetallePicking() {}
/*     */   
/*     */   public DetallePicking(Integer id)
/*     */   {
/*  73 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  77 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  81 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getReferencia() {
/*  85 */     return this.referencia;
/*     */   }
/*     */   
/*     */   public void setReferencia(String referencia) {
/*  89 */     this.referencia = referencia;
/*     */   }
/*     */   
/*     */   public String getArticulo() {
/*  93 */     return this.articulo;
/*     */   }
/*     */   
/*     */   public void setArticulo(String articulo) {
/*  97 */     this.articulo = articulo;
/*     */   }
/*     */   
/*     */   public String getOc() {
/* 101 */     return this.oc;
/*     */   }
/*     */   
/*     */   public void setOc(String oc) {
/* 105 */     this.oc = oc;
/*     */   }
/*     */   
/*     */   public Integer getCantidad() {
/* 109 */     return this.cantidad;
/*     */   }
/*     */   
/*     */   public void setCantidad(Integer cantidad) {
/* 113 */     this.cantidad = cantidad;
/*     */   }
/*     */   
/*     */   public Integer getConsolidado() {
/* 117 */     return this.consolidado;
/*     */   }
/*     */   
/*     */   public void setConsolidado(Integer consolidado) {
/* 121 */     this.consolidado = consolidado;
/*     */   }
/*     */   
/*     */   public Integer getAconsolidar() {
/* 125 */     return this.aconsolidar;
/*     */   }
/*     */   
/*     */   public void setAconsolidar(Integer aconsolidar) {
/* 129 */     this.aconsolidar = aconsolidar;
/*     */   }
/*     */   
/*     */   public Picking getPickingId() {
/* 133 */     return this.pickingId;
/*     */   }
/*     */   
/*     */   public void setPickingId(Picking pickingId) {
/* 137 */     this.pickingId = pickingId;
/*     */   }
/*     */   
/*     */   public Destino getDestinoId() {
/* 141 */     return this.destinoId;
/*     */   }
/*     */   
/*     */   public String getCodigo_retail() {
/* 145 */     return this.codigo_retail;
/*     */   }
/*     */   
/*     */   public void setCodigo_retail(String codigo_retail) {
/* 149 */     this.codigo_retail = codigo_retail;
/*     */   }
/*     */   
/*     */   public void setDestinoId(Destino destinoId) {
/* 153 */     this.destinoId = destinoId;
/*     */   }
/*     */   
/*     */   public int hashCode()
/*     */   {
/* 158 */     int hash = 0;
/* 159 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 160 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 166 */     if (!(object instanceof DetallePicking)) {
/* 167 */       return false;
/*     */     }
/* 169 */     DetallePicking other = (DetallePicking)object;
/* 170 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 171 */       return false;
/*     */     }
/* 173 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 178 */     return "Models.Entities.local.DetallePicking[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/DetallePicking.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */