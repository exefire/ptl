/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Collection;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.OneToMany;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlTransient;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="picking")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="Picking.findAll", query="SELECT p FROM Picking p"), @javax.persistence.NamedQuery(name="Picking.findById", query="SELECT p FROM Picking p WHERE p.id = :id"), @javax.persistence.NamedQuery(name="Picking.findByIdreferencia", query="SELECT p FROM Picking p WHERE p.idreferencia = :idreferencia"), @javax.persistence.NamedQuery(name="Picking.findByEstado", query="SELECT p FROM Picking p WHERE p.estado = :estado")})
/*     */ public class Picking
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="idreferencia")
/*     */   private String idreferencia;
/*     */   @Column(name="oc")
/*     */   private String oc;
/*     */   @Column(name="estado")
/*     */   private String estado;
/*     */   @Column(name="bulto_en_proceso")
/*     */   private String bultoEnProceso;
/*     */   @OneToMany(mappedBy="pickingId")
/*     */   private Collection<Caja> cajaCollection;
/*     */   
/*     */   public Picking() {}
/*     */   
/*     */   public Picking(Integer id)
/*     */   {
/*  61 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getOc() {
/*  65 */     return this.oc;
/*     */   }
/*     */   
/*     */   public void setOc(String oc) {
/*  69 */     this.oc = oc;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  73 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  77 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getIdreferencia()
/*     */   {
/*  82 */     return this.idreferencia;
/*     */   }
/*     */   
/*     */   public void setIdreferencia(String idreferencia) {
/*  86 */     this.idreferencia = idreferencia;
/*     */   }
/*     */   
/*     */   public String getEstado() {
/*  90 */     return this.estado;
/*     */   }
/*     */   
/*     */   public void setEstado(String estado) {
/*  94 */     this.estado = estado;
/*     */   }
/*     */   
/*     */   @XmlTransient
/*     */   public Collection<Caja> getCajaCollection() {
/*  99 */     return this.cajaCollection;
/*     */   }
/*     */   
/*     */   public void setCajaCollection(Collection<Caja> cajaCollection) {
/* 103 */     this.cajaCollection = cajaCollection;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBultoEnProceso()
/*     */   {
/* 116 */     return this.bultoEnProceso;
/*     */   }
/*     */   
/*     */   public void setBultoEnProceso(String bultoEnProceso) {
/* 120 */     this.bultoEnProceso = bultoEnProceso;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 128 */     int hash = 0;
/* 129 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 130 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 136 */     if (!(object instanceof Picking)) {
/* 137 */       return false;
/*     */     }
/* 139 */     Picking other = (Picking)object;
/* 140 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 141 */       return false;
/*     */     }
/* 143 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 148 */     return "Models.Entities.local.Picking[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/Picking.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */