/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="articulos_bulto")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="ArticulosBulto.findAll", query="SELECT a FROM ArticulosBulto a"), @javax.persistence.NamedQuery(name="ArticulosBulto.findById", query="SELECT a FROM ArticulosBulto a WHERE a.id = :id"), @javax.persistence.NamedQuery(name="ArticulosBulto.findByCodigoBulto", query="SELECT a FROM ArticulosBulto a WHERE a.codigoBulto = :codigoBulto"), @javax.persistence.NamedQuery(name="ArticulosBulto.findByArticulo", query="SELECT a FROM ArticulosBulto a WHERE a.articulo = :articulo"), @javax.persistence.NamedQuery(name="ArticulosBulto.findByCantidad", query="SELECT a FROM ArticulosBulto a WHERE a.cantidad = :cantidad"), @javax.persistence.NamedQuery(name="ArticulosBulto.findByDistribuidos", query="SELECT a FROM ArticulosBulto a WHERE a.distribuidos = :distribuidos"), @javax.persistence.NamedQuery(name="ArticulosBulto.findByArticuloAndCodigo", query="SELECT a FROM ArticulosBulto a WHERE a.articulo = :articulo and a.codigoBulto = :codigoBulto")})
/*     */ public class ArticulosBulto
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="codigo_bulto")
/*     */   private String codigoBulto;
/*     */   @Column(name="articulo")
/*     */   private String articulo;
/*     */   @Column(name="cantidad")
/*     */   private Integer cantidad;
/*     */   @Column(name="distribuidos")
/*     */   private Integer distribuidos;
/*     */   @Column(name="ola")
/*     */   private String ola;
/*     */   
/*     */   public ArticulosBulto() {}
/*     */   
/*     */   public ArticulosBulto(Integer id)
/*     */   {
/*  60 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  64 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  68 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getCodigoBulto() {
/*  72 */     return this.codigoBulto;
/*     */   }
/*     */   
/*     */   public void setCodigoBulto(String codigoBulto) {
/*  76 */     this.codigoBulto = codigoBulto;
/*     */   }
/*     */   
/*     */   public String getArticulo() {
/*  80 */     return this.articulo;
/*     */   }
/*     */   
/*     */   public void setArticulo(String articulo) {
/*  84 */     this.articulo = articulo;
/*     */   }
/*     */   
/*     */   public Integer getCantidad() {
/*  88 */     return this.cantidad;
/*     */   }
/*     */   
/*     */   public void setCantidad(Integer cantidad) {
/*  92 */     this.cantidad = cantidad;
/*     */   }
/*     */   
/*     */   public Integer getDistribuidos() {
/*  96 */     return this.distribuidos;
/*     */   }
/*     */   
/*     */   public void setDistribuidos(Integer distribuidos) {
/* 100 */     this.distribuidos = distribuidos;
/*     */   }
/*     */   
/*     */   public String getOla() {
/* 104 */     return this.ola;
/*     */   }
/*     */   
/*     */   public void setOla(String ola) {
/* 108 */     this.ola = ola;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 117 */     int hash = 0;
/* 118 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 119 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 125 */     if (!(object instanceof ArticulosBulto)) {
/* 126 */       return false;
/*     */     }
/* 128 */     ArticulosBulto other = (ArticulosBulto)object;
/* 129 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 130 */       return false;
/*     */     }
/* 132 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 137 */     return "Models.Entities.local.ArticulosBulto[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/ArticulosBulto.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */