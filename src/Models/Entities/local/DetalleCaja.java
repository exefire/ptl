/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Date;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.JoinColumn;
/*     */ import javax.persistence.ManyToOne;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.Table;
/*     */ import javax.persistence.Temporal;
/*     */ import javax.persistence.TemporalType;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="detalle_caja")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="DetalleCaja.findAll", query="SELECT d FROM DetalleCaja d"), @javax.persistence.NamedQuery(name="DetalleCaja.findById", query="SELECT d FROM DetalleCaja d WHERE d.id = :id"), @javax.persistence.NamedQuery(name="DetalleCaja.findByArticulo", query="SELECT d FROM DetalleCaja d WHERE d.articulo = :articulo"), @javax.persistence.NamedQuery(name="DetalleCaja.findByCodigoBarra", query="SELECT d FROM DetalleCaja d WHERE d.codigoBarra = :codigoBarra"), @javax.persistence.NamedQuery(name="DetalleCaja.findByCantidad", query="SELECT d FROM DetalleCaja d WHERE d.cantidad = :cantidad"), @javax.persistence.NamedQuery(name="DetalleCaja.findByFecha", query="SELECT d FROM DetalleCaja d WHERE d.fecha = :fecha"), @javax.persistence.NamedQuery(name="DetalleCaja.findAllByCaja", query="SELECT d FROM DetalleCaja d WHERE d.cajaId = :caja "), @javax.persistence.NamedQuery(name="DetalleCaja.findAllByCajaAndReferencia", query="SELECT d FROM DetalleCaja d WHERE d.cajaId = :caja and d.referencia = :referencia ")})
/*     */ public class DetalleCaja
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="articulo")
/*     */   private String articulo;
/*     */   @Column(name="codigo_barra")
/*     */   private String codigoBarra;
/*     */   @Column(name="codigo_retail")
/*     */   private String codigo_retail;
/*     */   @Column(name="oc")
/*     */   private String oc;
/*     */   @Column(name="cantidad")
/*     */   private Integer cantidad;
/*     */   @Column(name="created_at")
/*     */   @Temporal(TemporalType.DATE)
/*     */   private Date created;
/*     */   @Column(name="fecha")
/*     */   @Temporal(TemporalType.TIMESTAMP)
/*     */   private Date fecha;
/*     */   @Column(name="referencia")
/*     */   private String referencia;
/*     */   @JoinColumn(name="user_id", referencedColumnName="id")
/*     */   @ManyToOne
/*     */   private User userId;
/*     */   @JoinColumn(name="caja_id", referencedColumnName="id")
/*     */   @ManyToOne(optional=false)
/*     */   private Caja cajaId;
/*     */   
/*     */   public Date getCreated()
/*     */   {
/*  79 */     return this.created;
/*     */   }
/*     */   
/*     */   public void setCreated(Date created) {
/*  83 */     this.created = created;
/*     */   }
/*     */   
/*     */ 
/*     */   public DetalleCaja() {}
/*     */   
/*     */   public DetalleCaja(Integer id)
/*     */   {
/*  91 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  95 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  99 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getCodigo_retail() {
/* 103 */     return this.codigo_retail;
/*     */   }
/*     */   
/*     */   public void setCodigo_retail(String codigo_retail) {
/* 107 */     this.codigo_retail = codigo_retail;
/*     */   }
/*     */   
/*     */   public String getArticulo() {
/* 111 */     return this.articulo;
/*     */   }
/*     */   
/*     */   public void setArticulo(String articulo) {
/* 115 */     this.articulo = articulo;
/*     */   }
/*     */   
/*     */   public String getOc() {
/* 119 */     return this.oc;
/*     */   }
/*     */   
/*     */   public void setOc(String oc) {
/* 123 */     this.oc = oc;
/*     */   }
/*     */   
/*     */   public String getCodigoBarra() {
/* 127 */     return this.codigoBarra;
/*     */   }
/*     */   
/*     */   public void setCodigoBarra(String codigoBarra) {
/* 131 */     this.codigoBarra = codigoBarra;
/*     */   }
/*     */   
/*     */   public Integer getCantidad() {
/* 135 */     return this.cantidad;
/*     */   }
/*     */   
/*     */   public void setCantidad(Integer cantidad) {
/* 139 */     this.cantidad = cantidad;
/*     */   }
/*     */   
/*     */   public Date getFecha() {
/* 143 */     return this.fecha;
/*     */   }
/*     */   
/*     */   public void setFecha(String Date) {
/* 147 */     this.fecha = this.fecha;
/*     */   }
/*     */   
/*     */   public User getUserId() {
/* 151 */     return this.userId;
/*     */   }
/*     */   
/*     */   public void setUserId(User userId) {
/* 155 */     this.userId = userId;
/*     */   }
/*     */   
/*     */   public Caja getCajaId() {
/* 159 */     return this.cajaId;
/*     */   }
/*     */   
/*     */   public void setCajaId(Caja cajaId) {
/* 163 */     this.cajaId = cajaId;
/*     */   }
/*     */   
/*     */   public String getReferencia() {
/* 167 */     return this.referencia;
/*     */   }
/*     */   
/*     */   public void setReferencia(String referencia) {
/* 171 */     this.referencia = referencia;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 180 */     int hash = 0;
/* 181 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 182 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 188 */     if (!(object instanceof DetalleCaja)) {
/* 189 */       return false;
/*     */     }
/* 191 */     DetalleCaja other = (DetalleCaja)object;
/* 192 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 193 */       return false;
/*     */     }
/* 195 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 200 */     return "Models.Entities.local.DetalleCaja[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/DetalleCaja.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */