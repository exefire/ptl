/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Collection;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.OneToMany;
/*     */ import javax.persistence.Table;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlTransient;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="user")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="User.findAll", query="SELECT u FROM User u"), @javax.persistence.NamedQuery(name="User.findById", query="SELECT u FROM User u WHERE u.id = :id"), @javax.persistence.NamedQuery(name="User.findByUsername", query="SELECT u FROM User u WHERE u.username = :username"), @javax.persistence.NamedQuery(name="User.findByPassword", query="SELECT u FROM User u WHERE u.password = :password"), @javax.persistence.NamedQuery(name="User.findByRol", query="SELECT u FROM User u WHERE u.rol = :rol")})
/*     */ public class User
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="username")
/*     */   private String username;
/*     */   @Column(name="password")
/*     */   private String password;
/*     */   @Column(name="role_id")
/*     */   private Rol rol;
/*     */   @OneToMany(mappedBy="userId")
/*     */   private Collection<DetalleCaja> detalleCajaCollection;
/*     */   
/*     */   public User() {}
/*     */   
/*     */   public User(Integer id)
/*     */   {
/*  57 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  61 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  65 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getUsername() {
/*  69 */     return this.username;
/*     */   }
/*     */   
/*     */   public void setUsername(String username) {
/*  73 */     this.username = username;
/*     */   }
/*     */   
/*     */   public String getPassword() {
/*  77 */     return this.password;
/*     */   }
/*     */   
/*     */   public void setPassword(String password) {
/*  81 */     this.password = password;
/*     */   }
/*     */   
/*     */   public Rol getRol() {
/*  85 */     return this.rol;
/*     */   }
/*     */   
/*     */   public void setRoleId(Rol roleId) {
/*  89 */     this.rol = roleId;
/*     */   }
/*     */   
/*     */   @XmlTransient
/*     */   public Collection<DetalleCaja> getDetalleCajaCollection() {
/*  94 */     return this.detalleCajaCollection;
/*     */   }
/*     */   
/*     */   public void setDetalleCajaCollection(Collection<DetalleCaja> detalleCajaCollection) {
/*  98 */     this.detalleCajaCollection = detalleCajaCollection;
/*     */   }
/*     */   
/*     */   public int hashCode()
/*     */   {
/* 103 */     int hash = 0;
/* 104 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 105 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 111 */     if (!(object instanceof User)) {
/* 112 */       return false;
/*     */     }
/* 114 */     User other = (User)object;
/* 115 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 116 */       return false;
/*     */     }
/* 118 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 123 */     return "Models.Entities.local.User[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/User.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */