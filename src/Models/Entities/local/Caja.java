/*     */ package Models.Entities.local;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Collection;
/*     */ import java.util.Date;
/*     */ import javax.persistence.Basic;
/*     */ import javax.persistence.Column;
/*     */ import javax.persistence.Entity;
/*     */ import javax.persistence.GeneratedValue;
/*     */ import javax.persistence.GenerationType;
/*     */ import javax.persistence.Id;
/*     */ import javax.persistence.JoinColumn;
/*     */ import javax.persistence.ManyToOne;
/*     */ import javax.persistence.NamedQueries;
/*     */ import javax.persistence.OneToMany;
/*     */ import javax.persistence.Table;
/*     */ import javax.persistence.Temporal;
/*     */ import javax.persistence.TemporalType;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlTransient;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Entity
/*     */ @Table(name="caja")
/*     */ @XmlRootElement
/*     */ @NamedQueries({@javax.persistence.NamedQuery(name="Caja.findAll", query="SELECT c FROM Caja c"), @javax.persistence.NamedQuery(name="Caja.findById", query="SELECT c FROM Caja c WHERE c.id = :id"), @javax.persistence.NamedQuery(name="Caja.findByCodigo", query="SELECT c FROM Caja c WHERE c.codigo = :codigo"), @javax.persistence.NamedQuery(name="Caja.findByPosicionPtl", query="SELECT c FROM Caja c WHERE c.posicionPtl = :posicionPtl and c.estado = 'abierta'"), @javax.persistence.NamedQuery(name="Caja.findByEstado", query="SELECT c FROM Caja c WHERE c.estado = :estado"), @javax.persistence.NamedQuery(name="Caja.findByFechaCreacion", query="SELECT c FROM Caja c WHERE c.fechaCreacion = :fechaCreacion"), @javax.persistence.NamedQuery(name="Caja.findByFechaCierre", query="SELECT c FROM Caja c WHERE c.fechaCierre = :fechaCierre"), @javax.persistence.NamedQuery(name="Caja.findCajasAbiertasFromPicking", query="SELECT c FROM Caja c WHERE c.pickingId.id = :picking and c.estado = 'abierta'"), @javax.persistence.NamedQuery(name="Caja.findAbiertaByDestino", query="SELECT c FROM Caja c WHERE c.estado = 'abierta' and c.destinoId.id = :destino")})
/*     */ public class Caja
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   @Id
/*     */   @GeneratedValue(strategy=GenerationType.IDENTITY)
/*     */   @Basic(optional=false)
/*     */   @Column(name="id")
/*     */   private Integer id;
/*     */   @Column(name="codigo")
/*     */   private String codigo;
/*     */   @Column(name="posicion_ptl")
/*     */   private Integer posicionPtl;
/*     */   @Column(name="estado")
/*     */   private String estado;
/*     */   @Column(name="fecha_creacion")
/*     */   @Temporal(TemporalType.TIMESTAMP)
/*     */   private Date fechaCreacion;
/*     */   @Column(name="fecha_cierre")
/*     */   @Temporal(TemporalType.TIMESTAMP)
/*     */   private Date fechaCierre;
/*     */   @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="cajaId")
/*     */   private Collection<DetalleCaja> detalleCajaCollection;
/*     */   @JoinColumn(name="picking_id", referencedColumnName="id")
/*     */   @ManyToOne(optional=false)
/*     */   private Picking pickingId;
/*     */   @JoinColumn(name="destino_id", referencedColumnName="id")
/*     */   @ManyToOne(optional=false)
/*     */   private Destino destinoId;
/*     */   
/*     */   public Caja() {}
/*     */   
/*     */   public Caja(Integer id)
/*     */   {
/*  79 */     this.id = id;
/*     */   }
/*     */   
/*     */   public Integer getId() {
/*  83 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(Integer id) {
/*  87 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getCodigo() {
/*  91 */     return this.codigo;
/*     */   }
/*     */   
/*     */   public void setCodigo(String codigo) {
/*  95 */     this.codigo = codigo;
/*     */   }
/*     */   
/*     */   public Integer getPosicionPtl() {
/*  99 */     return this.posicionPtl;
/*     */   }
/*     */   
/*     */   public void setPosicionPtl(Integer posicionPtl) {
/* 103 */     this.posicionPtl = posicionPtl;
/*     */   }
/*     */   
/*     */   public String getEstado() {
/* 107 */     return this.estado;
/*     */   }
/*     */   
/*     */   public void setEstado(String estado) {
/* 111 */     this.estado = estado;
/*     */   }
/*     */   
/*     */   public Date getFechaCreacion() {
/* 115 */     return this.fechaCreacion;
/*     */   }
/*     */   
/*     */   public void setFechaCreacion(Date fechaCreacion) {
/* 119 */     this.fechaCreacion = fechaCreacion;
/*     */   }
/*     */   
/*     */   public Date getFechaCierre() {
/* 123 */     return this.fechaCierre;
/*     */   }
/*     */   
/*     */   public void setFechaCierre(Date fechaCierre) {
/* 127 */     this.fechaCierre = fechaCierre;
/*     */   }
/*     */   
/*     */   @XmlTransient
/*     */   public Collection<DetalleCaja> getDetalleCajaCollection() {
/* 132 */     return this.detalleCajaCollection;
/*     */   }
/*     */   
/*     */   public void setDetalleCajaCollection(Collection<DetalleCaja> detalleCajaCollection) {
/* 136 */     this.detalleCajaCollection = detalleCajaCollection;
/*     */   }
/*     */   
/*     */   public Picking getPickingId() {
/* 140 */     return this.pickingId;
/*     */   }
/*     */   
/*     */   public void setPickingId(Picking pickingId) {
/* 144 */     this.pickingId = pickingId;
/*     */   }
/*     */   
/*     */   public Destino getDestinoId() {
/* 148 */     return this.destinoId;
/*     */   }
/*     */   
/*     */   public void setDestinoId(Destino destinoId) {
/* 152 */     this.destinoId = destinoId;
/*     */   }
/*     */   
/*     */   public int hashCode()
/*     */   {
/* 157 */     int hash = 0;
/* 158 */     hash += (this.id != null ? this.id.hashCode() : 0);
/* 159 */     return hash;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean equals(Object object)
/*     */   {
/* 165 */     if (!(object instanceof Caja)) {
/* 166 */       return false;
/*     */     }
/* 168 */     Caja other = (Caja)object;
/* 169 */     if (((this.id == null) && (other.id != null)) || ((this.id != null) && (!this.id.equals(other.id)))) {
/* 170 */       return false;
/*     */     }
/* 172 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 177 */     return "Models.Entities.local.Caja[ id=" + this.id + " ]";
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Models/Entities/local/Caja.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */