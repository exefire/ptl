/*    */ package ui;
/*    */ 
/*    */ import java.awt.event.ActionEvent;
/*    */ import javax.swing.GroupLayout;
/*    */ import javax.swing.GroupLayout.Alignment;
/*    */ import javax.swing.GroupLayout.SequentialGroup;
/*    */ import javax.swing.JButton;
/*    */ 
/*    */ public class ConfirmationPanel extends javax.swing.JPanel
/*    */ {
/*    */   private JButton jButton1;
/*    */   private JButton jButton2;
/*    */   private javax.swing.JLabel jLabel1;
/*    */   
/*    */   public ConfirmationPanel()
/*    */   {
/* 17 */     initComponents();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private void initComponents()
/*    */   {
/* 29 */     this.jButton1 = new JButton();
/* 30 */     this.jButton2 = new JButton();
/* 31 */     this.jLabel1 = new javax.swing.JLabel();
/*    */     
/* 33 */     this.jButton1.setText("Aceptar");
/* 34 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*    */       public void actionPerformed(ActionEvent evt) {
/* 36 */         ConfirmationPanel.this.jButton1ActionPerformed(evt);
/*    */       }
/*    */       
/* 39 */     });
/* 40 */     this.jButton2.setText("Cancelar");
/* 41 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*    */       public void actionPerformed(ActionEvent evt) {
/* 43 */         ConfirmationPanel.this.jButton2ActionPerformed(evt);
/*    */       }
/*    */       
/* 46 */     });
/* 47 */     this.jLabel1.setText("jLabel1");
/*    */     
/* 49 */     GroupLayout layout = new GroupLayout(this);
/* 50 */     setLayout(layout);
/* 51 */     layout.setHorizontalGroup(layout
/* 52 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 53 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 54 */       .addGap(50, 50, 50)
/* 55 */       .addComponent(this.jLabel1, -1, -1, 32767)
/* 56 */       .addGap(60, 60, 60))
/* 57 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 58 */       .addGap(88, 88, 88)
/* 59 */       .addComponent(this.jButton2)
/* 60 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 327, 32767)
/* 61 */       .addComponent(this.jButton1)
/* 62 */       .addGap(103, 103, 103)));
/*    */     
/* 64 */     layout.setVerticalGroup(layout
/* 65 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 66 */       .addGroup(layout.createSequentialGroup()
/* 67 */       .addGap(41, 41, 41)
/* 68 */       .addComponent(this.jLabel1, -1, 167, 32767)
/* 69 */       .addGap(18, 18, 18)
/* 70 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 71 */       .addComponent(this.jButton2)
/* 72 */       .addComponent(this.jButton1))
/* 73 */       .addGap(49, 49, 49)));
/*    */   }
/*    */   
/*    */ 
/*    */   public void setText(String msg)
/*    */   {
/* 79 */     this.jLabel1.setText(msg);
/*    */   }
/*    */   
/*    */   private void jButton1ActionPerformed(ActionEvent evt) {}
/*    */   
/*    */   private void jButton2ActionPerformed(ActionEvent evt) {}
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/ConfirmationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */