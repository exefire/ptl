/*     */ package ui;
/*     */ 
/*     */ import Communication.CommunicationController;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.event.WindowAdapter;
/*     */ import java.awt.event.WindowEvent;
/*     */ import java.util.HashMap;
/*     */ import javax.swing.JOptionPane;
/*     */ import ui.headers.HeaderAdminPanel;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UiManager
/*     */ {
/*  21 */   private static BaseFrame uibase = null;
/*     */   
/*     */ 
/*  24 */   private PtlPanel currentPanel = null;
/*  25 */   private StatusBarPanel statusBar = null;
/*  26 */   private HeaderAdminPanel header = null;
/*     */   
/*  28 */   private boolean started = false;
/*     */   
/*  30 */   private static UiManager instance = null;
/*     */   
/*     */   protected UiManager()
/*     */   {
/*  34 */     uibase = new BaseFrame();
/*     */     
/*  36 */     uibase.setDefaultCloseOperation(0);
/*  37 */     uibase.addWindowListener(new WindowAdapter()
/*     */     {
/*     */       public void windowClosing(WindowEvent windowEvent) {
/*  40 */         Integer result = Integer.valueOf(JOptionPane.showConfirmDialog(UiManager.uibase, "¿Esta seguro que quiere cerrar la aplicacion?", "¿Esta seguro?", 0, 3));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  45 */         if (result.equals(Integer.valueOf(0))) {
/*  46 */           CommunicationController.getInstance().apagarTodasLasLuces();
/*  47 */           System.exit(0);
/*     */         }
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   public static UiManager getInstance()
/*     */   {
/*  55 */     if (instance == null) {
/*  56 */       instance = new UiManager();
/*     */     }
/*     */     
/*  59 */     return instance;
/*     */   }
/*     */   
/*     */   public void init()
/*     */   {
/*  64 */     startLoadingMode();
/*  65 */     uibase.setSize(new Dimension(400, 400));
/*  66 */     uibase.setLocationRelativeTo(null);
/*  67 */     uibase.setVisible(true);
/*     */     
/*  69 */     uibase.setTitle("PTL - Sportex");
/*     */     
/*     */ 
/*     */ 
/*  73 */     loadStatusBar();
/*     */   }
/*     */   
/*     */   public void loadStatusBar()
/*     */   {
/*  78 */     this.statusBar = new StatusBarPanel();
/*  79 */     this.statusBar.setPreferredSize(new Dimension(uibase.getWidth(), 20));
/*  80 */     uibase.add(this.statusBar, "South");
/*     */   }
/*     */   
/*     */   public void loadHeader(String layout) {
/*  84 */     if (layout.equals("admin")) {
/*  85 */       this.header = new HeaderAdminPanel();
/*     */       
/*  87 */       uibase.add(this.header, "North");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void startLoadingMode() {}
/*     */   
/*     */ 
/*     */   private void exitLoadingMode() {}
/*     */   
/*     */ 
/*     */   public void setFocusOnPanel()
/*     */   {
/* 101 */     this.currentPanel.setFocus();
/*     */   }
/*     */   
/*     */   public void showHeader(String layout) {
/* 105 */     loadHeader(layout);
/*     */   }
/*     */   
/*     */   public void loadPanel(PtlPanel panel) {
/* 109 */     if (!this.started) {
/* 110 */       exitLoadingMode();
/* 111 */       this.started = true;
/*     */     }
/*     */     
/* 114 */     if (panel == null) { return;
/*     */     }
/*     */     
/* 117 */     if (this.currentPanel != null)
/*     */     {
/* 119 */       this.currentPanel.setVisible(false);
/*     */     }
/*     */     
/*     */ 
/* 123 */     uibase.add(panel, "Center");
/*     */     
/* 125 */     panel.setVisible(true);
/*     */     
/* 127 */     uibase.setSize(panel.getPreferredSize().width + 50, panel.getPreferredSize().height + 50);
/* 128 */     uibase.setLocationRelativeTo(null);
/*     */     
/*     */ 
/* 131 */     this.currentPanel = panel;
/*     */     
/*     */ 
/* 134 */     uibase.validate();
/* 135 */     uibase.repaint();
/*     */   }
/*     */   
/*     */   public void clearPanel() {
/* 139 */     this.currentPanel.clearData();
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 144 */     this.currentPanel.notificate(msg);
/*     */   }
/*     */   
/*     */   public void setStatusInfo(String msg) {
/* 148 */     this.statusBar.setInfo(msg);
/*     */   }
/*     */   
/*     */   public void updateUiData(String event, HashMap atts)
/*     */   {
/* 153 */     this.currentPanel.incomingData(event, atts);
/*     */   }
/*     */   
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 159 */     this.currentPanel.actualizaInfo();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/UiManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */