/*     */ package ui.headers;
/*     */ 
/*     */ import Workflow.process.Administration.AdministrationJob;
/*     */ import java.awt.event.ActionEvent;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ 
/*     */ public class HeaderAdminPanel extends javax.swing.JPanel
/*     */ {
/*     */   private JButton jButton1;
/*     */   private JButton jButton2;
/*     */   private JButton jButton3;
/*     */   private JButton jButton4;
/*     */   private JButton jButton5;
/*     */   private javax.swing.JSeparator jSeparator1;
/*     */   
/*     */   public HeaderAdminPanel()
/*     */   {
/*  21 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  33 */     this.jButton2 = new JButton();
/*  34 */     this.jButton3 = new JButton();
/*  35 */     this.jButton1 = new JButton();
/*  36 */     this.jButton4 = new JButton();
/*  37 */     this.jSeparator1 = new javax.swing.JSeparator();
/*  38 */     this.jButton5 = new JButton();
/*     */     
/*  40 */     setBackground(java.awt.Color.white);
/*     */     
/*  42 */     this.jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/group-32.png")));
/*  43 */     this.jButton2.setText("Usuarios");
/*  44 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  46 */         HeaderAdminPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  49 */     });
/*  50 */     this.jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/warehouse-32.png")));
/*  51 */     this.jButton3.setText("Destinos");
/*  52 */     this.jButton3.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  54 */         HeaderAdminPanel.this.jButton3ActionPerformed(evt);
/*     */       }
/*     */       
/*  57 */     });
/*  58 */     this.jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/package-32.png")));
/*  59 */     this.jButton1.setText("Reiniciar N.V.");
/*  60 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  62 */         HeaderAdminPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/*  65 */     });
/*  66 */     this.jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/settings-32.png")));
/*  67 */     this.jButton4.setText("Configuración");
/*  68 */     this.jButton4.setToolTipText("Configuracion del controlador");
/*  69 */     this.jButton4.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  71 */         HeaderAdminPanel.this.jButton4ActionPerformed(evt);
/*     */       }
/*     */       
/*  74 */     });
/*  75 */     this.jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/xls-32.png")));
/*  76 */     this.jButton5.setText("Reporte");
/*  77 */     this.jButton5.setToolTipText("Configuracion del controlador");
/*  78 */     this.jButton5.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  80 */         HeaderAdminPanel.this.jButton5ActionPerformed(evt);
/*     */       }
/*     */       
/*  83 */     });
/*  84 */     GroupLayout layout = new GroupLayout(this);
/*  85 */     setLayout(layout);
/*  86 */     layout.setHorizontalGroup(layout
/*  87 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  88 */       .addGroup(layout.createSequentialGroup()
/*  89 */       .addComponent(this.jButton2, -2, 200, -2)
/*  90 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/*  91 */       .addComponent(this.jButton3, -2, 200, -2)
/*  92 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/*  93 */       .addComponent(this.jButton1, -2, 200, -2)
/*  94 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/*  95 */       .addComponent(this.jButton5, -2, 200, -2)
/*  96 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/*  97 */       .addComponent(this.jButton4, -2, 200, -2)
/*  98 */       .addContainerGap(-1, 32767))
/*  99 */       .addComponent(this.jSeparator1));
/*     */     
/* 101 */     layout.setVerticalGroup(layout
/* 102 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 103 */       .addGroup(layout.createSequentialGroup()
/* 104 */       .addContainerGap()
/* 105 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 106 */       .addComponent(this.jButton2)
/* 107 */       .addComponent(this.jButton3)
/* 108 */       .addComponent(this.jButton1)
/* 109 */       .addComponent(this.jButton4)
/* 110 */       .addComponent(this.jButton5))
/* 111 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, 32767)
/* 112 */       .addComponent(this.jSeparator1, -2, -1, -2)
/* 113 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */   private void jButton1ActionPerformed(ActionEvent evt)
/*     */   {
/* 118 */     AdministrationJob.getInstance().olaAdministration();
/*     */   }
/*     */   
/*     */   private void jButton2ActionPerformed(ActionEvent evt)
/*     */   {
/* 123 */     AdministrationJob.getInstance().usersAdministration();
/*     */   }
/*     */   
/*     */   private void jButton3ActionPerformed(ActionEvent evt)
/*     */   {
/* 128 */     AdministrationJob.getInstance().destinosAdministration();
/*     */   }
/*     */   
/*     */   private void jButton4ActionPerformed(ActionEvent evt) {
/* 132 */     AdministrationJob.getInstance().configAdministration();
/*     */   }
/*     */   
/*     */   private void jButton5ActionPerformed(ActionEvent evt) {
/* 136 */     AdministrationJob.getInstance().reportesAdministration();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/headers/HeaderAdminPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */