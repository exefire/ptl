/*     */ package ui.License;
/*     */ 
/*     */ import java.awt.Color;
/*     */ import java.awt.Font;
/*     */ import java.util.HashMap;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.LayoutStyle.ComponentPlacement;
/*     */ 
/*     */ public class LicensePanel extends ui.PtlPanel
/*     */ {
/*     */   private JLabel jLabel1;
/*     */   private JLabel jLabel2;
/*     */   private JLabel jLabel3;
/*     */   
/*     */   public LicensePanel()
/*     */   {
/*  21 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  33 */     this.jLabel3 = new JLabel();
/*  34 */     this.jLabel1 = new JLabel();
/*  35 */     this.jLabel2 = new JLabel();
/*     */     
/*  37 */     setBackground(new Color(255, 255, 255));
/*     */     
/*  39 */     this.jLabel3.setBackground(new Color(255, 255, 255));
/*  40 */     this.jLabel3.setHorizontalAlignment(0);
/*  41 */     this.jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/atait_test.png")));
/*     */     
/*  43 */     this.jLabel1.setFont(new Font("Tahoma", 1, 18));
/*  44 */     this.jLabel1.setForeground(new Color(153, 153, 255));
/*  45 */     this.jLabel1.setHorizontalAlignment(0);
/*  46 */     this.jLabel1.setText("Licencia Expirada");
/*     */     
/*  48 */     this.jLabel2.setHorizontalAlignment(0);
/*  49 */     this.jLabel2.setText("Pongase en contacto con su proveedor");
/*     */     
/*  51 */     GroupLayout layout = new GroupLayout(this);
/*  52 */     setLayout(layout);
/*  53 */     layout.setHorizontalGroup(layout
/*  54 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  55 */       .addComponent(this.jLabel3, -1, 400, 32767)
/*  56 */       .addComponent(this.jLabel1, GroupLayout.Alignment.TRAILING, -1, -1, 32767)
/*  57 */       .addComponent(this.jLabel2, GroupLayout.Alignment.TRAILING, -1, -1, 32767));
/*     */     
/*  59 */     layout.setVerticalGroup(layout
/*  60 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  61 */       .addGroup(layout.createSequentialGroup()
/*  62 */       .addComponent(this.jLabel3, -2, 197, -2)
/*  63 */       .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
/*  64 */       .addComponent(this.jLabel1, -2, 40, -2)
/*  65 */       .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
/*  66 */       .addComponent(this.jLabel2)
/*  67 */       .addGap(0, 33, 32767)));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/*  80 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/*  85 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void incomingData(String event, HashMap data)
/*     */   {
/*  90 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/*  95 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 100 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/License/LicensePanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */