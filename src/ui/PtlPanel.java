package ui;

import java.util.HashMap;
import javax.swing.JPanel;

public abstract class PtlPanel
  extends JPanel
{
  public abstract void clearData();
  
  public abstract void notificate(String paramString);
  
  public abstract void incomingData(String paramString, HashMap paramHashMap);
  
  public abstract void actualizaInfo();
  
  public abstract void setFocus();
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/PtlPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */