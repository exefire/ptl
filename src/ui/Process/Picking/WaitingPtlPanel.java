/*    */ package ui.Process.Picking;
/*    */ 
/*    */ import Workflow.process.Picking.SortingJob;
/*    */ import java.awt.Color;
/*    */ import java.awt.event.ActionEvent;
/*    */ import java.util.HashMap;
/*    */ import javax.swing.GroupLayout;
/*    */ import javax.swing.GroupLayout.Alignment;
/*    */ import javax.swing.GroupLayout.ParallelGroup;
/*    */ import javax.swing.GroupLayout.SequentialGroup;
/*    */ import javax.swing.JButton;
/*    */ import javax.swing.JLabel;
/*    */ 
/*    */ public class WaitingPtlPanel extends ui.PtlPanel
/*    */ {
/*    */   private JButton jButton1;
/*    */   private JLabel jLabel1;
/*    */   
/*    */   public WaitingPtlPanel()
/*    */   {
/* 21 */     initComponents();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private void initComponents()
/*    */   {
/* 33 */     this.jLabel1 = new JLabel();
/* 34 */     this.jButton1 = new JButton();
/*    */     
/* 36 */     setBackground(new Color(255, 255, 255));
/*    */     
/* 38 */     this.jLabel1.setFont(new java.awt.Font("Arial", 1, 24));
/* 39 */     this.jLabel1.setForeground(new Color(0, 102, 255));
/* 40 */     this.jLabel1.setHorizontalAlignment(0);
/* 41 */     this.jLabel1.setText("Presione la luz de la caja que desea cerrar");
/*    */     
/* 43 */     this.jButton1.setFont(new java.awt.Font("Tahoma", 1, 14));
/* 44 */     this.jButton1.setText("Continuar Sorting");
/* 45 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*    */       public void actionPerformed(ActionEvent evt) {
/* 47 */         WaitingPtlPanel.this.jButton1ActionPerformed(evt);
/*    */       }
/*    */       
/* 50 */     });
/* 51 */     GroupLayout layout = new GroupLayout(this);
/* 52 */     setLayout(layout);
/* 53 */     layout.setHorizontalGroup(layout
/* 54 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 55 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 56 */       .addContainerGap()
/* 57 */       .addComponent(this.jLabel1, -1, 817, 32767)
/* 58 */       .addContainerGap())
/* 59 */       .addGroup(layout.createSequentialGroup()
/* 60 */       .addGap(230, 230, 230)
/* 61 */       .addComponent(this.jButton1, -2, 375, -2)
/* 62 */       .addContainerGap(-1, 32767)));
/*    */     
/* 64 */     layout.setVerticalGroup(layout
/* 65 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 66 */       .addGroup(layout.createSequentialGroup()
/* 67 */       .addGap(98, 98, 98)
/* 68 */       .addComponent(this.jLabel1, -2, 112, -2)
/* 69 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 70 */       .addComponent(this.jButton1, -2, 45, -2)
/* 71 */       .addContainerGap(91, 32767)));
/*    */   }
/*    */   
/*    */   private void jButton1ActionPerformed(ActionEvent evt)
/*    */   {
/* 76 */     SortingJob.getInstance().endClosingBox();
/*    */   }
/*    */   
/*    */   public void clearData() {}
/*    */   
/*    */   public void notificate(String msg) {}
/*    */   
/*    */   public void incomingData(String event, HashMap data) {}
/*    */   
/*    */   public void actualizaInfo() {}
/*    */   
/*    */   public void setFocus() {}
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Picking/WaitingPtlPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */