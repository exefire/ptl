/*     */ package ui.Process.Picking;
/*     */ 
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ 
/*     */ public class InfoBultoPanel extends ui.PtlPanel
/*     */ {
/*     */   private javax.swing.JLabel bultoLabel;
/*     */   private javax.swing.JLabel bultoLabel1;
/*     */   private javax.swing.JButton jButton1;
/*     */   private javax.swing.JButton jButton2;
/*     */   private javax.swing.JLabel jLabel1;
/*     */   private javax.swing.JLabel jLabel2;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JPanel jPanel2;
/*     */   private javax.swing.JPanel jPanel3;
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   private javax.swing.JScrollPane jScrollPane2;
/*     */   private javax.swing.JTable jTable1;
/*     */   private javax.swing.JTable jTable2;
/*     */   
/*     */   public InfoBultoPanel()
/*     */   {
/*  26 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  38 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/*  39 */     this.jTable1 = new javax.swing.JTable();
/*  40 */     this.jPanel1 = new javax.swing.JPanel();
/*  41 */     this.jButton1 = new javax.swing.JButton();
/*  42 */     this.jLabel1 = new javax.swing.JLabel();
/*  43 */     this.bultoLabel = new javax.swing.JLabel();
/*  44 */     this.jPanel2 = new javax.swing.JPanel();
/*  45 */     this.jScrollPane2 = new javax.swing.JScrollPane();
/*  46 */     this.jTable2 = new javax.swing.JTable();
/*  47 */     this.jPanel3 = new javax.swing.JPanel();
/*  48 */     this.jButton2 = new javax.swing.JButton();
/*  49 */     this.jLabel2 = new javax.swing.JLabel();
/*  50 */     this.bultoLabel1 = new javax.swing.JLabel();
/*     */     
/*  52 */     setBackground(java.awt.Color.white);
/*     */     
/*  54 */     this.jScrollPane1.setBackground(java.awt.Color.white);
/*  55 */     this.jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle del nota de venta"));
/*     */     
/*  57 */     this.jTable1.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Articulo", "Cantidad", "Distribuido", "Pendiente" }));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  68 */     this.jScrollPane1.setViewportView(this.jTable1);
/*     */     
/*  70 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  71 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
/*     */     
/*  73 */     this.jButton1.setText("Volver");
/*  74 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  76 */         InfoBultoPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/*  79 */     });
/*  80 */     this.jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18));
/*  81 */     this.jLabel1.setForeground(new java.awt.Color(102, 102, 102));
/*  82 */     this.jLabel1.setText("Nota de Venta");
/*     */     
/*  84 */     this.bultoLabel.setFont(new java.awt.Font("Tahoma", 0, 14));
/*  85 */     this.bultoLabel.setText("B0000000000");
/*     */     
/*  87 */     GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
/*  88 */     this.jPanel1.setLayout(jPanel1Layout);
/*  89 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  90 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  91 */       .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/*  92 */       .addContainerGap()
/*  93 */       .addComponent(this.jLabel1)
/*  94 */       .addGap(18, 18, 18)
/*  95 */       .addComponent(this.bultoLabel, -1, 683, 32767)
/*  96 */       .addGap(18, 18, 18)
/*  97 */       .addComponent(this.jButton1, -2, 155, -2)
/*  98 */       .addGap(23, 23, 23)));
/*     */     
/* 100 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 101 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 102 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 103 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 104 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 105 */       .addGap(22, 22, 22)
/* 106 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 107 */       .addComponent(this.jLabel1)
/* 108 */       .addComponent(this.bultoLabel)))
/* 109 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 110 */       .addContainerGap()
/* 111 */       .addComponent(this.jButton1)))
/* 112 */       .addContainerGap(26, 32767)));
/*     */     
/*     */ 
/* 115 */     this.jPanel2.setBackground(java.awt.Color.white);
/*     */     
/* 117 */     this.jScrollPane2.setBackground(java.awt.Color.white);
/* 118 */     this.jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle del Nota Venta"));
/*     */     
/* 120 */     this.jTable2.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Articulo", "Cantidad", "Distribuido", "Pendiente" }));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 131 */     this.jScrollPane2.setViewportView(this.jTable2);
/*     */     
/* 133 */     this.jPanel3.setBackground(java.awt.Color.white);
/* 134 */     this.jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
/*     */     
/* 136 */     this.jButton2.setText("Volver");
/* 137 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 139 */         InfoBultoPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/* 142 */     });
/* 143 */     this.jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18));
/* 144 */     this.jLabel2.setForeground(new java.awt.Color(102, 102, 102));
/*     */     
/* 146 */     this.bultoLabel1.setFont(new java.awt.Font("Tahoma", 0, 14));
/* 147 */     this.bultoLabel1.setText("B0000000000");
/*     */     
/* 149 */     GroupLayout jPanel3Layout = new GroupLayout(this.jPanel3);
/* 150 */     this.jPanel3.setLayout(jPanel3Layout);
/* 151 */     jPanel3Layout.setHorizontalGroup(jPanel3Layout
/* 152 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 153 */       .addGroup(GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
/* 154 */       .addContainerGap()
/* 155 */       .addComponent(this.jLabel2, -2, 134, -2)
/* 156 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, 32767)
/* 157 */       .addComponent(this.bultoLabel1, -2, 639, -2)
/* 158 */       .addGap(18, 18, 18)
/* 159 */       .addComponent(this.jButton2, -2, 155, -2)
/* 160 */       .addGap(23, 23, 23)));
/*     */     
/* 162 */     jPanel3Layout.setVerticalGroup(jPanel3Layout
/* 163 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 164 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 165 */       .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 166 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 167 */       .addGap(22, 22, 22)
/* 168 */       .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 169 */       .addComponent(this.jLabel2)
/* 170 */       .addComponent(this.bultoLabel1)))
/* 171 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 172 */       .addContainerGap()
/* 173 */       .addComponent(this.jButton2)))
/* 174 */       .addContainerGap(26, 32767)));
/*     */     
/*     */ 
/* 177 */     GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
/* 178 */     this.jPanel2.setLayout(jPanel2Layout);
/* 179 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/* 180 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 181 */       .addGroup(GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
/* 182 */       .addContainerGap()
/* 183 */       .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
/* 184 */       .addComponent(this.jPanel3, -1, -1, 32767)
/* 185 */       .addComponent(this.jScrollPane2))
/* 186 */       .addContainerGap()));
/*     */     
/* 188 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/* 189 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 190 */       .addGroup(jPanel2Layout.createSequentialGroup()
/* 191 */       .addContainerGap()
/* 192 */       .addComponent(this.jScrollPane2, -1, 371, 32767)
/* 193 */       .addGap(18, 18, 18)
/* 194 */       .addComponent(this.jPanel3, -2, -1, -2)
/* 195 */       .addContainerGap()));
/*     */     
/*     */ 
/* 198 */     GroupLayout layout = new GroupLayout(this);
/* 199 */     setLayout(layout);
/* 200 */     layout.setHorizontalGroup(layout
/* 201 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 202 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 203 */       .addContainerGap()
/* 204 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
/* 205 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 206 */       .addComponent(this.jScrollPane1))
/* 207 */       .addContainerGap())
/* 208 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 209 */       .addGroup(layout.createSequentialGroup()
/* 210 */       .addGap(0, 0, 32767)
/* 211 */       .addComponent(this.jPanel2, -2, -1, -2)
/* 212 */       .addGap(0, 0, 32767))));
/*     */     
/* 214 */     layout.setVerticalGroup(layout
/* 215 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 216 */       .addGroup(layout.createSequentialGroup()
/* 217 */       .addContainerGap()
/* 218 */       .addComponent(this.jScrollPane1, -1, 371, 32767)
/* 219 */       .addGap(18, 18, 18)
/* 220 */       .addComponent(this.jPanel1, -2, -1, -2)
/* 221 */       .addContainerGap())
/* 222 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 223 */       .addGroup(layout.createSequentialGroup()
/* 224 */       .addGap(0, 0, 32767)
/* 225 */       .addComponent(this.jPanel2, -2, -1, -2)
/* 226 */       .addGap(0, 0, 32767))));
/*     */     
/*     */ 
/* 229 */     this.jScrollPane1.getAccessibleContext().setAccessibleName("Detalle de la Nota de Venta");
/*     */   }
/*     */   
/*     */   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
/* 233 */     Workflow.process.Picking.SortingJob.getInstance().comeBackToSorting();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 260 */     this.bultoLabel.setText("");
/*     */     
/* 262 */     javax.swing.table.DefaultTableModel dm = (javax.swing.table.DefaultTableModel)this.jTable1.getModel();
/* 263 */     for (int i = dm.getRowCount() - 1; i >= 0; i--) {
/* 264 */       dm.removeRow(i);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void notificate(String msg)
/*     */   {
/* 271 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/*     */     javax.swing.table.DefaultTableModel dm;
/* 277 */     if (event.equals("load")) {
/* 278 */       clearData();
/* 279 */       this.bultoLabel.setText(data.get("codigo").toString());
/* 280 */       java.util.List<Object> lista = (java.util.List)data.get("articulos");
/*     */       
/* 282 */       if (lista != null) {
/* 283 */         dm = (javax.swing.table.DefaultTableModel)this.jTable1.getModel();
/*     */         
/* 285 */         for (Object a : lista)
/*     */         {
/* 287 */           Object[] o = new Object[5];
/* 288 */           o[0] = ((String)((Object[])(Object[])a)[1]);
/* 289 */           o[1] = ((java.math.BigDecimal)((Object[])(Object[])a)[2]);
/* 290 */           o[2] = ((java.math.BigDecimal)((Object[])(Object[])a)[3]);
/* 291 */           o[3] = ((java.math.BigDecimal)((Object[])(Object[])a)[2]).toBigInteger().subtract(((java.math.BigDecimal)((Object[])(Object[])a)[3]).toBigInteger());
/* 292 */           dm.addRow(o);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 302 */     throw new UnsupportedOperationException("Info Bulto - Actualizar INFO NO DEBE OCURRIR)");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 307 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Picking/InfoBultoPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */