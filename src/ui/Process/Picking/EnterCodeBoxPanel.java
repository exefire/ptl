/*     */ package ui.Process.Picking;
/*     */ 
/*     */ import Workflow.process.Picking.SortingJob;
/*     */ import java.awt.Color;
/*     */ import java.awt.Font;
/*     */ import java.awt.event.ActionEvent;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextField;
/*     */ 
/*     */ public class EnterCodeBoxPanel extends ui.PtlPanel
/*     */ {
/*     */   private JButton jButton1;
/*     */   private JLabel jLabel1;
/*     */   private JTextField newCodeBox;
/*     */   private JLabel notificationLabel;
/*     */   
/*     */   public EnterCodeBoxPanel()
/*     */   {
/*  24 */     initComponents();
/*  25 */     this.newCodeBox.requestFocusInWindow();
/*  26 */     this.jButton1.setVisible(false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  38 */     this.jLabel1 = new JLabel();
/*  39 */     this.newCodeBox = new JTextField();
/*  40 */     this.notificationLabel = new JLabel();
/*  41 */     this.jButton1 = new JButton();
/*     */     
/*  43 */     setBackground(new Color(255, 255, 255));
/*     */     
/*  45 */     this.jLabel1.setFont(new Font("Tahoma", 0, 24));
/*  46 */     this.jLabel1.setForeground(new Color(102, 153, 255));
/*  47 */     this.jLabel1.setHorizontalAlignment(0);
/*  48 */     this.jLabel1.setText("Nuevo código de caja");
/*     */     
/*  50 */     this.newCodeBox.setFont(new Font("Tahoma", 1, 36));
/*  51 */     this.newCodeBox.setForeground(Color.red);
/*  52 */     this.newCodeBox.setHorizontalAlignment(0);
/*  53 */     this.newCodeBox.setEnabled(false);
/*  54 */     this.newCodeBox.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  56 */         EnterCodeBoxPanel.this.newCodeBoxActionPerformed(evt);
/*     */       }
/*     */       
/*  59 */     });
/*  60 */     this.notificationLabel.setFont(new Font("Tahoma", 0, 12));
/*  61 */     this.notificationLabel.setForeground(new Color(255, 0, 0));
/*  62 */     this.notificationLabel.setHorizontalAlignment(0);
/*     */     
/*  64 */     this.jButton1.setFont(new Font("Tahoma", 1, 18));
/*  65 */     this.jButton1.setForeground(javax.swing.UIManager.getDefaults().getColor("TextArea.selectionBackground"));
/*  66 */     this.jButton1.setText("Continuar");
/*  67 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  69 */         EnterCodeBoxPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/*  72 */     });
/*  73 */     GroupLayout layout = new GroupLayout(this);
/*  74 */     setLayout(layout);
/*  75 */     layout.setHorizontalGroup(layout
/*  76 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  77 */       .addGroup(layout.createSequentialGroup()
/*  78 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/*  79 */       .addGroup(layout.createSequentialGroup()
/*  80 */       .addContainerGap()
/*  81 */       .addComponent(this.jLabel1, -1, -1, 32767))
/*  82 */       .addGroup(layout.createSequentialGroup()
/*  83 */       .addGap(156, 156, 156)
/*  84 */       .addComponent(this.newCodeBox, -2, 555, -2)
/*  85 */       .addGap(0, 108, 32767))
/*  86 */       .addGroup(layout.createSequentialGroup()
/*  87 */       .addContainerGap()
/*  88 */       .addComponent(this.notificationLabel, -1, -1, 32767)))
/*  89 */       .addContainerGap())
/*  90 */       .addGroup(layout.createSequentialGroup()
/*  91 */       .addGap(211, 211, 211)
/*  92 */       .addComponent(this.jButton1, -2, 426, -2)
/*  93 */       .addContainerGap(-1, 32767)));
/*     */     
/*  95 */     layout.setVerticalGroup(layout
/*  96 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  97 */       .addGroup(layout.createSequentialGroup()
/*  98 */       .addGap(62, 62, 62)
/*  99 */       .addComponent(this.jLabel1, -2, 40, -2)
/* 100 */       .addGap(18, 18, 18)
/* 101 */       .addComponent(this.newCodeBox, -2, 65, -2)
/* 102 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 103 */       .addComponent(this.notificationLabel)
/* 104 */       .addGap(18, 18, 18)
/* 105 */       .addComponent(this.jButton1, -2, 53, -2)
/* 106 */       .addContainerGap(67, 32767)));
/*     */   }
/*     */   
/*     */ 
/*     */   private void newCodeBoxActionPerformed(ActionEvent evt)
/*     */   {
/* 112 */     SortingJob.getInstance().newCodeEntered(evt.getActionCommand());
/*     */   }
/*     */   
/*     */   private void jButton1ActionPerformed(ActionEvent evt) {
/* 116 */     SortingJob.getInstance().newCodeEntered(this.newCodeBox.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 128 */     this.notificationLabel.setText("");
/* 129 */     this.newCodeBox.setText("");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 134 */     this.notificationLabel.setText(msg);
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 139 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/* 144 */     Integer maxid = Models.JpaControllers.local.CajaJpaController.getInstance().getMaxId();
/* 145 */     String codigo = formatCode(Configuration.ConfigurationManager.getInstance().getProperty("id_ptl"), Integer.valueOf(maxid.intValue() + 1));
/* 146 */     this.newCodeBox.setText(codigo);
/* 147 */     Workflow.process.Picking.CloseBoxTask.getInstance().setBoxCode(codigo);
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 153 */     this.newCodeBox.requestFocusInWindow();
/*     */   }
/*     */   
/*     */ 
/*     */   private String formatCode(String prefijo, Integer numero)
/*     */   {
/* 159 */     String codigo = prefijo;
/*     */     
/* 161 */     String aux = numero.toString();
/* 162 */     if (aux.length() < 10) {
/* 163 */       for (int i = 0; i < 10 - numero.toString().length(); i++) {
/* 164 */         aux = "0" + aux;
/*     */       }
/*     */     }
/* 167 */     codigo = prefijo + aux;
/*     */     
/* 169 */     return codigo;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Picking/EnterCodeBoxPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */