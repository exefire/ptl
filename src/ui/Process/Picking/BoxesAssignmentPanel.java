/*     */ package ui.Process.Picking;
/*     */ 
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class BoxesAssignmentPanel extends ui.PtlPanel { private javax.swing.JButton BTN_IniciarSorting;
/*     */   private javax.swing.JTextField CodigoBarraInput;
/*     */   private JLabel jLabel1;
/*     */   private JLabel jLabel10;
/*     */   private JLabel jLabel2;
/*     */   private JLabel jLabel3;
/*     */   private JLabel jLabel4;
/*     */   private JLabel jLabel5;
/*     */   private JLabel jLabel6;
/*     */   private JLabel jLabel7;
/*     */   private JLabel jLabel8;
/*     */   private JLabel jLabel9;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JPanel jPanel2;
/*     */   private javax.swing.JPanel jPanel3;
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   private javax.swing.JTable jTable1;
/*     */   private JLabel notificationLabel;
/*     */   
/*  24 */   public BoxesAssignmentPanel() { initComponents();
/*     */     
/*  26 */     this.CodigoBarraInput.requestFocusInWindow();
/*  27 */     this.BTN_IniciarSorting.setEnabled(false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  39 */     this.jPanel1 = new javax.swing.JPanel();
/*  40 */     this.jLabel1 = new JLabel();
/*  41 */     this.jLabel2 = new JLabel();
/*  42 */     this.jPanel2 = new javax.swing.JPanel();
/*  43 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/*  44 */     this.jTable1 = new javax.swing.JTable();
/*  45 */     this.jPanel3 = new javax.swing.JPanel();
/*  46 */     this.BTN_IniciarSorting = new javax.swing.JButton();
/*  47 */     this.CodigoBarraInput = new javax.swing.JTextField();
/*  48 */     this.jLabel3 = new JLabel();
/*  49 */     this.jLabel4 = new JLabel();
/*  50 */     this.jLabel5 = new JLabel();
/*  51 */     this.jLabel6 = new JLabel();
/*  52 */     this.jLabel7 = new JLabel();
/*  53 */     this.jLabel8 = new JLabel();
/*  54 */     this.jLabel9 = new JLabel();
/*  55 */     this.notificationLabel = new JLabel();
/*  56 */     this.jLabel10 = new JLabel();
/*     */     
/*  58 */     setBackground(java.awt.Color.white);
/*  59 */     setMinimumSize(new java.awt.Dimension(1005, 508));
/*  60 */     setPreferredSize(new java.awt.Dimension(1005, 508));
/*     */     
/*  62 */     this.jPanel1.setBackground(new java.awt.Color(255, 255, 255));
/*  63 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
/*     */     
/*  65 */     this.jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18));
/*  66 */     this.jLabel1.setText("Asignación de Cajas");
/*     */     
/*  68 */     this.jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/logo_retailsoft_240.png")));
/*     */     
/*  70 */     javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.jPanel1);
/*  71 */     this.jPanel1.setLayout(jPanel1Layout);
/*  72 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  73 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  74 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  75 */       .addContainerGap()
/*  76 */       .addComponent(this.jLabel2, -2, 253, -2)
/*  77 */       .addGap(64, 64, 64)
/*  78 */       .addComponent(this.jLabel1, -2, 352, -2)
/*  79 */       .addContainerGap(296, 32767)));
/*     */     
/*  81 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/*  82 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  83 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  84 */       .addGap(37, 37, 37)
/*  85 */       .addComponent(this.jLabel1)
/*  86 */       .addContainerGap(37, 32767))
/*  87 */       .addComponent(this.jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, -2, 0, 32767));
/*     */     
/*     */ 
/*  90 */     this.jTable1.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null } }, new String[] { "Destino", "Posición en ptl", "Código de bulto" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 101 */       boolean[] canEdit = { false, false, false };
/*     */       
/*     */ 
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex)
/*     */       {
/* 106 */         return this.canEdit[columnIndex];
/*     */       }
/* 108 */     });
/* 109 */     this.jTable1.setEnabled(false);
/* 110 */     this.jScrollPane1.setViewportView(this.jTable1);
/*     */     
/* 112 */     javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(this.jPanel2);
/* 113 */     this.jPanel2.setLayout(jPanel2Layout);
/* 114 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/* 115 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 116 */       .addComponent(this.jScrollPane1, -1, 572, 32767));
/*     */     
/* 118 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/* 119 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 120 */       .addComponent(this.jScrollPane1, -2, 0, 32767));
/*     */     
/*     */ 
/* 123 */     this.jPanel3.setBackground(new java.awt.Color(255, 255, 255));
/* 124 */     this.jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Información"));
/*     */     
/* 126 */     this.BTN_IniciarSorting.setText("Iniciar Sorting");
/* 127 */     this.BTN_IniciarSorting.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 129 */         BoxesAssignmentPanel.this.BTN_IniciarSortingActionPerformed(evt);
/*     */       }
/*     */       
/* 132 */     });
/* 133 */     this.CodigoBarraInput.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 135 */         BoxesAssignmentPanel.this.CodigoBarraInputActionPerformed(evt);
/*     */       }
/*     */       
/* 138 */     });
/* 139 */     this.jLabel3.setHorizontalAlignment(0);
/* 140 */     this.jLabel3.setText("Código de Barras del bulto");
/*     */     
/* 142 */     this.jLabel4.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 143 */     this.jLabel4.setText("Destino:");
/*     */     
/* 145 */     this.jLabel5.setText("DESTINO");
/*     */     
/* 147 */     this.jLabel6.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 148 */     this.jLabel6.setText("Posición:");
/*     */     
/* 150 */     this.jLabel7.setText("1");
/*     */     
/* 152 */     this.jLabel8.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 153 */     this.jLabel8.setText("Ola:");
/*     */     
/* 155 */     this.jLabel9.setText("1 de 1");
/*     */     
/* 157 */     this.notificationLabel.setForeground(java.awt.Color.red);
/* 158 */     this.notificationLabel.setHorizontalAlignment(0);
/*     */     
/* 160 */     this.jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24));
/* 161 */     this.jLabel10.setForeground(java.awt.Color.red);
/* 162 */     this.jLabel10.setHorizontalAlignment(0);
/* 163 */     this.jLabel10.setText("0000000000000000");
/*     */     
/* 165 */     javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(this.jPanel3);
/* 166 */     this.jPanel3.setLayout(jPanel3Layout);
/* 167 */     jPanel3Layout.setHorizontalGroup(jPanel3Layout
/* 168 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 169 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 170 */       .addContainerGap()
/* 171 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 172 */       .addComponent(this.BTN_IniciarSorting, -1, -1, 32767)
/* 173 */       .addComponent(this.jLabel3, -1, 366, 32767)
/* 174 */       .addComponent(this.CodigoBarraInput, javax.swing.GroupLayout.Alignment.TRAILING)
/* 175 */       .addComponent(this.notificationLabel, -1, -1, 32767)
/* 176 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 177 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 178 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 179 */       .addComponent(this.jLabel6, -2, 64, -2)
/* 180 */       .addGap(18, 18, 18)
/* 181 */       .addComponent(this.jLabel7))
/* 182 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 183 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 184 */       .addComponent(this.jLabel4, -2, 64, -2)
/* 185 */       .addComponent(this.jLabel8))
/* 186 */       .addGap(18, 18, 18)
/* 187 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 188 */       .addComponent(this.jLabel9)
/* 189 */       .addComponent(this.jLabel5))))
/* 190 */       .addGap(0, 0, 32767))
/* 191 */       .addComponent(this.jLabel10, -1, -1, 32767))
/* 192 */       .addContainerGap()));
/*     */     
/* 194 */     jPanel3Layout.setVerticalGroup(jPanel3Layout
/* 195 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 196 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
/* 197 */       .addGap(5, 5, 5)
/* 198 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 199 */       .addComponent(this.jLabel8)
/* 200 */       .addComponent(this.jLabel9))
/* 201 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 202 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 203 */       .addComponent(this.jLabel4)
/* 204 */       .addComponent(this.jLabel5))
/* 205 */       .addGap(18, 18, 18)
/* 206 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 207 */       .addComponent(this.jLabel6)
/* 208 */       .addComponent(this.jLabel7))
/* 209 */       .addGap(18, 18, 18)
/* 210 */       .addComponent(this.jLabel3)
/* 211 */       .addGap(5, 5, 5)
/* 212 */       .addComponent(this.jLabel10, -2, 35, -2)
/* 213 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 214 */       .addComponent(this.CodigoBarraInput, -2, 43, -2)
/* 215 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 216 */       .addComponent(this.notificationLabel, -2, 26, -2)
/* 217 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, 32767)
/* 218 */       .addComponent(this.BTN_IniciarSorting, -2, 54, -2)
/* 219 */       .addContainerGap()));
/*     */     
/*     */ 
/* 222 */     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
/* 223 */     setLayout(layout);
/* 224 */     layout.setHorizontalGroup(layout
/* 225 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 226 */       .addGroup(layout.createSequentialGroup()
/* 227 */       .addContainerGap()
/* 228 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 229 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 230 */       .addGroup(layout.createSequentialGroup()
/* 231 */       .addComponent(this.jPanel2, -1, -1, 32767)
/* 232 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 233 */       .addComponent(this.jPanel3, -2, -1, -2)))
/* 234 */       .addContainerGap()));
/*     */     
/* 236 */     layout.setVerticalGroup(layout
/* 237 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 238 */       .addGroup(layout.createSequentialGroup()
/* 239 */       .addGap(7, 7, 7)
/* 240 */       .addComponent(this.jPanel1, -2, -1, -2)
/* 241 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 242 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 243 */       .addComponent(this.jPanel3, -1, -1, 32767)
/* 244 */       .addComponent(this.jPanel2, -1, -1, 32767))
/* 245 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */ 
/*     */   private void BTN_IniciarSortingActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 251 */     Workflow.process.Picking.BoxesAssigmentJob.getInstance().endAssignment();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void CodigoBarraInputActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 258 */     this.CodigoBarraInput.setEnabled(false);
/* 259 */     this.notificationLabel.setText("");
/* 260 */     Workflow.process.Picking.BoxesAssigmentJob.getInstance().newBoxCode("picking", this.jTable1.getModel().getValueAt(this.jTable1.getSelectedRow(), 0).toString(), "posicionthis", this.CodigoBarraInput.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 289 */     this.CodigoBarraInput.setText("");
/* 290 */     this.notificationLabel.setText("");
/* 291 */     this.BTN_IniciarSorting.setEnabled(false);
/* 292 */     this.jLabel10.setText("");
/*     */     
/* 294 */     javax.swing.table.DefaultTableModel dm = (javax.swing.table.DefaultTableModel)this.jTable1.getModel();
/* 295 */     for (int i = dm.getRowCount() - 1; i >= 0; i--) {
/* 296 */       dm.removeRow(i);
/*     */     }
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 302 */     this.notificationLabel.setText(msg);
/*     */   }
/*     */   
/*     */ 
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/*     */     Integer localInteger1;
/* 309 */     if (event.equals("loadDestinos")) {
/* 310 */       clearData();
/* 311 */       java.util.List<Models.Entities.local.Destino> lista = (java.util.List)data.get("destinos");
/*     */       
/* 313 */       javax.swing.table.DefaultTableModel dt = (javax.swing.table.DefaultTableModel)this.jTable1.getModel();
/* 314 */       Integer i = Integer.valueOf(1);
/* 315 */       Integer localInteger2; for (java.util.Iterator localIterator = lista.iterator(); localIterator.hasNext(); 
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 321 */           localInteger2 = i = Integer.valueOf(i.intValue() + 1))
/*     */       {
/* 315 */         Models.Entities.local.Destino d = (Models.Entities.local.Destino)localIterator.next();
/* 316 */         Object[] o = new Object[5];
/* 317 */         o[0] = d.getNombre();
/* 318 */         o[1] = i.toString();
/* 319 */         o[2] = "Sin Codigo";
/* 320 */         dt.addRow(o);
/* 321 */         localInteger1 = i;
/*     */       }
/*     */     }
/* 324 */     else if (event.equals("nextBox")) {
/* 325 */       Integer i = (Integer)data.get("position");
/* 326 */       String codigo = (String)data.get("codigo");
/* 327 */       String lastcodigo = (String)data.get("lastcodigo");
/* 328 */       boolean fin = ((Boolean)data.get("finalizado")).booleanValue();
/*     */       
/* 330 */       this.CodigoBarraInput.setText(codigo);
/* 331 */       this.jLabel10.setText(codigo);
/* 332 */       this.CodigoBarraInput.requestFocusInWindow();
/*     */       
/* 334 */       if (i.intValue() - 1 > 0) {
/* 335 */         this.jTable1.getModel().setValueAt(lastcodigo, i.intValue() - 2, 2);
/* 336 */         this.jTable1.removeRowSelectionInterval(i.intValue() - 2, i.intValue() - 2);
/*     */       }
/* 338 */       if (!fin)
/*     */       {
/*     */ 
/* 341 */         this.CodigoBarraInput.setEnabled(false);
/*     */         
/* 343 */         this.jTable1.addRowSelectionInterval(i.intValue() - 1, i.intValue() - 1);
/*     */         
/*     */ 
/* 346 */         Workflow.process.Picking.BoxesAssigmentJob.getInstance().newBoxCode("picking", this.jTable1.getModel().getValueAt(this.jTable1.getSelectedRow(), 0).toString(), "posicionthis", this.CodigoBarraInput.getText());
/*     */       }
/*     */       else
/*     */       {
/* 350 */         this.BTN_IniciarSorting.setEnabled(true);
/* 351 */         this.BTN_IniciarSorting.requestFocusInWindow();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 360 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 365 */     this.CodigoBarraInput.setText("");
/* 366 */     this.CodigoBarraInput.setEnabled(true);
/* 367 */     this.CodigoBarraInput.requestFocusInWindow();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Picking/BoxesAssignmentPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */