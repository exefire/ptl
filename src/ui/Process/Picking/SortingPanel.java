/*     */ package ui.Process.Picking;
/*     */ 
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class SortingPanel extends ui.PtlPanel
/*     */ {
/*     */   private JLabel articuloLabel;
/*     */   private javax.swing.JButton avanzarOlaButton;
/*     */   private JLabel cantidadOlaLabel;
/*     */   private javax.swing.JButton cerrarCajaButton;
/*     */   private JLabel codigoBarraLabel;
/*     */   private JLabel distribuidosArticuloLabel;
/*     */   private JLabel distribuidosOlaLabel;
/*     */   private javax.swing.JButton finalizarSortingButton;
/*     */   private javax.swing.JButton jButton1;
/*     */   private JLabel jLabel1;
/*     */   private JLabel jLabel10;
/*     */   private JLabel jLabel11;
/*     */   private JLabel jLabel13;
/*     */   private JLabel jLabel14;
/*     */   private JLabel jLabel15;
/*     */   private JLabel jLabel16;
/*     */   private JLabel jLabel17;
/*     */   
/*     */   public SortingPanel() {
/*  26 */     initComponents();
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  32 */     actualizaInfo();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  45 */     this.jPanel1 = new javax.swing.JPanel();
/*  46 */     this.jPanel5 = new javax.swing.JPanel();
/*  47 */     this.jLabel8 = new JLabel();
/*  48 */     this.jLabel9 = new JLabel();
/*  49 */     this.jLabel10 = new JLabel();
/*  50 */     this.jLabel11 = new JLabel();
/*  51 */     this.cantidadOlaLabel = new JLabel();
/*  52 */     this.olaLabel = new JLabel();
/*  53 */     this.distribuidosOlaLabel = new JLabel();
/*  54 */     this.pendientesOlaLabel = new JLabel();
/*  55 */     this.jPanel6 = new javax.swing.JPanel();
/*  56 */     this.jLabel13 = new JLabel();
/*  57 */     this.jLabel14 = new JLabel();
/*  58 */     this.jLabel15 = new JLabel();
/*  59 */     this.jLabel16 = new JLabel();
/*  60 */     this.codigoBarraLabel = new JLabel();
/*  61 */     this.articuloLabel = new JLabel();
/*  62 */     this.distribuidosArticuloLabel = new JLabel();
/*  63 */     this.pendientesArticulosLabel = new JLabel();
/*  64 */     this.jLabel17 = new JLabel();
/*  65 */     this.totalArticuloLabel = new JLabel();
/*  66 */     this.jPanel3 = new javax.swing.JPanel();
/*  67 */     this.skuinput = new javax.swing.JTextField();
/*  68 */     this.notificateLabel = new JLabel();
/*  69 */     this.cerrarCajaButton = new javax.swing.JButton();
/*  70 */     this.avanzarOlaButton = new javax.swing.JButton();
/*  71 */     this.finalizarSortingButton = new javax.swing.JButton();
/*  72 */     this.jLabel2 = new JLabel();
/*  73 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/*  74 */     this.tablatransacciones = new javax.swing.JTable();
/*  75 */     this.jButton1 = new javax.swing.JButton();
/*  76 */     this.jPanel2 = new javax.swing.JPanel();
/*  77 */     this.jLabel1 = new JLabel();
/*  78 */     this.jLabel3 = new JLabel();
/*     */     
/*  80 */     setBackground(new java.awt.Color(255, 255, 255));
/*  81 */     setLayout(new java.awt.BorderLayout());
/*     */     
/*  83 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  84 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Información"));
/*  85 */     this.jPanel1.setPreferredSize(new java.awt.Dimension(400, 700));
/*     */     
/*  87 */     this.jPanel5.setBackground(java.awt.Color.white);
/*  88 */     this.jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Información del Nota de Venta"));
/*     */     
/*  90 */     this.jLabel8.setText("Nota Venta");
/*     */     
/*  92 */     this.jLabel9.setText("Cantidad");
/*     */     
/*  94 */     this.jLabel10.setText("Distribuidos");
/*     */     
/*  96 */     this.jLabel11.setText("Pendientes");
/*     */     
/*  98 */     this.cantidadOlaLabel.setText("--");
/*     */     
/* 100 */     this.olaLabel.setText("--");
/*     */     
/* 102 */     this.distribuidosOlaLabel.setText("--");
/*     */     
/* 104 */     this.pendientesOlaLabel.setText("--");
/*     */     
/* 106 */     javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(this.jPanel5);
/* 107 */     this.jPanel5.setLayout(jPanel5Layout);
/* 108 */     jPanel5Layout.setHorizontalGroup(jPanel5Layout
/* 109 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 110 */       .addGroup(jPanel5Layout.createSequentialGroup()
/* 111 */       .addGap(23, 23, 23)
/* 112 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 113 */       .addGroup(jPanel5Layout.createSequentialGroup()
/* 114 */       .addComponent(this.jLabel8)
/* 115 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
/* 116 */       .addComponent(this.olaLabel, -2, 210, -2))
/* 117 */       .addGroup(jPanel5Layout.createSequentialGroup()
/* 118 */       .addComponent(this.jLabel9)
/* 119 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, 32767)
/* 120 */       .addComponent(this.cantidadOlaLabel, -2, 210, -2))
/* 121 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
/* 122 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 123 */       .addComponent(this.jLabel10)
/* 124 */       .addComponent(this.jLabel11))
/* 125 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
/* 126 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
/* 127 */       .addComponent(this.pendientesOlaLabel, -1, 210, 32767)
/* 128 */       .addComponent(this.distribuidosOlaLabel, -1, -1, 32767))))
/* 129 */       .addContainerGap()));
/*     */     
/* 131 */     jPanel5Layout.setVerticalGroup(jPanel5Layout
/* 132 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 133 */       .addGroup(jPanel5Layout.createSequentialGroup()
/* 134 */       .addContainerGap()
/* 135 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 136 */       .addComponent(this.jLabel8)
/* 137 */       .addComponent(this.olaLabel))
/* 138 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 139 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 140 */       .addComponent(this.jLabel9)
/* 141 */       .addComponent(this.cantidadOlaLabel))
/* 142 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 143 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 144 */       .addComponent(this.jLabel10)
/* 145 */       .addComponent(this.distribuidosOlaLabel))
/* 146 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 147 */       .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 148 */       .addComponent(this.jLabel11)
/* 149 */       .addComponent(this.pendientesOlaLabel))
/* 150 */       .addContainerGap(36, 32767)));
/*     */     
/*     */ 
/* 153 */     this.jPanel6.setBackground(java.awt.Color.white);
/* 154 */     this.jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Información de Articulo", 0, 0, new java.awt.Font("Dialog", 1, 12)));
/*     */     
/* 156 */     this.jLabel13.setText("Código de barra");
/*     */     
/* 158 */     this.jLabel14.setText("Articulo");
/*     */     
/* 160 */     this.jLabel15.setText("Distribuidos");
/*     */     
/* 162 */     this.jLabel16.setText("Pendientes");
/*     */     
/* 164 */     this.codigoBarraLabel.setText("--");
/*     */     
/* 166 */     this.articuloLabel.setText("--");
/*     */     
/* 168 */     this.distribuidosArticuloLabel.setText("--");
/*     */     
/* 170 */     this.pendientesArticulosLabel.setText("--");
/*     */     
/* 172 */     this.jLabel17.setText("Total");
/*     */     
/* 174 */     this.totalArticuloLabel.setText("--");
/*     */     
/* 176 */     javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(this.jPanel6);
/* 177 */     this.jPanel6.setLayout(jPanel6Layout);
/* 178 */     jPanel6Layout.setHorizontalGroup(jPanel6Layout
/* 179 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 180 */       .addGroup(jPanel6Layout.createSequentialGroup()
/* 181 */       .addGap(23, 23, 23)
/* 182 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 183 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
/* 184 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 185 */       .addComponent(this.jLabel15)
/* 186 */       .addComponent(this.jLabel16)
/* 187 */       .addComponent(this.jLabel17))
/* 188 */       .addGap(46, 46, 46)
/* 189 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 190 */       .addComponent(this.pendientesArticulosLabel, javax.swing.GroupLayout.Alignment.TRAILING, -1, -1, 32767)
/* 191 */       .addComponent(this.distribuidosArticuloLabel, -1, -1, 32767)
/* 192 */       .addComponent(this.totalArticuloLabel, -1, -1, 32767)))
/* 193 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
/* 194 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 195 */       .addComponent(this.jLabel13)
/* 196 */       .addComponent(this.jLabel14))
/* 197 */       .addGap(18, 18, 18)
/* 198 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 199 */       .addComponent(this.articuloLabel, -1, -1, 32767)
/* 200 */       .addComponent(this.codigoBarraLabel, -2, 207, -2))))
/* 201 */       .addContainerGap()));
/*     */     
/* 203 */     jPanel6Layout.setVerticalGroup(jPanel6Layout
/* 204 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 205 */       .addGroup(jPanel6Layout.createSequentialGroup()
/* 206 */       .addContainerGap()
/* 207 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 208 */       .addComponent(this.jLabel13)
/* 209 */       .addComponent(this.codigoBarraLabel))
/* 210 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 211 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 212 */       .addComponent(this.jLabel14)
/* 213 */       .addComponent(this.articuloLabel))
/* 214 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 215 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 216 */       .addComponent(this.jLabel15)
/* 217 */       .addComponent(this.distribuidosArticuloLabel, -2, 17, -2))
/* 218 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 219 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 220 */       .addComponent(this.jLabel16)
/* 221 */       .addComponent(this.pendientesArticulosLabel))
/* 222 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
/* 223 */       .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 224 */       .addComponent(this.jLabel17)
/* 225 */       .addComponent(this.totalArticuloLabel))));
/*     */     
/*     */ 
/* 228 */     javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.jPanel1);
/* 229 */     this.jPanel1.setLayout(jPanel1Layout);
/* 230 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/* 231 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 232 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 233 */       .addContainerGap()
/* 234 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 235 */       .addComponent(this.jPanel6, -2, 360, -2)
/* 236 */       .addComponent(this.jPanel5, -2, -1, -2))
/* 237 */       .addContainerGap(16, 32767)));
/*     */     
/* 239 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 240 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 241 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 242 */       .addComponent(this.jPanel6, -2, -1, -2)
/* 243 */       .addGap(28, 28, 28)
/* 244 */       .addComponent(this.jPanel5, -2, -1, -2)
/* 245 */       .addContainerGap(416, 32767)));
/*     */     
/*     */ 
/* 248 */     add(this.jPanel1, "West");
/*     */     
/* 250 */     this.jPanel3.setBackground(java.awt.Color.white);
/* 251 */     this.jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Busqueda de Artículo"));
/*     */     
/* 253 */     this.skuinput.setFont(new java.awt.Font("Dialog", 1, 18));
/* 254 */     this.skuinput.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 256 */         SortingPanel.this.skuinputActionPerformed(evt);
/*     */       }
/*     */       
/* 259 */     });
/* 260 */     this.notificateLabel.setForeground(new java.awt.Color(222, 17, 17));
/* 261 */     this.notificateLabel.setHorizontalAlignment(0);
/*     */     
/* 263 */     this.cerrarCajaButton.setText("Cerrar Caja");
/* 264 */     this.cerrarCajaButton.setMinimumSize(new java.awt.Dimension(99, 46));
/* 265 */     this.cerrarCajaButton.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 267 */         SortingPanel.this.cerrarCajaButtonActionPerformed(evt);
/*     */       }
/*     */       
/* 270 */     });
/* 271 */     this.avanzarOlaButton.setText("Cerrar Todas las cajas");
/* 272 */     this.avanzarOlaButton.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 274 */         SortingPanel.this.avanzarOlaButtonActionPerformed(evt);
/*     */       }
/*     */       
/* 277 */     });
/* 278 */     this.finalizarSortingButton.setText("Avanzar Ola");
/* 279 */     this.finalizarSortingButton.setMinimumSize(new java.awt.Dimension(113, 46));
/* 280 */     this.finalizarSortingButton.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 282 */         SortingPanel.this.finalizarSortingButtonActionPerformed(evt);
/*     */       }
/*     */       
/* 285 */     });
/* 286 */     this.jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 18));
/* 287 */     this.jLabel2.setHorizontalAlignment(0);
/* 288 */     this.jLabel2.setText("Leer Código de Barras");
/*     */     
/* 290 */     this.jScrollPane1.setBackground(java.awt.Color.white);
/* 291 */     this.jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Ultimas Transacciones"));
/*     */     
/* 293 */     this.tablatransacciones.setModel(new javax.swing.table.DefaultTableModel(new Object[0][], new String[] { "Fecha/hora", "Articulo", "Destino", "Código Caja", "Cantidad Confirmada", "Nota de Venta" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 301 */       boolean[] canEdit = { false, false, false, false, false, false };
/*     */       
/*     */ 
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex)
/*     */       {
/* 306 */         return this.canEdit[columnIndex];
/*     */       }
/* 308 */     });
/* 309 */     this.jScrollPane1.setViewportView(this.tablatransacciones);
/*     */     
/* 311 */     this.jButton1.setText("Ver Detalle Nota Venta");
/* 312 */     this.jButton1.setMinimumSize(new java.awt.Dimension(79, 46));
/* 313 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 315 */         SortingPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/* 318 */     });
/* 319 */     javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(this.jPanel3);
/* 320 */     this.jPanel3.setLayout(jPanel3Layout);
/* 321 */     jPanel3Layout.setHorizontalGroup(jPanel3Layout
/* 322 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 323 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 324 */       .addContainerGap()
/* 325 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 326 */       .addComponent(this.jScrollPane1)
/* 327 */       .addComponent(this.jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, -1, 900, 32767)
/* 328 */       .addComponent(this.skuinput)
/* 329 */       .addComponent(this.notificateLabel, -1, -1, 32767)
/* 330 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 331 */       .addComponent(this.cerrarCajaButton, -2, 160, -2)
/* 332 */       .addGap(18, 18, 18)
/* 333 */       .addComponent(this.finalizarSortingButton, -2, 160, -2)
/* 334 */       .addGap(18, 18, 18)
/* 335 */       .addComponent(this.jButton1, -2, 181, -2)
/* 336 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
/* 337 */       .addComponent(this.avanzarOlaButton, -2, 176, -2)))
/* 338 */       .addContainerGap()));
/*     */     
/* 340 */     jPanel3Layout.setVerticalGroup(jPanel3Layout
/* 341 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 342 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 343 */       .addGap(54, 54, 54)
/* 344 */       .addComponent(this.jLabel2)
/* 345 */       .addGap(18, 18, 18)
/* 346 */       .addComponent(this.skuinput, -2, 50, -2)
/* 347 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 348 */       .addComponent(this.notificateLabel, -2, 46, -2)
/* 349 */       .addGap(18, 18, 18)
/* 350 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 351 */       .addComponent(this.jButton1, javax.swing.GroupLayout.Alignment.TRAILING, -2, 46, -2)
/* 352 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 353 */       .addComponent(this.cerrarCajaButton, -2, 46, -2)
/* 354 */       .addComponent(this.finalizarSortingButton, -2, 46, -2)
/* 355 */       .addComponent(this.avanzarOlaButton, -2, 46, -2)))
/* 356 */       .addGap(18, 18, 18)
/* 357 */       .addComponent(this.jScrollPane1, -1, 456, 32767)
/* 358 */       .addContainerGap()));
/*     */     
/*     */ 
/* 361 */     add(this.jPanel3, "Center");
/*     */     
/* 363 */     this.jPanel2.setBackground(java.awt.Color.white);
/*     */     
/* 365 */     this.jLabel1.setFont(new java.awt.Font("Arial", 1, 24));
/* 366 */     this.jLabel1.setHorizontalAlignment(0);
/* 367 */     this.jLabel1.setText("Proceso de Sorting");
/* 368 */     this.jLabel1.setHorizontalTextPosition(0);
/*     */     
/* 370 */     this.jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/logo_retailsoft_240.png")));
/* 371 */     this.jLabel3.setText("jLabel3");
/*     */     
/* 373 */     javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(this.jPanel2);
/* 374 */     this.jPanel2.setLayout(jPanel2Layout);
/* 375 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/* 376 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 377 */       .addGroup(jPanel2Layout.createSequentialGroup()
/* 378 */       .addGap(62, 62, 62)
/* 379 */       .addComponent(this.jLabel3, -2, 242, -2)
/* 380 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 381 */       .addComponent(this.jLabel1, -1, 1013, 32767)
/* 382 */       .addContainerGap()));
/*     */     
/* 384 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/* 385 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 386 */       .addGroup(jPanel2Layout.createSequentialGroup()
/* 387 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 388 */       .addComponent(this.jLabel3, -1, -1, 32767)
/* 389 */       .addComponent(this.jLabel1))
/* 390 */       .addContainerGap()));
/*     */     
/*     */ 
/* 393 */     add(this.jPanel2, "North");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void finalizarSortingButtonActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 401 */     Workflow.process.Picking.SortingJob.getInstance().endSorting();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void cerrarCajaButtonActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 408 */     Workflow.process.Picking.SortingJob.getInstance().closeBox();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void avanzarOlaButtonActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 415 */     Workflow.process.Picking.SortingJob.getInstance().nextWave();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void skuinputActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 423 */     Workflow.process.Picking.SortingJob.getInstance().sortBarCode(this.skuinput.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 431 */     Workflow.process.Picking.SortingJob.getInstance().ShowBultoInfo(this.olaLabel.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private JLabel jLabel2;
/*     */   
/*     */ 
/*     */   private JLabel jLabel3;
/*     */   
/*     */ 
/*     */   private JLabel jLabel8;
/*     */   
/*     */ 
/*     */   private JLabel jLabel9;
/*     */   
/*     */   private javax.swing.JPanel jPanel1;
/*     */   
/*     */   private javax.swing.JPanel jPanel2;
/*     */   
/*     */   private javax.swing.JPanel jPanel3;
/*     */   
/*     */   private javax.swing.JPanel jPanel5;
/*     */   
/*     */   private javax.swing.JPanel jPanel6;
/*     */   
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   
/*     */   private JLabel notificateLabel;
/*     */   
/*     */   private JLabel olaLabel;
/*     */   
/*     */   private JLabel pendientesArticulosLabel;
/*     */   
/*     */   private JLabel pendientesOlaLabel;
/*     */   
/*     */   private javax.swing.JTextField skuinput;
/*     */   
/*     */   private javax.swing.JTable tablatransacciones;
/*     */   
/*     */   private JLabel totalArticuloLabel;
/*     */   
/*     */   public void clearData()
/*     */   {
/* 475 */     this.skuinput.setText("");
/* 476 */     this.notificateLabel.setText("");
/*     */     
/*     */ 
/* 479 */     this.codigoBarraLabel.setText("--");
/* 480 */     this.articuloLabel.setText("--");
/* 481 */     this.totalArticuloLabel.setText("--");
/* 482 */     this.pendientesArticulosLabel.setText("--");
/* 483 */     this.distribuidosArticuloLabel.setText("--");
/*     */     
/*     */ 
/* 486 */     Models.Entities.local.InformacionPicking ip = (Models.Entities.local.InformacionPicking)Core.ApplicationController.getInstance().getVariable("infoPicking");
/* 487 */     Models.Entities.local.InformacionBulto ib = (Models.Entities.local.InformacionBulto)Core.ApplicationController.getInstance().getVariable("infoBulto");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 496 */     if (ip != null) {
/* 497 */       this.olaLabel.setText((String)Core.ApplicationController.getInstance().getVariable("picking"));
/* 498 */       this.cantidadOlaLabel.setText(ip.getTotal().toString());
/* 499 */       this.pendientesOlaLabel.setText(ip.getPendiente().toString());
/* 500 */       this.distribuidosOlaLabel.setText(ip.getDistribuido().toString());
/* 501 */       if (Core.ApplicationController.getInstance().getVariable("nbultos") == null) {}
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 506 */     String ola = (String)Core.ApplicationController.getInstance().getVariable("ola");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void notificate(String msg)
/*     */   {
/* 513 */     this.notificateLabel.setText(msg);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 522 */     if (event.equals("transaccion")) {
/*     */       try {
/* 524 */         javax.swing.table.DefaultTableModel tb = (javax.swing.table.DefaultTableModel)this.tablatransacciones.getModel();
/*     */         
/* 526 */         tb.insertRow(0, new Object[] { data.get("fechahora").toString(), data.get("articulo").toString(), data.get("destino").toString(), data.get("caja").toString(), data.get("cantidad").toString(), data.get("docsalida").toString() });
/*     */       } catch (Exception ex) {
/* 528 */         Logging.LoggingManager.getInstance().log_error(ex);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 538 */     Models.Entities.local.InformacionPicking ip = (Models.Entities.local.InformacionPicking)Core.ApplicationController.getInstance().getVariable("infoPicking");
/* 539 */     Models.Entities.local.InformacionBulto ib = (Models.Entities.local.InformacionBulto)Core.ApplicationController.getInstance().getVariable("infoBulto");
/* 540 */     boolean CanSort = true;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 548 */     if (ip != null) {
/* 549 */       this.olaLabel.setText((String)Core.ApplicationController.getInstance().getVariable("picking"));
/* 550 */       this.cantidadOlaLabel.setText(ip.getTotal().toString());
/* 551 */       this.pendientesOlaLabel.setText(ip.getPendiente().toString());
/* 552 */       this.distribuidosOlaLabel.setText(ip.getDistribuido().toString());
/* 553 */       if (Core.ApplicationController.getInstance().getVariable("nbultos") == null) {}
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 559 */     String currentCodigo = "--";
/* 560 */     String currentArticulo = "--";
/* 561 */     String currentCantidadBulto = "--";
/* 562 */     String currentPendientesArticulo = "--";
/* 563 */     String currentDistribuidosArticulo = "--";
/* 564 */     boolean existe = false;
/*     */     
/* 566 */     if (Core.ApplicationController.getInstance().getVariable("currentExiste") != null) {
/* 567 */       existe = ((Boolean)Core.ApplicationController.getInstance().getVariable("currentExiste")).booleanValue();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 573 */     if (Core.ApplicationController.getInstance().getVariable("currentCodigo") != null) {
/* 574 */       currentCodigo = (String)Core.ApplicationController.getInstance().getVariable("currentCodigo");
/*     */     }
/*     */     
/* 577 */     if (Core.ApplicationController.getInstance().getVariable("currentArticulo") != null) {
/* 578 */       currentArticulo = (String)Core.ApplicationController.getInstance().getVariable("currentArticulo");
/*     */     }
/*     */     
/* 581 */     if (Core.ApplicationController.getInstance().getVariable("currentCantidadBulto") != null) {
/* 582 */       currentCantidadBulto = ((Integer)Core.ApplicationController.getInstance().getVariable("currentCantidadBulto")).toString();
/*     */     }
/*     */     
/* 585 */     if (Core.ApplicationController.getInstance().getVariable("currentPendientesArticulo") != null) {
/* 586 */       currentPendientesArticulo = ((Integer)Core.ApplicationController.getInstance().getVariable("currentPendientesArticulo")).toString();
/*     */     }
/*     */     
/* 589 */     if (Core.ApplicationController.getInstance().getVariable("currentDistribuidosArticulo") != null) {
/* 590 */       currentDistribuidosArticulo = ((Integer)Core.ApplicationController.getInstance().getVariable("currentDistribuidosArticulo")).toString();
/*     */     }
/*     */     
/* 593 */     Integer nolas = (Integer)Core.ApplicationController.getInstance().getVariable("nolas");
/* 594 */     Integer olaact = (Integer)Core.ApplicationController.getInstance().getVariable("olaactiva");
/*     */     
/* 596 */     if ((nolas != null) && (nolas.intValue() == 1)) {
/* 597 */       this.finalizarSortingButton.setText("Finalizar Picking");
/* 598 */       this.finalizarSortingButton.setVisible(false);
/* 599 */     } else if ((nolas != null) && (nolas.intValue() > 1)) {
/* 600 */       if ((olaact != null) && (olaact.intValue() < nolas.intValue())) {
/* 601 */         this.finalizarSortingButton.setText("Avanzar Ola");
/* 602 */         this.finalizarSortingButton.setVisible(true);
/*     */       } else {
/* 604 */         this.finalizarSortingButton.setText("Finalizar Picking");
/* 605 */         this.finalizarSortingButton.setVisible(true);
/*     */       }
/*     */     } else {
/* 608 */       this.finalizarSortingButton.setVisible(false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 613 */     this.codigoBarraLabel.setText(currentCodigo);
/* 614 */     this.articuloLabel.setText(currentArticulo);
/* 615 */     this.totalArticuloLabel.setText(currentCantidadBulto);
/* 616 */     this.pendientesArticulosLabel.setText(currentPendientesArticulo);
/* 617 */     this.distribuidosArticuloLabel.setText(currentDistribuidosArticulo);
/*     */     
/*     */ 
/* 620 */     if (Core.ApplicationController.getInstance().getVariable("CanSort") != null) {
/* 621 */       CanSort = ((Boolean)Core.ApplicationController.getInstance().getVariable("CanSort")).booleanValue();
/*     */     }
/*     */     
/* 624 */     this.skuinput.setEnabled(CanSort);
/* 625 */     if (CanSort) {
/* 626 */       setFocus();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 636 */     this.skuinput.requestFocusInWindow();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Picking/SortingPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */