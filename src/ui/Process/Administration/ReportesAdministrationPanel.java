/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ 
/*     */ public class ReportesAdministrationPanel extends ui.PtlPanel {
/*     */   private javax.swing.JTextField archivoText;
/*     */   private javax.swing.JLabel dirsalidalabel;
/*     */   private javax.swing.JLabel estadolabel;
/*     */   private javax.swing.JButton jButton1;
/*     */   private javax.swing.JButton jButton2;
/*     */   private javax.swing.JLabel jLabel1;
/*     */   private javax.swing.JLabel jLabel10;
/*     */   private javax.swing.JLabel jLabel11;
/*     */   private javax.swing.JLabel jLabel2;
/*     */   private javax.swing.JLabel jLabel3;
/*     */   private javax.swing.JLabel jLabel4;
/*     */   private javax.swing.JLabel jLabel6;
/*     */   private javax.swing.JLabel jLabel8;
/*     */   private javax.swing.JLabel jLabel9;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JPanel jPanel2;
/*     */   private javax.swing.JPanel jPanel3;
/*     */   private javax.swing.JSplitPane jSplitPane1;
/*     */   private javax.swing.JLabel nombrearchivolabel;
/*     */   private javax.swing.JLabel notificaitonLabel;
/*     */   private javax.swing.JTextField olaText;
/*     */   private javax.swing.JLabel olalabel;
/*     */   private javax.swing.JProgressBar progreso;
/*     */   private javax.swing.JTextField salidaTextField;
/*     */   
/*     */   public ReportesAdministrationPanel() {
/*  32 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  46 */     this.jSplitPane1 = new javax.swing.JSplitPane();
/*  47 */     this.jPanel1 = new javax.swing.JPanel();
/*  48 */     this.jPanel2 = new javax.swing.JPanel();
/*  49 */     this.olaText = new javax.swing.JTextField();
/*  50 */     this.jButton2 = new javax.swing.JButton();
/*  51 */     this.jLabel8 = new javax.swing.JLabel();
/*  52 */     this.jLabel9 = new javax.swing.JLabel();
/*  53 */     this.jLabel10 = new javax.swing.JLabel();
/*  54 */     this.archivoText = new javax.swing.JTextField();
/*  55 */     this.salidaTextField = new javax.swing.JTextField();
/*  56 */     this.jButton1 = new javax.swing.JButton();
/*  57 */     this.notificaitonLabel = new javax.swing.JLabel();
/*  58 */     this.jLabel3 = new javax.swing.JLabel();
/*  59 */     this.jPanel3 = new javax.swing.JPanel();
/*  60 */     this.jLabel1 = new javax.swing.JLabel();
/*  61 */     this.progreso = new javax.swing.JProgressBar();
/*  62 */     this.jLabel2 = new javax.swing.JLabel();
/*  63 */     this.olalabel = new javax.swing.JLabel();
/*  64 */     this.jLabel4 = new javax.swing.JLabel();
/*  65 */     this.dirsalidalabel = new javax.swing.JLabel();
/*  66 */     this.jLabel6 = new javax.swing.JLabel();
/*  67 */     this.nombrearchivolabel = new javax.swing.JLabel();
/*  68 */     this.jLabel11 = new javax.swing.JLabel();
/*  69 */     this.estadolabel = new javax.swing.JLabel();
/*     */     
/*  71 */     setBackground(java.awt.Color.white);
/*     */     
/*  73 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  74 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Generar Reporte de Nota de venta"));
/*     */     
/*  76 */     this.jPanel2.setBackground(java.awt.Color.white);
/*  77 */     this.jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuración de exportación"));
/*     */     
/*  79 */     this.jButton2.setText("Examinar...");
/*  80 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  82 */         ReportesAdministrationPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  85 */     });
/*  86 */     this.jLabel8.setText("Directorio de Salida");
/*     */     
/*  88 */     this.jLabel9.setText("Número de nota venta");
/*     */     
/*  90 */     this.jLabel10.setText("Nombre de Archivo");
/*     */     
/*  92 */     this.archivoText.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  94 */         ReportesAdministrationPanel.this.archivoTextActionPerformed(evt);
/*     */       }
/*     */       
/*  97 */     });
/*  98 */     this.salidaTextField.setEditable(false);
/*     */     
/* 100 */     this.jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/export-26.png")));
/* 101 */     this.jButton1.setText("Generar Reporte");
/* 102 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 104 */         ReportesAdministrationPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/* 107 */     });
/* 108 */     this.notificaitonLabel.setForeground(new java.awt.Color(255, 51, 51));
/* 109 */     this.notificaitonLabel.setHorizontalAlignment(0);
/*     */     
/* 111 */     this.jLabel3.setText("( Se generará en formato .csv )");
/*     */     
/* 113 */     javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(this.jPanel2);
/* 114 */     this.jPanel2.setLayout(jPanel2Layout);
/* 115 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/* 116 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 117 */       .addGroup(jPanel2Layout.createSequentialGroup()
/* 118 */       .addContainerGap()
/* 119 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 120 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
/* 121 */       .addGap(0, 0, 32767)
/* 122 */       .addComponent(this.jButton1, -2, 200, -2))
/* 123 */       .addComponent(this.jLabel9)
/* 124 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
/* 125 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/* 126 */       .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
/* 127 */       .addGap(130, 130, 130)
/* 128 */       .addComponent(this.salidaTextField))
/* 129 */       .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
/* 130 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 131 */       .addComponent(this.jLabel8)
/* 132 */       .addComponent(this.jLabel10))
/* 133 */       .addGap(18, 18, 18)
/* 134 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 135 */       .addComponent(this.jLabel3, -1, 252, 32767)
/* 136 */       .addComponent(this.olaText)
/* 137 */       .addComponent(this.archivoText))))
/* 138 */       .addGap(10, 10, 10)
/* 139 */       .addComponent(this.jButton2)))
/* 140 */       .addContainerGap())
/* 141 */       .addComponent(this.notificaitonLabel, -1, -1, 32767));
/*     */     
/* 143 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/* 144 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 145 */       .addGroup(jPanel2Layout.createSequentialGroup()
/* 146 */       .addGap(14, 14, 14)
/* 147 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 148 */       .addComponent(this.jLabel8)
/* 149 */       .addComponent(this.jButton2)
/* 150 */       .addComponent(this.salidaTextField, -2, -1, -2))
/* 151 */       .addGap(18, 18, 18)
/* 152 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 153 */       .addComponent(this.jLabel9)
/* 154 */       .addComponent(this.olaText, -2, -1, -2))
/* 155 */       .addGap(18, 18, 18)
/* 156 */       .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 157 */       .addComponent(this.jLabel10)
/* 158 */       .addComponent(this.archivoText, -2, -1, -2))
/* 159 */       .addGap(8, 8, 8)
/* 160 */       .addComponent(this.jLabel3)
/* 161 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 162 */       .addComponent(this.notificaitonLabel, -2, 34, -2)
/* 163 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, 32767)
/* 164 */       .addComponent(this.jButton1, -2, 40, -2)
/* 165 */       .addContainerGap()));
/*     */     
/*     */ 
/* 168 */     this.jPanel3.setBackground(java.awt.Color.white);
/* 169 */     this.jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Información de Exportación"));
/*     */     
/* 171 */     this.jLabel1.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 172 */     this.jLabel1.setText("Número de Nota Venta");
/*     */     
/* 174 */     this.progreso.setToolTipText("Progrreso");
/*     */     
/* 176 */     this.jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 177 */     this.jLabel2.setText("Progreso");
/*     */     
/* 179 */     this.olalabel.setText("--");
/*     */     
/* 181 */     this.jLabel4.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 182 */     this.jLabel4.setText("Directorio de Salida");
/*     */     
/* 184 */     this.dirsalidalabel.setText("--");
/*     */     
/* 186 */     this.jLabel6.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 187 */     this.jLabel6.setText("Nombre Archivo");
/*     */     
/* 189 */     this.nombrearchivolabel.setText("--");
/*     */     
/* 191 */     this.jLabel11.setFont(new java.awt.Font("Tahoma", 1, 13));
/* 192 */     this.jLabel11.setText("Estado");
/*     */     
/* 194 */     this.estadolabel.setText("Sin estado");
/*     */     
/* 196 */     javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(this.jPanel3);
/* 197 */     this.jPanel3.setLayout(jPanel3Layout);
/* 198 */     jPanel3Layout.setHorizontalGroup(jPanel3Layout
/* 199 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 200 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 201 */       .addContainerGap()
/* 202 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 203 */       .addComponent(this.dirsalidalabel, -1, -1, 32767)
/* 204 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 205 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 206 */       .addComponent(this.jLabel2)
/* 207 */       .addComponent(this.jLabel4)
/* 208 */       .addComponent(this.jLabel1, -2, 152, -2))
/* 209 */       .addContainerGap(59, 32767))
/* 210 */       .addComponent(this.nombrearchivolabel, -1, -1, 32767)
/* 211 */       .addComponent(this.olalabel, -1, -1, 32767)
/* 212 */       .addGroup(jPanel3Layout.createSequentialGroup()
/* 213 */       .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 214 */       .addComponent(this.jLabel6)
/* 215 */       .addComponent(this.jLabel11)
/* 216 */       .addComponent(this.estadolabel))
/* 217 */       .addGap(0, 0, 32767))
/* 218 */       .addComponent(this.progreso, javax.swing.GroupLayout.Alignment.TRAILING, -1, -1, 32767))));
/*     */     
/* 220 */     jPanel3Layout.setVerticalGroup(jPanel3Layout
/* 221 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 222 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
/* 223 */       .addGap(7, 7, 7)
/* 224 */       .addComponent(this.jLabel11)
/* 225 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 226 */       .addComponent(this.estadolabel)
/* 227 */       .addGap(18, 18, 18)
/* 228 */       .addComponent(this.jLabel1)
/* 229 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 230 */       .addComponent(this.olalabel)
/* 231 */       .addGap(18, 18, 18)
/* 232 */       .addComponent(this.jLabel4)
/* 233 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 234 */       .addComponent(this.dirsalidalabel)
/* 235 */       .addGap(18, 18, 18)
/* 236 */       .addComponent(this.jLabel6)
/* 237 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 238 */       .addComponent(this.nombrearchivolabel)
/* 239 */       .addGap(18, 18, 18)
/* 240 */       .addComponent(this.jLabel2)
/* 241 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 242 */       .addComponent(this.progreso, -2, 19, -2)
/* 243 */       .addContainerGap(-1, 32767)));
/*     */     
/*     */ 
/* 246 */     javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.jPanel1);
/* 247 */     this.jPanel1.setLayout(jPanel1Layout);
/* 248 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/* 249 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 250 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/* 251 */       .addContainerGap()
/* 252 */       .addComponent(this.jPanel2, -1, -1, 32767)
/* 253 */       .addGap(18, 18, 18)
/* 254 */       .addComponent(this.jPanel3, -1, -1, 32767)
/* 255 */       .addContainerGap()));
/*     */     
/* 257 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 258 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 259 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 260 */       .addContainerGap()
/* 261 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
/* 262 */       .addComponent(this.jPanel2, -1, -1, 32767)
/* 263 */       .addComponent(this.jPanel3, -1, -1, 32767))
/* 264 */       .addContainerGap(-1, 32767)));
/*     */     
/*     */ 
/* 267 */     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
/* 268 */     setLayout(layout);
/* 269 */     layout.setHorizontalGroup(layout
/* 270 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 271 */       .addGroup(layout.createSequentialGroup()
/* 272 */       .addContainerGap()
/* 273 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 274 */       .addContainerGap()));
/*     */     
/* 276 */     layout.setVerticalGroup(layout
/* 277 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/* 278 */       .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
/* 279 */       .addContainerGap()
/* 280 */       .addComponent(this.jPanel1, -2, -1, -2)
/* 281 */       .addContainerGap(-1, 32767)));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 288 */     clearData();
/*     */     
/* 290 */     if (this.salidaTextField.getText().equals(""))
/*     */     {
/* 292 */       this.notificaitonLabel.setText("Debe seleccionar un directorio de Salida");
/* 293 */       return;
/*     */     }
/*     */     
/* 296 */     if (this.olaText.getText().equals("")) {
/* 297 */       this.notificaitonLabel.setText("Debe ingresar el número de Nota de Venta");
/* 298 */       return;
/*     */     }
/*     */     
/* 301 */     if (!isInteger(this.olaText.getText())) {
/* 302 */       this.notificaitonLabel.setText("El dato ingresado en el número de nota de venta debe ser numérico");
/* 303 */       return;
/*     */     }
/*     */     
/* 306 */     if (this.archivoText.getText().equals("")) {
/* 307 */       this.notificaitonLabel.setText("Debe ingresar un nombre de archivo");
/* 308 */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 314 */     Workflow.process.Administration.AdministrationJob.getInstance().generarReporte(this.olaText.getText(), this.salidaTextField.getText(), this.archivoText.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 323 */     javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
/* 324 */     chooser.setCurrentDirectory(new java.io.File("."));
/* 325 */     chooser.setDialogTitle("Seleccione el directorio de Salida");
/* 326 */     chooser.setFileSelectionMode(1);
/*     */     
/*     */ 
/*     */ 
/* 330 */     chooser.setAcceptAllFileFilterUsed(false);
/*     */     
/* 332 */     if (chooser.showOpenDialog(this) == 0) {
/* 333 */       this.salidaTextField.setText(chooser.getSelectedFile().getAbsolutePath());
/* 334 */       System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
/* 335 */       System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
/*     */     }
/*     */     else {
/* 338 */       System.out.println("No Selection ");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void archivoTextActionPerformed(java.awt.event.ActionEvent evt) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 377 */     this.progreso.setValue(0);
/* 378 */     this.estadolabel.setText("Sin Estado");
/* 379 */     this.olalabel.setText("");
/* 380 */     this.nombrearchivolabel.setText("");
/* 381 */     this.dirsalidalabel.setText("");
/*     */     
/* 383 */     this.estadolabel.setForeground(java.awt.Color.black);
/*     */     
/* 385 */     actualizaInfo();
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 390 */     this.notificaitonLabel.setText(msg);
/*     */   }
/*     */   
/*     */ 
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 396 */     if (event.equals("progress")) {
/* 397 */       Integer val = (Integer)data.get("progreso");
/* 398 */       this.progreso.setValue(val.intValue());
/* 399 */     } else if (event.equals("fin")) {
/* 400 */       String estado = (String)data.get("estado_text");
/* 401 */       this.estadolabel.setText(estado);
/* 402 */       this.progreso.setValue(((Integer)data.get("progreso")).intValue());
/* 403 */       this.olalabel.setText((String)data.get("ola"));
/* 404 */       this.dirsalidalabel.setText((String)data.get("path"));
/* 405 */       this.nombrearchivolabel.setText((String)data.get("filename"));
/*     */       
/* 407 */       if (((String)data.get("estado")).equals("ok"))
/*     */       {
/* 409 */         this.estadolabel.setForeground(java.awt.Color.GREEN);
/*     */       }
/* 411 */       else if (((String)data.get("estado")).equals("fail")) {
/* 412 */         this.estadolabel.setForeground(java.awt.Color.RED);
/*     */       } else {
/* 414 */         this.estadolabel.setForeground(java.awt.Color.BLACK);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void actualizaInfo() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 433 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public boolean isInteger(String s)
/*     */   {
/*     */     try {
/* 439 */       Integer.parseInt(s);
/*     */     } catch (NumberFormatException e) {
/* 441 */       return false;
/*     */     }
/*     */     
/* 444 */     return true;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/ReportesAdministrationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */