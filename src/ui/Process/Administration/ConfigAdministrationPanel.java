/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ 
/*     */ public class ConfigAdministrationPanel extends ui.PtlPanel
/*     */ {
/*     */   private javax.swing.JTextField IpText;
/*     */   private javax.swing.JTextField idptl;
/*     */   private javax.swing.JButton jButton3;
/*     */   private javax.swing.JLabel jLabel1;
/*     */   private javax.swing.JLabel jLabel2;
/*     */   private javax.swing.JLabel jLabel3;
/*     */   private javax.swing.JLabel jLabel4;
/*     */   private javax.swing.JLabel jLabel5;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JTextField lucesText;
/*     */   private javax.swing.JTextField rutaEntradaText;
/*     */   private javax.swing.JTextField rutaSalidaText;
/*     */   
/*     */   public ConfigAdministrationPanel()
/*     */   {
/*  22 */     initComponents();
/*  23 */     actualizaInfo();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  35 */     this.jPanel1 = new javax.swing.JPanel();
/*  36 */     this.rutaSalidaText = new javax.swing.JTextField();
/*  37 */     this.idptl = new javax.swing.JTextField();
/*  38 */     this.IpText = new javax.swing.JTextField();
/*  39 */     this.jLabel1 = new javax.swing.JLabel();
/*  40 */     this.jLabel2 = new javax.swing.JLabel();
/*  41 */     this.jLabel3 = new javax.swing.JLabel();
/*  42 */     this.jLabel5 = new javax.swing.JLabel();
/*  43 */     this.lucesText = new javax.swing.JTextField();
/*  44 */     this.jButton3 = new javax.swing.JButton();
/*  45 */     this.jLabel4 = new javax.swing.JLabel();
/*  46 */     this.rutaEntradaText = new javax.swing.JTextField();
/*     */     
/*  48 */     setBackground(java.awt.Color.white);
/*     */     
/*  50 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  51 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuración Estación"));
/*     */     
/*  53 */     this.jLabel1.setText("Ruta Salida Archivos Texto");
/*     */     
/*  55 */     this.jLabel2.setText("Identificador Estación PTL");
/*     */     
/*  57 */     this.jLabel3.setText("IP controlador PTL");
/*     */     
/*  59 */     this.jLabel5.setText("Cantidad de Luces del PTL");
/*     */     
/*  61 */     this.jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/checkmark-26.png")));
/*  62 */     this.jButton3.setText("Guardar Cambios");
/*  63 */     this.jButton3.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  65 */         ConfigAdministrationPanel.this.jButton3ActionPerformed(evt);
/*     */       }
/*     */       
/*  68 */     });
/*  69 */     this.jLabel4.setText("Ruta Entrada Archivos");
/*     */     
/*  71 */     javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.jPanel1);
/*  72 */     this.jPanel1.setLayout(jPanel1Layout);
/*  73 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  74 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/*  75 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  76 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  77 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/*  78 */       .addContainerGap(-1, 32767)
/*  79 */       .addComponent(this.jButton3, -2, 200, -2))
/*  80 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/*  81 */       .addGap(24, 24, 24)
/*  82 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  83 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  84 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  85 */       .addComponent(this.jLabel3)
/*  86 */       .addComponent(this.jLabel5))
/*  87 */       .addGap(76, 76, 76)
/*  88 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  89 */       .addComponent(this.IpText, -1, 515, 32767)
/*  90 */       .addComponent(this.lucesText)))
/*  91 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  92 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  93 */       .addComponent(this.jLabel1, -2, 209, -2)
/*  94 */       .addComponent(this.jLabel2))
/*  95 */       .addGap(48, 48, 48)
/*  96 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  97 */       .addComponent(this.idptl)
/*  98 */       .addComponent(this.rutaSalidaText)))
/*  99 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 100 */       .addComponent(this.jLabel4, -2, 209, -2)
/* 101 */       .addGap(48, 48, 48)
/* 102 */       .addComponent(this.rutaEntradaText)))))
/* 103 */       .addContainerGap()));
/*     */     
/* 105 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 106 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 107 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 108 */       .addGap(30, 30, 30)
/* 109 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 110 */       .addComponent(this.rutaSalidaText, -2, -1, -2)
/* 111 */       .addComponent(this.jLabel1))
/* 112 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 113 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 114 */       .addComponent(this.rutaEntradaText, -2, -1, -2)
/* 115 */       .addComponent(this.jLabel4))
/* 116 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 117 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 118 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/* 119 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 120 */       .addComponent(this.jLabel2)
/* 121 */       .addComponent(this.idptl, -2, -1, -2))
/* 122 */       .addGap(23, 23, 23)
/* 123 */       .addComponent(this.jLabel3))
/* 124 */       .addComponent(this.IpText, javax.swing.GroupLayout.Alignment.TRAILING, -2, -1, -2))
/* 125 */       .addGap(17, 17, 17)
/* 126 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 127 */       .addComponent(this.jLabel5)
/* 128 */       .addComponent(this.lucesText, -2, -1, -2))
/* 129 */       .addGap(18, 18, 18)
/* 130 */       .addComponent(this.jButton3)
/* 131 */       .addContainerGap(74, 32767)));
/*     */     
/*     */ 
/* 134 */     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
/* 135 */     setLayout(layout);
/* 136 */     layout.setHorizontalGroup(layout
/* 137 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 138 */       .addGroup(layout.createSequentialGroup()
/* 139 */       .addContainerGap()
/* 140 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 141 */       .addContainerGap()));
/*     */     
/* 143 */     layout.setVerticalGroup(layout
/* 144 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/* 145 */       .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
/* 146 */       .addContainerGap()
/* 147 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 148 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 155 */     Workflow.process.Administration.AdministrationJob.getInstance().saveChangedConfig(this.rutaSalidaText.getText(), this.rutaEntradaText.getText(), this.idptl.getText(), this.IpText.getText(), this.lucesText.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 179 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 184 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 189 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 195 */     this.rutaSalidaText.setText(Configuration.ConfigurationManager.getInstance().getProperty("rutaSalida"));
/* 196 */     this.idptl.setText(Configuration.ConfigurationManager.getInstance().getProperty("id_ptl"));
/* 197 */     this.IpText.setText(Configuration.ConfigurationManager.getInstance().getProperty("ip"));
/* 198 */     this.lucesText.setText(Configuration.ConfigurationManager.getInstance().getProperty("luces"));
/* 199 */     this.rutaEntradaText.setText(Configuration.ConfigurationManager.getInstance().getProperty("rutaArchivo"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 208 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/ConfigAdministrationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */