/*    */ package ui.Process.Administration;
/*    */ 
/*    */ import java.awt.Color;
/*    */ import java.awt.Font;
/*    */ import java.util.HashMap;
/*    */ import javax.swing.GroupLayout;
/*    */ import javax.swing.GroupLayout.Alignment;
/*    */ import javax.swing.GroupLayout.ParallelGroup;
/*    */ import javax.swing.GroupLayout.SequentialGroup;
/*    */ import javax.swing.JLabel;
/*    */ import javax.swing.LayoutStyle.ComponentPlacement;
/*    */ 
/*    */ public class AdministrationPanel extends ui.PtlPanel
/*    */ {
/*    */   private JLabel jLabel1;
/*    */   private JLabel notificationLabel;
/*    */   
/*    */   public AdministrationPanel()
/*    */   {
/* 20 */     initComponents();
/* 21 */     clearData();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private void initComponents()
/*    */   {
/* 33 */     this.jLabel1 = new JLabel();
/* 34 */     this.notificationLabel = new JLabel();
/*    */     
/* 36 */     setBackground(new Color(255, 255, 255));
/*    */     
/* 38 */     this.jLabel1.setHorizontalAlignment(0);
/* 39 */     this.jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/atait_test.png")));
/*    */     
/* 41 */     this.notificationLabel.setFont(new Font("Ubuntu", 1, 15));
/* 42 */     this.notificationLabel.setForeground(new Color(56, 175, 67));
/* 43 */     this.notificationLabel.setHorizontalAlignment(0);
/* 44 */     this.notificationLabel.setText("jLabel2");
/*    */     
/* 46 */     GroupLayout layout = new GroupLayout(this);
/* 47 */     setLayout(layout);
/* 48 */     layout.setHorizontalGroup(layout
/* 49 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 50 */       .addComponent(this.jLabel1, GroupLayout.Alignment.TRAILING, -1, 943, 32767)
/* 51 */       .addComponent(this.notificationLabel, -1, -1, 32767));
/*    */     
/* 53 */     layout.setVerticalGroup(layout
/* 54 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 55 */       .addGroup(layout.createSequentialGroup()
/* 56 */       .addComponent(this.notificationLabel, -2, 31, -2)
/* 57 */       .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
/* 58 */       .addComponent(this.jLabel1, -1, 316, 32767)));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void clearData()
/*    */   {
/* 68 */     this.notificationLabel.setText("");
/*    */   }
/*    */   
/*    */ 
/*    */   public void notificate(String msg)
/*    */   {
/* 74 */     this.notificationLabel.setText(msg);
/*    */   }
/*    */   
/*    */   public void incomingData(String event, HashMap data)
/*    */   {
/* 79 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public void actualizaInfo()
/*    */   {
/* 84 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public void setFocus()
/*    */   {
/* 89 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/AdministrationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */