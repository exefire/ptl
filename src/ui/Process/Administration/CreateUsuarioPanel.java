/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ 
/*     */ public class CreateUsuarioPanel extends ui.PtlPanel
/*     */ {
/*     */   private javax.swing.JButton jButton1;
/*     */   private javax.swing.JButton jButton2;
/*     */   private javax.swing.JLabel jLabel1;
/*     */   private javax.swing.JLabel jLabel2;
/*     */   private javax.swing.JLabel jLabel3;
/*     */   private javax.swing.JLabel jLabel4;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JPasswordField pass2Text;
/*     */   private javax.swing.JPasswordField passText;
/*     */   private javax.swing.JComboBox rolText;
/*     */   private javax.swing.JTextField userText;
/*     */   
/*     */   public CreateUsuarioPanel()
/*     */   {
/*  23 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  35 */     this.jPanel1 = new javax.swing.JPanel();
/*  36 */     this.jLabel1 = new javax.swing.JLabel();
/*  37 */     this.userText = new javax.swing.JTextField();
/*  38 */     this.jLabel2 = new javax.swing.JLabel();
/*  39 */     this.jLabel3 = new javax.swing.JLabel();
/*  40 */     this.jButton1 = new javax.swing.JButton();
/*  41 */     this.jButton2 = new javax.swing.JButton();
/*  42 */     this.passText = new javax.swing.JPasswordField();
/*  43 */     this.pass2Text = new javax.swing.JPasswordField();
/*  44 */     this.rolText = new javax.swing.JComboBox();
/*  45 */     this.jLabel4 = new javax.swing.JLabel();
/*     */     
/*  47 */     setBackground(java.awt.Color.white);
/*     */     
/*  49 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  50 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Crear Usuario"));
/*     */     
/*  52 */     this.jLabel1.setText("Usuario");
/*     */     
/*  54 */     this.jLabel2.setText("Contraseña");
/*     */     
/*  56 */     this.jLabel3.setText("Repetir Contraseña");
/*     */     
/*  58 */     this.jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/checkmark-26.png")));
/*  59 */     this.jButton1.setText("Crear Usuario");
/*  60 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  62 */         CreateUsuarioPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/*  65 */     });
/*  66 */     this.jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/cancel-26.png")));
/*  67 */     this.jButton2.setText("Cancelar");
/*  68 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  70 */         CreateUsuarioPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  73 */     });
/*  74 */     this.rolText.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Administrador", "Operario", " " }));
/*  75 */     this.rolText.setSelectedIndex(1);
/*     */     
/*  77 */     this.jLabel4.setText("Permisos");
/*     */     
/*  79 */     GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
/*  80 */     this.jPanel1.setLayout(jPanel1Layout);
/*  81 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  82 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  83 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  84 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  85 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  86 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  87 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  88 */       .addGap(34, 34, 34)
/*  89 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  90 */       .addComponent(this.jLabel2)
/*  91 */       .addComponent(this.jLabel1, -2, 97, -2)
/*  92 */       .addComponent(this.jLabel4))
/*  93 */       .addGap(49, 49, 49))
/*  94 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/*  95 */       .addContainerGap()
/*  96 */       .addComponent(this.jLabel3)
/*  97 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
/*  98 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  99 */       .addComponent(this.pass2Text)
/* 100 */       .addComponent(this.passText)
/* 101 */       .addComponent(this.rolText, 0, 643, 32767)
/* 102 */       .addComponent(this.userText)))
/* 103 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 104 */       .addGap(0, 0, 32767)
/* 105 */       .addComponent(this.jButton2, -2, 200, -2)
/* 106 */       .addGap(18, 18, 18)
/* 107 */       .addComponent(this.jButton1, -2, 200, -2)))
/* 108 */       .addContainerGap()));
/*     */     
/* 110 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 111 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 112 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 113 */       .addGap(25, 25, 25)
/* 114 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 115 */       .addComponent(this.jLabel1)
/* 116 */       .addComponent(this.userText, -2, -1, -2))
/* 117 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 118 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 119 */       .addComponent(this.rolText, -2, -1, -2)
/* 120 */       .addComponent(this.jLabel4))
/* 121 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 122 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 123 */       .addComponent(this.passText, -2, -1, -2)
/* 124 */       .addComponent(this.jLabel2))
/* 125 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 126 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 127 */       .addComponent(this.pass2Text, -2, -1, -2)
/* 128 */       .addComponent(this.jLabel3))
/* 129 */       .addGap(18, 18, 18)
/* 130 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 131 */       .addComponent(this.jButton2)
/* 132 */       .addComponent(this.jButton1))
/* 133 */       .addContainerGap(81, 32767)));
/*     */     
/*     */ 
/* 136 */     GroupLayout layout = new GroupLayout(this);
/* 137 */     setLayout(layout);
/* 138 */     layout.setHorizontalGroup(layout
/* 139 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 140 */       .addGroup(layout.createSequentialGroup()
/* 141 */       .addContainerGap()
/* 142 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 143 */       .addContainerGap()));
/*     */     
/* 145 */     layout.setVerticalGroup(layout
/* 146 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 147 */       .addGroup(layout.createSequentialGroup()
/* 148 */       .addContainerGap()
/* 149 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 150 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 157 */     Workflow.process.Administration.AdministrationJob.getInstance().usersAdministration();
/*     */   }
/*     */   
/*     */ 
/*     */   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 163 */     Workflow.process.Administration.AdministrationJob.getInstance().createUser(this.userText.getText(), this.passText.getText(), this.rolText.getSelectedIndex());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 185 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 190 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 195 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/* 200 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 205 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/CreateUsuarioPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */