/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import Models.Entities.local.Rol;
/*     */ import Models.Entities.local.User;
/*     */ import Workflow.process.Administration.AdministrationJob;
/*     */ import java.awt.Color;
/*     */ import java.awt.event.ActionEvent;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JScrollPane;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.table.DefaultTableModel;
/*     */ 
/*     */ public class UsersAdministrationPanel extends ui.PtlPanel
/*     */ {
/*     */   private JButton deleteButton;
/*     */   private JButton jButton3;
/*     */   private JPanel jPanel2;
/*     */   private JScrollPane jScrollPane1;
/*     */   private JTable jTable1;
/*     */   
/*     */   public UsersAdministrationPanel()
/*     */   {
/*  28 */     initComponents();
/*  29 */     this.deleteButton.setEnabled(false);
/*  30 */     this.jTable1.getSelectionModel().addListSelectionListener(new javax.swing.event.ListSelectionListener() {
/*     */       public void valueChanged(javax.swing.event.ListSelectionEvent event) {
/*  32 */         if (event.getValueIsAdjusting()) {
/*  33 */           return;
/*     */         }
/*  35 */         if (UsersAdministrationPanel.this.jTable1.getSelectedRowCount() == 0) {
/*  36 */           UsersAdministrationPanel.this.deleteButton.setEnabled(false);
/*     */         } else {
/*  38 */           UsersAdministrationPanel.this.deleteButton.setEnabled(true);
/*     */         }
/*     */         
/*     */       }
/*     */       
/*     */ 
/*  44 */     });
/*  45 */     actualizaInfo();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  57 */     this.jPanel2 = new JPanel();
/*  58 */     this.jScrollPane1 = new JScrollPane();
/*  59 */     this.jTable1 = new JTable();
/*  60 */     this.jButton3 = new JButton();
/*  61 */     this.deleteButton = new JButton();
/*     */     
/*  63 */     setBackground(Color.white);
/*     */     
/*  65 */     this.jPanel2.setBackground(Color.white);
/*  66 */     this.jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
/*     */     
/*  68 */     this.jScrollPane1.setBackground(new Color(255, 255, 255));
/*     */     
/*  70 */     this.jTable1.setModel(new DefaultTableModel(new Object[][] { { null, null }, { null, null } }, new String[] { "Nombre de Usuario", "Permisos" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  79 */       boolean[] canEdit = { false, false };
/*     */       
/*     */ 
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex)
/*     */       {
/*  84 */         return this.canEdit[columnIndex];
/*     */       }
/*  86 */     });
/*  87 */     this.jScrollPane1.setViewportView(this.jTable1);
/*     */     
/*  89 */     GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
/*  90 */     this.jPanel2.setLayout(jPanel2Layout);
/*  91 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/*  92 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  93 */       .addComponent(this.jScrollPane1, GroupLayout.Alignment.TRAILING));
/*     */     
/*  95 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/*  96 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  97 */       .addComponent(this.jScrollPane1, GroupLayout.Alignment.TRAILING, -1, 247, 32767));
/*     */     
/*     */ 
/* 100 */     this.jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/add_user-26.png")));
/* 101 */     this.jButton3.setText("Agregar usuario");
/* 102 */     this.jButton3.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 104 */         UsersAdministrationPanel.this.jButton3ActionPerformed(evt);
/*     */       }
/*     */       
/* 107 */     });
/* 108 */     this.deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/remove_user-26.png")));
/* 109 */     this.deleteButton.setText("Eliminar");
/* 110 */     this.deleteButton.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 112 */         UsersAdministrationPanel.this.deleteButtonActionPerformed(evt);
/*     */       }
/*     */       
/* 115 */     });
/* 116 */     GroupLayout layout = new GroupLayout(this);
/* 117 */     setLayout(layout);
/* 118 */     layout.setHorizontalGroup(layout
/* 119 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 120 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 121 */       .addContainerGap()
/* 122 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
/* 123 */       .addComponent(this.jPanel2, -1, -1, 32767)
/* 124 */       .addGroup(layout.createSequentialGroup()
/* 125 */       .addComponent(this.deleteButton, -2, 200, -2)
/* 126 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 420, 32767)
/* 127 */       .addComponent(this.jButton3, -2, 200, -2)))
/* 128 */       .addContainerGap()));
/*     */     
/* 130 */     layout.setVerticalGroup(layout
/* 131 */       .createParallelGroup(GroupLayout.Alignment.TRAILING)
/* 132 */       .addGroup(layout.createSequentialGroup()
/* 133 */       .addContainerGap()
/* 134 */       .addComponent(this.jPanel2, -2, -1, -2)
/* 135 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, 32767)
/* 136 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 137 */       .addComponent(this.jButton3)
/* 138 */       .addComponent(this.deleteButton))
/* 139 */       .addGap(37, 37, 37)));
/*     */   }
/*     */   
/*     */ 
/*     */   private void jButton3ActionPerformed(ActionEvent evt)
/*     */   {
/* 145 */     AdministrationJob.getInstance().newUser();
/*     */     
/* 147 */     actualizaInfo();
/*     */   }
/*     */   
/*     */ 
/*     */   private void deleteButtonActionPerformed(ActionEvent evt)
/*     */   {
/* 153 */     Integer idxSelected = Integer.valueOf(this.jTable1.getSelectionModel().getLeadSelectionIndex());
/*     */     
/* 155 */     String username = this.jTable1.getModel().getValueAt(idxSelected.intValue(), 0).toString();
/*     */     
/* 157 */     AdministrationJob.getInstance().deleteUser(username);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 171 */     DefaultTableModel dm = (DefaultTableModel)this.jTable1.getModel();
/* 172 */     for (int i = dm.getRowCount() - 1; i >= 0; i--) {
/* 173 */       dm.removeRow(i);
/*     */     }
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 179 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 184 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/* 189 */     clearData();
/* 190 */     java.util.List<User> users = Models.JpaControllers.local.UserJpaController.getInstance().findUserEntities();
/*     */     
/* 192 */     DefaultTableModel dt = (DefaultTableModel)this.jTable1.getModel();
/*     */     
/* 194 */     for (User us : users)
/*     */     {
/*     */ 
/*     */ 
/* 198 */       if (us.getRol().equals(Rol.ADMINISTRADOR)) {
/* 199 */         Object[] o = new Object[2];
/* 200 */         o[0] = us.getUsername();
/* 201 */         o[1] = "Administrador";
/* 202 */         dt.addRow(o);
/* 203 */       } else if (us.getRol().equals(Rol.OPERADOR)) {
/* 204 */         Object[] o = new Object[3];
/* 205 */         o[0] = us.getUsername();
/* 206 */         o[1] = "Operador";
/* 207 */         dt.addRow(o);
/* 208 */       } else if (!us.getRol().equals(Rol.SUPERADMIN)) {}
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 226 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/UsersAdministrationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */