/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ 
/*     */ public class CreateDestinoPanel extends ui.PtlPanel
/*     */ {
/*     */   private javax.swing.JPasswordField descriptionText;
/*     */   private javax.swing.JTextField destinoText;
/*     */   private JButton jButton1;
/*     */   private JButton jButton2;
/*     */   private javax.swing.JLabel jLabel1;
/*     */   private javax.swing.JLabel jLabel2;
/*     */   private javax.swing.JLabel jLabel3;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JSpinner rankingText;
/*     */   
/*     */   public CreateDestinoPanel()
/*     */   {
/*  23 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  35 */     this.jPanel1 = new javax.swing.JPanel();
/*  36 */     this.jLabel1 = new javax.swing.JLabel();
/*  37 */     this.destinoText = new javax.swing.JTextField();
/*  38 */     this.jLabel2 = new javax.swing.JLabel();
/*  39 */     this.jLabel3 = new javax.swing.JLabel();
/*  40 */     this.jButton1 = new JButton();
/*  41 */     this.jButton2 = new JButton();
/*  42 */     this.descriptionText = new javax.swing.JPasswordField();
/*  43 */     this.rankingText = new javax.swing.JSpinner();
/*     */     
/*  45 */     setBackground(java.awt.Color.white);
/*     */     
/*  47 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  48 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Crear Destino"));
/*     */     
/*  50 */     this.jLabel1.setText("Destino");
/*     */     
/*  52 */     this.jLabel2.setText("Descripcion");
/*     */     
/*  54 */     this.jLabel3.setText("Posicion Ranking");
/*     */     
/*  56 */     this.jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/checkmark-26.png")));
/*  57 */     this.jButton1.setText("Crear Destino");
/*  58 */     this.jButton1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  60 */         CreateDestinoPanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/*  63 */     });
/*  64 */     this.jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/cancel-26.png")));
/*  65 */     this.jButton2.setText("Cancelar");
/*  66 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  68 */         CreateDestinoPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  71 */     });
/*  72 */     this.rankingText.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(999), Integer.valueOf(1), null, Integer.valueOf(1)));
/*     */     
/*  74 */     GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
/*  75 */     this.jPanel1.setLayout(jPanel1Layout);
/*  76 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  77 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  78 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  79 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
/*  80 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  81 */       .addContainerGap(-1, 32767)
/*  82 */       .addComponent(this.jButton2, -2, 200, -2)
/*  83 */       .addGap(18, 18, 18)
/*  84 */       .addComponent(this.jButton1, -2, 200, -2))
/*  85 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  86 */       .addGap(34, 34, 34)
/*  87 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/*  88 */       .addComponent(this.jLabel1, -2, 97, -2)
/*  89 */       .addComponent(this.jLabel2)
/*  90 */       .addComponent(this.jLabel3))
/*  91 */       .addGap(25, 25, 25)
/*  92 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/*  93 */       .addComponent(this.descriptionText)
/*  94 */       .addComponent(this.destinoText)
/*  95 */       .addComponent(this.rankingText, -1, 609, 32767))))
/*  96 */       .addContainerGap()));
/*     */     
/*  98 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/*  99 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 100 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 101 */       .addGap(25, 25, 25)
/* 102 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 103 */       .addComponent(this.jLabel1)
/* 104 */       .addComponent(this.destinoText, -2, -1, -2))
/* 105 */       .addGap(23, 23, 23)
/* 106 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 107 */       .addComponent(this.jLabel2)
/* 108 */       .addComponent(this.descriptionText, -2, -1, -2))
/* 109 */       .addGap(17, 17, 17)
/* 110 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 111 */       .addComponent(this.jLabel3)
/* 112 */       .addComponent(this.rankingText, -2, -1, -2))
/* 113 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, 32767)
/* 114 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 115 */       .addComponent(this.jButton1)
/* 116 */       .addComponent(this.jButton2))
/* 117 */       .addGap(43, 43, 43)));
/*     */     
/*     */ 
/* 120 */     GroupLayout layout = new GroupLayout(this);
/* 121 */     setLayout(layout);
/* 122 */     layout.setHorizontalGroup(layout
/* 123 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 124 */       .addGroup(layout.createSequentialGroup()
/* 125 */       .addContainerGap()
/* 126 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 127 */       .addContainerGap()));
/*     */     
/* 129 */     layout.setVerticalGroup(layout
/* 130 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 131 */       .addGroup(layout.createSequentialGroup()
/* 132 */       .addContainerGap()
/* 133 */       .addComponent(this.jPanel1, -1, -1, 32767)
/* 134 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */   private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 139 */     Workflow.process.Administration.AdministrationJob.getInstance().destinosAdministration();
/*     */   }
/*     */   
/*     */   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 144 */     Workflow.process.Administration.AdministrationJob.getInstance().createDestino(this.destinoText.getText(), (Integer)this.rankingText.getValue());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 163 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 168 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 173 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/* 178 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 183 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/CreateDestinoPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */