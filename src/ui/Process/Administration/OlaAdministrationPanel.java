/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JPanel;
/*     */ 
/*     */ public class OlaAdministrationPanel extends ui.PtlPanel
/*     */ {
/*     */   private JButton jButton3;
/*     */   private JLabel jLabel4;
/*     */   private JPanel jPanel1;
/*     */   private JLabel notificationString;
/*     */   private javax.swing.JTextField olaText;
/*     */   
/*     */   public OlaAdministrationPanel()
/*     */   {
/*  21 */     initComponents();
/*  22 */     clearData();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  34 */     this.jPanel1 = new JPanel();
/*  35 */     this.olaText = new javax.swing.JTextField();
/*  36 */     this.jButton3 = new JButton();
/*  37 */     this.jLabel4 = new JLabel();
/*  38 */     this.notificationString = new JLabel();
/*     */     
/*  40 */     setBackground(java.awt.Color.white);
/*     */     
/*  42 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  43 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
/*     */     
/*  45 */     this.jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/refresh-26.png")));
/*  46 */     this.jButton3.setText("Reiniciar Nota de Venta");
/*  47 */     this.jButton3.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  49 */         OlaAdministrationPanel.this.jButton3ActionPerformed(evt);
/*     */       }
/*     */       
/*  52 */     });
/*  53 */     this.jLabel4.setHorizontalAlignment(0);
/*  54 */     this.jLabel4.setText("Ingrese el número de nota de venta que desea reiniciar");
/*     */     
/*  56 */     this.notificationString.setForeground(new java.awt.Color(244, 27, 27));
/*     */     
/*  58 */     GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
/*  59 */     this.jPanel1.setLayout(jPanel1Layout);
/*  60 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  61 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  62 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  63 */       .addContainerGap(148, 32767)
/*  64 */       .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
/*  65 */       .addComponent(this.notificationString, -2, 475, -2)
/*  66 */       .addComponent(this.jLabel4, -2, 475, -2)
/*  67 */       .addComponent(this.olaText, -2, 475, -2)
/*  68 */       .addComponent(this.jButton3, -2, 475, -2))
/*  69 */       .addContainerGap(148, 32767)));
/*     */     
/*  71 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/*  72 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  73 */       .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
/*  74 */       .addGap(56, 56, 56)
/*  75 */       .addComponent(this.jLabel4)
/*  76 */       .addGap(32, 32, 32)
/*  77 */       .addComponent(this.olaText, -2, -1, -2)
/*  78 */       .addGap(18, 18, 18)
/*  79 */       .addComponent(this.jButton3)
/*  80 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/*  81 */       .addComponent(this.notificationString, -2, 35, -2)
/*  82 */       .addContainerGap(110, 32767)));
/*     */     
/*     */ 
/*  85 */     GroupLayout layout = new GroupLayout(this);
/*  86 */     setLayout(layout);
/*  87 */     layout.setHorizontalGroup(layout
/*  88 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  89 */       .addGroup(layout.createSequentialGroup()
/*  90 */       .addContainerGap()
/*  91 */       .addComponent(this.jPanel1, -1, -1, 32767)
/*  92 */       .addContainerGap()));
/*     */     
/*  94 */     layout.setVerticalGroup(layout
/*  95 */       .createParallelGroup(GroupLayout.Alignment.TRAILING)
/*  96 */       .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
/*  97 */       .addContainerGap()
/*  98 */       .addComponent(this.jPanel1, -1, -1, 32767)
/*  99 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */   private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 104 */     String ola = this.olaText.getText();
/* 105 */     clearData();
/* 106 */     Workflow.process.Administration.AdministrationJob.getInstance().olaReset(ola);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 120 */     this.notificationString.setText("");
/* 121 */     this.olaText.setText("");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 126 */     this.notificationString.setText(msg);
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 131 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/* 136 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 141 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/OlaAdministrationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */