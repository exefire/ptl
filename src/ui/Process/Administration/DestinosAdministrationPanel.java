/*     */ package ui.Process.Administration;
/*     */ 
/*     */ import Models.Entities.local.Destino;
/*     */ import Models.JpaControllers.local.DestinoJpaController;
/*     */ import Workflow.process.Administration.AdministrationJob;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.util.logging.Logger;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.event.ListSelectionEvent;
/*     */ import javax.swing.table.DefaultTableModel;
/*     */ 
/*     */ public class DestinosAdministrationPanel extends ui.PtlPanel
/*     */ {
/*     */   private JButton deleteButton;
/*     */   private JButton jButton3;
/*     */   private JButton jButton4;
/*     */   private javax.swing.JScrollPane jScrollPane3;
/*     */   private JTable jTable2;
/*     */   private JLabel notificationLabel;
/*     */   
/*     */   public DestinosAdministrationPanel()
/*     */   {
/*  30 */     initComponents();
/*  31 */     this.deleteButton.setEnabled(false);
/*  32 */     this.notificationLabel.setText("");
/*     */     
/*  34 */     this.jTable2.getSelectionModel().addListSelectionListener(new javax.swing.event.ListSelectionListener() {
/*     */       public void valueChanged(ListSelectionEvent event) {
/*  36 */         if (event.getValueIsAdjusting()) {
/*  37 */           return;
/*     */         }
/*     */         
/*  40 */         if (DestinosAdministrationPanel.this.jTable2.getSelectedRowCount() == 0) {
/*  41 */           DestinosAdministrationPanel.this.deleteButton.setEnabled(false);
/*     */         } else {
/*  43 */           DestinosAdministrationPanel.this.deleteButton.setEnabled(true);
/*     */         }
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  67 */     this.jScrollPane3 = new javax.swing.JScrollPane();
/*  68 */     this.jTable2 = new JTable();
/*  69 */     this.jButton3 = new JButton();
/*  70 */     this.jButton4 = new JButton();
/*  71 */     this.deleteButton = new JButton();
/*  72 */     this.notificationLabel = new JLabel();
/*     */     
/*  74 */     setBackground(java.awt.Color.white);
/*     */     
/*  76 */     this.jTable2.setModel(new DefaultTableModel(new Object[][] { { null, null }, { null, null }, { null, null }, { null, null } }, new String[] { "Destino", "Ranking" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  87 */       boolean[] canEdit = { false, true };
/*     */       
/*     */ 
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex)
/*     */       {
/*  92 */         return this.canEdit[columnIndex];
/*     */       }
/*  94 */     });
/*  95 */     this.jScrollPane3.setViewportView(this.jTable2);
/*     */     
/*  97 */     this.jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/checkmark-26.png")));
/*  98 */     this.jButton3.setText("Guardar Cambios");
/*  99 */     this.jButton3.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 101 */         DestinosAdministrationPanel.this.jButton3ActionPerformed(evt);
/*     */       }
/*     */       
/* 104 */     });
/* 105 */     this.jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/plus-26.png")));
/* 106 */     this.jButton4.setText("Agregar Destino");
/* 107 */     this.jButton4.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 109 */         DestinosAdministrationPanel.this.jButton4ActionPerformed(evt);
/*     */       }
/*     */       
/* 112 */     });
/* 113 */     this.deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/minus-26.png")));
/* 114 */     this.deleteButton.setText("Eliminar Destino");
/* 115 */     this.deleteButton.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 117 */         DestinosAdministrationPanel.this.deleteButtonActionPerformed(evt);
/*     */       }
/*     */       
/* 120 */     });
/* 121 */     this.notificationLabel.setFont(new java.awt.Font("Ubuntu", 1, 15));
/* 122 */     this.notificationLabel.setForeground(new java.awt.Color(230, 0, 27));
/* 123 */     this.notificationLabel.setHorizontalAlignment(0);
/* 124 */     this.notificationLabel.setText("jLabel1");
/*     */     
/* 126 */     GroupLayout layout = new GroupLayout(this);
/* 127 */     setLayout(layout);
/* 128 */     layout.setHorizontalGroup(layout
/* 129 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 130 */       .addGroup(layout.createSequentialGroup()
/* 131 */       .addContainerGap()
/* 132 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 133 */       .addComponent(this.notificationLabel, -1, -1, 32767)
/* 134 */       .addComponent(this.jScrollPane3, GroupLayout.Alignment.TRAILING)
/* 135 */       .addGroup(layout.createSequentialGroup()
/* 136 */       .addComponent(this.deleteButton, -2, 200, -2)
/* 137 */       .addGap(18, 18, 18)
/* 138 */       .addComponent(this.jButton4, -2, 199, -2)
/* 139 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, 32767)
/* 140 */       .addComponent(this.jButton3, -2, 200, -2)))
/* 141 */       .addContainerGap()));
/*     */     
/* 143 */     layout.setVerticalGroup(layout
/* 144 */       .createParallelGroup(GroupLayout.Alignment.TRAILING)
/* 145 */       .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
/* 146 */       .addContainerGap()
/* 147 */       .addComponent(this.jScrollPane3, -2, 234, -2)
/* 148 */       .addGap(18, 18, 18)
/* 149 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 150 */       .addComponent(this.deleteButton)
/* 151 */       .addComponent(this.jButton4)
/* 152 */       .addComponent(this.jButton3))
/* 153 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 154 */       .addComponent(this.notificationLabel)
/* 155 */       .addContainerGap(50, 32767)));
/*     */   }
/*     */   
/*     */   private void jButton4ActionPerformed(ActionEvent evt)
/*     */   {
/* 160 */     AdministrationJob.getInstance().newDestino();
/*     */   }
/*     */   
/*     */   private void deleteButtonActionPerformed(ActionEvent evt)
/*     */   {
/* 165 */     Integer idxSelected = Integer.valueOf(this.jTable2.getSelectionModel().getLeadSelectionIndex());
/*     */     
/* 167 */     String destino = this.jTable2.getModel().getValueAt(idxSelected.intValue(), 0).toString();
/*     */     
/* 169 */     AdministrationJob.getInstance().deleteDestino(destino);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void jButton3ActionPerformed(ActionEvent evt)
/*     */   {
/* 178 */     DefaultTableModel dm = (DefaultTableModel)this.jTable2.getModel();
/*     */     
/* 180 */     for (int i = 0; i < dm.getRowCount(); i++)
/*     */     {
/* 182 */       Destino d = DestinoJpaController.getInstance().findDestinoByNombre((String)dm.getValueAt(i, 0));
/* 183 */       if ((d != null) && (isInteger(dm.getValueAt(i, 1).toString()))) {
/* 184 */         Integer r = Integer.valueOf(Integer.parseInt(dm.getValueAt(i, 1).toString()));
/* 185 */         d.setRanking(r);
/*     */         try {
/* 187 */           DestinoJpaController.getInstance().edit(d);
/*     */         } catch (Models.JpaControllers.local.exceptions.NonexistentEntityException ex) {
/* 189 */           Logger.getLogger(DestinosAdministrationPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
/*     */         } catch (Exception ex) {
/* 191 */           Logger.getLogger(DestinosAdministrationPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 197 */     AdministrationJob.getInstance().destinosUpdated();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 212 */     this.notificationLabel.setText("");
/* 213 */     DefaultTableModel dm = (DefaultTableModel)this.jTable2.getModel();
/* 214 */     for (int i = dm.getRowCount() - 1; i >= 0; i--) {
/* 215 */       dm.removeRow(i);
/*     */     }
/*     */     
/* 218 */     actualizaInfo();
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 223 */     this.notificationLabel.setText(msg);
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 228 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 234 */     java.util.List<Destino> destinos = DestinoJpaController.getInstance().findDestinoEntitiesOrderByRank();
/*     */     
/* 236 */     DefaultTableModel dt = (DefaultTableModel)this.jTable2.getModel();
/*     */     
/* 238 */     for (Destino d : destinos)
/*     */     {
/*     */ 
/*     */ 
/* 242 */       Object[] o = new Object[2];
/* 243 */       o[0] = d.getNombre();
/* 244 */       o[1] = d.getRanking();
/* 245 */       dt.addRow(o);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 253 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public boolean isInteger(String s)
/*     */   {
/*     */     try {
/* 259 */       Integer.parseInt(s);
/*     */     } catch (NumberFormatException e) {
/* 261 */       return false;
/*     */     }
/*     */     
/* 264 */     return true;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Administration/DestinosAdministrationPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */