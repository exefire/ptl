/*     */ package ui.Process.GroupSelection;
/*     */ 
/*     */ import Workflow.process.GroupSelection.GroupSelectionJob;
/*     */ import java.awt.Color;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class InitMessagePanel extends ui.PtlPanel
/*     */ {
/*     */   private JButton jButton1;
/*     */   private JButton jButton2;
/*     */   private JLabel notificationLabel;
/*     */   
/*     */   public InitMessagePanel()
/*     */   {
/*  22 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  34 */     this.notificationLabel = new JLabel();
/*  35 */     this.jButton1 = new JButton();
/*  36 */     this.jButton2 = new JButton();
/*     */     
/*  38 */     setBackground(new Color(255, 255, 255));
/*     */     
/*  40 */     this.notificationLabel.setFont(new java.awt.Font("Arial", 1, 24));
/*  41 */     this.notificationLabel.setForeground(new Color(0, 102, 255));
/*  42 */     this.notificationLabel.setHorizontalAlignment(0);
/*     */     
/*  44 */     this.jButton1.setText("Cerrar Cajas");
/*  45 */     this.jButton1.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  47 */         InitMessagePanel.this.jButton1ActionPerformed(evt);
/*     */       }
/*     */       
/*  50 */     });
/*  51 */     this.jButton2.setText("Continuar Sorting");
/*  52 */     this.jButton2.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  54 */         InitMessagePanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  57 */     });
/*  58 */     GroupLayout layout = new GroupLayout(this);
/*  59 */     setLayout(layout);
/*  60 */     layout.setHorizontalGroup(layout
/*  61 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  62 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/*  63 */       .addContainerGap()
/*  64 */       .addComponent(this.notificationLabel, -1, 817, 32767)
/*  65 */       .addContainerGap())
/*  66 */       .addGroup(layout.createSequentialGroup()
/*  67 */       .addGap(132, 132, 132)
/*  68 */       .addComponent(this.jButton1, -2, 186, -2)
/*  69 */       .addGap(162, 162, 162)
/*  70 */       .addComponent(this.jButton2, -2, 186, -2)
/*  71 */       .addContainerGap(-1, 32767)));
/*     */     
/*  73 */     layout.setVerticalGroup(layout
/*  74 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  75 */       .addGroup(layout.createSequentialGroup()
/*  76 */       .addGap(98, 98, 98)
/*  77 */       .addComponent(this.notificationLabel, -2, 112, -2)
/*  78 */       .addGap(49, 49, 49)
/*  79 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/*  80 */       .addComponent(this.jButton1, -2, 45, -2)
/*  81 */       .addComponent(this.jButton2, -2, 45, -2))
/*  82 */       .addContainerGap(49, 32767)));
/*     */   }
/*     */   
/*     */ 
/*     */   private void jButton2ActionPerformed(ActionEvent evt)
/*     */   {
/*  88 */     GroupSelectionJob.getInstance().continueLastPicking();
/*     */   }
/*     */   
/*     */   private void jButton1ActionPerformed(ActionEvent evt) {
/*  92 */     GroupSelectionJob.getInstance().endLastPicking();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 103 */     this.notificationLabel.setText("");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/* 108 */     this.notificationLabel.setText(msg);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void incomingData(String event, java.util.HashMap data) {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void actualizaInfo() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 123 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/GroupSelection/InitMessagePanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */