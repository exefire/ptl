/*     */ package ui.Process.GroupSelection;
/*     */ 
/*     */ import Workflow.process.GroupSelection.GroupSelectionJob;
/*     */ import java.awt.Color;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.util.HashMap;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class CloseAllBoxesWarningPanel extends ui.PtlPanel
/*     */ {
/*     */   private JButton jButton2;
/*     */   private JLabel notificationLabel;
/*     */   
/*     */   public CloseAllBoxesWarningPanel()
/*     */   {
/*  22 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  34 */     this.notificationLabel = new JLabel();
/*  35 */     this.jButton2 = new JButton();
/*     */     
/*  37 */     setBackground(new Color(255, 255, 255));
/*     */     
/*  39 */     this.notificationLabel.setFont(new java.awt.Font("Arial", 1, 24));
/*  40 */     this.notificationLabel.setForeground(new Color(0, 102, 255));
/*  41 */     this.notificationLabel.setHorizontalAlignment(0);
/*  42 */     this.notificationLabel.setText("Debe cerrar todas las Cajas para Continuar");
/*     */     
/*  44 */     this.jButton2.setText("Aceptar");
/*  45 */     this.jButton2.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  47 */         CloseAllBoxesWarningPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  50 */     });
/*  51 */     GroupLayout layout = new GroupLayout(this);
/*  52 */     setLayout(layout);
/*  53 */     layout.setHorizontalGroup(layout
/*  54 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  55 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/*  56 */       .addContainerGap()
/*  57 */       .addComponent(this.notificationLabel, -1, 817, 32767)
/*  58 */       .addContainerGap())
/*  59 */       .addGroup(layout.createSequentialGroup()
/*  60 */       .addGap(307, 307, 307)
/*  61 */       .addComponent(this.jButton2, -2, 220, -2)
/*  62 */       .addContainerGap(-1, 32767)));
/*     */     
/*  64 */     layout.setVerticalGroup(layout
/*  65 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/*  66 */       .addGroup(layout.createSequentialGroup()
/*  67 */       .addGap(98, 98, 98)
/*  68 */       .addComponent(this.notificationLabel, -2, 112, -2)
/*  69 */       .addGap(46, 46, 46)
/*  70 */       .addComponent(this.jButton2, -2, 45, -2)
/*  71 */       .addContainerGap(52, 32767)));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void jButton2ActionPerformed(ActionEvent evt)
/*     */   {
/*  78 */     GroupSelectionJob.getInstance().restartSelection();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/*  90 */     this.notificationLabel.setText("");
/*     */   }
/*     */   
/*     */   public void notificate(String msg)
/*     */   {
/*  95 */     this.notificationLabel.setText(msg);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void incomingData(String event, HashMap data) {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void actualizaInfo() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void setFocus()
/*     */   {
/* 110 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/GroupSelection/CloseAllBoxesWarningPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */