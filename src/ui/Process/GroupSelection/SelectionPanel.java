/*     */ package ui.Process.GroupSelection;
/*     */ 
/*     */ import javax.swing.GroupLayout;
/*     */ 
/*     */ public class SelectionPanel extends ui.PtlPanel {
/*     */   private javax.swing.JButton BTN_ComenzarPicking;
/*     */   private javax.swing.JLabel MensajeLabel;
/*     */   private javax.swing.JLabel destinosLabel;
/*     */   private javax.swing.JButton endOlaButton;
/*     */   private javax.swing.JButton jButton2;
/*     */   private javax.swing.JLabel jLabel1;
/*     */   private javax.swing.JLabel jLabel2;
/*     */   private javax.swing.JLabel jLabel3;
/*     */   private javax.swing.JLabel jLabel4;
/*     */   private javax.swing.JLabel jLabel5;
/*     */   private javax.swing.JLabel jLabel6;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JPanel jPanel2;
/*     */   private javax.swing.JPanel jPanel3;
/*     */   private javax.swing.JPanel jPanel4;
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   private javax.swing.JTable jTable1;
/*     */   private javax.swing.JTextField jTextField1;
/*     */   private javax.swing.JLabel lucesLabel;
/*     */   private javax.swing.JLabel olasLabel;
/*     */   private javax.swing.JLabel pickingLabel;
/*     */   
/*  28 */   public SelectionPanel() { initComponents();
/*  29 */     clearData();
/*  30 */     this.jTextField1.requestFocusInWindow();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  42 */     this.BTN_ComenzarPicking = new javax.swing.JButton();
/*  43 */     this.jPanel1 = new javax.swing.JPanel();
/*  44 */     this.jLabel3 = new javax.swing.JLabel();
/*  45 */     this.jLabel4 = new javax.swing.JLabel();
/*  46 */     this.jLabel5 = new javax.swing.JLabel();
/*  47 */     this.pickingLabel = new javax.swing.JLabel();
/*  48 */     this.destinosLabel = new javax.swing.JLabel();
/*  49 */     this.olasLabel = new javax.swing.JLabel();
/*  50 */     this.jLabel6 = new javax.swing.JLabel();
/*  51 */     this.lucesLabel = new javax.swing.JLabel();
/*  52 */     this.endOlaButton = new javax.swing.JButton();
/*  53 */     this.jPanel2 = new javax.swing.JPanel();
/*  54 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/*  55 */     this.jTable1 = new javax.swing.JTable();
/*  56 */     this.jPanel4 = new javax.swing.JPanel();
/*  57 */     this.jTextField1 = new javax.swing.JTextField();
/*  58 */     this.jLabel1 = new javax.swing.JLabel();
/*  59 */     this.jButton2 = new javax.swing.JButton();
/*  60 */     this.MensajeLabel = new javax.swing.JLabel();
/*  61 */     this.jPanel3 = new javax.swing.JPanel();
/*  62 */     this.jLabel2 = new javax.swing.JLabel();
/*     */     
/*  64 */     setBackground(java.awt.Color.white);
/*  65 */     setMinimumSize(new java.awt.Dimension(1005, 508));
/*     */     
/*  67 */     this.BTN_ComenzarPicking.setText("Comenzar Sorting");
/*  68 */     this.BTN_ComenzarPicking.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  70 */         SelectionPanel.this.BTN_ComenzarPickingActionPerformed(evt);
/*     */       }
/*     */       
/*  73 */     });
/*  74 */     this.jPanel1.setBackground(java.awt.Color.white);
/*  75 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Información de la nota de venta"));
/*     */     
/*  77 */     this.jLabel3.setText("Nota de veta");
/*     */     
/*  79 */     this.jLabel4.setText("Destinos:");
/*     */     
/*  81 */     this.jLabel5.setText("Nro OLA");
/*     */     
/*  83 */     this.pickingLabel.setText("---");
/*     */     
/*  85 */     this.destinosLabel.setText("---");
/*     */     
/*  87 */     this.olasLabel.setText("---");
/*     */     
/*  89 */     this.jLabel6.setText("Luces:");
/*     */     
/*  91 */     this.lucesLabel.setText("---");
/*     */     
/*  93 */     this.endOlaButton.setText("Generar Archivo");
/*  94 */     this.endOlaButton.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  96 */         SelectionPanel.this.endOlaButtonActionPerformed(evt);
/*     */       }
/*     */       
/*  99 */     });
/* 100 */     GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
/* 101 */     this.jPanel1.setLayout(jPanel1Layout);
/* 102 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/* 103 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 104 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 105 */       .addGap(21, 21, 21)
/* 106 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 107 */       .addComponent(this.jLabel3)
/* 108 */       .addComponent(this.jLabel5)
/* 109 */       .addComponent(this.jLabel4)
/* 110 */       .addComponent(this.jLabel6))
/* 111 */       .addGap(31, 31, 31)
/* 112 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 113 */       .addComponent(this.lucesLabel)
/* 114 */       .addComponent(this.olasLabel)
/* 115 */       .addComponent(this.destinosLabel)
/* 116 */       .addComponent(this.pickingLabel))
/* 117 */       .addContainerGap(-1, 32767))
/* 118 */       .addComponent(this.endOlaButton, -1, -1, 32767));
/*     */     
/* 120 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 121 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 122 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 123 */       .addGap(37, 37, 37)
/* 124 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 125 */       .addComponent(this.jLabel3)
/* 126 */       .addComponent(this.pickingLabel))
/* 127 */       .addGap(18, 18, 18)
/* 128 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 129 */       .addComponent(this.jLabel4)
/* 130 */       .addComponent(this.destinosLabel))
/* 131 */       .addGap(18, 18, 18)
/* 132 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 133 */       .addComponent(this.jLabel5)
/* 134 */       .addComponent(this.olasLabel))
/* 135 */       .addGap(18, 18, 18)
/* 136 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 137 */       .addComponent(this.jLabel6)
/* 138 */       .addComponent(this.lucesLabel))
/* 139 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
/* 140 */       .addComponent(this.endOlaButton, -2, 50, -2)));
/*     */     
/*     */ 
/* 143 */     this.jPanel2.setBackground(java.awt.Color.white);
/* 144 */     this.jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle del grupo picking"));
/*     */     
/* 146 */     this.jTable1.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Destino", "Nota Venta", "Artículo", "Cantidad" }));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 157 */     this.jScrollPane1.setViewportView(this.jTable1);
/*     */     
/* 159 */     GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
/* 160 */     this.jPanel2.setLayout(jPanel2Layout);
/* 161 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/* 162 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 163 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
/* 164 */       .addContainerGap()
/* 165 */       .addComponent(this.jScrollPane1)
/* 166 */       .addContainerGap()));
/*     */     
/* 168 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/* 169 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 170 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
/* 171 */       .addContainerGap()
/* 172 */       .addComponent(this.jScrollPane1, -1, 282, 32767)
/* 173 */       .addContainerGap()));
/*     */     
/*     */ 
/* 176 */     this.jPanel4.setBackground(java.awt.Color.white);
/* 177 */     this.jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Busqueda de archivo de nota de venta"));
/*     */     
/* 179 */     this.jTextField1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 181 */         SelectionPanel.this.jTextField1ActionPerformed(evt);
/*     */       }
/*     */       
/* 184 */     });
/* 185 */     this.jLabel1.setText(" N° Nota Venta");
/*     */     
/* 187 */     this.jButton2.setText("Buscar Nota de Venta");
/* 188 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 190 */         SelectionPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/* 193 */     });
/* 194 */     this.MensajeLabel.setForeground(new java.awt.Color(255, 51, 51));
/* 195 */     this.MensajeLabel.setHorizontalAlignment(0);
/*     */     
/* 197 */     GroupLayout jPanel4Layout = new GroupLayout(this.jPanel4);
/* 198 */     this.jPanel4.setLayout(jPanel4Layout);
/* 199 */     jPanel4Layout.setHorizontalGroup(jPanel4Layout
/* 200 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 201 */       .addGroup(jPanel4Layout.createSequentialGroup()
/* 202 */       .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
/* 203 */       .addGroup(jPanel4Layout.createSequentialGroup()
/* 204 */       .addContainerGap()
/* 205 */       .addComponent(this.MensajeLabel, -1, -1, 32767))
/* 206 */       .addGroup(jPanel4Layout.createSequentialGroup()
/* 207 */       .addComponent(this.jLabel1)
/* 208 */       .addGap(18, 18, 18)
/* 209 */       .addComponent(this.jTextField1, -2, 391, -2)
/* 210 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 211 */       .addComponent(this.jButton2, -1, 173, 32767)))
/* 212 */       .addContainerGap(54, 32767)));
/*     */     
/* 214 */     jPanel4Layout.setVerticalGroup(jPanel4Layout
/* 215 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 216 */       .addGroup(jPanel4Layout.createSequentialGroup()
/* 217 */       .addContainerGap()
/* 218 */       .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 219 */       .addComponent(this.jTextField1, -2, 35, -2)
/* 220 */       .addComponent(this.jLabel1)
/* 221 */       .addComponent(this.jButton2, -2, 29, -2))
/* 222 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 223 */       .addComponent(this.MensajeLabel, -1, 3, 32767)));
/*     */     
/*     */ 
/* 226 */     this.jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/logo_retailsoft_240.png")));
/* 227 */     this.jLabel2.setAlignmentY(0.0F);
/*     */     
/* 229 */     GroupLayout jPanel3Layout = new GroupLayout(this.jPanel3);
/* 230 */     this.jPanel3.setLayout(jPanel3Layout);
/* 231 */     jPanel3Layout.setHorizontalGroup(jPanel3Layout
/* 232 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 233 */       .addComponent(this.jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, -1, -1, 32767));
/*     */     
/* 235 */     jPanel3Layout.setVerticalGroup(jPanel3Layout
/* 236 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 237 */       .addComponent(this.jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, -2, 0, 32767));
/*     */     
/*     */ 
/* 240 */     GroupLayout layout = new GroupLayout(this);
/* 241 */     setLayout(layout);
/* 242 */     layout.setHorizontalGroup(layout
/* 243 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 244 */       .addGroup(layout.createSequentialGroup()
/* 245 */       .addContainerGap()
/* 246 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
/* 247 */       .addComponent(this.jPanel3, -1, -1, 32767)
/* 248 */       .addComponent(this.jPanel1, -1, -1, 32767))
/* 249 */       .addGap(10, 10, 10)
/* 250 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 251 */       .addComponent(this.jPanel4, -1, -1, 32767)
/* 252 */       .addComponent(this.BTN_ComenzarPicking, -1, -1, 32767)
/* 253 */       .addComponent(this.jPanel2, -1, -1, 32767))
/* 254 */       .addContainerGap()));
/*     */     
/* 256 */     layout.setVerticalGroup(layout
/* 257 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 258 */       .addGroup(layout.createSequentialGroup()
/* 259 */       .addContainerGap()
/* 260 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
/* 261 */       .addComponent(this.jPanel3, -1, -1, 32767)
/* 262 */       .addComponent(this.jPanel4, -1, -1, 32767))
/* 263 */       .addGap(18, 18, 18)
/* 264 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 265 */       .addGroup(layout.createSequentialGroup()
/* 266 */       .addComponent(this.jPanel2, -1, -1, 32767)
/* 267 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 268 */       .addComponent(this.BTN_ComenzarPicking, -2, 57, -2))
/* 269 */       .addComponent(this.jPanel1, -1, -1, 32767))
/* 270 */       .addContainerGap()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void BTN_ComenzarPickingActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 277 */     Workflow.process.GroupSelection.GroupSelectionJob.getInstance().groupSelected(this.pickingLabel.getText());
/*     */   }
/*     */   
/*     */   private void procesaBulto()
/*     */   {
/* 282 */     String id = this.jTextField1.getText();
/* 283 */     clearData();
/* 284 */     Workflow.process.GroupSelection.GroupSelectionJob.getInstance().loadGroup(id);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 292 */     procesaBulto();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 301 */     procesaBulto();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void endOlaButtonActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 312 */     Workflow.process.GroupSelection.GroupSelectionJob.getInstance().FinalizaOlaPorCambioDeBulto();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clearData()
/*     */   {
/* 344 */     this.BTN_ComenzarPicking.setEnabled(false);
/*     */     
/* 346 */     this.jTextField1.setText("");
/* 347 */     this.MensajeLabel.setText("");
/* 348 */     javax.swing.table.DefaultTableModel dm = (javax.swing.table.DefaultTableModel)this.jTable1.getModel();
/* 349 */     for (int i = dm.getRowCount() - 1; i >= 0; i--) {
/* 350 */       dm.removeRow(i);
/*     */     }
/*     */     
/* 353 */     this.pickingLabel.setText("---");
/* 354 */     this.olasLabel.setText("---");
/* 355 */     this.destinosLabel.setText("---");
/* 356 */     this.lucesLabel.setText("---");
/*     */     
/*     */ 
/* 359 */     if (Core.ApplicationController.getInstance().getVariable("picking") == null) {
/* 360 */       this.endOlaButton.setEnabled(false);
/*     */     } else {
/* 362 */       this.endOlaButton.setEnabled(true);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void notificate(String msg)
/*     */   {
/* 370 */     this.MensajeLabel.setText(msg);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 377 */     if (event.equals("loadPicking"))
/*     */     {
/* 379 */       clearData();
/* 380 */       java.util.Collection<Models.Entities.local.DetallePicking> lista = (java.util.Collection)data.get("DetallePicking");
/* 381 */       javax.swing.table.DefaultTableModel dm = (javax.swing.table.DefaultTableModel)this.jTable1.getModel();
/*     */       
/* 383 */       for (Models.Entities.local.DetallePicking ds : lista) {
/* 384 */         Object[] o = new Object[5];
/* 385 */         o[0] = ds.getDestinoId().getNombre();
/* 386 */         o[1] = ds.getReferencia();
/* 387 */         o[2] = ds.getArticulo();
/* 388 */         o[3] = Integer.valueOf(ds.getCantidad().intValue());
/* 389 */         dm.addRow(o);
/*     */       }
/*     */       
/*     */ 
/* 393 */       this.pickingLabel.setText((String)data.get("picking"));
/* 394 */       this.olasLabel.setText(((Integer)data.get("ola")).toString());
/* 395 */       this.destinosLabel.setText(((Integer)data.get("ndestinos")).toString());
/* 396 */       this.lucesLabel.setText(Configuration.ConfigurationManager.getInstance().getProperty("luces"));
/* 397 */       this.BTN_ComenzarPicking.setEnabled(true);
/* 398 */       this.endOlaButton.setEnabled(true);
/*     */     }
/*     */     else {
/* 401 */       throw new UnsupportedOperationException("Not supported yet.");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void actualizaInfo()
/*     */   {
/* 408 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 413 */     this.jTextField1.requestFocusInWindow();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/GroupSelection/SelectionPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */