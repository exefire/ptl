/*     */ package ui.Process.Auth;
/*     */ 
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class LoginPanel extends ui.PtlPanel
/*     */ {
/*     */   private javax.swing.JButton jButton2;
/*     */   private JLabel jLabel1;
/*     */   private JLabel jLabel2;
/*     */   private JLabel jLabel3;
/*     */   private JLabel jLabel4;
/*     */   private javax.swing.JPanel jPanel1;
/*     */   private javax.swing.JPanel jPanel2;
/*     */   private JLabel notification;
/*     */   private javax.swing.JPasswordField password;
/*     */   private javax.swing.JTextField usuario;
/*     */   
/*     */   public LoginPanel()
/*     */   {
/*  22 */     initComponents();
/*  23 */     this.usuario.requestFocusInWindow();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  37 */     this.jPanel1 = new javax.swing.JPanel();
/*  38 */     this.usuario = new javax.swing.JTextField();
/*  39 */     this.jButton2 = new javax.swing.JButton();
/*  40 */     this.jLabel1 = new JLabel();
/*  41 */     this.jLabel2 = new JLabel();
/*  42 */     this.notification = new JLabel();
/*  43 */     this.password = new javax.swing.JPasswordField();
/*  44 */     this.jPanel2 = new javax.swing.JPanel();
/*  45 */     this.jLabel3 = new JLabel();
/*  46 */     this.jLabel4 = new JLabel();
/*     */     
/*  48 */     setBackground(new java.awt.Color(255, 255, 255));
/*  49 */     setPreferredSize(new java.awt.Dimension(415, 425));
/*     */     
/*  51 */     this.jPanel1.setBackground(new java.awt.Color(255, 255, 255));
/*  52 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Ingreso de Usuarios"));
/*  53 */     this.jPanel1.setPreferredSize(new java.awt.Dimension(400, 300));
/*     */     
/*  55 */     this.usuario.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  57 */         LoginPanel.this.usuarioActionPerformed(evt);
/*     */       }
/*     */       
/*  60 */     });
/*  61 */     this.jButton2.setText("Ingresar");
/*  62 */     this.jButton2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  64 */         LoginPanel.this.jButton2ActionPerformed(evt);
/*     */       }
/*     */       
/*  67 */     });
/*  68 */     this.jLabel1.setText("Nombre de usuario");
/*     */     
/*  70 */     this.jLabel2.setText("Contraseña");
/*     */     
/*  72 */     this.notification.setForeground(new java.awt.Color(255, 51, 102));
/*     */     
/*  74 */     this.password.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  76 */         LoginPanel.this.passwordActionPerformed(evt);
/*     */       }
/*     */       
/*  79 */     });
/*  80 */     GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
/*  81 */     this.jPanel1.setLayout(jPanel1Layout);
/*  82 */     jPanel1Layout.setHorizontalGroup(jPanel1Layout
/*  83 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  84 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  85 */       .addGap(69, 69, 69)
/*  86 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  87 */       .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/*  88 */       .addGroup(jPanel1Layout.createSequentialGroup()
/*  89 */       .addGap(8, 8, 8)
/*  90 */       .addComponent(this.notification, -2, 240, -2))
/*  91 */       .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/*  92 */       .addComponent(this.password, -2, 240, -2)
/*  93 */       .addComponent(this.usuario, javax.swing.GroupLayout.Alignment.LEADING, -2, 240, -2)
/*  94 */       .addComponent(this.jButton2, -2, 240, -2)))
/*  95 */       .addComponent(this.jLabel1, -2, 240, -2)
/*  96 */       .addComponent(this.jLabel2, -2, 240, -2))
/*  97 */       .addContainerGap(62, 32767)));
/*     */     
/*  99 */     jPanel1Layout.setVerticalGroup(jPanel1Layout
/* 100 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 101 */       .addGroup(jPanel1Layout.createSequentialGroup()
/* 102 */       .addGap(31, 31, 31)
/* 103 */       .addComponent(this.notification)
/* 104 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 105 */       .addComponent(this.jLabel1)
/* 106 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 107 */       .addComponent(this.usuario, -2, 30, -2)
/* 108 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 109 */       .addComponent(this.jLabel2)
/* 110 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 111 */       .addComponent(this.password, -2, 35, -2)
/* 112 */       .addGap(18, 18, 18)
/* 113 */       .addComponent(this.jButton2, -2, 37, -2)
/* 114 */       .addContainerGap(-1, 32767)));
/*     */     
/*     */ 
/* 117 */     this.jPanel2.setBackground(new java.awt.Color(255, 255, 255));
/*     */     
/* 119 */     this.jLabel3.setBackground(new java.awt.Color(255, 255, 255));
/* 120 */     this.jLabel3.setHorizontalAlignment(0);
/* 121 */     this.jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/logo_retailsoft_240.png")));
/*     */     
/* 123 */     GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
/* 124 */     this.jPanel2.setLayout(jPanel2Layout);
/* 125 */     jPanel2Layout.setHorizontalGroup(jPanel2Layout
/* 126 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 127 */       .addComponent(this.jLabel3, -1, 391, 32767));
/*     */     
/* 129 */     jPanel2Layout.setVerticalGroup(jPanel2Layout
/* 130 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 131 */       .addComponent(this.jLabel3, -1, 100, 32767));
/*     */     
/*     */ 
/* 134 */     this.jLabel4.setHorizontalAlignment(0);
/* 135 */     this.jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/icons/atait_test.png")));
/*     */     
/* 137 */     GroupLayout layout = new GroupLayout(this);
/* 138 */     setLayout(layout);
/* 139 */     layout.setHorizontalGroup(layout
/* 140 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 141 */       .addGroup(layout.createSequentialGroup()
/* 142 */       .addContainerGap()
/* 143 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 144 */       .addGroup(layout.createSequentialGroup()
/* 145 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 146 */       .addComponent(this.jPanel2, -2, -1, -2)
/* 147 */       .addComponent(this.jPanel1, -2, 391, -2))
/* 148 */       .addGap(0, 0, 32767))
/* 149 */       .addComponent(this.jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, -1, -1, 32767))
/* 150 */       .addContainerGap()));
/*     */     
/* 152 */     layout.setVerticalGroup(layout
/* 153 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 154 */       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 155 */       .addContainerGap()
/* 156 */       .addComponent(this.jPanel2, -2, -1, -2)
/* 157 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 158 */       .addComponent(this.jPanel1, -2, 250, -2)
/* 159 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767)
/* 160 */       .addComponent(this.jLabel4)
/* 161 */       .addGap(21, 21, 21)));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 168 */     doLogin();
/*     */   }
/*     */   
/*     */   private void doLogin()
/*     */   {
/* 173 */     Workflow.process.Auth.LoginJob.getInstance().autenticar(this.usuario.getText(), this.password.getText());
/*     */   }
/*     */   
/*     */   public void clearData()
/*     */   {
/* 178 */     this.usuario.setText("");
/* 179 */     this.password.setText("");
/*     */   }
/*     */   
/*     */ 
/*     */   private void usuarioActionPerformed(java.awt.event.ActionEvent evt) {}
/*     */   
/*     */ 
/*     */   private void passwordActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 188 */     doLogin();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void notificate(String txt)
/*     */   {
/* 209 */     this.notification.setText(txt);
/*     */   }
/*     */   
/*     */   public void incomingData(String event, java.util.HashMap data)
/*     */   {
/* 214 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void actualizaInfo()
/*     */   {
/* 219 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */   public void setFocus()
/*     */   {
/* 224 */     this.usuario.requestFocusInWindow();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/Process/Auth/LoginPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */