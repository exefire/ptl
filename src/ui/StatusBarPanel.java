/*    */ package ui;
/*    */ 
/*    */ import javax.swing.BoxLayout;
/*    */ import javax.swing.GroupLayout;
/*    */ import javax.swing.GroupLayout.Alignment;
/*    */ import javax.swing.GroupLayout.ParallelGroup;
/*    */ import javax.swing.JLabel;
/*    */ import javax.swing.JPanel;
/*    */ import javax.swing.border.BevelBorder;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class StatusBarPanel
/*    */   extends JPanel
/*    */ {
/*    */   private JLabel statusLabel;
/*    */   
/*    */   public StatusBarPanel()
/*    */   {
/* 24 */     initComponents();
/*    */     
/* 26 */     this.statusLabel = new JLabel("Iniciando...");
/* 27 */     setBorder(new BevelBorder(1));
/*    */     
/*    */ 
/* 30 */     setLayout(new BoxLayout(this, 0));
/*    */     
/* 32 */     this.statusLabel.setHorizontalAlignment(2);
/* 33 */     add(this.statusLabel);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private void initComponents()
/*    */   {
/* 45 */     GroupLayout layout = new GroupLayout(this);
/* 46 */     setLayout(layout);
/* 47 */     layout.setHorizontalGroup(layout
/* 48 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 49 */       .addGap(0, 400, 32767));
/*    */     
/* 51 */     layout.setVerticalGroup(layout
/* 52 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 53 */       .addGap(0, 300, 32767));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   void setInfo(String msg)
/*    */   {
/* 60 */     this.statusLabel.setText(msg);
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/StatusBarPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */