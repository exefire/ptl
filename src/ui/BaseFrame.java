/*    */ package ui;
/*    */ 
/*    */ import java.awt.BorderLayout;
/*    */ import java.awt.Container;
/*    */ import java.awt.EventQueue;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ import javax.swing.JFrame;
/*    */ import javax.swing.UIManager;
/*    */ import javax.swing.UIManager.LookAndFeelInfo;
/*    */ import javax.swing.UnsupportedLookAndFeelException;
/*    */ import org.netbeans.lib.awtextra.AbsoluteLayout;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class BaseFrame
/*    */   extends JFrame
/*    */ {
/*    */   public BaseFrame()
/*    */   {
/* 23 */     initComponents();
/*    */     
/*    */ 
/* 26 */     setLayout(new BorderLayout());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private void initComponents()
/*    */   {
/* 42 */     setDefaultCloseOperation(3);
/* 43 */     getContentPane().setLayout(new AbsoluteLayout());
/*    */     
/* 45 */     pack();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void main(String[] args)
/*    */   {
/*    */     try
/*    */     {
/* 58 */       for (UIManager.LookAndFeelInfo info : ) {
/* 59 */         if ("Nimbus".equals(info.getName())) {
/* 60 */           UIManager.setLookAndFeel(info.getClassName());
/* 61 */           break;
/*    */         }
/*    */       }
/*    */     } catch (ClassNotFoundException ex) {
/* 65 */       Logger.getLogger(BaseFrame.class.getName()).log(Level.SEVERE, null, ex);
/*    */     } catch (InstantiationException ex) {
/* 67 */       Logger.getLogger(BaseFrame.class.getName()).log(Level.SEVERE, null, ex);
/*    */     } catch (IllegalAccessException ex) {
/* 69 */       Logger.getLogger(BaseFrame.class.getName()).log(Level.SEVERE, null, ex);
/*    */     } catch (UnsupportedLookAndFeelException ex) {
/* 71 */       Logger.getLogger(BaseFrame.class.getName()).log(Level.SEVERE, null, ex);
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 76 */     EventQueue.invokeLater(new Runnable() {
/*    */       public void run() {
/* 78 */         new BaseFrame().setVisible(true);
/*    */       }
/*    */     });
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ui/BaseFrame.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */