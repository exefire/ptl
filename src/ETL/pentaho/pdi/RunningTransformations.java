/*     */ package ETL.pentaho.pdi;
/*     */ 
/*     */ import Logging.LoggingManager;
/*     */ import java.io.PrintStream;
/*     */ import java.util.HashMap;
/*     */ import org.pentaho.di.core.KettleEnvironment;
/*     */ import org.pentaho.di.core.Result;
/*     */ import org.pentaho.di.core.exception.KettleException;
/*     */ import org.pentaho.di.core.logging.LogLevel;
/*     */ import org.pentaho.di.repository.Repository;
/*     */ import org.pentaho.di.trans.Trans;
/*     */ import org.pentaho.di.trans.TransMeta;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RunningTransformations
/*     */ {
/*     */   private static RunningTransformations instance;
/*     */   
/*     */   public static RunningTransformations getInstance()
/*     */   {
/*  28 */     if (instance == null) {
/*  29 */       instance = new RunningTransformations();
/*     */     }
/*  31 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected RunningTransformations()
/*     */   {
/*     */     try
/*     */     {
/*  41 */       KettleEnvironment.init();
/*     */     } catch (KettleException e) {
/*  43 */       e.printStackTrace();
/*  44 */       LoggingManager.getInstance().log_error(e);
/*     */       
/*  46 */       return;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public synchronized Trans runTransformationFromFileSystem(String transfile, HashMap<String, String> parametros)
/*     */   {
/*     */     try
/*     */     {
/*  71 */       System.out.println("***************************************************************************************");
/*  72 */       LoggingManager.getInstance().log("Attempting to run transformation " + transfile + " from file system");
/*  73 */       System.out.println("***************************************************************************************\n");
/*     */       
/*     */ 
/*  76 */       TransMeta transMeta = new TransMeta(transfile, (Repository)null);
/*     */       
/*     */ 
/*     */ 
/*  80 */       System.out.println("Attempting to read and set named parameters");
/*     */       
/*  82 */       for (String key : parametros.keySet())
/*     */       {
/*  84 */         transMeta.setParameterValue(key, (String)parametros.get(key));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  91 */       Trans transformation = new Trans(transMeta);
/*     */       
/*     */ 
/*  94 */       transformation.setLogLevel(LogLevel.ERROR);
/*     */       
/*  96 */       System.out.println("\nStarting transformation");
/*     */       
/*     */ 
/*  99 */       transformation.execute(new String[0]);
/*     */       
/*     */ 
/*     */ 
/* 103 */       transformation.waitUntilFinished();
/*     */       
/*     */ 
/* 106 */       Result result = transformation.getResult();
/*     */       
/*     */ 
/* 109 */       String outcome = "\nTrans " + transfile + " executed " + (result.getNrErrors() == 0L ? "successfully" : new StringBuilder().append("with ").append(result.getNrErrors()).append(" errors").toString());
/* 110 */       System.out.println(outcome);
/*     */       
/* 112 */       return transformation;
/*     */     }
/*     */     catch (Exception e) {
/* 115 */       LoggingManager.getInstance().log_error(e);
/*     */       
/*     */ 
/* 118 */       e.printStackTrace(); }
/* 119 */     return null;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ETL/pentaho/pdi/RunningTransformations.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */