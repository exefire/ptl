/*     */ package ETL.pentaho.pdi;
/*     */ 
/*     */ import Configuration.ConfigurationManager;
/*     */ import Database.DataSourceController;
/*     */ import java.util.HashMap;
/*     */ import java.util.Properties;
/*     */ import org.pentaho.di.trans.Trans;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EtlController
/*     */ {
/*  20 */   private static EtlController instance = null;
/*     */   
/*     */ 
/*     */ 
/*     */   public static EtlController getInstance()
/*     */   {
/*  26 */     if (instance == null) {
/*  27 */       instance = new EtlController();
/*     */     }
/*     */     
/*  30 */     return instance;
/*     */   }
/*     */   
/*     */   public boolean cargaOla(String ola)
/*     */   {
/*  35 */     RunningTransformations rt = RunningTransformations.getInstance();
/*  36 */     HashMap<String, String> parametros = new HashMap();
/*  37 */     String path = ConfigurationManager.getInstance().getProperty("rutaArchivo");
/*     */     
/*     */ 
/*  40 */     parametros.put("ola", ola);
/*  41 */     parametros.put("file_path", path);
/*     */     
/*  43 */     parametros.put("HOST_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("host"));
/*  44 */     parametros.put("DB_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("db"));
/*  45 */     parametros.put("PORT_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("port"));
/*  46 */     parametros.put("USER_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("user"));
/*  47 */     parametros.put("PASSWORD_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("pass"));
/*     */     
/*  49 */     parametros.put("HOST_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_host"));
/*  50 */     parametros.put("DB_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_db"));
/*  51 */     parametros.put("PORT_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_port"));
/*  52 */     parametros.put("USER_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_user"));
/*  53 */     parametros.put("PASSWORD_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_pass"));
/*     */     
/*  55 */     Trans trans = rt.runTransformationFromFileSystem("traspaso.ktr", parametros);
/*  56 */     while (!trans.isFinished()) {}
/*     */     
/*  58 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean cargaDestinosNuevos(String ola)
/*     */   {
/*  64 */     RunningTransformations rt = RunningTransformations.getInstance();
/*  65 */     HashMap<String, String> parametros = new HashMap();
/*  66 */     String path = ConfigurationManager.getInstance().getProperty("rutaArchivo");
/*     */     
/*  68 */     parametros.put("ola", ola);
/*  69 */     parametros.put("file_path", path);
/*     */     
/*  71 */     parametros.put("HOST_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("host"));
/*  72 */     parametros.put("DB_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("db"));
/*  73 */     parametros.put("PORT_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("port"));
/*  74 */     parametros.put("USER_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("user"));
/*  75 */     parametros.put("PASSWORD_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("pass"));
/*     */     
/*  77 */     parametros.put("HOST_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_host"));
/*  78 */     parametros.put("DB_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_db"));
/*  79 */     parametros.put("PORT_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_port"));
/*  80 */     parametros.put("USER_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_user"));
/*  81 */     parametros.put("PASSWORD_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_pass"));
/*     */     
/*  83 */     Trans trans = rt.runTransformationFromFileSystem("destinos.ktr", parametros);
/*  84 */     while (!trans.isFinished()) {}
/*     */     
/*  86 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean cargaBulto(String codigo, String ola)
/*     */   {
/*  92 */     String path = ConfigurationManager.getInstance().getProperty("rutaArchivo");
/*  93 */     RunningTransformations rt = RunningTransformations.getInstance();
/*  94 */     HashMap<String, String> parametros = new HashMap();
/*     */     
/*  96 */     parametros.put("codigobulto", codigo);
/*  97 */     parametros.put("ola", ola);
/*  98 */     parametros.put("file_path", path);
/*     */     
/* 100 */     parametros.put("HOST_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("host"));
/* 101 */     parametros.put("DB_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("db"));
/* 102 */     parametros.put("PORT_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("port"));
/* 103 */     parametros.put("USER_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("user"));
/* 104 */     parametros.put("PASSWORD_LOCAL", DataSourceController.getInstance().getDbConfig().getProperty("pass"));
/*     */     
/* 106 */     parametros.put("HOST_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_host"));
/* 107 */     parametros.put("DB_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_db"));
/* 108 */     parametros.put("PORT_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_port"));
/* 109 */     parametros.put("USER_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_user"));
/* 110 */     parametros.put("PASSWORD_REMOTE", DataSourceController.getInstance().getDbConfig().getProperty("remote_pass"));
/*     */     
/* 112 */     Trans trans = rt.runTransformationFromFileSystem("articulosBulto.ktr", parametros);
/*     */     
/* 114 */     while (!trans.isFinished()) {}
/*     */     
/* 116 */     return true;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ETL/pentaho/pdi/EtlController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */