/*     */ package ETL.pentaho.pdi;
/*     */ 
/*     */ import java.io.PrintStream;
/*     */ import org.pentaho.di.core.KettleEnvironment;
/*     */ import org.pentaho.di.core.Result;
/*     */ import org.pentaho.di.core.exception.KettleException;
/*     */ import org.pentaho.di.core.logging.LogLevel;
/*     */ import org.pentaho.di.job.Job;
/*     */ import org.pentaho.di.job.JobMeta;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RunningJobs
/*     */ {
/*     */   public RunningJobs()
/*     */   {
/*     */     try
/*     */     {
/*  22 */       KettleEnvironment.init();
/*     */     } catch (KettleException e) {
/*  24 */       e.printStackTrace();
/*  25 */       return;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void administrarPobreza(String npicking)
/*     */   {
/*  34 */     Job job = runJobFromFileSystem("administra_pobreza.kjb", "npicking", npicking);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  43 */     System.out.println("************************************************************************************************");
/*  44 */     System.out.println("LOG REPORT: Job generated the following log lines:\n");
/*     */     
/*  46 */     System.out.println("END OF LOG REPORT");
/*  47 */     System.out.println("************************************************************************************************");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Job runJobFromFileSystem(String filename, String parametro, String valorParametro)
/*     */   {
/*     */     try
/*     */     {
/*  72 */       System.out.println("***************************************************************************************");
/*  73 */       System.out.println("Attempting to run Job " + filename + " from file system");
/*  74 */       System.out.println("***************************************************************************************\n");
/*     */       
/*     */ 
/*     */ 
/*  78 */       JobMeta jobMeta = new JobMeta(filename, null);
/*     */       
/*     */ 
/*     */ 
/*  82 */       System.out.println("Attempting to read and set named parameters");
/*     */       
/*     */ 
/*     */ 
/*  86 */       jobMeta.setParameterValue(parametro, valorParametro);
/*     */       
/*     */ 
/*  89 */       Job job = new Job(null, jobMeta);
/*  90 */       job.setLogLevel(LogLevel.ERROR);
/*  91 */       System.out.println("\nStarting JOB");
/*     */       
/*  93 */       job.start();
/*     */       
/*  95 */       job.waitUntilFinished();
/*     */       
/*  97 */       if (job.getErrors() != 0) {
/*  98 */         System.out.println("Error encountered!");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 105 */       Result result = job.getResult();
/*     */       
/*     */ 
/* 108 */       String outcome = "\nTrans " + filename + " executed " + (result.getNrErrors() == 0L ? "successfully" : new StringBuilder().append("with ").append(result.getNrErrors()).append(" errors").toString());
/* 109 */       System.out.println(outcome);
/*     */       
/* 111 */       return job;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 115 */       e.printStackTrace(); }
/* 116 */     return null;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/ETL/pentaho/pdi/RunningJobs.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */