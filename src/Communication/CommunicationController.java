/*     */ package Communication;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CommunicationController
/*     */ {
/*  19 */   private static volatile CommunicationController instance = null;
/*  20 */   private List _listeners = new ArrayList();
/*     */   
/*     */   private CommunicationClient client;
/*     */   
/*     */ 
/*     */   public static CommunicationController getInstance()
/*     */   {
/*  27 */     if (instance == null) {
/*  28 */       synchronized (CommunicationController.class) {
/*  29 */         if (instance == null) {
/*  30 */           instance = new CommunicationController();
/*     */         }
/*     */       }
/*     */     }
/*  34 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public synchronized void addEventListener(CommunicationListener listener)
/*     */   {
/*  40 */     this.client.addListener(listener);
/*     */   }
/*     */   
/*     */   public synchronized void removeEventListener(CommunicationListener listener)
/*     */   {
/*  45 */     this.client.removeListener(listener);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void init()
/*     */   {
/*  60 */     this.client = CommunicationClientFactory.getClient("atop");
/*  61 */     this.client.connect();
/*     */   }
/*     */   
/*     */   public boolean conectado()
/*     */   {
/*  66 */     return this.client.isConnected();
/*     */   }
/*     */   
/*     */   public void prenderLuz(Integer posicion, Integer numero) {
/*  70 */     if ((posicion != null) && (numero == null)) {
/*  71 */       this.client.prendeluz(posicion.intValue(), "blue");
/*     */     }
/*     */   }
/*     */   
/*     */   public void prenderLuz(Integer posicion, Integer numero, String color) {
/*  76 */     if ((posicion != null) && (numero == null)) {
/*  77 */       this.client.prendeluz(posicion.intValue(), color);
/*     */     }
/*     */   }
/*     */   
/*     */   public void prenderLuzConNumero(Integer posicion, Integer numero)
/*     */   {
/*  83 */     if ((posicion != null) && (numero != null)) {
/*  84 */       this.client.numero(numero.intValue(), posicion.intValue());
/*     */     }
/*     */   }
/*     */   
/*     */   public void prenderLuces(String color)
/*     */   {
/*  90 */     this.client.prenderluces(color);
/*     */   }
/*     */   
/*     */   public void apagarTodasLasLuces() {
/*  94 */     this.client.apagarluces();
/*     */   }
/*     */   
/*     */ 
/*     */   public void prenderNluces(Integer n, String color)
/*     */   {
/* 100 */     this.client.prenderNluces(n, color);
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/CommunicationController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */