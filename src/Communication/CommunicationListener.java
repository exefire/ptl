package Communication;

public abstract interface CommunicationListener
{
  public abstract void manageIncomingData(IncomingDataEvent paramIncomingDataEvent);
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/CommunicationListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */