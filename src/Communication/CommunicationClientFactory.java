/*    */ package Communication;
/*    */ 
/*    */ import Communication.Clients.Atop.PtlAtopClient;
/*    */ import Communication.Clients.Koke.kokeClient;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CommunicationClientFactory
/*    */ {
/*    */   public static CommunicationClient getClient(String criteria)
/*    */   {
/* 18 */     if (criteria.equals("atop"))
/* 19 */       return PtlAtopClient.getInstance();
/* 20 */     if (criteria.equals("koke")) {
/* 21 */       return kokeClient.getInstance();
/*    */     }
/* 23 */     return null;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/CommunicationClientFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */