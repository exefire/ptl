package Communication;

public abstract interface CommunicationClient
{
  public abstract boolean connect();
  
  public abstract boolean disconnect();
  
  public abstract CommunicationStatus getStatus();
  
  public abstract boolean isConnected();
  
  public abstract void addListener(CommunicationListener paramCommunicationListener);
  
  public abstract void removeListener(CommunicationListener paramCommunicationListener);
  
  public abstract void apagarluces();
  
  public abstract void prendeluz(int paramInt, String paramString);
  
  public abstract void prenderluces(String paramString);
  
  public abstract void numero(int paramInt1, int paramInt2);
  
  public abstract void prenderNluces(Integer paramInteger, String paramString);
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/CommunicationClient.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */