/*     */ package Communication.Clients.Koke;
/*     */ 
/*     */ import Communication.CommunicationClient;
/*     */ import Communication.CommunicationListener;
/*     */ import Communication.CommunicationStatus;
/*     */ import Communication.IncomingDataEvent;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class kokeClient
/*     */   implements CommunicationClient
/*     */ {
/*     */   private Thread t;
/*     */   private UiKokeCLient ui;
/*  23 */   private List _listeners = new ArrayList();
/*     */   
/*     */ 
/*  26 */   private static kokeClient instance = null;
/*     */   
/*     */   protected kokeClient() {
/*  29 */     this.ui = new UiKokeCLient();
/*     */   }
/*     */   
/*     */   public static kokeClient getInstance() {
/*  33 */     if (instance == null) {
/*  34 */       synchronized (kokeClient.class) {
/*  35 */         if (instance == null) {
/*  36 */           instance = new kokeClient();
/*     */         }
/*     */       }
/*     */     }
/*  40 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean connect()
/*     */   {
/*  48 */     this.t = new Thread(this.ui);
/*  49 */     this.t.start();
/*  50 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public boolean disconnect()
/*     */   {
/*  57 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   public CommunicationStatus getStatus()
/*     */   {
/*  63 */     return CommunicationStatus.CONNECTED;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean isConnected()
/*     */   {
/*  69 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   public void addListener(CommunicationListener listener)
/*     */   {
/*  75 */     this._listeners.add(listener);
/*     */   }
/*     */   
/*     */ 
/*     */   public void removeListener(CommunicationListener listener)
/*     */   {
/*  81 */     this._listeners.remove(listener);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void apagarluces()
/*     */   {
/*  88 */     this.ui.apagarTodo();
/*     */   }
/*     */   
/*     */   public void prendeluz(int luz, String color)
/*     */   {
/*  93 */     this.ui.prendeLuz(luz);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void prenderluces(String color)
/*     */   {
/* 100 */     this.ui.prendeLuces();
/*     */   }
/*     */   
/*     */   public void numero(int numero, int luz)
/*     */   {
/* 105 */     this.ui.numero(luz, numero);
/*     */   }
/*     */   
/*     */ 
/*     */   public void prenderNluces(Integer n, String color)
/*     */   {
/* 111 */     for (int i = 1; i <= n.intValue(); i++) {
/* 112 */       this.ui.prendeLuz(i);
/*     */     }
/*     */   }
/*     */   
/*     */   public synchronized void fireEvent(int posicion, String data)
/*     */   {
/* 118 */     IncomingDataEvent event = new IncomingDataEvent(this, Integer.valueOf(posicion), data);
/* 119 */     Iterator i = this._listeners.iterator();
/* 120 */     while (i.hasNext()) {
/* 121 */       ((CommunicationListener)i.next()).manageIncomingData(event);
/*     */     }
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/Clients/Koke/kokeClient.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */