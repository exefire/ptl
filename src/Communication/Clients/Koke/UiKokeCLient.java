/*     */ package Communication.Clients.Koke;
/*     */ 
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JSpinner;
/*     */ 
/*     */ public class UiKokeCLient extends javax.swing.JFrame implements Runnable
/*     */ {
/*     */   private JSpinner numero1;
/*     */   private JSpinner numero2;
/*     */   private JSpinner numero3;
/*     */   private JSpinner numero4;
/*     */   private JButton ok1;
/*     */   private JButton ok2;
/*     */   private JButton ok3;
/*     */   private JButton ok4;
/*     */   
/*     */   public UiKokeCLient()
/*     */   {
/*  20 */     initComponents();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  32 */     this.ok1 = new JButton();
/*  33 */     this.ok2 = new JButton();
/*  34 */     this.ok3 = new JButton();
/*  35 */     this.ok4 = new JButton();
/*  36 */     this.numero2 = new JSpinner();
/*  37 */     this.numero1 = new JSpinner();
/*  38 */     this.numero3 = new JSpinner();
/*  39 */     this.numero4 = new JSpinner();
/*     */     
/*  41 */     setDefaultCloseOperation(3);
/*     */     
/*  43 */     this.ok1.setText("001");
/*  44 */     this.ok1.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  46 */         UiKokeCLient.this.ok1ActionPerformed(evt);
/*     */       }
/*     */       
/*  49 */     });
/*  50 */     this.ok2.setText("002");
/*  51 */     this.ok2.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  53 */         UiKokeCLient.this.ok2ActionPerformed(evt);
/*     */       }
/*     */       
/*  56 */     });
/*  57 */     this.ok3.setText("003");
/*  58 */     this.ok3.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  60 */         UiKokeCLient.this.ok3ActionPerformed(evt);
/*     */       }
/*     */       
/*  63 */     });
/*  64 */     this.ok4.setText("004");
/*  65 */     this.ok4.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/*  67 */         UiKokeCLient.this.ok4ActionPerformed(evt);
/*     */       }
/*     */       
/*  70 */     });
/*  71 */     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
/*  72 */     getContentPane().setLayout(layout);
/*  73 */     layout.setHorizontalGroup(layout
/*  74 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  75 */       .addGroup(layout.createSequentialGroup()
/*  76 */       .addGap(32, 32, 32)
/*  77 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
/*  78 */       .addGroup(layout.createSequentialGroup()
/*  79 */       .addComponent(this.numero1)
/*  80 */       .addGap(18, 18, 18)
/*  81 */       .addComponent(this.numero2, -2, 53, -2))
/*  82 */       .addGroup(layout.createSequentialGroup()
/*  83 */       .addComponent(this.ok1)
/*  84 */       .addGap(18, 18, 18)
/*  85 */       .addComponent(this.ok2)))
/*  86 */       .addGap(18, 18, 18)
/*  87 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/*  88 */       .addGroup(layout.createSequentialGroup()
/*  89 */       .addComponent(this.ok3)
/*  90 */       .addGap(18, 18, 18)
/*  91 */       .addComponent(this.ok4))
/*  92 */       .addGroup(layout.createSequentialGroup()
/*  93 */       .addComponent(this.numero3, -2, 53, -2)
/*  94 */       .addGap(18, 18, 18)
/*  95 */       .addComponent(this.numero4, -2, 53, -2)))
/*  96 */       .addContainerGap(31, 32767)));
/*     */     
/*  98 */     layout.setVerticalGroup(layout
/*  99 */       .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
/* 100 */       .addGroup(layout.createSequentialGroup()
/* 101 */       .addGap(17, 17, 17)
/* 102 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 103 */       .addComponent(this.numero2, -2, -1, -2)
/* 104 */       .addComponent(this.numero1, -2, -1, -2)
/* 105 */       .addComponent(this.numero3, -2, -1, -2)
/* 106 */       .addComponent(this.numero4, -2, -1, -2))
/* 107 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
/* 108 */       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
/* 109 */       .addComponent(this.ok1)
/* 110 */       .addComponent(this.ok2)
/* 111 */       .addComponent(this.ok3)
/* 112 */       .addComponent(this.ok4))
/* 113 */       .addContainerGap(29, 32767)));
/*     */     
/*     */ 
/* 116 */     pack();
/*     */   }
/*     */   
/*     */   private void ok1ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 121 */     this.ok1.setBackground(java.awt.Color.GRAY);
/* 122 */     this.ok1.setEnabled(false);
/* 123 */     this.numero1.setEnabled(false);
/* 124 */     kokeClient.getInstance().fireEvent(1, ((Integer)this.numero1.getValue()).toString());
/*     */     
/* 126 */     this.numero1.setValue(Integer.valueOf(0));
/*     */   }
/*     */   
/*     */ 
/*     */   private void ok2ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 132 */     this.ok2.setBackground(java.awt.Color.GRAY);
/* 133 */     this.ok2.setEnabled(false);
/* 134 */     this.numero2.setEnabled(false);
/* 135 */     kokeClient.getInstance().fireEvent(2, ((Integer)this.numero2.getValue()).toString());
/*     */     
/* 137 */     this.numero2.setValue(Integer.valueOf(0));
/*     */   }
/*     */   
/*     */ 
/*     */   private void ok3ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 143 */     this.ok3.setBackground(java.awt.Color.GRAY);
/* 144 */     this.ok3.setEnabled(false);
/* 145 */     this.numero3.setEnabled(false);
/* 146 */     kokeClient.getInstance().fireEvent(3, ((Integer)this.numero3.getValue()).toString());
/*     */     
/* 148 */     this.numero3.setValue(Integer.valueOf(0));
/*     */   }
/*     */   
/*     */   private void ok4ActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 153 */     this.ok4.setBackground(java.awt.Color.GRAY);
/* 154 */     this.ok4.setEnabled(false);
/* 155 */     this.numero4.setEnabled(false);
/* 156 */     kokeClient.getInstance().fireEvent(4, ((Integer)this.numero4.getValue()).toString());
/*     */     
/* 158 */     this.numero4.setValue(Integer.valueOf(0));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void main(String[] args)
/*     */   {
/*     */     try
/*     */     {
/* 172 */       for (javax.swing.UIManager.LookAndFeelInfo info : ) {
/* 173 */         if ("Nimbus".equals(info.getName())) {
/* 174 */           javax.swing.UIManager.setLookAndFeel(info.getClassName());
/* 175 */           break;
/*     */         }
/*     */       }
/*     */     } catch (ClassNotFoundException ex) {
/* 179 */       java.util.logging.Logger.getLogger(UiKokeCLient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
/*     */     } catch (InstantiationException ex) {
/* 181 */       java.util.logging.Logger.getLogger(UiKokeCLient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
/*     */     } catch (IllegalAccessException ex) {
/* 183 */       java.util.logging.Logger.getLogger(UiKokeCLient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
/*     */     } catch (javax.swing.UnsupportedLookAndFeelException ex) {
/* 185 */       java.util.logging.Logger.getLogger(UiKokeCLient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 190 */     java.awt.EventQueue.invokeLater(new Runnable() {
/*     */       public void run() {
/* 192 */         new UiKokeCLient().setVisible(true);
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void run()
/*     */   {
/* 210 */     setVisible(true);
/*     */   }
/*     */   
/*     */   public void bloqueatodo() {
/* 214 */     this.ok1.setEnabled(false);
/* 215 */     this.ok2.setEnabled(false);
/* 216 */     this.ok3.setEnabled(false);
/* 217 */     this.ok4.setEnabled(false);
/*     */     
/* 219 */     this.ok1.setBackground(java.awt.Color.GRAY);
/* 220 */     this.ok2.setBackground(java.awt.Color.GRAY);
/* 221 */     this.ok3.setBackground(java.awt.Color.GRAY);
/* 222 */     this.ok4.setBackground(java.awt.Color.GRAY);
/*     */   }
/*     */   
/*     */   public void apagaNumeros() {
/* 226 */     this.numero1.setValue(Integer.valueOf(0));
/* 227 */     this.numero1.setEnabled(false);
/* 228 */     this.numero2.setValue(Integer.valueOf(0));
/* 229 */     this.numero2.setEnabled(false);
/* 230 */     this.numero3.setValue(Integer.valueOf(0));
/* 231 */     this.numero3.setEnabled(false);
/* 232 */     this.numero4.setValue(Integer.valueOf(0));
/* 233 */     this.numero4.setEnabled(false);
/*     */   }
/*     */   
/*     */   public void numero(int pos, int numero) {
/* 237 */     if (pos == 1) {
/* 238 */       this.numero1.setValue(Integer.valueOf(numero));
/* 239 */       this.numero1.setEnabled(true);
/* 240 */       this.ok1.setEnabled(true);
/* 241 */       this.ok1.setBackground(java.awt.Color.RED);
/* 242 */     } else if (pos == 2) {
/* 243 */       this.numero2.setValue(Integer.valueOf(numero));
/* 244 */       this.numero2.setEnabled(true);
/* 245 */       this.ok2.setBackground(java.awt.Color.RED);
/* 246 */       this.ok2.setEnabled(true);
/* 247 */     } else if (pos == 3) {
/* 248 */       this.numero3.setValue(Integer.valueOf(numero));
/* 249 */       this.numero3.setEnabled(true);
/* 250 */       this.ok3.setBackground(java.awt.Color.RED);
/* 251 */       this.ok3.setEnabled(true);
/* 252 */     } else if (pos == 4) {
/* 253 */       this.numero4.setValue(Integer.valueOf(numero));
/* 254 */       this.numero4.setEnabled(true);
/* 255 */       this.ok4.setBackground(java.awt.Color.RED);
/* 256 */       this.ok4.setEnabled(true);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   void prendeLuces()
/*     */   {
/* 263 */     this.ok1.setBackground(java.awt.Color.RED);
/* 264 */     this.ok1.setEnabled(true);
/*     */     
/* 266 */     this.ok2.setBackground(java.awt.Color.RED);
/* 267 */     this.ok2.setEnabled(true);
/*     */     
/* 269 */     this.ok3.setBackground(java.awt.Color.RED);
/* 270 */     this.ok3.setEnabled(true);
/*     */     
/* 272 */     this.ok4.setBackground(java.awt.Color.RED);
/* 273 */     this.ok4.setEnabled(true);
/*     */   }
/*     */   
/*     */ 
/*     */   void prendeLuz(int luz)
/*     */   {
/* 279 */     if (luz == 1) {
/* 280 */       this.ok1.setBackground(java.awt.Color.RED);
/* 281 */       this.ok1.setEnabled(true);
/* 282 */     } else if (luz == 2) {
/* 283 */       this.ok2.setBackground(java.awt.Color.RED);
/* 284 */       this.ok2.setEnabled(true);
/* 285 */     } else if (luz == 3) {
/* 286 */       this.ok3.setBackground(java.awt.Color.RED);
/* 287 */       this.ok3.setEnabled(true);
/* 288 */     } else if (luz == 4) {
/* 289 */       this.ok4.setBackground(java.awt.Color.RED);
/* 290 */       this.ok4.setEnabled(true);
/*     */     }
/*     */   }
/*     */   
/*     */   void apagarTodo() {
/* 295 */     bloqueatodo();
/* 296 */     apagaNumeros();
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/Clients/Koke/UiKokeCLient.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */