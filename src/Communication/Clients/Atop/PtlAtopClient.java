/*     */ package Communication.Clients.Atop;
/*     */ 
/*     */ import Communication.CommunicationClient;
/*     */ import Communication.CommunicationListener;
/*     */ import Communication.CommunicationStatus;
/*     */ import Configuration.ConfigurationManager;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.net.InetAddress;
/*     */ import java.net.Socket;
/*     */ import java.net.UnknownHostException;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PtlAtopClient
/*     */   implements CommunicationClient
/*     */ {
/*  31 */   public static final String[] PICK = { "\\0F\\00\\60\\00\\00\\00\\00", "\\00\\00\\00" };
/*  32 */   public static final String[] RED = { "\\0A\\00\\60\\00\\00\\00\\1F", "\\00\\00" };
/*  33 */   public static final String[] BLUE = { "\\0A\\00\\60\\00\\00\\00\\1F", "\\00\\03" };
/*  34 */   public static final String[] GREEN = { "\\0A\\00\\60\\00\\00\\00\\1F", "\\00\\01" };
/*  35 */   public static final String[] CYAN = { "\\0A\\00\\60\\00\\00\\00\\1F", "\\00\\05" };
/*  36 */   public static final String[] TURN_ON = { "\\08\\00\\60\\00\\00\\00\\02", "" };
/*  37 */   public static final String[] TURN_OFF = { "\\08\\00\\60\\00\\00\\00\\01", "" };
/*  38 */   public static final String[] BLINK = { "\\08\\00\\60\\00\\00\\00\\02", "" };
/*     */   
/*     */ 
/*  41 */   private Socket socket = null;
/*     */   
/*     */   private int luces;
/*     */   private int barras;
/*     */   private String ip;
/*  46 */   private int puerto = 4660;
/*     */   
/*  48 */   private boolean bConnected = false;
/*     */   
/*     */   private DataReceiver receiver;
/*     */   
/*     */   private DataSender sender;
/*     */   
/*     */   Thread tr;
/*  55 */   private static volatile PtlAtopClient instance = null;
/*     */   
/*     */   public static PtlAtopClient getInstance() {
/*  58 */     if (instance == null) {
/*  59 */       synchronized (PtlAtopClient.class) {
/*  60 */         if (instance == null) {
/*  61 */           instance = new PtlAtopClient();
/*     */         }
/*     */       }
/*     */     }
/*  65 */     return instance;
/*     */   }
/*     */   
/*     */   protected PtlAtopClient()
/*     */   {
/*  70 */     this.receiver = new DataReceiver();
/*  71 */     this.sender = new DataSender();
/*  72 */     this.tr = new Thread(this.receiver);
/*     */     
/*     */ 
/*     */ 
/*  76 */     this.ip = ConfigurationManager.getInstance().getProperty("ip");
/*     */     
/*     */ 
/*  79 */     this.luces = Integer.parseInt(ConfigurationManager.getInstance().getProperty("luces"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void addListener(CommunicationListener listener)
/*     */   {
/*  86 */     this.receiver.addEventListener(listener);
/*     */   }
/*     */   
/*     */   public void removeListener(CommunicationListener listener)
/*     */   {
/*  91 */     this.receiver.removeEventListener(listener);
/*     */   }
/*     */   
/*     */   public boolean disconnect()
/*     */   {
/*  96 */     System.out.println("Close");
/*     */     try
/*     */     {
/*  99 */       this.sender.disconnect();
/* 100 */       this.receiver.disconnect();
/*     */       
/* 102 */       if (this.socket != null) {
/* 103 */         this.socket.close();
/* 104 */         this.socket = null;
/*     */       }
/* 106 */       return true;
/*     */     }
/*     */     catch (IOException e) {
/* 109 */       e.printStackTrace(); }
/* 110 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean connect()
/*     */   {
/*     */     try
/*     */     {
/* 120 */       this.socket = new Socket(InetAddress.getByName(this.ip), this.puerto);
/* 121 */       this.sender.init(this.socket);
/* 122 */       this.receiver.init(this.socket);
/*     */       
/* 124 */       this.bConnected = true;
/* 125 */       this.tr.start();
/*     */     }
/*     */     catch (UnknownHostException e)
/*     */     {
/* 129 */       e.printStackTrace();
/* 130 */       this.bConnected = false;
/*     */     }
/*     */     catch (IOException e)
/*     */     {
/* 134 */       e.printStackTrace();
/* 135 */       this.bConnected = false;
/*     */     }
/* 137 */     return this.bConnected;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean isConnected()
/*     */   {
/* 143 */     return this.bConnected;
/*     */   }
/*     */   
/*     */   public CommunicationStatus getStatus()
/*     */   {
/* 148 */     throw new UnsupportedOperationException("Not supported yet.");
/*     */   }
/*     */   
/*     */ 
/*     */   public void prenderluces(String color)
/*     */   {
/* 154 */     String[] col = null;
/* 155 */     switch (color) {
/*     */     case "blue": 
/* 157 */       col = BLUE;
/* 158 */       break;
/*     */     case "red": 
/* 160 */       col = RED;
/* 161 */       break;
/*     */     case "green": 
/* 163 */       col = GREEN;
/* 164 */       break;
/*     */     case "cyan": 
/* 166 */       col = CYAN;
/*     */     }
/*     */     
/* 169 */     for (int i = 1; i < this.luces + 1; i++)
/*     */     {
/* 171 */       ledOn(1, 1, 1, i, col);
/*     */     }
/*     */   }
/*     */   
/*     */   public void prenderNluces(Integer n, String color)
/*     */   {
/* 177 */     String[] col = null;
/* 178 */     switch (color) {
/*     */     case "blue": 
/* 180 */       col = BLUE;
/* 181 */       break;
/*     */     case "red": 
/* 183 */       col = RED;
/* 184 */       break;
/*     */     case "green": 
/* 186 */       col = GREEN;
/* 187 */       break;
/*     */     case "cyan": 
/* 189 */       col = CYAN;
/*     */     }
/*     */     
/* 192 */     for (int i = 1; i < n.intValue() + 1; i++)
/*     */     {
/* 194 */       ledOn(1, 1, 1, i, col);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void prendeluz(int luz, String color)
/*     */   {
/* 201 */     String[] col = null;
/* 202 */     switch (color) {
/*     */     case "blue": 
/* 204 */       col = BLUE;
/* 205 */       break;
/*     */     case "red": 
/* 207 */       col = RED;
/* 208 */       break;
/*     */     case "cyan": 
/* 210 */       col = CYAN;
/* 211 */       break;
/*     */     case "green": 
/* 213 */       col = GREEN;
/*     */     }
/*     */     
/* 216 */     ledOn(1, 1, 1, luz, col);
/*     */   }
/*     */   
/*     */   public void numero(int numero, int luz) {
/* 220 */     showNumber(1, 1, 1, luz, numero, RED);
/*     */   }
/*     */   
/* 223 */   public void parpadealuz(int luz) { parpadear(1, 1, 1, luz, RED); }
/*     */   
/*     */   public void parpadear(int controlador, int estacion, int barra, int luz, String[] color) {
/* 226 */     String setColor = color[0] + getPos(estacion, barra, luz) + color[1];
/* 227 */     String turnOn = BLINK[0] + getPos(estacion, barra, luz) + BLINK[1];
/* 228 */     this.sender.executeCommand(controlador, setColor);
/* 229 */     this.sender.executeCommand(controlador, turnOn);
/*     */   }
/*     */   
/*     */   public void ledOn(int controlador, int estacion, int barra, int luz, String[] color)
/*     */   {
/* 234 */     String setColor = color[0] + getPos(estacion, barra, luz) + color[1];
/* 235 */     String turnOn = TURN_ON[0] + getPos(estacion, barra, luz) + TURN_ON[1];
/* 236 */     this.sender.executeCommand(controlador, setColor);
/* 237 */     this.sender.executeCommand(controlador, turnOn);
/* 238 */     System.out.println(turnOn);
/*     */   }
/*     */   
/*     */   public void ledOff(int controlador, int estacion, int barra, int luz) {
/* 242 */     String turnOff = TURN_OFF[0] + getPos(estacion, barra, luz) + TURN_OFF[1];
/* 243 */     this.sender.executeCommand(controlador, turnOff);
/*     */   }
/*     */   
/*     */   public void apagarluces()
/*     */   {
/* 248 */     for (int i = 1; i < this.luces + 1; i++) {
/* 249 */       ledOff(1, 1, 1, i);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private String int2hex(int numero)
/*     */   {
/* 256 */     String command = "\\";
/* 257 */     if (numero / 100 == 0) {
/* 258 */       command = command + "00\\";
/*     */     }
/* 260 */     if (numero / 10 == 0) {
/* 261 */       command = command + "00\\";
/*     */     }
/* 263 */     char[] number = String.valueOf(numero).toCharArray();
/* 264 */     for (int i = 0; i < number.length; i++) {
/* 265 */       command = command + (number[i] - '\022');
/* 266 */       command = command + "\\";
/*     */     }
/* 268 */     command = command + "00";
/* 269 */     return command;
/*     */   }
/*     */   
/*     */   private String getPos(int estacion, int barra, int luz) {
/* 273 */     int controlador = (estacion - 1) / 4 + 1;
/* 274 */     int pos = (barra - 1) * this.luces + luz + (estacion - (controlador - 1) * 4 - 1) * (this.barras * this.luces);
/* 275 */     String hex = Integer.toHexString(pos).toUpperCase();
/* 276 */     if (hex.length() == 1) {
/* 277 */       hex = "0" + hex;
/*     */     }
/* 279 */     return "\\" + hex;
/*     */   }
/*     */   
/*     */   public void showNumber(int controlador, int estacion, int barra, int luz, int numero, String[] color) {
/* 283 */     String setColor = color[0] + getPos(estacion, barra, luz) + color[1];
/* 284 */     String putNumber = PICK[0] + getPos(estacion, barra, luz) + PICK[1] + int2hex(numero);
/* 285 */     this.sender.executeCommand(controlador, setColor);
/* 286 */     this.sender.executeCommand(controlador, putNumber);
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/Clients/Atop/PtlAtopClient.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */