/*    */ package Communication.Clients.Atop;
/*    */ 
/*    */ import java.io.BufferedOutputStream;
/*    */ import java.io.DataOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.net.Socket;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DataSender
/*    */ {
/*    */   DataOutputStream bw;
/*    */   PtlAtopClient ptl;
/*    */   
/*    */   void init(Socket socket)
/*    */     throws IOException
/*    */   {
/* 22 */     this.bw = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
/*    */   }
/*    */   
/*    */   void disconnect() throws IOException {
/* 26 */     this.bw.close();
/* 27 */     this.bw = null;
/*    */   }
/*    */   
/*    */   public void executeCommand(int controlador, String command)
/*    */   {
/* 32 */     if (PtlAtopClient.getInstance().isConnected()) {
/* 33 */       byte[] ns = new byte['Ā'];
/* 34 */       char[] tmpChar = new char[1];
/*    */       
/* 36 */       StringBuilder s = new StringBuilder();
/*    */       
/* 38 */       String msg = command;
/* 39 */       int len = msg.length();
/* 40 */       int idx = 0;
/* 41 */       for (int i = 0; i < len; i++) {
/* 42 */         String tmpStr = msg.substring(i, i + 1);
/* 43 */         if (tmpStr.equals("\\")) {
/* 44 */           tmpStr = msg.substring(i + 1, i + 3);
/* 45 */           i += 2;
/* 46 */           ns[(idx++)] = ((byte)Integer.parseInt(tmpStr, 16));
/*    */         }
/*    */         else
/*    */         {
/* 50 */           tmpChar = tmpStr.toCharArray();
/* 51 */           ns[(idx++)] = ((byte)tmpChar[0]);
/*    */         }
/*    */       }
/*    */       
/*    */       try
/*    */       {
/* 57 */         this.bw.write(ns, 0, idx);
/* 58 */         this.bw.flush();
/*    */       }
/*    */       catch (IOException e)
/*    */       {
/* 62 */         e.printStackTrace();
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/Clients/Atop/DataSender.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */