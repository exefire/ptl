/*     */ package Communication.Clients.Atop;
/*     */ 
/*     */ import Communication.CommunicationListener;
/*     */ import Communication.IncomingDataEvent;
/*     */ import java.io.BufferedInputStream;
/*     */ import java.io.DataInputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.net.Socket;
/*     */ import java.net.SocketException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DataReceiver
/*     */   implements Runnable
/*     */ {
/*     */   DataInputStream br;
/*     */   PtlAtopClient client;
/*  27 */   private List _listeners = new ArrayList();
/*     */   
/*     */   void init(Socket socket) throws IOException {
/*  30 */     this.br = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
/*     */   }
/*     */   
/*     */   public void disconnect() throws IOException {
/*  34 */     this.br.close();
/*  35 */     this.br = null;
/*     */   }
/*     */   
/*     */   public synchronized void addEventListener(CommunicationListener listener) {
/*  39 */     this._listeners.add(listener);
/*     */   }
/*     */   
/*     */   public synchronized void removeEventListener(CommunicationListener listener) {
/*  43 */     this._listeners.remove(listener);
/*     */   }
/*     */   
/*     */ 
/*     */   private synchronized void fireEvent(int posicion, String data)
/*     */   {
/*  49 */     IncomingDataEvent event = new IncomingDataEvent(this, Integer.valueOf(posicion), data);
/*  50 */     Iterator i = this._listeners.iterator();
/*  51 */     while (i.hasNext()) {
/*  52 */       ((CommunicationListener)i.next()).manageIncomingData(event);
/*     */     }
/*     */   }
/*     */   
/*     */   public int hex2int(String[] message)
/*     */   {
/*  58 */     int number = 0;
/*     */     
/*  60 */     int a = Integer.parseInt(message[12]) - 30;
/*  61 */     int b = Integer.parseInt(message[13]) - 30;
/*  62 */     int c = Integer.parseInt(message[14]) - 30;
/*  63 */     if (a < 0) a = 0;
/*  64 */     if (b < 0) b = 0;
/*  65 */     if (c < 0) { c = 0;
/*     */     }
/*     */     
/*     */ 
/*  69 */     number = 100 * a + 10 * b + c;
/*  70 */     return number; }
/*     */   
/*  72 */   byte[] in_buf = new byte[15];
/*     */   int len_recv;
/*     */   String resp;
/*     */   String hs;
/*     */   String stmp;
/*     */   String hexresp;
/*     */   String[] hw;
/*     */   
/*     */   public void run() {
/*  81 */     try { while (PtlAtopClient.getInstance().isConnected())
/*     */       {
/*  83 */         this.len_recv = this.br.read(this.in_buf, 0, 15);
/*  84 */         this.resp = new String(this.in_buf, 0, this.in_buf.length);
/*     */         
/*  86 */         this.hs = "";
/*  87 */         this.stmp = "";
/*     */         
/*  89 */         for (int n = 0; n < this.in_buf.length; n++)
/*     */         {
/*  91 */           this.stmp = Integer.toHexString(this.in_buf[n] & 0xFF);
/*     */           
/*  93 */           if (this.stmp.length() == 1)
/*  94 */             this.hs = (this.hs + " " + "0" + this.stmp); else {
/*  95 */             this.hs = (this.hs + " " + this.stmp);
/*     */           }
/*     */         }
/*  98 */         this.hw = this.hs.split(" ");
/*  99 */         hex2int(this.hw);
/*     */         
/* 101 */         int data = hex2int(this.hw);
/*     */         
/*     */ 
/* 104 */         posicion(this.hs, Integer.valueOf(data));
/*     */       }
/*     */       
/*     */       return;
/*     */     }
/*     */     catch (SocketException se)
/*     */     {
/* 111 */       System.out.println("Exit");
/*     */     }
/*     */     catch (IOException ioe)
/*     */     {
/* 115 */       ioe.printStackTrace();
/*     */     }
/*     */     finally
/*     */     {
/* 119 */       if (this.br != null)
/*     */       {
/*     */         try
/*     */         {
/* 123 */           this.br.close();
/* 124 */           this.br = null;
/*     */         }
/*     */         catch (IOException e)
/*     */         {
/* 128 */           e.printStackTrace();
/*     */         } }
/*     */     }
/*     */   }
/*     */   
/*     */   private void posicion(String mensage, Integer data) {
/* 134 */     String[] d = mensage.split(" ");
/*     */     
/* 136 */     int pos = Integer.parseInt(d[8], 16);
/*     */     
/* 138 */     String dato = data.toString();
/* 139 */     fireEvent(pos, dato);
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/Clients/Atop/DataReceiver.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */