/*    */ package Communication;
/*    */ 
/*    */ import java.util.EventObject;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class IncomingDataEvent
/*    */   extends EventObject
/*    */ {
/*    */   private Integer _posicion;
/*    */   private String _data;
/*    */   
/*    */   public IncomingDataEvent(Object source, Integer posicion, String data)
/*    */   {
/* 19 */     super(source);
/* 20 */     setPoscion(posicion);
/* 21 */     setData(data);
/*    */   }
/*    */   
/*    */   public String getData() {
/* 25 */     return this._data;
/*    */   }
/*    */   
/*    */   private void setData(String d) {
/* 29 */     this._data = d;
/*    */   }
/*    */   
/*    */   public Integer getPoscion() {
/* 33 */     return this._posicion;
/*    */   }
/*    */   
/*    */   private void setPoscion(Integer p) {
/* 37 */     this._posicion = p;
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/IncomingDataEvent.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */