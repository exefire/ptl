package Communication;

public enum CommunicationStatus
{
  CONNECTED,  DISCONNECTED,  UNKNOWN;
  
  private CommunicationStatus() {}
}


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Communication/CommunicationStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */