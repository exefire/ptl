/*    */ package Utils;
/*    */ 
/*    */ import java.security.MessageDigest;
/*    */ import java.security.NoSuchAlgorithmException;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Functions
/*    */ {
/*    */   public static String int2hex(int numero)
/*    */   {
/* 17 */     String command = "\\";
/* 18 */     if (numero / 100 == 0) {
/* 19 */       command = command + "00\\";
/*    */     }
/* 21 */     if (numero / 10 == 0) {
/* 22 */       command = command + "00\\";
/*    */     }
/* 24 */     char[] number = String.valueOf(numero).toCharArray();
/* 25 */     for (int i = 0; i < number.length; i++) {
/* 26 */       command = command + (number[i] - '\022');
/* 27 */       command = command + "\\";
/*    */     }
/* 29 */     command = command + "00";
/* 30 */     return command;
/*    */   }
/*    */   
/*    */   public static String toSHA1(String str)
/*    */   {
/* 35 */     byte[] convertme = str.getBytes();
/* 36 */     MessageDigest md = null;
/*    */     try {
/* 38 */       md = MessageDigest.getInstance("SHA-1");
/*    */     }
/*    */     catch (NoSuchAlgorithmException e) {
/* 41 */       e.printStackTrace();
/*    */     }
/* 43 */     return new String(md.digest(convertme));
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Utils/Functions.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */