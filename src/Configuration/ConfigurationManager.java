/*    */ package Configuration;
/*    */ 
/*    */ import Logging.LoggingManager;
/*    */ import java.io.FileInputStream;
/*    */ import java.io.FileNotFoundException;
/*    */ import java.io.FileOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.util.HashMap;
/*    */ import java.util.InvalidPropertiesFormatException;
/*    */ import java.util.Properties;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConfigurationManager
/*    */ {
/*    */   private Properties lectorxml;
/* 25 */   private static ConfigurationManager instance = null;
/*    */   
/*    */ 
/*    */   private HashMap properties;
/*    */   
/*    */ 
/*    */ 
/*    */   public static ConfigurationManager getInstance()
/*    */   {
/* 34 */     if (instance == null) {
/* 35 */       instance = new ConfigurationManager();
/*    */     }
/* 37 */     return instance;
/*    */   }
/*    */   
/*    */   public void init()
/*    */   {
/*    */     try {
/* 43 */       this.lectorxml = new Properties();
/*    */       
/* 45 */       this.lectorxml.loadFromXML(new FileInputStream("config.xml"));
/*    */ 
/*    */     }
/*    */     catch (InvalidPropertiesFormatException ex)
/*    */     {
/* 50 */       Logger.getLogger(ConfigurationManager.class.getName()).log(Level.SEVERE, null, ex);
/* 51 */       LoggingManager.getInstance().log_error(ex);
/*    */     } catch (FileNotFoundException ex) {
/* 53 */       Logger.getLogger(ConfigurationManager.class.getName()).log(Level.SEVERE, null, ex);
/*    */       
/* 55 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */     catch (IOException ex) {
/* 58 */       Logger.getLogger(ConfigurationManager.class.getName()).log(Level.SEVERE, null, ex);
/* 59 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getProperty(String key)
/*    */   {
/* 67 */     return this.lectorxml.getProperty(key);
/*    */   }
/*    */   
/*    */   public void setProperty(String key, String value) {
/* 71 */     this.lectorxml.setProperty(key, value);
/*    */   }
/*    */   
/*    */   public void saveConfig()
/*    */   {
/*    */     try
/*    */     {
/* 78 */       FileOutputStream xmlStream = new FileOutputStream("config.xml");
/* 79 */       this.lectorxml.storeToXML(xmlStream, "");
/*    */     }
/*    */     catch (IOException e) {
/* 82 */       LoggingManager.getInstance().log_error(e);
/*    */     } catch (Exception ex) {
/* 84 */       LoggingManager.getInstance().log_error(ex);
/*    */     }
/*    */   }
/*    */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Configuration/ConfigurationManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */