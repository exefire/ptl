/*     */ package Database;
/*     */ 
/*     */ import Logging.LoggingManager;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.IOException;
/*     */ import java.util.HashMap;
/*     */ import java.util.InvalidPropertiesFormatException;
/*     */ import java.util.Map;
/*     */ import java.util.Properties;
/*     */ import javax.persistence.EntityManagerFactory;
/*     */ import javax.persistence.Persistence;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DataSourceController
/*     */ {
/*  26 */   private Properties dbConfig = null;
/*  27 */   private Map datasource = null;
/*  28 */   private Map remoteDatasource = null;
/*  29 */   private static DataSourceController instance = null;
/*  30 */   private EntityManagerFactory emf = null;
/*  31 */   private EntityManagerFactory remote_emf = null;
/*     */   
/*     */   protected DataSourceController()
/*     */   {
/*  35 */     this.dbConfig = new Properties();
/*  36 */     this.datasource = new HashMap();
/*  37 */     this.remoteDatasource = new HashMap();
/*     */   }
/*     */   
/*     */   public static DataSourceController getInstance() {
/*  41 */     if (instance == null)
/*     */     {
/*     */ 
/*  44 */       instance = new DataSourceController();
/*     */     }
/*     */     
/*     */ 
/*  48 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */   public void init()
/*     */   {
/*  54 */     readDatabaseConfig();
/*  55 */     generateDataSourceProperties();
/*  56 */     generateEntityManagerFactory();
/*  57 */     generateRemoteEntityManagerFactory();
/*     */   }
/*     */   
/*     */   public boolean isConfigOk()
/*     */   {
/*  62 */     if ((this.dbConfig != null) && (this.dbConfig.getProperty("status").equals("OK"))) {
/*  63 */       return true;
/*     */     }
/*  65 */     return false;
/*     */   }
/*     */   
/*     */   private void readDatabaseConfig()
/*     */   {
/*     */     try
/*     */     {
/*  72 */       this.dbConfig.loadFromXML(new FileInputStream("database.xml"));
/*  73 */       this.dbConfig.setProperty("status", "OK");
/*     */     }
/*     */     catch (InvalidPropertiesFormatException ex)
/*     */     {
/*  77 */       LoggingManager.getInstance().log_error(ex);
/*  78 */       this.dbConfig.setProperty("status", "FAIL");
/*     */     }
/*     */     catch (FileNotFoundException ex) {
/*  81 */       LoggingManager.getInstance().log_error(ex);
/*  82 */       this.dbConfig.setProperty("status", "FAIL");
/*     */     }
/*     */     catch (IOException ex) {
/*  85 */       LoggingManager.getInstance().log_error(ex);
/*  86 */       this.dbConfig.setProperty("status", "FAIL");
/*     */     } catch (Exception ex) {
/*  88 */       LoggingManager.getInstance().log_error(ex);
/*  89 */       this.dbConfig.setProperty("status", "FAIL");
/*     */     }
/*  91 */     LoggingManager.getInstance().log("Saliendo Carga attrs. DB");
/*     */   }
/*     */   
/*     */   public Map getDataSourceProperties()
/*     */   {
/*  96 */     return this.datasource;
/*     */   }
/*     */   
/*  99 */   public Map getRemoteDataSourceProperties() { return this.remoteDatasource; }
/*     */   
/*     */ 
/*     */   private void generateDataSourceProperties()
/*     */   {
/* 104 */     switch (this.dbConfig.getProperty("dbms"))
/*     */     {
/*     */     case "mysql": 
/* 107 */       this.datasource.put("javax.persistence.jdbc.url", "jdbc:mysql://" + this.dbConfig.getProperty("host") + ":" + this.dbConfig.getProperty("port") + "/" + this.dbConfig.getProperty("db") + "?zeroDateTimeBehavior=convertToNull");
/* 108 */       this.datasource.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
/* 109 */       this.datasource.put("javax.persistence.jdbc.user", this.dbConfig.getProperty("user"));
/* 110 */       this.datasource.put("javax.persistence.jdbc.password", this.dbConfig.getProperty("pass"));
/* 111 */       this.datasource.put("eclipselink.ddl-generation", "CREATE_ONLY");
/*     */       
/* 113 */       this.datasource.put("eclipselink.ddl-generation", "create-tables");
/* 114 */       this.datasource.put("eclipselink.ddl-generation.output-mode", "database");
/* 115 */       this.datasource.put("eclipselink.deploy-on-startup", "true");
/*     */       
/* 117 */       this.datasource.put("javax.persistence.schema-generation.database.action", "create");
/*     */       
/* 119 */       break;
/*     */     case "postgresql": 
/* 121 */       this.datasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 122 */       this.datasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 123 */       this.datasource.put("javax.persistence.jdbc.user", "ptl");
/* 124 */       this.datasource.put("javax.persistence.jdbc.password", "ptllocal");
/* 125 */       this.datasource.put("eclipselink.ddl-generation", "create-tables");
/* 126 */       break;
/*     */     
/*     */     case "derby": 
/* 129 */       this.datasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 130 */       this.datasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 131 */       this.datasource.put("javax.persistence.jdbc.user", "ptl");
/* 132 */       this.datasource.put("javax.persistence.jdbc.password", "ptllocal");
/* 133 */       this.datasource.put("eclipselink.ddl-generation", "create-tables");
/* 134 */       break;
/*     */     
/*     */     case "sqlite": 
/* 137 */       this.datasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 138 */       this.datasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 139 */       this.datasource.put("javax.persistence.jdbc.user", "ptl");
/* 140 */       this.datasource.put("javax.persistence.jdbc.password", "ptllocal");
/* 141 */       this.datasource.put("eclipselink.ddl-generation", "create-tables");
/* 142 */       break;
/*     */     
/*     */     case "sqlserver": 
/* 145 */       this.datasource.put("javax.persistence.jdbc.url", "jdbc:sqlserver://" + this.dbConfig.getProperty("remote_host") + "\\K_INT:" + this.dbConfig.getProperty("remote_port") + ";databaseName=" + this.dbConfig.getProperty("remote_db") + ";user=" + this.dbConfig.getProperty("remote_user") + ";password=" + this.dbConfig.getProperty("remote_pass") + "");
/* 146 */       this.datasource.put("javax.persistence.jdbc.driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
/* 147 */       this.datasource.put("javax.persistence.jdbc.user", this.dbConfig.getProperty("remote_user"));
/* 148 */       this.datasource.put("javax.persistence.jdbc.password", this.dbConfig.getProperty("remote_pass"));
/* 149 */       this.datasource.put("eclipselink.ddl-generation", "create-tables");
/* 150 */       break;
/*     */     default: 
/* 152 */       this.datasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 153 */       this.datasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 154 */       this.datasource.put("javax.persistence.jdbc.user", "ptl");
/* 155 */       this.datasource.put("javax.persistence.jdbc.password", "ptllocal");
/* 156 */       this.datasource.put("eclipselink.ddl-generation", "create-tables");
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 161 */     switch (this.dbConfig.getProperty("remote_dbms"))
/*     */     {
/*     */     case "local": 
/* 164 */       this.remoteDatasource = this.datasource;
/* 165 */       break;
/*     */     case "mysql": 
/* 167 */       this.remoteDatasource.put("javax.persistence.jdbc.url", "jdbc:mysql://" + this.dbConfig.getProperty("host") + ":" + this.dbConfig.getProperty("port") + "/" + this.dbConfig.getProperty("db") + "?zeroDateTimeBehavior=convertToNull");
/* 168 */       this.remoteDatasource.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
/* 169 */       this.remoteDatasource.put("javax.persistence.jdbc.user", this.dbConfig.getProperty("user"));
/* 170 */       this.remoteDatasource.put("javax.persistence.jdbc.password", this.dbConfig.getProperty("pass"));
/* 171 */       this.remoteDatasource.put("eclipselink.ddl-generation", "none");
/*     */       
/* 173 */       break;
/*     */     case "postgresql": 
/* 175 */       this.remoteDatasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 176 */       this.remoteDatasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 177 */       this.remoteDatasource.put("javax.persistence.jdbc.user", "ptl");
/* 178 */       this.remoteDatasource.put("javax.persistence.jdbc.password", "ptllocal");
/*     */       
/* 180 */       break;
/*     */     
/*     */     case "derby": 
/* 183 */       this.remoteDatasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 184 */       this.remoteDatasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 185 */       this.remoteDatasource.put("javax.persistence.jdbc.user", "ptl");
/* 186 */       this.remoteDatasource.put("javax.persistence.jdbc.password", "ptllocal");
/*     */       
/* 188 */       break;
/*     */     
/*     */     case "sqlite": 
/* 191 */       this.remoteDatasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 192 */       this.remoteDatasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 193 */       this.remoteDatasource.put("javax.persistence.jdbc.user", "ptl");
/* 194 */       this.remoteDatasource.put("javax.persistence.jdbc.password", "ptllocal");
/*     */       
/* 196 */       break;
/*     */     
/*     */     case "sqlserver": 
/* 199 */       this.remoteDatasource.put("javax.persistence.jdbc.url", "jdbc:sqlserver://" + this.dbConfig.getProperty("remote_host") + "\\K_INT:" + this.dbConfig.getProperty("remote_port") + ";databaseName=" + this.dbConfig.getProperty("remote_db") + ";user=" + this.dbConfig.getProperty("remote_user") + ";password=" + this.dbConfig.getProperty("remote_pass") + "");
/* 200 */       this.remoteDatasource.put("javax.persistence.jdbc.driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
/* 201 */       this.remoteDatasource.put("javax.persistence.jdbc.user", this.dbConfig.getProperty("remote_user"));
/* 202 */       this.remoteDatasource.put("javax.persistence.jdbc.password", this.dbConfig.getProperty("remote_pass"));
/* 203 */       this.datasource.put("eclipselink.ddl-generation", "none");
/* 204 */       break;
/*     */     default: 
/* 206 */       this.remoteDatasource.put("javax.persistence.jdbc.url", "jdbc:derby://localhost:1527/ptllocal");
/* 207 */       this.remoteDatasource.put("javax.persistence.jdbc.driver", "org.apache.derby.jdbc.ClientDriver");
/* 208 */       this.remoteDatasource.put("javax.persistence.jdbc.user", "ptl");
/* 209 */       this.remoteDatasource.put("javax.persistence.jdbc.password", "ptllocal");
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void generateEntityManagerFactory()
/*     */   {
/* 219 */     this.emf = Persistence.createEntityManagerFactory("ptl_local_pu", this.datasource);
/*     */   }
/*     */   
/*     */   public EntityManagerFactory getEntityManagerFactory() {
/* 223 */     if (this.emf != null) {
/* 224 */       return this.emf;
/*     */     }
/* 226 */     generateEntityManagerFactory();
/*     */     
/*     */ 
/* 229 */     return this.emf;
/*     */   }
/*     */   
/*     */   private void generateRemoteEntityManagerFactory()
/*     */   {
/* 234 */     this.remote_emf = Persistence.createEntityManagerFactory("ptl_remote_pu", this.remoteDatasource);
/*     */   }
/*     */   
/*     */   public Properties getDbConfig() {
/* 238 */     return this.dbConfig;
/*     */   }
/*     */   
/*     */   public EntityManagerFactory getRemoteEntityManagerFactory()
/*     */   {
/* 243 */     if (this.remote_emf != null) {
/* 244 */       return this.remote_emf;
/*     */     }
/* 246 */     generateRemoteEntityManagerFactory();
/*     */     
/*     */ 
/* 249 */     return this.remote_emf;
/*     */   }
/*     */ }


/* Location:              /Users/ovalenzuela/Desktop/PTL Exefire/ptl_core.jar!/Database/DataSourceController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */